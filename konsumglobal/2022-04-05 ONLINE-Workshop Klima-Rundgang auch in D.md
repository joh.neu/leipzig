---
id: "885655758781052"
title: "ONLINE-Workshop: Klima-Rundgang auch in Deiner Stadt ?!"
start: 2022-04-05 17:00
end: 2022-04-05 19:00
link: https://www.facebook.com/events/885655758781052/
image: 272614980_5548700671812114_6118696144570905362_n.jpg
isCrawled: true
---
WeltOffen e.V. bietet einen 2-stündigen (öffentlichen) Stadtrundgang zur Klimakrise an: Der Klima-Rundgang “CO2 in Leipzig und der Welt”


Anders als bei einem konsumkritischer Stadtrundgänge legen wir fast ausschließlich den Fokus auf die Aspekte Klima, persönliche Einflussnahme vor Ort und Kommunalpolitik. Zunehmend erfahren wir dafür großes Interesse – so werden wir zuletzt auch von (Schul- und anderen) Gruppen gebucht und haben den „eku Zukunftspreis 2020“ des Landes Sachsen gewonnen.


Im Workshop werden wir Euch einmal vorstellen, wie unser Rundgang abläuft und was wir dabei für wichtig halten. Wir setzen auf Ideen aus der Umweltschutzpsychologie, um zu klimaschützendem Verhalten zu motivieren, und suchen Anknüpfungspunkte direkt vor Ort. Das besondere beim Rundgang: Der Klima-Rucksack, gefüllt mit einigen Tonnen CO2: zum Einpacken, Auspacken und Verstehen.
Wir werden ausloten, ob Ihr auch selbst in Eurer Stadt einen solchen Rundgang anbieten möchtet. Dabei unterstützen wir Euch sehr gern auch nach dem Workshop per E-Mail und Telefon – und legen im Online-Workshop dafür den Grundstein.  Oder aber Ihr informiert Euch einfach mal worum es geht:

- das Konzept “Klima-Rundgang” Kennenlernen
- Hintergründe aus der Umweltschutzpsychologie
- Startschuss für eigenen Klima-Rundgang in Deiner Stadt

Die Veranstaltung findet online statt. Der Workshop und bei Bedarf spätere Beratung dazu sind kostenfrei - die Teilnehmendenzahl allerdings begrenzt. Daher bitten wir um:
Anmeldung bis zum 28.03.2022 unter: k.hinkefuss@weltoffen-leipzig.de

    Wir freuen uns auf Dich!

Mehr Infos und schickere Bilder hier: https://weltoffen-leipzig.de/online-workshop-klima-rundgang-auch-in-deiner-stadt/