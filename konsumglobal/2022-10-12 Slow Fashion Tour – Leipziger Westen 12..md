---
id: "3159347497638943"
title: Slow Fashion Tour – Leipziger Westen 12.10.
start: 2022-10-12 16:00
end: 2022-10-12 18:00
locationName: Primark (Leipzig, 04109)
address: Hainstraße 21-23
link: https://www.facebook.com/events/3159347497638943/
image: 276310320_7146144612094680_1192249539342409303_n.jpg
isCrawled: true
---

Stadtführung zum Kleiderkonsum durch den Leipziger Westen


Billige Massenware im Onlineshop, Fashion Influencer-Marketing und das schnelle Geschäft mit der Mode ist nicht erst in Zeiten von Corona stark in die Kritik gekommen. Aber was verbirgt sich eigentlich wirklich dahinter, welche globalen Auswirkungen hat das Geschäftsmodell Fast Fashion auf Mensch und Umwelt und wie können wir bei unserem eigenen Modekonsum bewusster werden? In diesem interaktiven Stadtrundgang geht es um diese Fragen, das eigene Konsumbewusstsein zu erweitern und auch Handlungsalternativen vor Ort kennenzulernen.

Die interaktive Tour startet am Vorplatz von Primark und führt über mehrere Stationen durch die Leipziger Innenstadt. Es werden verschiedene Läden gestreift und die Auswirkungen der globalen Lieferketten aufgezeigt, aber auch was sich eigentlich hinter Alternativen und Reaktionen wie „Faire Mode“ oder „Slow Fashion“ verbirgt. Die Tour endet am Weltladen des Eine Welt e.V. am Thomaskirchhof.


Dieser Stadtrundgang richtet sich an InteressentInnen, die einen tieferen Einblick gewinnen wollen, welche Wege unsere Kleidungstücke bis zum eigenen Kleiderschrank zurücklegen. Es klären sich Fragen zu gängigen Textilsiegeln und wie es möglich wird, ganz individuell das eigene Konsumverhalten mit globaler Verantwortung zu verbinden.


Die Tour startet pünktlich um 16.00. Bitte sei 5 Minuten vorab da. Die Veranstaltung ist kostenfrei.

Die aktuellen Coronabestimmungen werden eingehalten, daher bitten wir um Vorabanmeldung.

Anmeldung: https://eveeno.com/slowfashiontourleipzigwest121022

Dies ist eine öffentliche Stadtführung des Weltoffen e.V. in Kooperation mit dem Fair Fashion Lab Leipzig.

Das Programm wird gefördert durch: Brot für die Welt, Katholischer Fond, Engagement Global mit Mitteln des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung sowie der Stadt Leipzig.
