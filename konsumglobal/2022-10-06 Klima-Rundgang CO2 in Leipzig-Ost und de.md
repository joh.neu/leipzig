---
id: "672602787303583"
title: "Klima-Rundgang: CO2 in Leipzig-Ost und der Welt"
start: 2022-10-06 16:00
end: 2022-10-06 18:00
locationName: "Blockheizkraftwerke: Hildegardstraße 1a, Volkmarsdorf"
link: https://www.facebook.com/events/672602787303583/
image: 279313820_7312641405444999_687813111069800958_n.jpg
isCrawled: true
---
Auch 2022 gehen wir auf Klima-Rundgang. 
2 Stunden lang geht es durch den Leipziger Osten. Warum hier? Na, weil hier auch einiges passiert und noch viel mehr möglich ist.

Mit dabei ist der Klimarucksack, welchen wir nach und nach mit CO2 befüllen. Daran sehen wir was so richtig schwer ins Gewicht fällt und wie wir besonders viel aus unserem Rucksack rauswerfen können. 

Wer Fragen stellt, kann auch Antworten bekommen - zum Beispiel beim Rundgang:
Besser die Gurke aus Spanien oder die Bio-Gurke in Plastik?
Lieber eine Scheibe Wurst als drei Scheiben Käse?
Wie eigentlich Fernwärme ohne CO2?
Wer ist Dieter II?
Wie weit komme ich mit einem Kilo CO2?
Wer kümmert sich in Leipzigs Osten eigentlich mal um den Klimawandel?

Die Veranstaltung ist kostenfrei und für alle offen - trotz und vor Ort ohne Corona! Dafür haben wir ein Hygienekonzept und Ihr kommt bitte nur gesund oder gar geimpft.
Start ist 16:00 (dann bis 18:00).

Alle Termine 2022 im Osten:

Do 22.09. in Reudnitz
(Start: Blockheizkraftwerke: Karl-Siegismund-Straße 2a, Reudnitz)

Mi 06.10. um die Eisenbahnstraße
(Start: Blockheizkraftwerke: Hildegardstraße 1a, Volkmarsdorf)

Wir sind im Juni aber auch in der Innenstadt unterwegs! (siehe unsere weiteren Veranstaltungen)