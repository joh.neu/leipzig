---
id: "947907442570448"
title: Slow Fashion Tour – Innenstadt Leipzig 08.06.
start: 2022-06-08 16:00
end: 2022-06-08 18:00
locationName: Primark (Leipzig, 04109)
address: Hainstraße 21-23
link: https://www.facebook.com/events/947907442570448/
image: 276288137_7146100768765731_5389930716747571093_n.jpg
isCrawled: true
---

Stadtführung zum Kleiderkonsum durch die Innenstadt Leipzigs


Billige Massenware im Onlineshop, Fashion Influencer-Marketing und das schnelle Geschäft mit der Mode ist nicht erst in Zeiten von Corona stark in die Kritik gekommen. Aber was verbirgt sich eigentlich wirklich dahinter, welche globalen Auswirkungen hat das Geschäftsmodell Fast Fashion auf Mensch und Umwelt und wie können wir bei unserem eigenen Modekonsum bewusster werden? In diesem interaktiven Stadtrundgang geht es um diese Fragen, das eigene Konsumbewusstsein zu erweitern und auch Handlungsalternativen vor Ort kennenzulernen.

Die interaktive Tour startet am Vorplatz von Primark und führt über mehrere Stationen durch die Leipziger Innenstadt. Es werden verschiedene Läden gestreift und die Auswirkungen der globalen Lieferketten aufgezeigt, aber auch was sich eigentlich hinter Alternativen und Reaktionen wie „Faire Mode“ oder „Slow Fashion“ verbirgt. Die Tour endet am Weltladen des Eine Welt e.V. am Thomaskirchhof.


Dieser Stadtrundgang richtet sich an InteressentInnen, die einen tieferen Einblick gewinnen wollen, welche Wege unsere Kleidungstücke bis zum eigenen Kleiderschrank zurücklegen. Es klären sich Fragen zu gängigen Textilsiegeln und wie es möglich wird, ganz individuell das eigene Konsumverhalten mit globaler Verantwortung zu verbinden.


Die Tour startet pünktlich um 16.00. Bitte sei 5 Minuten vorab da. Die Veranstaltung ist kostenfrei.

Die aktuellen Coronabestimmungen werden eingehalten, daher bitten wir um Vorabanmeldung.

Anmeldung: https://eveeno.com/slowfashiontourleipzigzentrum080622

Dies ist eine öffentliche Stadtführung des Weltoffen e.V. in Kooperation mit dem Fair Fashion Lab Leipzig.

Das Programm wird gefördert durch: Brot für die Welt, Katholischer Fond, Engagement Global mit Mitteln des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung sowie der Stadt Leipzig.
