---
id: "827805298204409"
title: "Klima-Rundgang: CO2 in Leipzig und der Welt"
start: 2022-06-09 16:00
end: 2022-06-09 18:00
locationName: Neues Rathaus (Leipzig)
link: https://www.facebook.com/events/827805298204409/
image: 279197773_7312604792115327_6461657345824163910_n.jpg
isCrawled: true
---
Auch 2022 gehen wir auf Klima-Rundgang. 
2 Stunden lang geht es durch die Leipziger Innenstadt. Wir sehen so, was in unserer Stadt und Region zum Thema Klimawandel passiert. Passiert überhaupt was Neues oder gibt nur noch andere Themen? Doch doch, einiges ist neu.

Mit dabei ist der Klimarucksack, welchen wir nach und nach mit CO2 befüllen. Daran sehen wir was so richtig schwer ins Gewicht fällt und wie wir besonders viel aus unserem Rucksack rauswerfen können. 

Wer Fragen stellt, kann auch Antworten bekommen - zum Beispiel beim Rundgang:
Besser die Gurke aus Spanien oder die Bio-Gurke in Plastik?
Lieber eine Scheibe Wurst als drei Scheiben Käse?
Wo kann ich CO2-Zertifikate kaufen?
Wie weit komme ich mit einem Kilo CO2?
Wer kümmert sich hier in Leipzig eigentlich mal um den Klimawandel?

Die Veranstaltung ist kostenfrei und für alle offen - trotz und vor Ort ohne Corona! Dafür haben wir ein Hygienekonzept und Ihr kommt bitte nur gesund oder gar geimpft.
Start ist 16:00 (dann bis 18:00) am Haupteingang Neues Rathaus.

Alle Termine 2022: Innenstadt Do 09.06./ Mi 15.06.
Dieses Jahr sind wir aber auch im Osten unterwegs! (siehe unsere weiteren Veranstaltungen)