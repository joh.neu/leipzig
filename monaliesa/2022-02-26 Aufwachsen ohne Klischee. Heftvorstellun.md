---
id: "1517527641999405"
title: FÄLLT AUS - Aufwachsen ohne Klischee. Heftvorstellung mit Emilia Miguez
  und Koschka Linkerhand
start: 2022-02-26 19:00
end: 2022-02-26 21:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz
link: https://www.facebook.com/events/1517527641999405/
image: 266129684_3123836771219094_2816431196215487034_n.jpg
isCrawled: true
---
Hier als Livestream:

Die Veranstaltung muss leider aufgrund von Krankheit entfallen, wird aber nachgeholt!

https://studio.youtube.com/video/opCim7cye3s/livestreaming


Was bedeutet die Diskussion um Geschlechterrollen und Geschlechtervielfalt für die frühe Bildung? Wie können Pädagog*innen, Familien und weitere Bezugspersonen dazu beitragen, dass sich die Jüngsten möglichst frei von Jungs- und Mädchen-Klischees entwickeln können? Koschka Linkerhand und Emilia Miguez haben diverse Expert*innen eingeladen, sich zu diesen Fragen zu positionieren. In der Veranstaltung stellen sie ihre Broschüre »Aufwachsen ohne Klischee – Geschlechtsidentität in der frühen Kindheit« vor und laden dazu ein, sich über Strategien aus der Rosa-Hellblau-Falle auszutauschen und zu diskutieren – fachlich, theoretisch und über eigene Erfahrungen. 

Es gilt die 2Gplus-Regelung (geimpft, genesen plus tagesaktueller Coronatest).

Über Spenden freuen wir uns. Spendenempfehlung: 2–10 EUR
