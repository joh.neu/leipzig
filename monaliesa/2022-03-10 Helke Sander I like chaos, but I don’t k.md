---
id: "610198773749894"
title: 'Helke Sander: I like chaos, but I don’t know whether chaos likes me –
  Texte aus "Frauen und Film"'
start: 2022-03-10 19:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz
link: https://www.facebook.com/events/610198773749894/
image: 275059189_3178420452427392_863311255103878996_n.jpg
isCrawled: true
---
Helke Sander: I like chaos, but I don’t know whether chaos likes me. Texte aus Frauen und Film

Helke Sander mit Achim Lengerer und Janine Sack

Moderation und Lesung: Katharina Zimmerhackl

Hg. Achim Lengerer und Janine Sack

 Mit Beiträgen von Helke Sander und Uta Berg-Ganschow, Valie Export, Helge Heberle, Elfriede Irrall, Elena Meilicke und Ula Stöckl

Die monografische Publikation Helke Sander: I like chaos, but I don’t know whether chaos likes me umfasst Texte aus Frauen und Film, der ersten feministischen deutschsprachigen Filmzeitschrift, die 1974 von Helke Sander gegründet und herausgegeben wurde.
Die für die Publikation ausgewählten Texte fokussieren zentrale Fragestellungen der feministischen Filmarbeit, ökonomische sowie recht-liche Bedingungen und vor allem deren strukturelle Bedingtheit in gesellschaftlichen Verhältnissen und ihre radikale Kritik daran.

In der Publikation werden die Wiederabdrucke der Texte aus Frauen und Film von einem Gespräch zwischen Helke Sander und der Filmwissenschaftlerin Elena Meilicke zu den Anfängen und dem Entstehungskontext der Zeitschrift begleitet. Um zusätzlich die enge Verbindung zwischen der Praxis des Schreibens und der filmischen Arbeit zu verdeutlichen, werden erstmals Ausschnitte aus einer Arbeitsversion des Drehbuchs von Die allseitig reduzierte Persönlichkeit – Redupers veröffentlicht.
Die Publikation macht, sowohl in analoger als auch digitaler Version, Sanders Texte für die anhaltende Debatte zu Produktionsbedingungen und Geschlechterverhältnissen für nachfolgende Künstler*innengenerationen zugänglich.


Scriptings’ Reader-Format „Political Scenarios“ wird in Zusammenarbeit mit Archive Books und  dem E-Book-Verlag EECLECTIC herausgegeben.

Für die Veranstaltung gilt 2G+ (2mal geimpft und tagesaktueller Test, dreifach Geimpfte brauchen keinen Test), FFP2 Maske während der Veranstaltung und maximal 20 Teilnehmende. Danke für euer Verständnis und Mitwirken in dieser Sache. 

Über Spenden freuen wir uns. Spendenempfehlung: 2–10 EUR