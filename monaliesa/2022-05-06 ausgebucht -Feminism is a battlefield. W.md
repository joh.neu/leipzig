---
id: "470449247772777"
title: ausgebucht -Feminism is a battlefield. Wie erforschen wir Konflikte der
  Queer- und Frauenbewegungen?
start: 2022-05-06 10:00
end: 2022-05-07 17:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152
link: https://www.facebook.com/events/470449247772777/
image: 271721629_3142930535976384_6023138298794882049_n.jpg
isCrawled: true
---
--- der Workshop ist leider schon ausgebucht ---

“Wie erforschen, was sich bewegt?” (Susanne Maurer)

Wie lassen sich Queer- und Frauenbewegungen analysieren und befragen, wenn sie gleichzeitig als soziale Bewegungen stetig in Veränderung, in Bewegung begriffen sind?

Im Rahmen des interdisziplinären Workshops Feminism is a battlefield von und für Nachwuchswissenschaftler:innen, Aktivist:innen und Künstler:innen wollen wir uns einem Verständnis von sozialen Bewegungen über ihre Konflikte nähern. Dabei können sowohl historische als auch gegenwärtige Konflikt- und Bewegungsräume der Queer- und Frauenbewegungen im Fokus stehen. Die Erziehungswissenschaftlerin Susanne Maurer hat im Rahmen dieses Spannungsfeldes auf die Brisanz von Konflikten für die Frauenbewegungsforschung hingewiesen. Sie versteht Konflikte als Spuren und als Schlüssel zum Verständnis für längerwährende Prozesse und wichtige Ereignisse und Themen der Bewegung, aber auch der gesamten Gesellschaft.

Alle Queer- und Frauenbewegungen lagen und liegen mit gesellschaftlichen Herrschaftsverhältnissen im Streit, führ(t)en aber auch Konflikte unter- und zwischeneinander. Sie kämpf(t)en gegen Unterdrückung, Ausbeutung und Enteignung, rungen und ringen um Differenzen, Anerkennung und Umverteilung und formulier(t)en Utopien für eine andere Zukunft. Indem wir uns speziell den streitbaren Räumen und Konflikten, in denen Bewegungsziele ausgehandelt werden, zuwenden, wollen wir eine Perspektive eröffnen, die es ermöglicht Queer- und Frauenbewegungen in ihrer Heterogenität und Komplexität zu erforschen.

Mögliche Themen/Fragen für Beiträge könnten sein:

Wie lässt sich eine Geschichte der Queer- und Frauenbewegungen schreiben, die Vielfältigkeit und Differenzen der Akteur:innen sowie ihrer feministischen Kritik und Visionen erfassen kann?

Welche Formen von Konflikten der Queer- und Frauenbewegungen finden sich in Gegenwart und Geschichte? Z.B. Aushandlungen um Inhalt und Ausrichtung feministischer Kritik oder Kämpfe um die politischen Subjekte der Bewegungen

Wie gestalten sich Konflikte zwischen den einzelnen Bewegungen und ihren Akteur:innen?

Wie reagier(t)en Queer- und Frauenbewegungen auf sich verschärfende gesellschaftliche Konflikte und Angriffe gegen emanzipatorische Errungenschaften?

Welche Konflikte und Akteur:innen werden erinnert und welche geraten in Vergessenheit?

In welchen Bewegungsräumen und -medien wurden und werden Konflikte ausgehandelt?

Wie wird Zugehörigkeit in Bewegungsräumen hergestellt? Welche Inklusions- und Exlusionsmechanismen lassen sich beschreiben?

Welches methodische Handwerkszeug braucht es, um Konflikte als produktive Analysegegenstände in der sozialwissenschaftlichen und historischen Geschlechterforschung interdisziplinär zu erschließen?

Wir möchten am 6. und 7. Mai 2022 gemeinsam in Leipzig Ihre und Eure Forschungsarbeiten rund um Konflikt- und Bewegungsräume in Queer- und Frauenbewegungen – vor allem anhand mitgebrachter Quellen – diskutieren. Bringt mit, was euch bewegt!

Der Workshop wird Werkstattcharakter haben und ist interdisziplinär angelegt. Er richtet sich sowohl an historisch, soziologisch, kulturwissenschaftlich, philosophisch und künstlerisch Arbeitende sowie an politisch Aktive.

Es können auch Vorschläge eingereicht werden, die am Rande des Themas zu liegen scheinen. Die inhaltliche Schwerpunktsetzung des Workshops wird durch die Einreichungen mitgestaltet.

Wir bemühen uns um Finanzierung der Fahrt- und Übernachtungskosten.

Bei Interesse senden Sie/sendet bitte ein kurzes Exposé (ca. eine Seite) bis zum 20. Januar an: constanze.stutz@tu-dresden.de

Der genaue Zeitplan und Veranstaltungsort wird zeitnah veröffentlicht. 

Der Workshop ist ein Kooperationsprojekt mit ist ein Kooperationsprojekt mit der Feministischen Bibliothek MONAliesA Leipzig sowie dem Lehrstuhl für Geschlechtergeschichte der Friedrich-Schiller-Universität Jena.