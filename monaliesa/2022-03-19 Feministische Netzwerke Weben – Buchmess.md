---
id: "1730775153947911"
title: Feministische Netzwerke Weben – Buchmesse Edition
start: 2022-03-19 14:00
end: 2022-03-19 16:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152, 04277 Leipzig-Connewitz
link: https://www.facebook.com/events/1730775153947911/
image: 275639559_2137331759755247_3114695483192552817_n.jpg
isCrawled: true
---
Zur Leipziger Buchmesse laden INSERT FEMALE ARTIST & PS-Politisch Schreiben zu einem gemeinsamen Bandentreff in der feministischen Bibliothek MONAliesA ein. 

Unter der Moderation beider Teams können sich Literaturakteur*innen hier in lockerer Atmosphäre kennenlernen. Vielleicht bildet sich zwischen Kaffee und Kuchen ja sogar die ein oder andere Verbindung, ein neues Netzwerk oder gemeinsames Projekt?


Die Teilnahme ist kostenlos. 
Die Plätze sind begrenzt – bitte meldet Euch unter info@insertfemaleartist.de an. 

Der Raum ist barrierefrei zu erreichen. 

Zutritt: 2G + tagesaktueller Bürger*innentest

Kaffee & Kuchen (auch vegan) bieten wir gegen eine kleine Spende an.



Ort:
Feministische Bibliothek MONAliesA
– Haus der Demokratie –
Bernhard-Göring-Straße 152
04277 Leipzig
