---
id: "631053704695514"
title: Die Angst linker Männer vor den Feministinnen
start: 2022-03-05 18:00
address: Institut für Zukunft
link: https://www.facebook.com/events/631053704695514/
image: 267516230_3125879064348198_422399355424680758_n.jpg
isCrawled: true
---
Linke Männer haben Angst vor Feministinnen. Doch gesteht man sich eine Furcht nicht ein und verdrängt sie, zum Beispiel, weil sie dem positiven Selbstbild als nettem Typen oder Profeministen widerspricht, handelt man nicht weniger in ihrem Bann. Dabei wäre allein die aggressionsbereite Angst vor Frauen, die Männern im Patriarchat ganz generell gemein ist, schon schlimm genug. Doch unter Linken verfügen Frauen nicht bloß durch ihre Körper und ihre weibliche Sexualität über etwas, das Männer zugleich anzieht, sie aber auch in ihren maskulinen Autonomiewünschen verunsichert. In linken Milieus sind sich Frauen als auch andere marginalisierte Geschlechter darüber hinaus ihrer objektiv beschissenen gesellschaftlichen Lage bewusst. Sie organisieren sich, kämpfen kollektiv gegen Sexismus, sexuelle Gewalt und männliche Dominanz. Und weil das so ist, reicht es für linke Männer nicht mehr aus, das Patriarchat stets nur bei anderen, schlechteren, fremden, rechten oder rassifizierten Männern zu verorten. Sie spüren: Das eigene zwielichtige Verhältnis zu Frauen und Sexualität steht im Scheinwerferlicht. Und kann Konsequenzen haben. Darum bekennen sich linke Männer zwar öffentlich artig zum Feminismus und finden laute Frauen auch irgendwie ganz gut, bekämpfen sie jedoch durch allerlei Mittel: durch Passivität, Sturheit und Selbstmitleid bis hin zur männerbündischen Intrige. Im Vortrag sollen einige Beobachtungen und Mechanismen des Phänomens dargelegt und so gezeigt werden, dass es für praktischen Antifeminismus keine "Antifeministen" braucht - oder, anders gesagt: dass keine Feinde braucht, wer solche Verbündeten hat.

Zur Referent:in: Jeja Klein macht freien Journalismus unter anderem für queer.de, das nd, Supernova oder die analyse & kritik und beschäftigt sich mit Geschlecht, Männlichkeit und sexueller Gewalt. Pronomen: Es ("Sie" ist aber auch okay, so lange es für nichtbinäre Personen kein anerkanntes Pronomen gibt).

Regelungen in Bezug auf COVID-19 folgen.

Über Spenden freuen wir uns. Spendenempfehlung: 2–10 EUR
