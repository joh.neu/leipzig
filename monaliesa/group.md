---
name: MONAliesA
website: https://monaliesa.de
email: MONAliesA_leipzig@gmx.de
address: Bernhard-Göring-Straße 152, 04277 Leipzig
scrape:
  source: facebook
  options:
    page_id: 1428787284057393
---
Seit 1990 bietet die MONAliesA professionelle Bibliotheks-, Archiv-, Bildungs- und Kulturarbeit, um die Vielfalt feministischer Ideen und Konzepte sowie das reichhaltige Erbe der von Frauen in Politik, Kultur, Wissenschaft und Gesellschaft erbrachten Leistungen zu bewahren und zu vermitteln.
