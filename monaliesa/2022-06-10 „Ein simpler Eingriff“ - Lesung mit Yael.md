---
id: "1215289455665781"
title: „Ein simpler Eingriff“ - Lesung mit Yael Inokai
start: 2022-06-10 19:00
locationName: MONAliesA
address: Bernhard-Göring-Straße 152
link: https://www.facebook.com/events/1215289455665781/
image: 272719393_3154517304817707_8580879857296067769_n.jpg
isCrawled: true
---
Yael Inokai liest aus ihrem neuen Roman "Ein simpler Eingriff“

Moderation: Kaśka Bryla

Ein neuartiger Eingriff verspricht Patientinnen ein besseres, ein normaleres Leben. Noch ist er unerprobt, doch bisher hatte Meret nie Grund, als Krankenschwester an der Macht der Medizin zu zweifeln. Dann lernt sie Sarah kennen und muss sich plötzlich fragen, was ein besseres, normales Leben überhaupt ist. Und ob sie ein solches leben will.

Yael Inokai, Autorin und Redaktionsmitglied der Zeitschrift PS: Politisch Schreiben, erzählt vom Ausbruch dreier Frauen und der emanzipatorischen Kraft der Liebe. Moderiert wird der Abend von der Autorin und Redaktionskollegin Kaśka Bryla.

Für die Veranstaltung gilt 2G+ (2mal geimpft und tagesaktueller Test, dreifach Geimpfte brauchen keinen Test), FFP2 Maske während der Veranstaltung und maximal 20 Teilnehmende. Danke für euer Verständnis und Mitwirken in dieser Sache!

Über Spenden freuen wir uns. Spendenempfehlung: 2–10 EUR 
