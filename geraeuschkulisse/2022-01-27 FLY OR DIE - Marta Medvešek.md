---
id: "1454505528279647"
title: FLY OR DIE - Marta Medvešek
start: 2022-01-27 19:00
link: https://www.facebook.com/events/1454505528279647/
image: 271585671_3057994787806536_2258386078231452595_n.jpg
isCrawled: true
---
Der ZOOM-LINK wird am Tag der Veranstaltung hier gepostet. //
The ZOOM-LINK will be posted here on the day of the event.

*English below* (Feature in Croatian with English subtitles)
.........................................

GERÄUSCHKULISSE

– Gemeinsam Lauschen –
präsentiert

FLY OR DY
Feature von Marta Medvešek

Gemeinsam Feature hören und im Anschluss mit der Autorin darüber diskutieren. 

In Kroatisch mit englischen Untertiteln. 

Vor 28 Jahren wird ein junges Storchenweibchen in Kroatien von einem Jäger angeschossen und am Flügel verletzt. Es kann nicht mehr fliegen, sich nicht mehr mit Nahrung versorgen. Sein Leben stünde auf dem Spiel, wäre da nicht Stjepan, der es findet. Er liest es auf und errichtet dem Weibchen, das er Malena tauft, ein Nest im Hinterhof der Dorfschule, an der er bis zu seiner Pensionierung als Hausmeister gearbeitet hat. Täglich pflegt und versorgt er sie. Eines Tages sucht ein Storchenmann sie auf, Stjepan tauft ihn Klepetan. Seitdem kehrt Klepetan jedes Jahr zu Malena zurück, zieht mit ihr die gemeinsamen Nachkömmlinge auf, die er beim nächsten Vogelzug mit nach Afrika nimmt. Allmählich verbreitet sich die ungewöhnliche Liebesgeschichte; Journalisten aus der ganzen Welt suchen den Hinterhof der Schule auf, um über die romantische Storchenliebe zu berichten. Doch Stjepan zahlt einen hohen Preis, um Malena am Leben zu erhalten. Jemandes Flügel zu sein, ist ein Vollzeitjob – keine Ferien, kein Ruhestand. Das Feature begleitet Stjepan und Malena und erzählt ihre besondere Geschichte.

Ein Feature von Marta Medvešek
Musik: Jan Johansson
Coach: Gabriela Hermer
Sound: Bodo Pasternak, Erik Lehman, Marta Medvešek
Produktion: für den Åke Blomström Award mit Unterstützung des RBB, 2020

Gewinnerin des Prix Europa 2021 in der Kategorie Radiofeature

*****************
GERÄUSCHKULISSE presents 

FLY OR DY
feature by Marta Medvešek

Listening to the feature together and discussing it with the author afterwards. 

28 years ago, a man found an injured baby stork. Shot in the wing, she couldn’t fly. She was predestined to die. But the man changed her destiny. Did he know that she would change his destiny just as much? To keep her safe, warm and well fed, Stjepan had to give up on his own freedom of movement. Thanks to him – and against all odds – Malena was not only able to live, but also to find love – Klepetan. About 10 years ago an article about the old man and the stork came out in a local newspaper. Little by little the news spread, gaining worldwide fame as an unusual love story. But life is not a novel. Being someone’s wings is a full-time position. There’s no vacation, no retirement and especially not when the spotlights are on.

A feature by Marta Medvešek
Music: Jan Johansson
Coach: Gabriela Hermer
Sound: Bodo Pasternak, Erik Lehman, Marta Medvešek
Production: for the Åke Blomström Award with support of RBB, 2020.

Winner of Best European Radio Documentary at the Prix Europa 2021
