---
id: "503008507951336"
title: OCEANUS von & mit Chris Watson (Konzert-Premiere)
start: 2022-05-01 18:30
end: 2022-05-01 21:00
locationName: Schaubühne Lindenfels, Karl-Heine-Straße 50, 04229 Leipzig
link: https://www.facebook.com/events/503008507951336/
image: 276005656_3108200046119343_2084392572153190205_n.jpg
isCrawled: true
---
*English below
...................................................
OCEANUS (Weltpremiere)

Live-Surround-Konzert
von & mit Chris Watson


Das Konzert entführt in den größten und klangreichsten Lebensraum unseres Planeten. Eine 45-minütige, raumgreifende Klangdiffusion, die die Geräusche, rhythmen und Musik der Weltmeere feiert. Das Stück umspült die Ohren, durchbricht die Idylle mit menschengemachtem Lärm, umwogt die Körper im dunklen Saal und gibt Raum für Fantasie.

Auf der Ostseeinsel Rügen taucht das Publikum unter die Meeresoberfläche, wird mit der Brandung hinaus auf eine ozeanische Rundreise gespült. Robben singen unter dem antarktischen Meereseis, ein Korallenriff knistert und knackt im Südchinesischen Meer, Buckelwale singen markdurchdringend in der Karibik und die jagenden Orcas im Nordatlantik – All das ist zu hören, bevor das Publikum wieder an den Strand von Rügen gespült wird.

Im Anschluss offenes Publikumsgespräch mit Chris Watson auf englisch, bei Bedarf Übersetzung ins Deutsche durch die Moderatorinnen.

EINLASS: 18:30
BEGINN: 19:00
KONZERTDAUER: 00:45
GESPRÄCHSDAUER: 00:45

Chris Watson ist Gründungsmitglied der experimentellen Band Cabaret Voltaire und gilt als weltweit führender Sound Recordist von Naturaufnahmen. Die Klänge, aus denen das Stück besteht, hat er im Laufe seiner 30-jährigen Karriere gesammelt, unter anderem während seiner Arbeit an BBC-Naturdokumentationen wie »Frozen Planet« mit Sir David Attenborough, für die er 2012 mit dem BAFTA Award for Best Factual Sound ausgezeichnet wurde.

Gefördert vom Musikfonds mit Mitteln der Beauftragten der Bundesregierung für Kultur und Medien, Kulturstiftung des Freistaates Sachsen, Kulturamt der Stadt Leipzig. Lautsprecher gesponsert von Genelec.

.......................................................
OCEANUS (world premiere)

Live Surround Concert
by Chris Watson


The concert takes you into the largest and most sound-rich habitat on our planet. A 45 minute spacious sound diffusion celebrating the sounds, rhythms and music of the world's oceans. The piece bathes the ears, breaks the idyll with man-made noise, swirls around the bodies in the dark ballroom, and gives space for imagination.

On the island of Rügen in the Baltic Sea, the audience dives under the surface of the sea, is flushed out with the surf on an oceanic round trip. Seals singing under Antarctic sea ice, a coral reef crackling and poping in the South China Sea, humpback whales singing piercingly in the Caribbean and the hunting orcas in the North Atlantic – all this can be heard before the audience is washed back onto the beach of Rügen.

Afterwards: open audience discussion with Chris Watson in English, translation into German by the presenters if needed.

ADMISSION: 6:30 pm
START: 7 pm
CONCERT: 00:45
TALK: 00:45

Chris Watson Chris Watson is a founding member of the experimental band Cabaret Voltaire and is considered the world's leading sound recordist of nature recordings. He has collected the sounds that make up this piece over the course of his 30-year career, including during his work on BBC nature documentaries such as »Frozen Planet« with Sir David Attenborough, for which he won the BAFTA Award for Best Factual Sound in 2012.

Funded by Musikfonds with means of the Commissioner of the Federal Government of Germany for Culture and Media, Cultural Foundation of the Free State of Saxony, Cultural Office of the City of Leipzig. Loudspeakers sponsored by Genelec.
