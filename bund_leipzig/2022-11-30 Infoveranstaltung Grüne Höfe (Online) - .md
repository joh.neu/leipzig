---
id: infoveranstaltung-gruene-hoefe-alles-zu-anmeldung-und-teilnahme-am-projekt-1-1
title: '"Grüne Höfe" - So kann es laufen'
start: 2023-03-10 17:00
end: 2023-03-10 19:30
address: Stadtteilbüro Leipziger Westen, Karl-Heine-Straße 54, 04229 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/infoveranstaltung-gruene-hoefe-alles-zu-anmeldung-und-teilnahme-am-projekt-1-1/
image: csm_220824_Gruene_Hoefe_Logo_52ecc21785.png
isCrawled: true
---
Klimatische Veränderungen und bauliche Verdichtung machen der Stadtnatur das Leben schwer. Indem wir unsere Höfe und Grünflächen ökologisch aufwerten und naturnah umstrukturieren, können wir einen Teil dazu beitragen, die einheimische Tier- und Pflanzenvielfalt zu fördern und ausgleichend auf das Stadtklima einzuwirken. Es entstehen wertvolle und nachbarschaftliche Orte für Mensch und Tier. Räume der Erholung und Naturerfahrung für die einen und essenzielle Räume zum Leben für die anderen. Diesen Entstehungsprozess zu begleiten und zu unterstützen, ist das Ziel des Projektes "Grüne Höfe" von der BUND Regionalgruppe Leipzig.
Zu diesem Termin stellen wir das Projekt sowie die Teilnahmebedingungen vor und garnieren die Veranstaltung mit Erfahrungsberichten aus dem „Hinterhofwestbewerb“ von 2019, welcher vom Stadtteilbüro Leipziger Westen ausgeschrieben wurde. Außerdem bieten wir Raum für den Austausch zu Themen der Hinterhofbegrünung an und stellen weitere hilfreiche Kooperationspartnerschaften vor.
Das Projekt "Grüne Höfe" richtet sich sowohl an Haus- und Mietgemeinschaften als auch an Hauseigentümer*innen und Wohnungsbaugenossenschaften. Mehr Informationen zum Projekt: www.bund-leipzig.de/gruenehoefe
Machen wir uns gemeinsam für die Stadtnatur stark!







	Mehr Informationen
	
		Um Anmeldung wird gebeten an kommunikation-gruenehoefe(at)bund-leipzig.de