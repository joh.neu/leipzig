---
id: klasse-klima-workshop
title: "BUNDjugend: Klasse Klima Multiplikator*innen Workshop"
start: 1970-01-01 01:00
end: 2022-05-01 20:30
address: Café im Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/klasse-klima-workshop/
image: klasse-klima-logo-2.png
isCrawled: true
---
Du möchtest Workshops zu den Themen Klimawandel, Klimaschutz oder Klimagerechtigkeit gestalten? Vom 29. April bis zum 1. Mai bietet die BUNDjugend Leipzig eine Multiplikator*innen Schulung für junge Menschen zwischen 18 und 27 Jahren an. Ziel des Workshopwochenendes ist es, die nötigen Methoden und Inhalte zu erlernen, um eigene Projekttage und Workshops an Schulen durchführen zu können.
Zusammen mit 19 weiteren Teilnehmer*innen wirst du einen tieferen Einblick in Klimabildungskonzepte bekommen, deine pädagogischen Kompetenzen schärfen und dein Wissen zum Thema Klimaschutz auffrischen und erweitern. Außerdem werden wir das Material für Klasse Klima direkt ausprobieren, damit du erfährst, wie es sich anfühlt, Multiplikator*in zu sein.Themenschwerpunkte sind Klimagerechtigkeit, Klimawandel, Klimaschutz, Umweltpsychologie und Projektmethoden.







	Mehr Informationen
	
		Die Anmeldung erfolgt über das Anmeldeformular.
Fragen zum Workshop können an umweltpaedagogik(at)bund-leipzig.de gestellt werden.