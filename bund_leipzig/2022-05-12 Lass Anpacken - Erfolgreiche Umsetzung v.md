---
id: lass-anpacken-erfolgreiche-umsetzung-von-buergerbegehren-fuer-mehr-kommunalen-klimaschutz-fachgespraech-und-diskussion-1930-uhr
title: Lass Anpacken - Erfolgreiche Umsetzung von Bürgerbegehren für mehr
  kommunalen Klimaschutz - Fachgespräch und Diskussion, 19:30 Uhr
start: 2022-10-04 19:30
end: 2022-10-04 21:00
address: Online
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/lass-anpacken-erfolgreiche-umsetzung-von-buergerbegehren-fuer-mehr-kommunalen-klimaschutz-fachgespraech-und-diskussion-1930-uhr/
image: csm_2019-11-29_Klimastreik_24_Martin_Hilbrecht_b007f4bec3.jpg
isCrawled: true
---
Der Arbeitskreis Klima & Energie lädt im Rahmen der Veranstaltungsreihe "Klimaschutz zum Selbermachen" zum Fachgespräch zur erfolgreichen Gestaltung und Umsetzung direkter Demokratieinstrumente, wie z.B. Bürgerbegehren, für einen verstärkten kommunalen Klimaschutz ein. Ziel des Fachgesprächs ist es den Teilnehmer*Innen Möglichkeiten für basisdemokratische Kommunalpolitik zu vermitteln und positive Beispiele für direktdemokratischen Klimaschutz aufzuzeigen.
Das Fachgespräch umfasst hierzu zwei Impulsvorträge, einerseits vom Umweltinstitut München e.V. und andererseits von der GermanZero Lokalgruppe GöttingenZero. Im Anschluss an diese Impulse werden wir uns in einer gemeinsamen Diskussionsrunde der Frage widmen, was wir aus den Impulsen lernen können und wie wir darauf basierend in Leipzig den Klimaschutz durch direkte Demokratie voranbringen können.
Referent:
Henning Peters (Umweltinstitut München e.V.) ; GöttingenZero
 Weiterführende Hintergrundinformationen zu dem Thema findet ihr unter:
 https://www.klimawende.org/
https://www.goettingen-klimaneutral.de/
https://germanzero.de/
 







	Mehr Informationen
	
		Die Veranstaltung findet online statt. Bitte melden Sie sich unter der Emailadresse anmelden(at)bund-leipzig.de an, um die Zugangsdaten für die Online-Veranstaltung zu erhalten.
 
Weitere Informationen zum Arbeitskreis Klima und Energie sind hier zu finden.