---
id: schmetterlingsspaziergang-12-uhr-1
title: Schmetterlingsspaziergang, 12 Uhr
start: 2022-08-27 12:00
address: Gemeinschaftsgarten Grünau, Miltitzer Allee 2, WK 8 Lausen-Grünau 04207 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/schmetterlingsspaziergang-12-uhr-1/
image: schmetterlingsspaziergang.jpg
isCrawled: true
---
Mit der Volkshochschule Leipzig geht es auf die Suche nach Tagfaltern.
Dabei könnt ihr spannendes über die bunten Insekten lernen, warum wir sie brauchen und wie ihr helfen könnt sie zu erhalten! Natürlich halten wir währenddessen auch immer Ausschau nach den Schmetterlingen und mit etwas Glück könnt ihr Admiral und Co. ganz aus der Nähe betrachten!
Der Spaziergang ist natürlich wie immer kostenlos und für die ganze Familie geeignet! Anmeldung erforderlich!
Um die Kapazitäten für das Event zu planen, bitten wir euch um eine kurze Anmeldung. So kriegen wir ein Feedback über Teilnehmerzahlen und ihr könnt sicher gehen, dass das Event stattfindet.
Bitte hier anmelden: https://www.vhs-leipzig.de/.../schmetterlinge-am...
Wann: 27. August, 12-14 Uhr
Wo: Gemeinschaftsgarten Grünau, Miltitzer Allee 2, WK 8 Lausen-Grünau 04207 Leipzig







	Mehr Informationen
	
		Bitte hier anmelden: https://www.vhs-leipzig.de/.../schmetterlinge-am...
Wann: 27. August, 12-14 Uhr
Wo: Gemeinschaftsgarten Grünau, Miltitzer Allee 2, WK 8 Lausen-Grünau 04207 Leipzig