---
id: buchvorstellung-von-aufs-land-mit-dr-ernst-paul-doerfler
title: Buchvorstellung von "Aufs Land" mit Dr. Ernst Paul Dörfler, 18 Uhr
start: 2022-04-07 18:00
address: Stadtbibliothek Leipzig, Oberlichtsaal, Wilhelm-Leuschner-Platz 10 – 11
  04107 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/buchvorstellung-von-aufs-land-mit-dr-ernst-paul-doerfler/
image: csm_Ernst_Paul_Doerfler_Copyright__c__Peter-Andreas_Hassiepen_2_cb0004d4e0.jpg
isCrawled: true
---
Wir laden herzlich zu einer Buchvorstellung von „Aufs Land. Wege aus Klimakrise, Monokultur und Konsumzwang“ mit Autor Dr. Ernst Paul Dörfler ein.
In seinem Buch beschreibt Dörfler die Krisen um unser Klima, um Natur, Umwelt und Gesundheit. Seit über vier Jahrzehnten sucht der Ökologe nach Lösungen, für sich persönlich und für die Gesellschaft. Sein Weg führte ihn aufs Land mit dem Ziel, sein Leben weitgehend klimaneutral und im Einklang mit der Natur zu gestalten. Die Erkenntnis: Nahrung, Wohnung, Energie, Mobilität und ganz besonders der Konsum sind die mächtigsten Hebel, um den eigenen CO2-Fußabdruck sowie den Ressourcenverbrauch zu minimieren.
Ihnen passt der Termin nicht? Die BUNDjugend plant ein Interview mit Dr. Dörfler für ihren Podcast "Grün auf dem Ohr", den Sie sich im Nachhinein anhören können.







	Mehr Informationen
	
		Über den Autor: Ernst Paul Dörfler, geboren 1950 in Kemberg bei Lutherstadt Wittenberg, ist promovierter Ökoche-miker. Sein Buch „Zurück zur Natur?“ (1986) wurde zum Kultbuch der ostdeutschen Umweltbewegung. Er wurde mit zahlreichen Preisen, zuletzt 2010 zusammen mit Jonathan Franzen mit dem EU-RONATUR-Preis der Stiftung Europäisches Naturerbe ausgezeichnet. (https://elbeinsel.de) Bei Hanser erschienen: Nestwärme. Was wir von Vögeln lernen können