---
id: janes-walk-2022-welt-ohne-wachstum-entdecke-lokale-initiativen-15-uhr
title: "Jane's Walk 2022: Welt ohne Wachstum? Entdecke lokale Initiativen!, 15 Uhr"
start: 2022-05-08 15:00
end: 2022-05-08 17:30
address: Platz an der Karl-Heine-Str. Höhe Hausnr. 28, 04229 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/janes-walk-2022-welt-ohne-wachstum-entdecke-lokale-initiativen-15-uhr/
image: csm_Leipzig_vom_Uniriese__Jana_Burmeister__header_2b2f2b77e5.jpg
isCrawled: true
---
Der BUND Arbeitskreis Postwachstum lädt ein zu einem lehrreichen Spaziergang im Stadtteil Lindenau/Plagwitz und möchte Orte aufzeigen, welche im Sinne einer Postwachstumsgesellschaft zukunftsträchtig sind. Hier gibt es im Stadtteil einiges zu entdecken! Ergänzt wird der Lauf mit informativen Inputs an den Stationen. Die Mitglieder des Arbeitskreises stehen aber auch während des Laufes für deine Fragen zur Verfügung. Abgerundet wird der Ausflug mit der Einkehr in ein kleines feines Café.
Komm einfach vorbei! Wir freuen uns auf Dich.
 
Weitere Informationen zum diesjährigen Jane's Walk auf Facebook: Jane's Walk Leipzig und FUSS e.V..







	Mehr Informationen
	
		Weitere Informationen zum Arbeitskreis Postwachstum sind hier zu finden.
Wichtig: Wir treffen uns auf dem Platz auf der Karl-Heine-Straße Ecke Zschochersche Straße, welcher sich unweit von der Hausnr. 28 der Karl-Heine-Straße befindet. Wir treffen uns aber nicht bei der Hausnr. 28 direkt. Danke.
Eine Voranmeldung für den Spaziergang ist nicht nötig.