---
id: eine-solarzelle-fuer-den-eigenen-balkon-19-uhr
title: Eine Solarzelle für den eigenen Balkon, 19 Uhr
start: 2022-07-04 19:00
address: Café im Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/eine-solarzelle-fuer-den-eigenen-balkon-19-uhr/
image: Leipzig_vom_Uniriese__Jana_Burmeister__weboptimiert.jpg
isCrawled: true
---
Der Arbeitskreis Klima und Energie lädt zu einer Infoveranstaltung über Steckersolarzellen. Lohnen sich diese für den eigenen Balkon? Was muss bei der Installation beachtet werden? Fördert die Stadt Leipzig eine Anschaffung? All diese Fragen beantwortet Christian Neuperger am 04.07. um 19 Uhr im Haus der Demokratie. Seien Sie dabei und werden auch Teil der Energiewende