---
id: klimaschutz-zum-selber-machen-fermentieren-lernen
title: "Klimaschutz zum selber machen: Fermentieren lernen, 17 Uhr"
start: 2022-10-04 17:00
address: Cafe - Haus der Demokratie, Bernhard-Göring-Straße 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/klimaschutz-zum-selber-machen-fermentieren-lernen/
image: _c_Philip.Knoll_Einfach_Unverpackt_Leipzig_9_webseite.jpg
isCrawled: true
---
Veranstaltung im Rahmen der Erntedankwoche 2022
Fermentations Workshop mit den AKs Landwirtschaft & Ernährung und Klima & Energie.
Sauerkraut, Joghurt, Sojasauce, Kimchi, Salami und sogar Tabak - Lebensmittel Fermentieren bedeutet mehr als sie nur haltbar zu machen. Der Gärungsprozess lässt probiotische Nährstoffe entstehen und setzt einen besonderen Geschmack frei. Gemeinsam mit euch wollen wir die altbewährte Konservierungsmethode testen und geben euch genug Wissen mit auf den Weg, damit ihr eure Lebensmittel auch Zuhause fermentieren könnt.  Bringt gerne ein eigenes Einmachglas mit! Anmeldung erwünscht.
Weitere Informationen zu den anderen Veranstaltungen der Erntedankwoche 2022 erfahrt ihr unter "Termine" auf unserer Webseite und auf unserer Facebook Seite.
 
 







	Mehr Informationen
	
		Eintritt frei – Spenden erwünscht. 
Bitte melden Sie sich über anmelden@bund-leipzig.de an.
Für Rollstuhlnutzende: barrierefrei