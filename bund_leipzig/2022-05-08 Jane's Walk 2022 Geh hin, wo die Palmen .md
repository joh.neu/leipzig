---
id: janes-walk-2022-geh-hin-wo-die-palmen-wachsen-13-uhr
title: "Jane's Walk 2022: Geh hin, wo die Palmen wachsen!, 13 Uhr"
start: 2022-05-08 13:00
end: 2022-05-08 15:00
address: Neues Augusteum Universität Leipzig, Augustusplatz 10, 04109 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/janes-walk-2022-geh-hin-wo-die-palmen-wachsen-13-uhr/
image: VFG.jpg
isCrawled: true
---
Auf den Spuren des Botanischen Gartens geht es zu verschwundenem und vergessenem Grün in Leipzig. Stationen sind der Klostergarten (Augusteum), die Wasserkunst (Albertina) und das Postfeld (Hortus Botanicus). Ein Spaziergang zu Baukultur und Stadtnatur im heute und gestern. Gestaltet von: Michael Berninger, LeipzigGrün im Rahmen von VielFalterGarten
Das Projekt Jane’s Walk ist ein internationales „Fußverkehrsfestival“, das jährlich am ersten Mai-Wochenende in über 200 Städten auf der Welt stattfindet. Weitere Jane's Walks und Informationen finden Sie hier.







	Mehr Informationen
	
		Weitere Information zum Projekt VielFalterGarten sind hier zu finden.
Eine Anmeldung für den Spaziergang ist nicht nötig. Schauen Sie gerne einfach vorbei!