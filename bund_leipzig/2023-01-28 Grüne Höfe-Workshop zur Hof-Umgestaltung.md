---
id: gruene-hoefe-workshop-zur-hof-umgestaltungsplanung
title: '"Grüne Höfe"-Workshop zur Hof-Umgestaltungsplanung'
start: 2023-01-28 15:00
end: 2023-01-28 18:00
address: Stadtteilbüro Leipziger Westen, Karl-Heine-Straße 54
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/gruene-hoefe-workshop-zur-hof-umgestaltungsplanung/
image: csm_221114_Illu_Workshop_bbd6ff7c11.jpg
isCrawled: true
---
Manchmal ist es so schwer, anzufangen!
Das wissen wir vom Projekt "Grüne Höfe" natürlich und bieten deshalb Starthilfe in Form dieses Workshops. Gemeinsam wollen wir Schritt für Schritt durch die relevanten Fragen der Hofumgestaltung nach ökologischen Gesichtspunkten gehen und im Austausch miteinander Möglichkeiten der Begrünung erarbeiten. Dabei werden uns u. a. folgende Fragen beschäftigen: 	Was gibt es bereits im Hof (z. B. Strukturen, Arten)? Was davon ist verzichtbar oder gar invasiv? 	Wie finde ich die passenden Umgestaltungsmaßnahmen für meinen Hof? 	Zu welchem Zeitpunkt macht eine Umsetzung der Maßnahmen Sinn? Wir möchten unsere Workshop-Teilnehmenden bitten, Bildmaterial ihrer Höfe und idealerweise Informationen über die Bestandsbepflanzung mitzubringen - so können wir leichter in den Planungsteil einsteigen. Zur Bestimmung eignen sich auch Apps wie z. B. "Flora Incognita".
Der Workshop richtet sich an die Teilnehmenden unseres Projekts und alle, die es noch werden wollen. Mehr Infos zum Projekt: www.bund-leipzig.de/gruenehoefe







	Mehr Informationen
	
		Um Anmeldung wird gebeten unter kommunikation-gruenehoefe(at)bund-leipzig.de. Die Teilnahmezahl ist auf 15 Plätze beschränkt, es wird eine Warteliste geben.