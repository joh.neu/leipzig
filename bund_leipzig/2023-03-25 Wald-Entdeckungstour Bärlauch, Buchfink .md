---
id: wald-entdeckungstour-baerlauch-buchfink-und-begegnung
title: "Wald-Entdeckungstour: Bärlauch, Buchfink und Begegnung"
start: 2023-03-25 11:00
address: Sachsenbrücke, Anton-Bruckner-Allee 50, 04107 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/wald-entdeckungstour-baerlauch-buchfink-und-begegnung/
image: csm_baerlauch_6b2077818e.jpg
isCrawled: true
---
Es ist faszinierend, was die Natur uns schenkt. Es gibt mehr als Bärlauch, ganz ohne Preis-Etikett. Der Frühjahrswald bietet Orte der Entspannung und der Naturschauspiele. Wenn Singvögel und andere Waldbewohner aktiv werden, die Bäume aber noch kein Blätterdach tragen, können wir in aller Ruhe entdecken. Der AK Postwachstum lädt ein, gemeinsam durch den Nonnenwald zu spazieren, ihn zu erkunden und uns dabei über das Geschenkte auszutauschen. 
Kommen Sie einfach vorbei! Wir freuen uns auf Sie.                                                       
Treffpunkt: Beim Glücksbaum an der Sachsenbrücke, Clara-Zetkin-Park







	Mehr Informationen
	
		Weitere Informationen zum Arbeitskreis Postwachstum sind hier zu finden.