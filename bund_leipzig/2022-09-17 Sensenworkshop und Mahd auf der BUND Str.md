---
id: sensenworkshop-und-mahd-auf-der-bund-streuobstwiese-9-uhr
title: Sensenworkshop und Mahd auf der BUND Streuobstwiese, 9 Uhr
start: 2022-09-17 09:15
address: Stahmelner Str. 37, 04159 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/sensenworkshop-und-mahd-auf-der-bund-streuobstwiese-9-uhr/
image: 2017-5-20_Heumahd_Sensenkurs_1B8A6009_Susanne_Wagner__weboptimiert.jpg
isCrawled: true
---
Die Herbst-Mahd auf der BUND-Streuobstwiese in Wahren steht an!
Für alle, die selbst einmal zur Sense greifen möchten, bietet der BUND Leipzig einen Sensenworkshop an, bei dem das fachgerechte Mähen mittels Sense und das Dengeln der Sensen von Thomas Thiel erläutert und vorgeführt wird. Verpflegung für ein anschließendes Picknick oder auch vorhandene Sensen dürfen gern mitgebracht werden. Festes Schuhwerk wird empfohlen.
 
Die Teilnahme ist kostenlos. Eine Anmeldung ist erforderlich.
Wir freuen uns über helfende Hände!
Wegbeschreibung:
An Öffnungstagen ist die Wiese an der Stahmelner Straße, stadtauswärts auf der linken Straßenseite, zu erreichen. Hinter einer Steinmauer mit einem großen Tor ohne Straßennummer liegt sie versteckt. Tram: Linie 11 (Richtung Schkeuditz), Haltestelle Pittlerstraße.
zu finden mit den Koordinaten: 51°22'25.1"N 12°18'52.8"E







	Mehr Informationen
	
		Für die Teilnahme ist eine Anmeldung erforderlich unter: anmelden(at)bund-leipzig.de oder 0341 / 9899 1050
ANMELDUNG
Mehr zu unserer Streuobswiese erfahren!
Zur Seite des AK Streuobst.