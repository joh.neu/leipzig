---
id: theaterworkshop-mit-dem-ak-postwachstum
title: Theaterworkshop mit dem AK Postwachstum
start: 2022-07-15 18:00
end: 2022-07-15 20:30
address: Hochschule für Musik und Theater, Tanzsaal, Grassistraße 8, 04107 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/theaterworkshop-mit-dem-ak-postwachstum/
image: csm_Globaler_Klimastreik_September_2021__Mascha_Boehm___20__d8e4f8e9bb.jpg
isCrawled: true
---
Trainieren Sie mit uns unter professioneller Anleitung, wie sich Passant*innen wirkungsvoll ansprechen lassen. Denn dieser Kontakt ist der direkteste Weg, diejenigen zum Nachdenken anzuregen, die wir sonst nicht erreichen.
Wie können Sie in nur wenigen Minuten eine Wirkung erzielen? Genau das werden wir trainieren und es auch direkt praktisch anwenden. Wenn Sie möchten, können Sie sich im Anschluss an einer augenzwinkernden und doch bewegenden Aktion anlässlich des Erdüberlastungstages am 29.07 anschließen. Natürlich können Sie den Workshop auch für eigene Aktionen nutzen.  Werden Sie mutig und trauen Sie sich mit einer gemeinsamen Aktion etwas für unsere Erde zu bewegen.
Bei Rückfragen können Sie an kontakt(at)bund-leipzig.de schreiben. 







	Mehr Informationen
	
		Um eine Anmeldung wird gebeten: anmelden(at)bund-leipzig.de