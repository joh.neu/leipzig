---
id: landart-kunst-in-und-mit-der-natur-11-uhr
title: +++ VERSCHOBEN +++ Landart - Kunst in und mit der Natur, 11 Uhr
start: 2022-10-29 11:00
end: 2022-10-29 14:00
address: wird noch bekannt gegeben
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/landart-kunst-in-und-mit-der-natur-11-uhr/
image: Landart__Antonia_Kern___3__skaliert.JPG
isCrawled: true
---
***Der Termin muss leider verschoben werden. Neuer Termin ist der 13.11.***
Kommen Sie mit auf einen Streifzug durch den Leipziger Auwald und lassen Sie sich überraschen, was die Natur alles bereithält, um kleine und große Kunstwerke entstehen zu lassen. Wir arbeiten mit dem, was uns die Natur zur Verfügung stellt und holen die Schönheiten unserer Natur mit einer guten Portion Kreativität und einer Prise Hilfsmitteln in den Augenblick.
 Bitte mitbringen: wettergerechte Kleidung; wenn möglich ein Schnitzmesser







	Mehr Informationen
	
		Anmeldung unter umweltpaedagogik(at)bund-leipzig.de