---
id: filmvorfuehrung-und-anschliessende-diskussion-zu-den-ewigen-fluorchemikalien
title: Filmvorführung mit anschließender Diskussion zu den ewigen Fluorchemikalien
start: 2022-09-03 18:00
end: 2022-09-03 20:00
address: Kinobar Prager Frühling, Haus der Demokratie, Bernhard-Göring-Straße
  152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/filmvorfuehrung-und-anschliessende-diskussion-zu-den-ewigen-fluorchemikalien/
image: csm_Prager_Fruehling_Handzettel_7b57d84a5f.jpg
isCrawled: true
---
Als menschengemachte Chemikalien werden per- und polyfluorierte Alkylverbindungen (PFAS) seit etwa 70 Jahren produziert und verwendet. Ihre thermische Stabilität und oberflächenaktiven Eigenschaften machen den Einsatz von PFAS attraktiv für diverse Beschichtungen. Ihre toxischen und bioakkumulativen Eigenschaften machen sie allerdings zu einer gesundheitlichen Bedrohung für den Menschen und seine Umwelt.
Der Leipziger BUND Arbeitskreis Chemie und Umwelt zeigt mit dem Film Vergiftete Wahrheit die Wichtigkeit der Auseinandersetzung mit den gesundheitsschädlichen PFAS auf. Der Film basiert auf der wahren Geschichte des Rechtsanwaltes Bilott, der einen der größten Umweltskandale in den USA  aufdeckte und in einen erbitterten Rechtsstreit gegen den Chemiekonzern DuPont trat.
Am 03.09.2022 um 18 Uhr lädt daher der BUND Leipzig zur Filmvorführung mit anschließender Diskussion in die Kinobar Prager Frühling ein!
Wir freuen uns über Ihr Erscheinen.
 







	Mehr Informationen
	
		Weitere Informationen zum BUND Arbeitskreis Chemie und Umwelt sind hier zu finden.