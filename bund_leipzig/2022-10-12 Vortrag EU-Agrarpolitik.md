---
id: vortrag-eu-agrarpolitik
title: Vortrag EU-Agrarpolitik
start: 2022-10-12 19:00
address: " Gaststätte “Goldene Höhe”, Virchowstraße 90a, 04157 Leipzig"
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/vortrag-eu-agrarpolitik/
image: Falter__Guy_Peer___2_weboptimiert.jpg
isCrawled: true
---
Schmetterlinge, Bienen und EU-Agrarpolitik
Dr. Guy Pe’er, Schmetterlingsspezialist und Experte für die GAP, wird in einem spannenden Vortrag mit anschließender Diskussion auf die Zusammenhänge von Landwirtschaft und dem Rückgang von Insekten eingehen.
 Pestizide und monokultureller Anbau in der Landwirtschaft führen zu einem Rückgang der Insektenvielfalt. Die gemeinsame Agrarpolitik der EU (GAP) wurde nun reformiert und könnte einen wichtigen Schritt für die Förderung der Vielfalt an Insekten bedeuten.
In dem Vortrag soll analysiert werden, ob die Agrarpolitik der EU den Bedürfnissen der Insekten gerecht wird.
Außerdem werden potenzielle Handlungsmöglichkeiten und Maßnahmen beleuchtet.







	Mehr Informationen
	
		Die Veranstaltung ist kostenlos!