---
id: suffiziente-gastronomie-nachhaltiger-wareneinkauf-14-uhr
title: Suffiziente Gastronomie - Nachhaltiger Wareneinkauf, 14 Uhr
start: 2022-11-14 14:00
end: 2022-11-14 17:00
address: Impact Hub Leipzig, Industriestr. 95, Konsumzentrale, 04229 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/suffiziente-gastronomie-nachhaltiger-wareneinkauf-14-uhr/
image: csm_2022-06-07_Gemuese_Kisten_Suffiziente_Gastronomie__c_ThomasPuschmann__9__65e246e2c7.jpg
isCrawled: true
---
Veranstaltungsreihe regional - saisonal - unverpackt vom BUND Projekt Suffiziente Gastronomie
Die Vermeidung von Verpackungs- und Lebensmittelabfällen beginnt bereits bei der Planung des Wareneinkaufs. Dabei entscheidet die Wahl der Bezugsquelle darüber, ob die Produkte in (Um)verpackungen geliefert werden, welche Transportwege zurückgelegt werden und wie frisch die Lebensmittel sind. Mit den drei Kriterien regional, saisonal und unverpackt lässt sich der Wareneinkauf umweltfreundlicher gestalten.
In unserer Netzwerkveranstaltung werden wir aufzeigen, wie Gastronom*innen die benötigten Lebensmittel und Produkte aus der Region und möglichst ohne Verpackung beziehen können. Wir stellen Lebensmittelerzeuger*innen, Produzent*innen und Lieferant*innen vor, die genau dieses Angebot abdecken. Gastronom*innen können Kontakte zu neuen Bezugsquellen und Partner*innen knüpfen und sich untereinander vernetzen. Gemeinsam sprechen wir über Bedarfe und Probleme und werden daran arbeiten, regionale Wertschöpfungsketten zu fördern.
Teilnahme kostenlos
Anmeldung: gastronomie(at)bund-leipzig.de







	Mehr Informationen
	
		jeweils 14–17 Uhr im Impact Hub Leipzig Industriestr. 95, Konsumzentrale, 04229 Leipzig
Teilnahme kostenlos
Anmeldung: gastronomie(at)bund-leipzig.de