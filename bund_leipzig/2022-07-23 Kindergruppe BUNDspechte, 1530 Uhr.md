---
id: kindergruppe-bundspechte-1530-uhr-2
title: Kindergruppe BUNDspechte, 15:30 Uhr
start: 2023-01-21 15:30
end: 2023-01-21 17:30
address: Stötteritzer Wäldchen, Kletterspielplatz
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/kindergruppe-bundspechte-1530-uhr-2/
image: IMG_5535_Antonia_Kern_..JPG
isCrawled: true
---
***english version below***
Was ist eigentlich ein Wald? Was macht ihn so besonders? Welche Tiere und Pflanzen finden wir hier? Und was bedeutet er für uns Menschen?
Die BUNDspechte sind Kinder im Alter von 5 bis 10 Jahren, die sich im Stadtwald bewegen. Gemeinsam begeben sie sich auf spannende Entdeckungsreisen durch den Wald und lernen Pflanzen, Tiere und Waldmythen kennen. Der Arbeitskreis Umweltpädagogik freut sich insbesondere auch über Kinder mit Fluchterfahrung, deren Eltern und Vertrauenspersonen gerne mitmachen können. Eine Verdolmetschung kann leider nicht garantiert werden.
Treffpunkt: Stötteritzer Wäldchen, Kletterspielplatz
Anmeldung erforderlich bei umweltpaedagogik(at)bund-leipzig.de bis Freitags 15 Uhr
Es wird um eine Spende gebeten.
***english version***
What actually is a forest? What makes it so special? What animals and plants do we find here? And what does it mean for us humans?
The BUNDspechte are children between the ages of 5 and 10 who move around in the city forest. Together they go on exciting journeys of discovery through the forest and learn about plants, animals and forest myths. The Environmental Education Working Group is especially happy to welcome children with refugee experience, whose parents and confidants are welcome to join in. Unfortunately a translation cannot be guaranteed.
Meeting place: Stötteritzer Wäldchen, Kletterspielplatz
Registration required at umweltpaedagogik(at)bund-leipzig.de until Friday 3pm
A donation is kindly requested.