---
id: wir-sind-das-wachstum
title: Wir sind das Wachstum!?, 18:30 Uhr
start: 2022-06-10 18:30
end: 2022-06-10 20:00
address: Online
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/wir-sind-das-wachstum/
image: csm_Globus-Marie-AnnMueller_7625d79d0b.jpg
isCrawled: true
---
Der AK Postwachstum lädt zu einem Vortrag und Diskussion über Postwachstum und Perspektiven für eine positive Entwicklung ein. Was macht "Wachstum" mit uns? Können wir Fortschritt neu denken? Und wie sieht eine Welt aus, in der andere Werte zählen als das Höher, Schneller, Weiter?
 
 
 
 
 
 







	Mehr Informationen
	
		Bitte anmeldung unter anmelden(at)bund-leipzig.de.
Die Zugangsdaten für die Online-Veranstaltung werden den Teilnehmenden wenige Tage vorher zugesandt.
Weitere Informationen zum Arbeitskreis Postwachstum sind hier zu finden.