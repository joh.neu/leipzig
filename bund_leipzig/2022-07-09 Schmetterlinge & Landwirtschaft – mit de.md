---
id: schmetterlinge-landwirtschaft-mit-der-kola-leipzig-1530-uhr
title: Schmetterlinge & Landwirtschaft – mit der KoLa Leipzig, 15:30 Uhr
start: 2022-07-09 15:30
address: Engelsdorfer Straße 99, 04425 Taucha
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/schmetterlinge-landwirtschaft-mit-der-kola-leipzig-1530-uhr/
image: Schmetterling__Guy_Peer___2__weboptimiert.jpg
isCrawled: true
---
Was haben Schmetterlinge mit einer Solidarischen Landwirtschaft zu tun? Gehen Sie mit dem Projekt VielFalterGarten auf Schmetterlingstour. Ein interaktiver Workshop auf dem Gelände der Kooperativen Landwirtschaft „KoLa Leipzig“.