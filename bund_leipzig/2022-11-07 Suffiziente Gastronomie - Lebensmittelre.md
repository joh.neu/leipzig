---
id: suffiziente-gastronomie-lebensmittel-reduzieren-14-uhr
title: Suffiziente Gastronomie - Lebensmittelreste reduzieren, 14 Uhr
start: 2022-11-07 14:00
end: 2022-11-07 17:00
address: MALA Leipzig und Element Ost Yoga Studio, Schulze-Delitzsch-Str. 19,
  04315 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/suffiziente-gastronomie-lebensmittel-reduzieren-14-uhr/
image: csm_2022-06-07_Waage_Suffiziente_Gastronomie__c_ThomasPuschmann__10__157c79afeb.jpg
isCrawled: true
---
Veranstaltungsreihe regional - saisonal - unverpackt vom BUND Projekt Suffiziente Gastronomie
In Deutschland werden pro Jahr 11,8 Mio. Tonnen Lebensmittel unverbraucht entsorgt. 14 % des Lebensmittelabfalls stammen von Restaurants, Mensen und Imbissen. Studien zufolge sind 72 % davon vermeidbar. Erfolgreiche Konzepte der Zero-Waste-Gastronomie zeigen, wie eine höhere Wertschätzung für Lebensmittel in den Gastronomiealltag einfließen kann. Ob bereits beim Transport, während der Lagerung, bei der Zubereitung oder durch Tellerreste – im gesamten Ablauf der gastronomischen Verarbeitung lassen sich Lebensmittelabfälle reduzieren. Wir zeigen Ihnen Möglichkeiten und Maßnahmen, wie Sie in Ihrem Gastronomiebetrieb anfallende Abfälle messen, Ihre Kalkulation anpassen oder die Menügestaltung optimieren können. Gemeinsam möchten wir Ideen sammeln, wie auch bei der Zubereitung möglichst restlos gearbeitet werden kann oder wie übrige Zutaten kreativ und sinnvoll weiterverarbeiten werden können.
Teilnahme kostenlos
Anmeldung: gastronomie(at)bund-leipzig.de







	Mehr Informationen
	
		jeweils 14–17 Uhr im MALA Leipzig und Element Ost Yoga Studio, Schulze-Delitzsch-Str. 19, 04315 Leipzig
Teilnahme kostenlos
Anmeldung: gastronomie(at)bund-leipzig.de