---
id: nachhaltiger-spaziergang
title: "Interkulturelle Wochen: Nachhaltigkeitstour, 14-18 Uhr"
start: 2022-09-24 14:00
end: 2022-09-24 18:00
address: vor dem Haus der Demokratie - Bernhard-Göring-Straße 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/nachhaltiger-spaziergang/
image: csm_DSCF7057_555f016a71.jpg
isCrawled: true
---
***english & ukrainian version below***
Wie funktioniert Einkaufen ohne Verpackungsmüll? Wie komme ich nachhaltig an Kleidung und Einrichtungsgegenstände? Und wo kann ich am besten vegan und ökologisch essen gehen?
Die Nachhaltigkeitstour im Leipziger Süden zeigt einige mögliche Alternativen für einen nachhaltigeren Lifestyle auf. Zusammen erkunden wir, welche Möglichkeiten die Südvorstadt und der Leipziger Osten zu bieten haben. Dabei besichtigen wir den BUND-Gemeinschaftsgarten am Lene-Voigt Park und lassen die Tour im Japanischen Haus bei einer Küche für Alle (Küfa) ausklingen. Kommt dafür bitte mit dem Fahrrad.  Es wird eine Übersetzung ins Englische und Ukrainische geben.
***english version***
How does shopping work without packaging waste? How do I get clothes and furnishings sustainably? And where is the best place to go for vegan and organic food?
The sustainability tour in the south of Leipzig shows some possible alternatives for a more sustainable lifestyle. Together we will explore the possibilities that Südvorstadt and Leipzig East have to offer. We will visit the BUND community garden at Lene-Voigt Park and finish the tour at the Japanese House with a Kitchen for All (Küfa). Please come by bike.  There will be a translation into English and Ukrainian.
***ukrainian version***
Як працює шопінг без відходів упаковки? Як отримати одяг та предмети інтер'єру стійко? А де я можу найкраще їсти веганське та екологічне?
Тур по сталому розвитку на півдні Лейпцига показує деякі можливі альтернативи для більш стійкого способу життя. Разом ми досліджуємо можливості, які можуть запропонувати Зюдворштадт і схід Лейпцига. Ми відвідуємо громадський сад БУНД в парку Лене-Фойгт і закінчуємо екскурсію в Японському домі з кухнею для всіх (Кюфа). Будь ласка, приїжджайте на велосипеді. 
Буде переклад на англійську та українську мови.







	Mehr Informationen
	
		Treffpunkt: vor dem Haus der Demokratie (Bernhard-Göring-Straße 152, 04277 Leipzig) um 14 Uhr.
Meeting point: in front of the House of Democracy (Bernhard-Göring-Straße 152, 04277 Leipzig) at 2 pm.
Місце зустрічі: перед Палатою демократії (Bernhard-Göring-Straße 152, 04277 Leipzig) в 14:00.