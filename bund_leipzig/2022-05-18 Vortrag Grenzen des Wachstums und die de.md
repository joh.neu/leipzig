---
id: vortrag-grenzen-des-wachstums-und-die-deutsche-linke-19-uhr
title: 'Vortrag: "Grenzen des Wachstums" und die deutsche Linke, 19:15 Uhr'
start: 2022-05-18 19:15
end: 2022-05-18 20:45
address: Hörsaal 8, Hörsaalgebäude, Campus Augustusplatz, Universität Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/vortrag-grenzen-des-wachstums-und-die-deutsche-linke-19-uhr/
image: csm_Alexander_Amberger_Foto_privat_20c56a929d.png
isCrawled: true
---
1972 erschien der Club-of-Rome-Bericht über die "Grenzen des Wachstums". Er gilt heute als historischer Ausgangspunkt moderner Wachstumskritik. Der Bericht stellt der Industriegesellschaft ein fatales Zeugnis aus und fordert eine ökologische, auf ein Ende des quantitativen Wachstums ausgerichtete Politik. Diese Forderung klingt nach einer antikapitalistischen Steilvorlage für linke Politik. Der Bericht wurde allerdings sowohl von der SED als auch von den meisten Linken im Westen ideologiekritisch abgelehnt. Nur wenige, wie z.B. die DDR-Oppositionellen Rudolf Bahro, Wolfgang Harich und Robert Havemann forderten im Anschluss eine öko-sozialistische Wende. Der Vortrag stellt ihre Ideen vor und fragt danach, warum das Thema damals (und teilweise bis heute) in der Linken für Bauchschmerzen sorgt.
Referent: Dr. Alexander Amberger (Berlin). Autor des Buches "Bahro - Harich - Havemann. Marxistische Systemkritik und politische Utopie in der DDR".
Weitere Texte und Informationen zum Autor: www.alexander-amberger.de
Die Veranstaltung wird vom BUND Arbeitskreis Postwachstum organisiert und findet im Rahmen der Public Climate School (16. - 20. Mai 2022) der Students For Future Germany statt.
Weitere Informationen zum Programm und den Veranstaltungen der Public Climate School finden Sie unter www.publicclimateschool.de.