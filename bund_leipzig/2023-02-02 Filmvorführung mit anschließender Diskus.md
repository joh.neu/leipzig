---
id: filmvorfuehrung-mit-anschliessender-diskussion-zu-den-ewigen-fluorchemikalien
title: Filmvorführung mit anschließender Diskussion zu den ewigen Fluorchemikalien
start: 2023-02-02 18:00
end: 2023-02-02 20:30
address: Schaubühne Lindenfels, Karl-Heine-Straße 50, 04229 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/filmvorfuehrung-mit-anschliessender-diskussion-zu-den-ewigen-fluorchemikalien/
image: Vergiftete_Wahrheit.jpg
isCrawled: true
---
Als menschengemachte Chemikalien werden per- und polyfluorierte Alkylverbindungen (PFAS) seit etwa 70 Jahren produziert und verwendet. Ihre thermische Stabilität und oberflächenaktiven Eigenschaften machen den Einsatz von PFAS attraktiv für diverse Beschichtungen. Ihre toxischen und bioakkumulativen Eigenschaften machen sie allerdings zu einer gesundheitlichen Bedrohung für den Menschen und seine Umwelt.
Der Leipziger BUND Arbeitskreis Chemie und Umwelt zeigt mit dem Film Vergiftete Wahrheit die Wichtigkeit der Auseinandersetzung mit den gesundheitsschädlichen PFAS auf. Der Film basiert auf der wahren Geschichte des Rechtsanwaltes Bilott, der einen der größten Umweltskandale in den USA  aufdeckte und in einen erbitterten Rechtsstreit gegen den Chemiekonzern DuPont trat.
Am 02.02.2023 um 18 Uhr lädt daher der BUND Leipzig zur Filmvorführung mit anschließender Diskussion in die Schaubühne Lindenfels ein! 
Wir freuen uns über Ihr Erscheinen.
 







	Mehr Informationen
	
		Weitere Informationen zum BUND Arbeitskreis Chemie und Umwelt sind hier zu finden.