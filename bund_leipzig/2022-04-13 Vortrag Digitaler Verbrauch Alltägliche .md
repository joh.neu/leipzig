---
id: online-vortrag-digitaler-verbrauch-alltaegliche-internetnutzung-und-ihr-energieverbrauch-19-uhr
title: "Vortrag: Digitaler Verbrauch: Alltägliche Internetnutzung und ihr
  Energieverbrauch, 15:15 Uhr"
start: 2022-05-16 15:15
end: 2022-05-16 16:45
address: Seminargebäude Raum 313, Campus Augustusplatz, Universität Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/online-vortrag-digitaler-verbrauch-alltaegliche-internetnutzung-und-ihr-energieverbrauch-19-uhr/
image: Webinar_digitale_verbrauch_grafik_BUND_leipzig.jpg
isCrawled: true
---
Wir alle tun es: Mal schnell etwas googeln, mailen, liken oder Inhaltestreamen. Weltweit werden täglich ca. 320 Milliarden E-Mails verschickt und über 1 Milliarde Stunden Videos auf Youtube geschaut!   Im Vortrag wird sich mit Fragen beschäftigt, wie: Was bedeutet der zunehmende Datenverkehr für die Umwelt? Welche Alternativen gibt es?
Fühlen Sie sich eingeladen mit zu diskutieren und Ideen für den umweltfreundlicheren Umgang mit dem Internet zu sammeln.
Die Veranstaltung wird vom BUND Arbeitskreis Klima und Energie organisiert und findet im Rahmen der Public Climate School (16. - 20. Mai 2022) der Students For Future Germany statt.
Weitere Informationen zum Programm und den Veranstaltungen der Public Climate School finden Sie unter www.publicclimateschool.de.