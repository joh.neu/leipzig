---
id: suffiziente-gastronomie-kundeneigene-behaeltnisse-14-uhr-1
title: Suffiziente Gastronomie - Kundeneigene Behältnisse, 14 Uhr
start: 2022-10-10 14:00
end: 2022-10-10 17:00
address: Impact Hub Leipzig, Industriestr. 95, Konsumzentrale, 04229 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/suffiziente-gastronomie-kundeneigene-behaeltnisse-14-uhr-1/
image: csm_Mehrweggefaesse_-_kundeneigener_Behaelter_gefuellt_in_Haenden_Mirko_Schimmelschmidt_WEB_4139a25321.jpg
isCrawled: true
---
Veranstaltungsreihe regional - saisonal - unverpackt vom BUND Projekt Suffiziente Gastronomie
Ab dem 1. Januar 2023 tritt im Rahmen des neuen Verpackungsgesetzes (VerpackG) eine Mehrwegangebotspflicht für Gastronomiebetriebe in Kraft. Der Verbrauch von Einwegverpackungen aus Kunststoff für Essen und Getränke zum Mitnehmen – Becher für Kaffee (Coffee-to-go) oder Boxen für Speisen (Takeaway-Essen) – soll dadurch reduziert werden. Große Betriebe sind dazu verpflichtet, eine Mehrwegverpackung im Betrieb vorzuhalten. Kleine Betriebe mit einer Verkaufsfläche von bis zu 80 Quadratmetern (inklusive frei zugänglicher Sitz- und Aufenthaltsbereiche) und weniger als fünf Beschäftigten müssen es ermöglichen, mitgebrachte Gefäße der Kundschaft zu befüllen.
In unserer Netzwerkveranstaltung informieren wir über die Thematik des Befüllens kundeneigener Behältnisse. Welche Arbeitsabläufe sind zu beachten? Welche Hygienevorgaben gibt es? Wie weise ich meine Mitarbeiter*innen richtig ein? Teilen Sie Ihre Erfahrungen und Probleme mit uns und lassen Sie uns gemeinsam nach Lösungen suchen.
Teilnahme kostenlos
Anmeldung: gastronomie(at)bund-leipzig.de







	Mehr Informationen
	
		jeweils 14–17 Uhr im Impact Hub Leipzig Industriestr. 95, Konsumzentrale, 04229 Leipzig
Teilnahme kostenlos
Anmeldung: gastronomie(at)bund-leipzig.de