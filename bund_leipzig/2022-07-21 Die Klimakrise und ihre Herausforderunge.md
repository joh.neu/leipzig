---
id: die-klimakrise-und-ihre-herausforderungen-wie-mit-den-eigenen-emotionen-umgehen
title: Die Klimakrise und ihre Herausforderungen – Wie mit den eigenen Emotionen
  umgehen?
start: 2023-02-02 18:30
end: 2023-02-02 20:30
address: Café im Haus d. Demokratie, B.-Göring-Str. 152, 04277 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/die-klimakrise-und-ihre-herausforderungen-wie-mit-den-eigenen-emotionen-umgehen/
image: 2018-07-29_Klimacamp_Leipziger_Land__Thomas_Puschmann__weboptimiert.jpg
isCrawled: true
---
Die erschreckenden Nachrichten von Naturkatastrophen, bedrückenden Klima-Prognosen und das Erleben des Klimawandels lösen belastende Gefühle wie Angst, Verzweiflung, Ohnmacht oder Überforderung aus.
Doch wie können wir mit diesen Gefühlen umgehen? Wie können wir betroffene Menschen in unserem Umfeld emotional stärken? Und welche psychologischen Strategien können helfen, um in Zukunft mehr auf unser eigenes Wohlbefinden und unsere psychische Gesundheit zu achten? Der Arbeitskreis Klima und Energie veranstaltet einen interaktiven Vortrag zu diesen Fragen und möchte sich Themen wie Selbstfürsorge, Resilienz Stärkung und dem Umgang mit Klimawandelängsten widmen.
Sebastian Funke (Dipl. Heilpädagoge) und Stefan Kurth (Dipl. Psychologe) werden uns durch diesen Abend begleiten. Beide arbeiten als systemische Therapeuten sowie Paar- und Familientherapeuten bei FamThera in Leipzig und setzen sich in ihrer Arbeit mit den psychologischen Aspekten der Klimakrise auseinander.
Fühlen Sie sich eingeladen an dieser Veranstaltung teilzunehmen und mit anderen Menschen in Austausch zu gehen. Wir möchten gemeinsam schauen wie wir uns und andere im Bezug zur Klimakrise nachhaltig unterstützen können.







	Mehr Informationen
	
		Anmeldung erforderlich unter: anmelden(at)bund-leipzig.de