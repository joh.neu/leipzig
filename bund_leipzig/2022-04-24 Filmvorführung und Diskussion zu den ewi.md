---
id: filmvorfuehrung-und-diskussion-zu-den-ewigen-fluorchemikalien
title: Filmvorführung und Diskussion zu den ewigen Fluorchemikalien
start: 2022-04-24 19:00
end: 2022-04-24 21:30
address: die naTo, Karl-Liebknecht-Straße 46, 04275 Leipzig
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/filmvorfuehrung-und-diskussion-zu-den-ewigen-fluorchemikalien/
image: film_ak_chemie_post_2.jpg
isCrawled: true
---
Als menschengemachte Chemikalien werden per- und polyfluorierte Alkylverbindungen (PFAS) seit etwa 70 Jahren produziert und verwendet. Ihre thermische Stabilität und oberflächenaktiven Eigenschaften machen den Einsatz von PFAS attraktiv für diverse Beschichtungen. Ihre toxischen und bioakkumulativen Eigenschaften machen sie allerdings zu einer gesundheitlichen Bedrohung für den Menschen und seine Umwelt.
Der Leipziger BUND Arbeitskreis Chemie und Umwelt zeigt mit dem Film Vergiftete Wahrheit die Wichtigkeit der Auseinandersetzung mit den gesundheitsschädlichen PFAS auf. Der Film basiert auf der wahren Geschichte des Rechtsanwaltes Bilott, der einen der größten Umweltskandale in den USA  aufdeckte und in einen erbitterten Rechtsstreit gegen den Chemiekonzern DuPont trat.
Am 24.04.2022 um 19 Uhr lädt daher der BUND Leipzig zur Filmvorführung in die naTo ein!
Nach dem Film werden die wichtigsten Aspekte nochmals aufgegriffen und in einer gemeinsamen Diskussion besprochen.
 
Wir freuen uns sehr über Ihr Erscheinen.
 







	Mehr Informationen
	
		Weitere Informationen zum BUND Arbeitskreis Chemie und Umwelt sind hier zu finden.