---
id: kindergruppe-bundspechte-15-uhr
title: +++FÄLLT AUS+++ Kindergruppe BUNDspechte, 15 Uhr
start: 2022-12-17 15:00
end: 2022-12-17 17:00
address: Hinterer Grillplatz im Rosenthal (beim Wackelturm)
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/kindergruppe-bundspechte-15-uhr/
image: 2017-05-05_Schulprojekt_Susanne_Wagner_Kinder___5_.JPG
isCrawled: true
---
***english version below***
Was ist eigentlich ein Wald? Was macht ihn so besonders? Welche Tiere und Pflanzen finden wir hier? Und was bedeutet er für uns Menschen?
Die BUNDspechte sind Kinder im Alter von 5 bis 10 Jahren, die sich im Stadtwald bewegen. Gemeinsam begeben sie sich auf spannende Entdeckungsreisen durch den Wald und lernen Pflanzen, Tiere und Waldmythen kennen. Der Arbeitskreis Umweltpädagogik freut sich insbesondere auch über Kinder mit Fluchterfahrung, deren Eltern und Vertrauenspersonen gerne mitmachen können. Eine Verdolmetschung kann leider nicht garantiert werden.
Treffpunkt: Hinterer Grillplatz im Rosenthal (beim Wackelturm)
Anmeldung erforderlich bei umweltpaedagogik(at)bund-leipzig.de bis zum Freitag davor 15 Uhr
Es wird um eine Spende gebeten.
***english version***
What actually is a forest? What makes it so special? What animals and plants do we find here? And what does it mean for us humans?
The BUNDspechte are children between the ages of 5 and 10 who move around in the city forest. Together they go on exciting journeys of discovery through the forest and learn about plants, animals and forest myths. The Environmental Education Working Group is especially happy to welcome children with refugee experience, whose parents and confidants are welcome to join in. Unfortunately a translation cannot be guaranteed.
Meeting place: the fire space in the "Rosenthal" near the "Wackelturm"
Registration required at umweltpaedagogik(at)bund-leipzig.de until Friday 3 pm
A donation is kindly requested.