---
id: multiplikatorinnenworkshop-umwelt-und-wildnispaedagogik-2
title: Multiplikator*innenworkshop Umwelt-und Wildnispädagogik, 10-15 Uhr
start: 2022-07-02 10:00
end: 2022-07-02 15:00
address: wird noch bekannt gegeben
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/multiplikatorinnenworkshop-umwelt-und-wildnispaedagogik-2/
image: 2017-05-05_Schulprojekt_Susanne_Wagner_Kinder___32_.JPG
isCrawled: true
---
***aktuell nur noch Plätze auf der Warteliste***
Multiplikator*innenworkshop Umwelt- und Wildnispädagogik
Du möchtest in der Umweltpädagogik aktiv werden? Du denkst, dir fehlt dazu das „Know-How“? Dann bist du bei unserem Multiplikator*innen-Workshop genau richtig! An vier Terminen lernst du die Grundlagen der Umwelt- und Wildnispädagogik kennen, kannst verschiedene Methoden ausprobieren und selbst in die Rolle der Gruppenleitung schlüpfen.
Der Workshop richtet sich an alle, die zu unserem Umweltpädagogik-Team stoßen möchten, um beispielsweise eine Waldgruppe zu leiten. Es ist keine Vorerfahrung notwendig.
Der Workshop findet an vier Terminen statt. Die einzelnen Teile bauen aufeinander auf und sollten daher alle besucht werden:
Sonntag, 22.05.22, 10-15 Uhr: Thema „Pflanzen“ 	Essbare Pflanzen, Kräuter kennen lernen, verkosten und einen Salat anrichten 	regionale Baumarten kennenlernen 	naturpädagogische Methoden kennenlernen Samstag, 18.06.22, 10-15 Uhr: Thema „Bauernhofpädagogik“ 	wildnispädagogische Methoden 	die Dölitzer Wassermühle kennenlernen Samstag, 02.07.22, 10-15 Uhr: Thema „Sinne“ 	zu den Sinnen fühlen, schmecken, riechen, sehen und hören Methoden zum Erleben und Erfahren der Umwelt und des Waldes ausprobieren Samstag, 24.09.22, 10-15 Uhr: Thema „Praxis“ 	Hier hast du die Möglichkeit, die erlernten Methoden in der Gruppe auszuprobieren und dir ein Feedback von den anderen Teilnehmer*innen einzuholen. 







	Mehr Informationen
	
		Die genauen Treffpunkte werden spätestens eine Woche vor Beginn kommuniziert. Die Teilnahme ist kostenfrei. Eine Anmeldung ist nötig, da die Teilnehmer*innenzahl auf 15 beschränkt ist. Anmeldung bitte bis zum 08.05.22 an: umweltpaedagogik(at)bund-leipzig.de