---
id: janes-walk-2022-plagwitzer-buergerbahnhof-zwischen-buergerinnenengagement-und-flaechenversiegelung-17-uhr
title: "Jane's Walk 2022: Plagwitzer Bürgerbahnhof – Zwischen
  Bürger*innenengagement und Flächenversiegelung, 17 Uhr"
start: 2022-05-06 17:00
end: 2022-05-06 19:00
address: Treffpunkt am S-Bahnhof Leipzig-Plagwitz
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/janes-walk-2022-plagwitzer-buergerbahnhof-zwischen-buergerinnenengagement-und-flaechenversiegelung-17-uhr/
image: Janes_Walk_am_Bayerischen_Bahnhof_2019__Josephine_Michalke__weboptimiert.jpeg
isCrawled: true
---
Das Projekt Jane’s Walk ist ein internationales „Fußverkehrsfestival“, das jährlich am ersten Mai-Wochenende in über 200 Städten auf der Welt stattfindet. Der Name des Projektes geht auf die Amerikanerin Jane Jacobs (1916-2006) zurück. Jacobs gilt als eine der bedeutendsten Stadt-Architekturkritikerinnen des 20. Jahrhunderts
Der diesjährige Jane's Walk wird am Wochenende vom 06. - 08. Mai 2022 stattfinden. In diesem Jahr wollen wir wieder Outdoor-Walks anbieten (natürlich unter Einhaltung der entsprechenden Corona-Maßnahmen).
Beim Spaziergang des AK Stadtentwicklung und Verkehr am 06. Mai um 17 Uhr werden wir den Leipziger Bürgerbahnhof näher kennen lernen. Wie sah es auf dem Gelände früher aus? Wie kam es dazu, dass es zu einem „Bürgerbahnhof“ wurde? Und wie kann die Zukunft des Bahnhofs aussehen? Wir entdecken Tauschläden, Kletterfelsen, Bauwagenplätze und den Plagwitzer Friedhof. Wir sprechen mit Menschen, die sich auf dem Gelände engagieren und solchen, die sich für den Erhalt des „Bürgerbahnhofs“ einsetzen.
Den genauen Treffpunkt am S-Bahnhof Plagwitz könnt ihr euch hier anschauen.







	Mehr Informationen
	
		Mehr zum Jane's Walk.
Eine Anmeldung für den Spaziergang ist nicht nötig. Schauen Sie gerne einfach vorbei!