---
id: ak-umweltpaedagogik-og-ost-benjeshecken-bauen-fuer-kinder-1030-uhr
title: "AK Umweltpädagogik & OG Ost: Benjeshecken bauen für Kinder, 10:30 Uhr"
start: 2022-04-10 10:30
address: VAGaBUND Lene im Lene-Voigt-Park (Höhe Reichpietschstraße 53-55)
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/ak-umweltpaedagogik-og-ost-benjeshecken-bauen-fuer-kinder-1030-uhr/
image: 2019-10-20_VAGaBUND-Lene_Jana-Burmeister_DSCN9833__weboptimiert_.jpg
isCrawled: true
---
Der BUND Leipzig veranstaltet einen Workshop für Kinder zum Anlegen einer Benjeshecke im VAGaBUND Lene im Lene-Voigt-Park auf Höhe der Reichpietschstraße 53-55. Der Arbeitskreis Umweltpädagogik veranstaltet mit der Ortgruppe Ost diesen Workshop, damit der Gemeinschaftsgarten für die neue Saison hergerichtet wird. Für ein gemütliches und spaßiges Umfeld, bringt bitte eine Sitzgelegenheit wie ein Kissen oder eine Decke, Snacks und Getränke mit. Für Werkzeug und Material sorgt der BUND Leipzig. Wenn Kinder-Arbeitshandschuhe und eine Gartenschere vorhanden sind, bringt sie gerne mit!







	Mehr Informationen
	
		Wir bitten um eine Anmeldung unter umweltpaedagogik(at)bund-leipzig.de