---
id: landart-kunst-in-und-mit-der-natur-11-uhr-1
title: Landart - Kunst in und mit der Natur, 11 Uhr
start: 2022-11-13 11:00
end: 2022-11-13 14:00
address: vor dem Freibad Kleinzschocher
link: https://www.bund-leipzig.de/service/termine-bund-leipzig/detail/event/landart-kunst-in-und-mit-der-natur-11-uhr-1/
image: Landart__Antonia_Kern___3__skaliert.JPG
isCrawled: true
---
Kommen Sie mit auf einen Streifzug durch den Leipziger Auwald und lassen Sie sich überraschen, was die Natur alles bereithält, um kleine und große Kunstwerke entstehen zu lassen. Wir arbeiten mit dem, was uns die Natur zur Verfügung stellt und holen die Schönheiten unserer Natur mit einer guten Portion Kreativität und einer Prise Hilfsmitteln in den Augenblick.
 Bitte mitbringen: wettergerechte Kleidung; wenn möglich ein Schnitzmesser







	Mehr Informationen
	
		Anmeldung unter umweltpaedagogik(at)bund-leipzig.de