---
id: "1306153276558659"
title: Solidarität mit der Ukraine
start: 2022-02-28 18:00
locationName: Nikolaikirchhof
address: Leipzig
link: https://www.facebook.com/events/1306153276558659/
image: 274608411_401403458535554_4253673049523741372_n.jpg
isCrawled: true
---
Gemeinsam mit der Stiftung „Friedliche Revolution“ rufen wir euch zu einer Mahnwache am 28. Februar 2022, ab 18:00 Uhr, auf dem Nikolaikirchhof auf.

Die Bemühungen, einen Krieg zu verhindern, sind gescheitert, doch die Bemühungen, ihn zu beenden, müssen Erfolg haben. Und zwar schnell.
„Wir wollen uns versammeln, um gegen Krieg, die Invasion Russlands in die Ukraine und gegen Hetze und die Verbreitung von Unwahrheiten zu demonstrieren“