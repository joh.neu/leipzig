---
id: "3318575985036303"
title: Das Ende der Geduld - Aufmarsch von rechten Demokratiefeind*innen verhindern!
start: 2022-01-10 17:30
locationName: Leipziger Innenstadt
link: https://www.facebook.com/events/3318575985036303/
image: 271442841_6749402941797864_8553384485507823843_n.jpg
isCrawled: true
---
Protest gegen rechte Demokratiefeind*innen!

Montag, 10. Januar 2022
ab 17:30 Uhr
Leipziger Innenstadt 

***

Das Ende der Geduld.

Das Aktionsnetzwerk „Leipzig nimmt Platz“ hat zuletzt nicht zu eigenen Versammlungen aufgerufen, aus Respekt vor der möglichen Virusausbreitung und in Solidarität mit allen, die eine Erkrankung schwer treffen. Die derzeitigen Einschränkungen der Versammlungsfreiheit lehnten und lehnen wir, wie auch schon ausführlicher in vielen Texten geschrieben, ab und halten auch die derzeitigen Regelungen für überzogen. Gleichwohl wollten wir in der Krise unseren Beitrag dafür leisten, dass das Pflegepersonal in den Krankenhäusern nicht noch weiter durch eine ansteigende Inzidenz belastet wird und weitere Menschen in Gefahr einer ernsthaften Erkrankung kommen. 

Wir folgten dabei der Annahme, dass der Freistaat die aufgestellten Regelungen umsetzen würde. Diese Annahme war bereits von Anfang gewagt und hat sich als Fehler erwiesen. Festzustellen ist, dass sich Montag für Montag in sächsischen Gemeinden und Städten Menschen zu sogenannten Corona-Spaziergängen zusammenfinden und kein Problem haben, Seit an Seit mit Rechten und Neonazis zu laufen und den Aufrufen der rechten Kleinstpartei „Freie Sachsen“ zu folgen.

Während anfänglich in Leipzig die Situation noch relativ ruhig blieb und die sogenannten Spaziergänge in der Innenstadt keine Sichtbarkeit entfalten konnten, so hat sich die Situation jetzt geändert. Am vergangenen Montag konnten bis zu 200 Personen, darunter auch viele Rechte, begleitet von der Polizei, durch die Innenstadt ziehen, während Personen, die dagegen demonstrierten, mehrfach kontrolliert und mit Platzverweisen versehen wurden. Das stellenweise Nichtagieren der Polizeikräfte, dass von der Führung in Dresden ausdrücklich unterstützt wird, vermittelt den sogenannten Spaziergänger:innen eine Wirkmächtigkeit, die sie zu weiteren Aktionen motivieren wird. 

Es ist für uns nicht länger hinnehmbar, dass es von der politischen Gesinnung von Personen oder Gruppen abhängt, ob Corona-Schutzregeln für diese gelten oder nicht. Gesetzesbrüche von Corona Leugner:innen werden in Sachsen nicht nur toleriert, Nein, führende Politiker:innen äußern sogar ausdrücklich Verständnis dafür. Einen anderen Teil, welcher von der Polizei politisch links gelesen wird, überziehen die Behörden aber sehr gerne mit zum Teil sinnwidrigen Verfahren.

Wir behaupten nicht, dass alle sogenannten Spaziergänger:innen „Nazis“ oder „Rechte“ wären. Tatsächlich trifft sich dort eine nach wie vor heterogene Mischung, die alle das ungefähre Gefühl einer Ablehnung der Corona-Maßnahmen eint. Allerdings werden Hintergründe und Zusammenhänge der Versammlungen, das Agieren der „Freien Sachsen“, welche die Spaziergänge „orchestrieren“ und deren Inhalte auch bei der „Bewegung Leipzig“ geteilt werden, durch die Teilnehmenden ausgeblendet und durch die eigene verzerrte Wahrnehmung ersetzt. Weil man selbst keine Rechten gesehen habe und die Freien Sachsen nicht kenne, kann es auch keine Rechten bei den „Spaziergängen“ geben und man findet sich völlig zu Unrecht als „Nazi“ bezeichnet. Verantwortung für das eigene Handeln will man nicht übernehmen. Hinter dem Ansinnen der rechten Kräfte steht nichts weiter als das Ziel eine vorrevolutionäre Situation heraufzubeschwören, um das „System“ zu beseitigen. Die „ahnungslosen Spaziergänger:innen“, gefangen im deutschen Narrativ zwischen „nichts wissen wollen“ und „nichts gewusst haben“; machen sich damit zu den nützlichen „Idioten“ der organisierten Rechten, die nichts sehnlicher wollen als einen Bürgerkrieg im Sinne einer „nationalen Wiedergeburt“. All das ist bekannt. Seit Jahren und unablässig weisen viele Aktive und auch Wissenschaftler:innen daraufhin, ohne dass es zu einem erkennbaren Handeln bei den Sicherheitsbehörden führen würde.

Die Gefahr ist, dass die Spaziergänge weiter anwachsen, die Teilnehmer:innen sich weiter radikalisieren und entsprechend handeln, dass es aufgrund der Radikalisierung Opfer geben wird, ist mithin keine Frage mehr des Ob sondern des Wann. 

Nach dem Fackelmarsch vor dem Wohnhaus der Gesundheitsministerin, der für einen landesweiten Aufschrei gesorgt hatte, hat ausgerechnet der sächsische Innenminister, der sonst keine Gelegenheit auslässt, Verständnis für Corona-Demonstrant:innen zu äußern und zivilgesellschaftliche Bündnisse zu kriminalisieren, nach der Zivilgesellschaft gerufen. Es ist an dieser Stelle wichtig darauf hinzuweisen, dass die Entgrenzung nicht allein der Fackelmarsch war, sondern im Winter 2020/21 auch Corona-Demonstrant:innen vor dem Wohnhaus des Ministerpräsidenten auftauchten und von diesem dafür mit einer exklusiven Gesprächseinladung geadelt wurden. Das Verhalten der Sicherheitsbehörden muss konsequent gedacht, als Einladung verstanden werden, zusammen mit der seltsamen ambivalenten Haltung der Landesregierung, die jeder sich radikal zeigenden Minderheit, so lange sie nur rechts ist, einen Sprachraum gewährt. Wir können und wollen dabei nicht schweigend zusehen, wenn bei den Spaziergängen Rechte, Reichsbürger:innen und Hooligans Seite an Seite mit Bürger:innen, die nichts gesehen und wahrgenommen haben wollen, durch die Stadt ziehen. Wenn die Sicherheitsbehörden nicht bereit sind, das Treiben zu stoppen, wir sind es.

Auch unter den Einschränkungen der Corona-Notfall-Verordnung rufen wir dazu auf, sich – mit Maske und dem gebotenen Abstand – gegen diese teils offen rechte Mobilisierung zu stellen und deutlich zu machen, dass in Leipzig kein Platz ist für Verschwörungsmythen, Wissenschaftsleugnung, Antisemitismus und Menschenverachtung. 

Die Zivilgesellschaft wurde gerufen und sie wird jetzt antworten! 