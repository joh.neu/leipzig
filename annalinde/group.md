---
name: ANNALINDE
website: https://annalinde-leipzig.de/
email: info@annalinde-leipzig.de
scrape:
  source: facebook
  options:
    page_id: 188613374504145
---
Die ANNALINDE gGmbH betreibt multifunktionale urbane Landwirtschaft in Leipzig. Sie ist aus der kooperativen Eigeninitiative und Kreativität von jungen Menschen mit Spaß am unternehmerischen Tun entstanden.