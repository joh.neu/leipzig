---
id: "672126070679013"
title: "Annalinde Jungpflanzenverkauf: 15.04. - 25.05.2022"
start: 2022-05-24 14:00
end: 2022-05-24 18:00
locationName: Annalinde Leipzig
link: https://www.facebook.com/events/672126024012351/
image: 278123018_5288104391221659_3604627659651751413_n.jpg
isCrawled: true
---
👉 Der diesjährige Annalinde Jungpflanzenverkauf ist vom 15. April bis 25. Mai! 

🛒 Ihr könnt eure Jungpflanzen im Webshop vorbestellen und an einem gewünschten Tag zwischen 15. April und 25. Mai in unserer Gärtnerei in Lindenau (Lützner Str. 108) abholen. Hier geht's zum Shop: https://shop.annalinde-leipzig.de/

🌱 Es warten über 200 verschiedene Gemüse-, Kräuter- und Zierjungpflanzen auf euch. Freut euch auf eine großartige Vielfalt! 

🌿 Die Jungpflanzen werden handwerklich und nachhaltig in unseren Gärtnereien produziert. Die Anzucht erfolgt weitgehend ungeheizt und mit biologisch zertifiziertem Saatgut und für den Ökolandbau zugelassenen Substraten. Pflanzenschutz betreiben wir ohne den Einsatz von Pestiziden, dafür mit der Unterstützung von Nützlingen.

➡️ So funktioniert's in Kürze: 
1. Jungpflanzen & andere Produkte auswählen
2. Abholtag & Zahlweise bestimmen
3. Jungpflanzen abholen & einpflanzen

👉 Hier findet ihr eure Jungpflanzen: https://shop.annalinde-leipzig.de/

📷: Dominik Wolf