---
name: Rojava Soli Bündnis Leipzig
website: https://rsbl.noblogs.org/
email: kontakt@rojava-soli-leipzig.de
scrape:
    source: wp-event-list
    options:
        url: https://rsbl.noblogs.org/termine/
---
Hoch die internationale Solidarität!
