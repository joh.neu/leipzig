---
name: Ende Gelände Leipzig
website: https://www.facebook.com/Ende-Gel%C3%A4nde-Leipzig-286052808874449/
email: leipzig@ende-gelaende.org
scrape:
  source: endegelaende
  options:
    city: Leipzig
---
Das Bündnis Ende Gelände fordert Klimagerechtigkeit. Dafür braucht es einen sofortigen Ausstieg aus der Braunkohle. 