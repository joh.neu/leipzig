---
id: aktionstraining-luetzerath-leipzig
title: Aktionstraining für Lützerath
start: 2023-01-07 10:00
address: auf Nachfrage in Leipzig
link: https://www.ende-gelaende.org/events/aktionstraining-luetzerath-leipzig/
isCrawled: true
---
Basis-Aktionstraining für Lützerath. Ort wird auf Nachfrage bekanntgegeben, Anmeldung unter leipzig@luetzilebt.org erforderlich