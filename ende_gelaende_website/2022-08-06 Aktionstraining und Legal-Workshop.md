---
id: aktionstraining-und-legal-workshop
title: Aktionstraining und Legal-Workshop
start: 2022-08-06 12:00
address: Ostwache (Gregor-Fuchs-Straße 45) in Leipzig
link: https://www.ende-gelaende.org/events/aktionstraining-und-legal-workshop/
isCrawled: true
---
Wir stellen uns im Rahmen der diesjährigen Massenaktion gemeinsam dem Bau von LNG-Terminals in den Weg und sägen dabei die neokolonialen Strukturen des fossilen Kapitalismus an. 
Du möchtest mitmachen?
Dann komm zum Vorbereitungstag! Damit eine Aktion zivilen Ungehorsams möglichst sicher abläuft, braucht es eine grundlegende Vorbereitung - egal ob erfahrene*r Aktivisti oder Neueinsteiger*in. 
Im Aktionstraining und dem Legal-Workshop wollen wir diese Grundlagen gemeinsam erarbeiten. Wir treffen uns dafür um 12:00 an der Ostwache.
Auch wenn es auch auf dem Camp noch Möglichkeiten zur Vorbereitung gibt -  versucht möglichst am Samstag zu kommen! 
Du bist alleine und hast noch keine Bezugsgruppe?
Der Vorbereitungstag bietet sich wie immer auch an, um sich untereinander zu connecten.
Mehr Infos zu Bezugsgruppen, Aktionskonsens und vielem mehr finden sich hier:
https://ende-gelaende.org/massenaktion-2022/
Mehr Infos aus Leipzig:
https://t.me/EndeGelaendeLeipzig