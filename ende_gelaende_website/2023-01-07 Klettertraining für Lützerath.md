---
id: klettertraining-luetzerath-leipzig
title: Klettertraining für Lützerath
start: 2023-01-07 10:00
address: auf Nachfrage in Leipzig
link: https://www.ende-gelaende.org/events/klettertraining-luetzerath-leipzig/
isCrawled: true
---
Klettertraining für Lützerath. 10 bis ca. 16 Uhr. Ort wird auf Nachfrage bekanntgegeben, Anmeldung unter kletteradatz@riseup.net erforderlich, Material vorhanden -> evtl. Wartezeiten – gern sagen, wann du kommen willst