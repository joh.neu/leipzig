---
name: Diem25 Leipzig DSC
website: https://diem25-leipzig.de/
email: info@diem25-leipzig.de
scrape:
  source: facebook
  options:
    page_id: 168160784114092
---
DiEM25 ist eine europaweite, grenzüberschreitende Bewegung von Demokrat_Innen. Wir glauben, dass die Europäische Union dabei ist zu zerfallen. Die Europäer_Innen verlieren ihren Glauben an die Möglichkeit, europäische Lösungen für europäische Probleme zu finden. Zur gleichen Zeit wie das Vertrauen in die EU schwindet, sehen wir einen Anstieg von Menschenverachtung, Fremdenfeindlichkeit und Nationalismus.