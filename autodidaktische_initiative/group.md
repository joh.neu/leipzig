---
name: autodidaktische initiative
website: https://adi-leipzig.net/
email: info@adi-leipzig.net
address: Georg-Schwarz-Straße 19, Leipzig
scrape:
  source: facebook
  options:
    page_id: 430986233593239
  exclude: []
---
Die ADI ist eine Plattform für freie Bildung, Philosophie und Intervention.