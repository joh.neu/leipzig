---
name: oikos Leipzig
website: https://oikos-international.org/leipzig/
email: info@oikos-leipzig.org
scrape:
  source: facebook
  options:
    page_id: 278948995552943
---
oikos ist eine internationale Studierendenorganisation, die sich für die Förderung einer nachhaltigen Gesellschaft im Spannungsfeld zwischen Ökonomie, Ökologie und Sozialem einsetzt. Insbesondere soll das Bewusstsein der Studierenden, Wissenschaftler und Unternehmen für Themen der Nachhaltigkeit gestärkt werden.
