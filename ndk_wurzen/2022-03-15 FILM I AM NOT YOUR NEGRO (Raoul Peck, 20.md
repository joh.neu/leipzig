---
id: "1951029961768370"
title: "FILM: I AM NOT YOUR NEGRO (Raoul Peck, 2016)"
start: 2022-03-15 19:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/1951029961768370/
image: 272868259_3070562163219979_8917715482961188608_n.jpg
isCrawled: true
---
Der Film gilt als Meisterwerk des jüngeren
politischen Kinos. Einerseits ist der Film
eine eindrückliche Analyse der Repräsenta­tion von Afro-Amerikaner:innen in der US-
Kulturgeschichte. Andererseits versteht er
es, mit seiner kraftvollen visuellen Spra­che Themen wie institutionellem Rassis­mus oder Alltagsdiskriminierung Raum zu
geben.