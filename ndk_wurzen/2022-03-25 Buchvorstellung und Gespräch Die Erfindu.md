---
id: "1088697791983181"
title: "Buchvorstellung und Gespräch: Die Erfindung des Muslimischen Anderen"
start: 2022-03-25 19:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/1088697791983181/
image: 273721839_3080648932211302_2063189722773885943_n.jpg
isCrawled: true
---
// Beschreibung

In unserer Gesellschaft wird viel über „den“ Islam und „die“ Muslim:innen gesprochen. Oft werden dabei rassistische Bilder, Vor­stellungen und Denkweisen verwendet, die sich fest in das kulturelle Gedächtnis der Gesellschaft eingebrannt haben.

Im Vor­trag der Autor:innen Anna Sabel und Özcan Karadeniz soll nicht gezeigt werden, wie „Muslim:innen wirklich sind“, sondern er­klärt werden, wie rassistische Bilder pro­duziert und weitergetragen werden.