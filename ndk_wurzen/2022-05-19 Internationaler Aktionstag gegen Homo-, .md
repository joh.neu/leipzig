---
id: "310929114405063"
title: Internationaler Aktionstag gegen Homo-, Bi-, Trans*- und Inter*
  Feindlichkeit (IDAHIT*)
start: 2022-05-19 16:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5
link: https://www.facebook.com/events/310929114405063/
image: 273972834_3083536838589178_5694901940405775123_n.jpg
isCrawled: true
---
// Beschreibung

Mit einer Kundgebung auf dem Wurzener Marktplatz wollen wir im Rahmen des Internationalen Aktionstags die Sichtbarkeit von Geschlechtervielfalt stärken. Der diesjährige Schwerpunkt unserer Veranstaltung ist das Themenfeld queere (Familien-) Konstellationen. Am Abend wird im D5 außerdem ein Film gezeigt.