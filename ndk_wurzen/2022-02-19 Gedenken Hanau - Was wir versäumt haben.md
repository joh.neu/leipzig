---
id: "469076127924810"
title: "Gedenken: Hanau - Was wir versäumt haben"
start: 2022-02-19 13:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/469076127924810/
image: 271728565_3058251817784347_4491972794593673865_n.jpg
isCrawled: true
---
// Beschreibung

Vor zwei Jahren wurde in Hanau ein rassistisch motiviertes Attentat verübt und bis heute sind viele Fragen offen. Diese lassen nicht nur die Angehörigen, sondern auch uns daran zweifeln, ob echte Aufklärung wirklich gewollt wird. Deshalb müssen wir uns erinnern und Verantwortungsübernahme einfordern. 

Im Rahmen des Gedenktages wird es in Wurzen verschiedene Aktionen im öffentlichen Raum geben.