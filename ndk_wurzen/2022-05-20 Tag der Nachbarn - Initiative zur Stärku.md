---
id: "632176711175631"
title: Tag der Nachbarn - Initiative zur Stärkung des nachbarschaftlichen Miteinanders
start: 2022-05-20 15:30
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5
link: https://www.facebook.com/events/632176711175631/
image: 274031376_3083538535255675_6618163073653912394_n.jpg
isCrawled: true
---
// Beschreibung

Zum Tag der bundesweiten Initiative zur Stärkung des nachbarschaftlichen Miteinanders „Tag der Nachbarn“ wollen wir wieder gemeinsam mit anderen Akteur:innen in der Wurzener Innenstadt ein Zeichen setzen für gute Nachbarschaft und miteinander das Zusammenleben feiern. Das genaue Programm wird rechtzeitig bekannt gegeben.