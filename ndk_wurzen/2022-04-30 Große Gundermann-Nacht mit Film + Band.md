---
id: "1012454662699131"
title: Große Gundermann-Nacht mit Film + Band
start: 2022-04-30 18:00
locationName: Alte Leuchtenmanufaktur
link: https://www.facebook.com/events/1012454662699131/
image: 277227936_3112703639005831_6068551736324517749_n.jpg
isCrawled: true
---
// Beschreibung

Das Netzwerk für Demokratische Kultur e.V. (NDK) lädt für den 30. April, 18 Uhr zu einem einmaligen Konzert mit „Linda und die lauten Bräute“ und dem preisgekrönten Film „Gundermann“ (D2019, 127 min) ins Soziokulturelle Zentrum ALM – Alte Leuchtenmanufaktur ein.

Der Film startet 18 Uhr, das Konzert beginnt ca. 20.30 Uhr. Bitte voranmelden über 034 25 – 85 27 10 oder team@ndk-wurzen.de. Eintritt nachSelbsteinschätzung zwischen 6,- und 14,- Euro. Bitte informiert euch über die dann bestehenden Corona-Hygieneregeln. Getränke können vor Ort gekauft werden.

Die Veranstaltung findet in Kooperation mit dem Schweizerhaus Püchau e.V., dem Filmverband Sachsen e.V. und Kippelsteiner Filme statt und wird maßgeblich gefördert vom Kulturraum Leipziger Raum aus Steuermitteln auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

// Zum Film

Gundermann (D2019)
1992: Einige Jahre nach dem Mauerfall arbeitet Gerhard Gundermann (Alexander Scheer) immer noch im Tagebau in Hoyerswerda. Der Mittdreißiger möchte aber eine neue Band gründen und auf Tour gehen. Seine Texte über „einfache“ Menschen, Ausbeutung und Ökologie sprachen dem Publikum schon immer aus der Seele. Dennoch behielt der Musiker seinen Job als Baggerfahrer bei, um unabhängig vom Erfolg seiner Kunst zu sein. Doch die Vergangenheit holt ihn ein, als herauskommt, dass Gundermann ein Informant der Stasi war. Während immer mehr ans Licht kommt, wie viel er aus Liebe zum Land über seine Freunde verraten hat, zerbricht Gundermanns Bild von sich selbst. Parallel dazu beginnt Gundermanns Geschichte im Jahr 1975: Der Querdenker ist gerade aus dem Militär geschmissen worden und tritt mit der Werkband auf, deren Mitglied auch seine Jugendliebe Conny (Anna Unterberger) ist, die später mal seine Frau wird. (Regie: Andreas Dresen)

// Zur Band

Linda und die lauten Bräute - Musik aus dem Tagebau // ein Gundermann-Backup
Gundi Gundermann dichtete, während er einen gigantischen Bagger durch den Tagebau Spreetal lenkte. Die Tage der Kohle sind gezählt – die regenerativen Energien seiner Lieder werden bleiben. Sie graben tief, sie wärmen, sie entzünden. Eine Kumpelschaft von lauten Bräuten um Linda Gundermann trägt dieses Feuer in die nächste Generation und legt mit eigenen Kompositionen nach. Liedermachende und Lieder von Hasenscheiße, Schnaps im Silbersee, Axel Stiller, Lari und der Pausenmusik erklingen gemeinsam mit Gundis Werken – so wachsen Brücken zwischen gestern und heute, Ost und West, Poesie und Politik. Jung, mitreißend, schräg, geradlinig, emotional.