---
id: "459170915752492"
title: "Gedenkfahrt: Für die Opfer der Todesmärsche"
start: 2022-05-08 09:15
locationName: Heimatmuseum Borsdorf
address: Heimatmuseum Borsdorf
link: https://www.facebook.com/events/459170915752492/
image: 273669841_3080651972210998_284623009698578849_n.jpg
isCrawled: true
---
// Beschreibung

Am 8. Mai 2022 jährt sich das Ende des Zweiten Weltkrieges und der Nazidiktatur in Europa zum 77. Mal. . Noch in den letzten Kriegstagen versuchten SS und Gestapo die von ihnen begangenen Verbrechen durch neue Verbrechen zu verwischen. Konzentrationslager, Zwangsarbeitslager und Haftlager der verschiedensten Art in Leipzig und Umland wurden durch sie geräumt und tausende dem Tod entronnene Häftlinge auf lange, oft ziellose Märsche auch durch das Muldental gehetzt.

Viele der Gehetzten und Geschundenen überlebten diese Qualen nicht. Sie starben vor Hunger und Durst, an völliger Entkräftung und durch die Schüsse der SS-Wachen und wurden oftmals einfach am Straßenrand liegen gelassen. Diese letzten Verbrechen der Nazis, die so kurz vor der ersehnten Freiheit noch so vielen Menschen das Leben kosteten, kennen wir heute als die Todesmärsche.

Um die Erinnerung an die vielen Opfer, deren Qualen und deren Tod aufrecht zu erhalten und um ihrer zu gedenken, rufen wir alle Bürgerinnen und Bürger der Region dazu auf, gemeinsam am 8. Mai 2022 am 23. Gedenken für die Opfer der Todesmärsche teilzunehmen und damit ein Zeichen für Demokratie, Frieden und Menschlichkeit, gegen Neonazismus, Rassismus und Antisemitismus zu setzen.

Ablaufplan:

// Borsdorf

09:00 Uhr: Jugenhaus - Eintreffen der Teilnehmenden
09:15 Uhr: Leipziger Straße - Begrüßung und Eröffnung

// Gerichshain

09:40 Uhr: Parkplatz/ Gasthof - Begrüßung und Geleitprogramm durch den Chor der Ev.-Luth. Kirchgemeinde

// Machern

10:20 Uhr: Leipziger Str. 6 - Gedenken an Gedenktafel
10:30 Uhr: Parkplatz an der B6 - Geleitworte

// Bennewitz

11:00 Uhr: Friedhof  - Gedenken der polnischen Opfer durch die Gemeindeverwaltung

11:30 Uhr: Gedenkstein am Jugendhaus - Begrüßung und Geleitworte

// Wurzen

12:15 Uhr: Friedhof -  Gedenkveranstaltung

Ende ca. 12:45 Uhr: Dresdener Straße
	
Wichtig: Auch in diesem Jahr werden wir von Bordsorf mit dem Fahrrad nach Wurzen fahren.
