---
id: "1074559160127049"
title: Interkulturelles Frühlingsfest
start: 2022-04-06 16:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/1074559160127049/
image: 273950294_3083544741921721_3718285768063769404_n.jpg
isCrawled: true
---
Wir laden herzlich ein, zusammen den  Frühling zu begrüßen! Dafür haben wir verschiedene Mitmach-Angebote für Groß und Klein vorbereitet. So wird unter anderem das Kinder-Figurenstück „Der Vogel anderswo“ vom Theater der Jungen Weltaus Leipzig in deutscher und arabischer Sprache zu sehen sein. Für Getränke sowie einen Grundstock an Speisen ist gesorgt, und wir freuen uns über Ihre und Eure Beiträge zum bunten Büfett.