---
id: "3046389715621413"
title: "Öffentliche Lesung: Gedenken an den Tag der Bücherverbrennung"
start: 2022-05-10 15:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5
link: https://www.facebook.com/events/3046389715621413/
image: 273697891_3080652828877579_2434863491817265599_n.jpg
isCrawled: true
---
// Beschreibung

Am 10. Mai 1933 kam es in vielen deutschen Städten zu Bücherverbrennungen. Wer­ke, die die Nazis als „undeutsches Schrift­tum“ bezeichneten, wurden zu Scheiter­haufen auf­getürmt und verbrannt. 

Mit ei­ner öffent­lichen Lesung auf dem Wur­zener Markt wollen wir an die Betroffenen erin­nern und auf gegenwärtige Verbote von kri­tischen Büchern hinweisen, die in anderen Teilen der Welt stattfinden.