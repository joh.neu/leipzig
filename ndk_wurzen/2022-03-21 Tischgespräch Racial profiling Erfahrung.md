---
id: "1569382573417562"
title: "Tischgespräch: Racial profiling: Erfahrungen - Wirkung - Widerstand"
start: 2022-03-21 18:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/1569382573417562/
image: 273699713_3080645512211644_8891887863540301457_n.jpg
isCrawled: true
---
// Beschreibung 

Racial Profiling und rassistische Polizei­ge­walt sind nicht erst seit dem Tod von George Floyd 2020 auch in Deutschland ein großes Thema. Gegen die deutsche Polizei gibt es ebenfalls immer wieder Vor­würfe.

In der Veranstaltung erör­tern wir mit Expert:innen von copwatch Leipzig, was unter Racial Profiling zu verstehen ist, worauf dieses aufbaut und welche Verän­derungen nötig sind.