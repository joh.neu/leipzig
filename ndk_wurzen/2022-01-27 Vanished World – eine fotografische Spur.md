---
id: "2991707064415640"
title: Vanished World – eine fotografische Spurensuche jüdischen Lebens in Osteuropa
start: 2022-01-27 19:00
link: https://www.facebook.com/events/2991707064415640/
image: 271599977_3057674961175366_332805949816833681_n.jpg
isCrawled: true
---
// Beschreibung

Anlässlich des Gedenktages für die Opfer des Holocaust stellt Fotograf Christian Herrmann eine Auswahl seiner beeindruckenden Bilder aus der Ukraine, Belarus, Polen und Moldawien vor. Dort, zwischen Baltikum und Schwarzem Meer, lebte einst die Mehrheit der europäischen Jüdinnen und Juden.

Während des Zweiten Weltkriegs wurden sie von den deutschen Besatzern und ihren Helfer*innen nahezu vollständig ermordet. Geblieben sind die Spuren früheren jüdischen Lebens: zerstörte oder zweckentfremdete Synagogen, überwucherte Friedhöfe, Grabsteine im Straßenpflaster, Spuren von Haussegen an den Türpfosten.

Seit den 90ern reist Christian Herrmann immer wieder nach Osteuropa, um diese Orte und ihre Zerstörung an die Oberfläche zu holen, zu dokumentieren und sichtbar zu machen. Er wird uns anhand einer Auswahl von Fotografien einen Überblick über seine Erfahrungen und seine Arbeit geben, von seinen Beobachtungen erzählen und über Gespräche mit den Menschen vor Ort berichten. Wir freuen uns auf einen anregenden Austausch.

// Referent

Christian Herrmann (*1962) lebt in Köln und arbeitet für eine Non-Profit-Organisation in Bonn. Seit Jahren bereist er Osteuropa auf der Suche nach Spuren jüdischen Lebens. Aus seinen Fotografien entstehen Ausstellungen und Bücher. Seine Erlebnisse dokumentiert er außerdem im fortlaufenden Online-Archiv https://vanishedworld.blog/.

Ende 2020 erhielt er das Verdienstkreuz am Bande der Bundesrepublik Deutschland.

// Anmeldung

Anmelden können Sie sich über team[at]ndk-wurzen.de. Nach der Anmeldung erhalten Sie alle weiteren Informationen.