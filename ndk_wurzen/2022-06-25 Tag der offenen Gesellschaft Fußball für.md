---
id: "281226633960993"
title: Summer Bash - Vielfalt 2022
start: 2022-06-25 13:00
locationName: Station der Jugend - Lüptitzer Straße 24 04808 Wurzen
address: Domplatz 5
link: https://www.facebook.com/events/281226633960993/
image: 280749599_3153559331586928_8479199174104790651_n.jpg
isCrawled: true
---
Am 25.06. ist es soweit! Der SUMMER BASH - VIELFALT 2022 in Wurzen findet statt. Darauf könnt ihr euch freuen:

• Ab 13:00 Uhr gibt es ein Fußballturnier im Stadion der Jugend. Direkt daneben befindet sich der Wurzener Skatepark, der den ganzen Tag bespielt wird und bei dem ihr euch ausprobieren könnt. Also bringt eure Skates, BMX oder Skateboards mit! Für leckeres Essen - selbstverständlich auch in veganer Variante, kalte Getränke und gute Musik ist gesorgt.

• Ab 20:00 Uhr geht es dann im D5 (Domplatz 5) weiter. Im Kulturkeller könnt ihr euch auf das Astro Ritter Kollektiv freuen! Das Duo macht eine Mischung aus Elektro, Rap und Punk. Weiter gehts mit Aftershow mit Herr Dr. Demidov.

Also: Kommt vorbei, bringt eure Freundinnen und Freunde mit und lasst uns zusammen den Sommer feiern!