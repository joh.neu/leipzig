---
id: "3127229847603616"
title: "Gedenkveranstaltung: Gedenken an die Opfer der Todesmärsche 1945"
start: 2022-04-13 17:00
locationName: Freibad Dreibrücken
address: Leipziger Str. 1a, 04808 Wurzen
link: https://www.facebook.com/events/3127229847603616/
image: 272185027_3066176320325230_2404350404883621845_n.jpg
isCrawled: true
---
Gemeinsam wollen wir der Opfer der
Todes­märsche 1945 gedenken. Wir tref­fen uns am Gedenkstein an den Mulden­
wiesen (Dreibrückenbad) in Wurzen, um
mit Gedich­ten und Texten an die Opfer der
Ver­brechen des Naziregimes in den letzten
Tagen des Zweiten Weltkrieges zu erinnern.
Wir laden herzlich dazu ein, Blumen mitzu­bringen, die am Gedenkstein niedergelegt
werden können.