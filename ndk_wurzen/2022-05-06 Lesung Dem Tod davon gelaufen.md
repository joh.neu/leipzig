---
id: "470127837983667"
title: "Lesung: Dem Tod davon gelaufen"
start: 2022-05-06 16:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5
link: https://www.facebook.com/events/470127837983667/
image: 273625122_3080650625544466_4792788423191275687_n.jpg
isCrawled: true
---
// Beschreibung

„Wir wollen leben und wir werden es wagen, weil wir dieses wunderbare, freie, abenteu­erliche Leben zurückhaben wollen.“

Das Buch handelt von neun Frauen, die eine schreckliche Geschichte teilen und sich dennoch ihre Liebe zum Leben bewah­ren. Auf dem Todesmarsch 1945 fassen sie den Entschluss, einen Fluchtversuch zu wagen. 

Erzählt wird die Geschichte von der Französin Suzanne Maudet, die kurz nach dem Krieg ihre eigenen Erlebnisse aufschrieb. 2021 ist das Buch in deutscher Übersetzung von Ingrid Scherf erschienen, die wir als Autorin bei uns begrüßen dür­fen.