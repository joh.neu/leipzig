---
id: "4718260838291948"
title: NDK Sommerfest
start: 2022-06-11 16:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5
link: https://www.facebook.com/events/4718260838291948/
image: 273880678_3083541011922094_4516844865702046045_n.jpg
isCrawled: true
---
// Beschreibung

Das NDK lädt zum Sommerfest rund um das Kultur- und Bürger:innenzentrum D5 ein. Auf dem Domplatz, im Vorgarten, auf der Terrasse und im Haus erwartet euch ein buntes Programm für Kinder und Erwachsene. Es wird geschminkt, gemalt, gebastelt, gebaut. Es gibt Livemusik, Siebdruck, Essen und Trinken und einiges mehr. Lasst euch überraschen!