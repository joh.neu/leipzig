---
id: "991343738478310"
title: "Film: Kabul Kinderheim (Shahrbanoo Sadat, 2019)"
start: 2022-03-03 19:00
locationName: Netzwerk für Demokratische Kultur
address: Domplatz 5, 04808 Wurzen
link: https://www.facebook.com/events/991343738478310/
image: 273624302_3080632755546253_971052277897226787_n.jpg
isCrawled: true
---
// Beschreibung

Der 15-jährige Quodrat lebt auf den Stra­ßen Kabuls, ohne Familie und ohne Dach über dem Kopf. Nachdem er eines Tages dabei er­wischt wird, wie er über­teuerte Tickets für Blockbusterfilme auf dem Schwarzmarkt verkauft, erwartet ihn ein neues Leben in einem von den ­Sowjets betriebenen Auf­fangheim für elternlose Kinder.

Dort ver­bündet er sich mit anderen Teenagern. Vor dem Hintergrund der politischen Ereignis­se im Afghanistan der 80er Jahre erzählt diese Geschichte übers Erwachsenwerden zärtlich und humorvoll von Freundschaft und Solidarität.