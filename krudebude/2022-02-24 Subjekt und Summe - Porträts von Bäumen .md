---
id: "602843434335732"
title: Subjekt und Summe - Porträts von Bäumen und Menschen
start: 2022-02-24 18:00
end: 2022-03-06 18:00
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13, 04347 Leipzig
link: https://www.facebook.com/events/602843434335732/
image: 271876746_4871350899623887_2702112347709516167_n.jpg
isCrawled: true
---
Subjekt und Summe - Porträts von Bäumen und Menschen

24.02.-06.03. 
Öffnungszeiten: 
Do-Sa 17-20h , So 15-18h

// VERNISSAGE 24.02. ab 18h //

Die Anzahl an Bäumen auf der Welt übersteigt die der Menschen bei Weitem. Dennoch ist es der Mensch, der mit Gewalt über den Lebensraum Erde und seine Bevölkerung entscheidet.
Weite Teile des Planeten sind der Natur überlassen, während diese sich an anderen Orten der fortschreitenden Zivilisation fügen muss. 
Dabei ist bekannt, wie wichtig die Wälder für unser eigenes Überleben sind. Der Mensch braucht den Wald zur Selbsterhaltung - der Wald braucht die Gnade des Menschen, um weiter bestehen zu können. Wie bewusst sind wir uns dieser gegenseitigen Abhängigkeit?

Von besonderer Dringlichkeit ist die Beziehung zwischen Mensch und Wald auf einer Halbinsel an der Ostseeküste vor Litauen bzw. Kaliningrad geprägt. Die sogenannte Kurische Nehrung ist an den meisten Stellen nur 2 bis 3 km breit, wodurch alle Lebewesen dort ständig Wind und Meer ausgesetzt sind. Während der Ort im Sommer als schönstes Ferienziel Litauens gilt, beschränkt man sich im Winter auf das Wesentliche.

Der Fotograf Max Schaible (*1998) setzt in einer stichprobenartigen Porträtreihe Baum und Mensch in Austausch – auf Augenhöhe. Von den individuellen Betrachtungen (SW-/Farbfilm) geht eine erfrischende und unschuldige Leichtigkeit aus. Die Bewohner:innen zeigen Respekt vor der Natur. Entwickeln sich Zivilisation und Wald mit- oder gegeneinander? Wieviel Kontrolle darf sich der Mensch leisten, wieviel Kontrolle ist für den Erhalt erforderlich?

Neben den Fotografien von Max Schaible ist der Film Acid Forest der litauischen Filmemacherin Rugilė Barzdžiukaitė zu sehen, der ebenfalls auf der Kurischen Nehrung gedreht wurde.


// RAHMENPROGRAMM //

04.03. ab 14 Uhr

UNDER THE SURFACE_REFRESH EVENT 


Das Projekt wird vom Kulturamt der Stadt Leipzig gefördert.

Die Ausstellung findet unter Einhaltung der zu diesem Zeitpunkt geltenden Infektionsschutzregelungen statt. 