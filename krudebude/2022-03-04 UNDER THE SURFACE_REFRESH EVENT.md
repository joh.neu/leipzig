---
id: "422687039605166"
title: UNDER THE SURFACE_REFRESH EVENT
start: 2022-03-04 14:30
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13, 04347 Leipzig
link: https://www.facebook.com/events/422687039605166/
image: 272694629_4926704420755201_6222233028954480408_n.jpg
isCrawled: true
---
Im Rahmen der Ausstellung Subjekt und Summe findet die Veranstaltung UnderTheSurface_Refresh Event bei uns statt. Das tschechisch-deutsche Projekt beschäftigt sich mit dem Thema Braunkohlenbergbau und ihrer natürlichen und gesellschaftlichen Folgelandschaft. 

24.02. // Eröffnung der Installation SOL(i)D von Kateřina Szymanski

04.03. // 14:30 - 15:30 Workshop Verbindung durch den KörperGeist 
16:00 Diskussion

Installation SOL(i)D

Wie sehen die Machtverhältnisse aus, wenn ein Unternehmen in die Umwelt eingreifen will? Wessen Selbsterhaltungstrieb ist hier relevanter für die Gesellschaft, die des Unternehmens oder der Natur? Wie gehen wir mit dieser Ungleichheit der Kräfte um, und auf welche Hindernisse stoßen wir ständig, die eine offene und gesunde Diskussion verhindern? 

Die Installation SOL(i)D von Kateřina Szymanski spiegelt den sanften und sensiblen Tätigkeitsdrang von Julius Kraft - als Naturwissenschaftler, Umweltschützer und vor allem als Individuum - wieder. Als Einzelperson hat man gegenüber einem großen Investor kaum Chancen, in einen Dialog zu treten und ein sensibles und respektvolles Landschaftsmanagement zu erreichen. Krafts Ziel ist es, die Natur einfach Natur sein zu lassen. Er streift durch das Gelände auf der Suche nach gefährdeten Tier- und Pflanzenarten, um sie dann in Datenbanken gefährdeter Arten einzutragen. Der Investor ist dadurch gezwungen, auf diese Tatsache zu reagieren. So werden umweltzerstörende Handlungen zumindest verlangsamt und kompliziert. 
Können aber dadurch die Pläne, die lokale Lebensräume gefährden, wirklich gestoppt werden? 

Verbindung durch den KörperGeist - Workshop mit Natascha Noack

Wenn Perspektiven, Motive und Werte unvereinbar erscheinen, wird es zu einer großen Herausforderung, sich respektvoll zu begegnen (oder überhaupt zu verbinden). Die Verbindung ganz aufzugeben, war jedoch immer nur für diejenigen eine Option, die von Gewalt und Grabenkämpfen profitieren. Was also tun? Wir wollen Verbindung über den Körper erforschen (Verbindung zu uns selbst, zu einem Partner, zu einer Gruppe und zu dem, was uns umgibt), um neue Wege zu entdecken und alternative Strategien entwickeln zu können...

Die Installation wird von einer Diskussion mit Vertretern aus Wissenschaft und Kunst, Kateřina Szymanski(CZ), Kristyna Svobodova(CZ/DE) und Natascha Noack(DE) begleitet. Sie sprechen darüber, wie unterschiedlichen Disziplinen eine gemeinsame Sprache gegen die Zerstörung finden können, um ihre Anliegen der Öffentlichkeit zu vermitteln.

// Aufgrund der aktuellen Corona-Verordnung findet der Workshop und die Diskussion voraussichtlich unter freiem Himmel in unserem Hinterhof bei Tee und Lagerfeuer statt. //

Das Projekt wird vom Deutsch-Tschechischer Zukunftsfonds unterstützt, Studio ALTA und Bazaar Festival. 