---
id: "480735373705975"
title: Come Live With Me | Malina Vaagt
start: 2022-03-25 17:00
end: 2022-03-25 20:00
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13, 04347 Leipzig
link: https://www.facebook.com/events/480735373705975/
image: 273395894_4980304225395220_1170479508272781847_n.jpg
isCrawled: true
---
Come Live With Me | Rauminstallation von Malina Vaagt 

24.03. - 10.04.22

Es ist der Versuch eines Entwurfs, einer Idee und der darauffolgende Abgesang desselben. Die Ekstase an der Spracherzeugung und das Verzweifeln an der Sprachgestalt. Der idealisierte Zustand entpuppt sich im Moment seiner Realisierung als leeres Versprechen. 

Malina Vaagts immersive Rauminstallation lädt dazu ein, sich in diese Verdichtung zu begeben, sich in einen Raum aus Zuständen zu legen. Auf eindringliche Weise erzählt COME LIVE WITH ME von der Utopie der Liebe und dessen Scheitern. 

Malina Vaagt (geb. 1990) studierte bildende Kunst in Wien (Akademie der bildenden Künste) und London (CCW Foundation Course, UAL). Seit zwei Jahren lebt und arbeitet sie als freie Künstlerin in Leipzig. 
In ihren Arbeiten thematisiert sie Empfindsamkeit und Verletzlichkeit und unternimmt den Versuch sich in einer fragmentierten Welt zu verorten. Oftmals steht eine gewisse Sprachlosigkeit im Zentrum der Arbeiten, welche sie versucht zu manifestieren und greifbar zu machen. In ihren Rauminstallationen arbeitet die Künstlerin mit Leerstellen und Bezügen zwischen den Objekten, die sie wie Zeichen zu einem Satz zusammenfügt. 

Sound: Max Müller & Malina Vaagt


Öffnungszeiten:

Vernissage 24.03. ab 18h 
Do-Sa: 17-20 Uhr
So: 15-18 Uhr

Es gilt die 2G-Regel + FFP2-Maskenpflicht.

Die Ausstellung wird gefördert durch das Kulturamt Leipzig.
