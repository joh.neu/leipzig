---
id: "391708456147065"
title: Subtiles Korsett - Wie beeinflusst meine Sozialisierung von damals meinen
  Sex von heute?
start: 2022-04-28 16:00
end: 2022-04-28 20:00
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13
link: https://www.facebook.com/events/391708452813732/
image: 278885929_5164105803681727_4359648163591868184_n.jpg
isCrawled: true
---
Laufzeit 21.04. - 01.05.2022
 
Vernissage 21.04. ab 18 Uhr

Do & Fr 16-20 Uhr
Sa 12 - 22 Uhr
So 12-20 Uhr

Performancezeiten siehe unten​​

Künstler*innen: Sophie Borchert, Miriam Bovenschen, Nicolai Buchner, Anna Herbert, Lisa Kanter, Hannah Kohn, Clara Mylog, Romina Raabe, Kristina Rothe

Kuration: Clara Mylog

Sexuelle Freiheit – was bedeutet das für mich? Wie frei fühle ich mich wirklich? Wage ich, mich explizit ultra-non-sexy oder super-duper-aufreizend zu geben ohne Angst vor negativen Bewertungen zu haben? Wie beeinflusst meine Sozialisierung von damals meinen Sex von heute? Mach' ich mir Gedanken, wie ich beim Sex wirke? Ob stark, lässig, lustvoll, leidenschaftlich, ...? Ist er somit wirklich frei von meinem kritischen Blick auf mich selbst?

Es geht in die zweite Runde! 9 Künstler*innen haben sich erneut mit diesen und vielen weiteren Fragen künstlerisch und performativ auseinandergesetzt. Diese Ausstellung lädt zur Reflexion über die eigene Sexualität und den Bedingungen ihrer (Un-)Freiheit ein. Inwieweit können wir unsere (sexuellen) Bedürfnisse kommunizieren, ohne dass uns die eigene Sozialisierung permanent Grenzen setzt? Die Ausstellung soll dazu anregen, sowohl Tabuisierungen und Schubladendenken aus dem Weg zu räumen, als auch genormte Geschlechterbilder, vermeintliche Schönheitsideale aus Werbung, Filmen und Pornografie und unser vielleicht verzerrtes Selbstbild zu hinterfragen. Denn auch wenn wir möglicherweise denken, dass wir diese Steine beseitigt und das patriarchale Korsett hinter uns gelassen haben, was kommt da vielleicht auf subtile Weise zum Vorschein, wenn wir die Oberfläche aufzukratzen wagen?

​Tanzperformance: Miriam Bovenschen
Do. 21.4. 19:30 Uhr
Fr. 22.4. 18 Uhr
Sa. 23.4. 16 Uhr
Fr. 29.4. 18 Uhr
Sa. 30.4. 18:30 Uhr
So. 1.5. 14 Uhr

Installative Performance: Romina Raabe
Do 28.4. ab 18 Uhr
Fr 29.4. ab 18:15 Uhr
Sa 30.4. ab 18:45 Uhr
So 1.5. ab 15 Uhr

Performance: Lisa Kanter & Andere
Do 21.4. 20-22 Uhr
So 23.4. 13-15 Uhr
Do 28.4. 17-19 Uhr
Sa 30.4. 16-18 Uhr

Die Ausstellung wird gefördert durch das Kulturamt Leipzig und aus Mitteln des Sächsischen Staatsministeriums für Soziales und Gesellschaftlichen Zusammenhalt.