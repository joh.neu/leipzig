---
id: "412279077409262"
title: Gruppenausstellung | Utopie in der Parzelle
start: 2022-05-22 15:00
end: 2022-05-22 18:00
locationName: Projektwohnung "krudebude"
address: Stannebeinplatz 13
link: https://www.facebook.com/events/412279084075928/
image: 280568013_5219668891458751_3110292967635559806_n.jpg
isCrawled: true
---
// ​English version below //

Utopie in der Parzelle | Gruppenausstellung
13.05. - 29.05.22

Beteiligte Künstler*innen:
Nick Harde, Fabian Heublein, Elena Kirchhoff, Bea Nielsen, Anne Pruy, Susanne Ruccius, Yoav Schutzer, Michael Seefeldt, David Tiefenthaler

Vernissage | 13.05. ab 18 Uhr 
Öffnungszeiten | Do-Sa 17-20 Uhr, So 15-18 Uhr


Die von der krudebude kuratierte Gruppenausstellung fragt nach dem Kleingarten als Sehnsuchtsort des Stadtmenschen: Welche Utopien werden in den Parzellen ausgelebt? Welche Vorstellungen von Zusammenleben stecken dahinter? In welchem Verhältnis stehen Parzellen zur Stadt? Welche Vorschläge bieten sie uns für eine nachhaltige, gerechtere Zukunft? 

​Neun Künstler*innen zeigen in der Projektwohnung Arbeiten in Skulptur, Fotografie, Performance und Installation, die aus der Auseinandersetzung mit diesen Fragen entstanden sind. 


Begleitprogramm:
15.05. 17 Uhr | Lesung “Fallschirmseide” von Deborah Jeromin
21.05. 10-22 Uhr | Aktions- und Begegnungstag im KGV An der Parthe

Dieses Projekt wird gefördert aus Mitteln des Verfügungsfonds WEP Aufwertungsgebiet Schönefeld, aus Mitteln des Sächsischen Staatsministeriums für Soziales und Gesellschaftlichen Zusammenhalt, durch die Leipzigstiftung und das Kulturamt Leipzig.

Grafik: Manja Schönerstedt | ahoibuero
_____________

Utopie in der Parzelle | Group exhibition
13.05. - 29.05.22

Artists:
Nick Harde, Fabian Heublein, Elena Kirchhoff, Bea Nielsen, Anne Pruy, Susanne Ruccius, Yoav Schutzer, Michael Seefeldt, David Tiefenthaler

Vernissage | 13.05. 6pm
Opening hours | Thursday-Saturday 5-8pm, Sunday 3-6pm


The group exhibition curated by krudebude asks about the allotment garden as a place of longing for urban people: Which utopias are lived out in the plots? What ideas of living together are behind them? What is the relationship between allotments and the city? What proposals do they offer us for a sustainable, more just future?

Nine artists will show works in sculpture, photography, performance and installation in the project apartment, which have emerged from the examination of these questions.


Accompanying program:
15.05. 5pm | Reading “Fallschirmseide” by Deborah Jeromin, 
21.05. 10am-10pm | Action and Encounter Day at KGV An der Parthe

Graphic design: Manja Schönerstedt | ahoibuero
