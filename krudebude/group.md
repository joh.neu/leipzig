---
name: Projektwohnung "krudebude"
website: https://www.krudebude.de
email: kontakt@krudebude.de
scrape:
  source: facebook
  options:
    page_id: Projektwohnungkrudebude
---
Im Herbst 2013 gründete sich die Initiative Projektwohnung „krudebude“ mit dem Ziel, eine Dreiraumwohnung im Leipziger Nordosten als Off-Space kulturell neu zu beleben und einen lebendigen Ort der Kommunikation und Begegnung zu schaffen.

Die Projektwohnung „krudebude“ e.V. versteht sich als ein dynamisches Projekt, bei dem die künstlerische Produktion und die offene Begegnung und Kommunikation zwischen den Kunst- und Kulturschaffenden und ihren Rezipienten im Mittelpunkt stehen. Kunst und Kultur soll unabhängig vom sozio-ökonomischen Hintergrund erfahrbar gemacht und Partizipationsmöglichkeiten geschaffen werden.