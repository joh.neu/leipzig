---
name: Kleine Beete
website: https://kleinebeete.de
email: garten@kleinebeete.de
scrape:
  source: facebook
  options:
    page_id: 365089857279485
---
Die Kleine Beete ist ein Gartenprojekt nach dem Prinzip der solidarischen Landwirtschaft, im Nord-Osten von Leipzig. Gemüse verschiedener Arten, Blumen und Kräuter finden auf diesem Stück Land einen Platz zum Blühen und Gedeihen. Die biologische und schonende Bewirtschaftung dieses Fleckchen Erde ist dabei ein Grundprinzip.
