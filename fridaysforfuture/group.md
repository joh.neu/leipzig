---
name: Fridays For Future
website: https://fridaysforfuture.de/
scrape:
  source: facebook
  options:
    page_id: 707644592969681
---
Schulstreik für's Klima.

Europaweit und jetzt auch in Leipzig.
