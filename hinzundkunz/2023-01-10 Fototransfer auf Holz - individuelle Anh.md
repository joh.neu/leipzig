---
id: 6312-1673355600-1673364600
title: Fototransfer auf Holz - individuelle Anhänger
start: 2023-01-10 13:00
end: 2023-01-10 15:30
address: Treffpunkt Schönefeld,, Waldbaurstraße 9, Leipzig, 04347
link: https://kunzstoffe.de/event/fototransfer-auf-holz-individuelle-anhaenger/
teaser: "Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns
  in Schönefeld zum gemeinsamen "
isCrawled: true
---
Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns in Schönefeld zum gemeinsamen Basteln und Werkeln 



10.02.2022 Fototransfer auf Holz – individuelle Anhänger

17.02.2022 Korkuntersetzer

24.02.2022 aus alten T-Shirts werden Beutel



——————————————————————————————————————————————————————— 

Liebe Leute, 

für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 

Wir wünschen euch viel Spaß. Euer kunZstoffe-Team 

 