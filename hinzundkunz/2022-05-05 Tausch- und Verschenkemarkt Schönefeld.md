---
id: 5669-1651752000-1651759200
title: Tausch- und Verschenke-Treff Schönefeld
start: 2022-05-05 12:00
end: 2022-05-05 14:00
address: Treffpunkt Schönefeld,, Waldbaurstraße 9, Leipzig, 04347
link: https://kunzstoffe.de/event/tausch-und-verschenkemarkt-schoenefeld-6/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Ob ausgelesene Bücher, längst vergessene Schallplatten im Regal, Cd’s und Kasetten die man nicht mehr hört, ungenutzte Haushaltswaren, Gesellschaftspiele…bitte nicht wegwerfen! 

Jemand anderes freut sich noch darüber, muss nichts Neues kaufen, kann deine Dinge weiter nutzen und wertschätzen. Ressourcenschutz und nachhaltiges Handeln sind heutzutage unumgänglich und geht uns alle an. 

Beim Tausch- und Verschenketreff  bekommen deine alten Schätze eine neue Chance. Kommt gerne mit euren ausgedienten Kostbarkeiten zu unseren Terminen Donnerstags 

 03.02.2022 (12-14 Uhr) | 17.02.2022 (15-17 Uhr) | 03.03.2022 (12-14 Uhr) | 17.03.2022 (15-17 Uhr) | 07.04.2022 (12-14 Uhr) | 21.04.2022 (15-17 Uhr) | 05.05.2022 (12-14 Uhr) | 19.05.2022 (15-17 Uhr) | 02.06.2022 (12-14 Uhr) | 16.06.2022 (15-17 Uhr) 

Treffpunkt Schönefeld, Waldbaurstraße 9, Leipzig 

