---
id: 6104-1661990400-1667260799
title: HuUpA! 2022 | Handwerk- und Upcycling-Akademie
start: 2022-09-01 00:00
end: 2022-11-01 00:00
link: https://kunzstoffe.de/event/huupa-2022-handwerk-und-upcycling-akademie/
teaser: Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück.
  Dieses Jahr ist die HuUpA! ein ge
isCrawled: true
---
Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück. Dieses Jahr ist die HuUpA! ein gemeinsames Projekt mit dem GRASSI Museum für Angewandte Kunst. An unterschiedlichen Orten veranstalten wir eine Vielzahl an Kursen zu den Themen Handwerk, Upcycling und Selbermachen. Mit dabei sind wieder bekannte und beliebte aber auch neue Themen sowie Handwerker:innen und kreative Menschen aus der Nachbarschaft und der Stadt Leipzig. Außerdem können Kurse auch von Gebärdensprachdolmetscher:innen begleitet werden. Du hast die Qual der Wahl zwischen mehrtägigen Kursen und einmaligen Veranstaltungen. Finde deinen Kurs und melde dich an!! 

Termine findet ihr hier im Veranstaltungskalender oder auf der HuUpA! Webseite. Außerdem könnt ihr euch hier über die Kursinhalten, Anmeldung sowie Teilnahmebetrag informieren.. 

