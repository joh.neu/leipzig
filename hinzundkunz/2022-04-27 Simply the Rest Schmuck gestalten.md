---
id: 5750-1651071600-1651082400
title: "Simply the Rest: Schmuck gestalten"
start: 2022-04-27 15:00
end: 2022-04-27 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/5750/
teaser:   Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de
  oder *telefonisch/ SMS unter 0
isCrawled: true
---
  

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Heute werden aus verschiedene Materialien wie Fahrradschlauch, Perlen, Tetra Pak … Ohrringe, Armbänder, Ketten, Schlüsselanhänger … selbst entworfen und hergestellt. 

