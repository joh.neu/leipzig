---
id: 5755-1647442800-1647453600
title: Korb aus Fahrradschlauch knüpfen
start: 2022-03-16 15:00
end: 2022-03-16 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/korb-aus-fahrradschlauch-knuepfen/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Aus Fahrradschlauch Ringen können wir kleine Körbchen knüpfen. Bitte Geduld mitbringen. 

   

