---
id: 6272-1670605200-1670612400
title: Weihnachtsbaumschmuck DIY
start: 2022-12-09 17:00
end: 2022-12-09 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/weihnachtsbaumschmuck-diy/
teaser: "… aus Allerlei querbeet durch die Materialsammlung   Anmeldung
  erwünscht: unter workshop@kunzstoff"
isCrawled: true
---
… aus Allerlei querbeet durch die Materialsammlung 

  

Anmeldung erwünscht: unter workshop@kunzstoffe.de oder im Laden (Kontaktdaten nicht vergessen!) 

————————————————————————————————————————————————————- 

Liebe Leute, 

für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 

Wir wünschen euch viel Spaß. Euer kunZstoffe-Team