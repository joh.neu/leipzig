---
id: 6221-1671436800-1671469200
title: UKW - UpcyclingKinderWerkstatt
start: 2022-12-19 08:00
end: 2022-12-19 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcyclingkinderwerkstatt-4/
teaser: In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen
  kunterbunten Materialien experim
isCrawled: true
---
In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:30 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 

!!Bitte meldet euch an. Dazu reicht auch eine kurze Mail an workshop@kunzstoffe oder eine spontane SMS an 0163 4846916.!!