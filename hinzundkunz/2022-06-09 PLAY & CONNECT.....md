---
id: 5910-1654783200-1654794000
title: PLAY & CONNECT....
start: 2022-06-09 14:00
end: 2022-06-09 17:00
address: Theater der Jungen Welt, Lindenauer Markt 21, Leipzig, 04177
link: https://kunzstoffe.de/event/play-connect/
teaser: … ist ein Café für Familien, Kinder und Alle im Theater der jungen Welt
  und richtet sich an Geflücht
isCrawled: true
---
… ist ein Café für Familien, Kinder und Alle im Theater der jungen Welt und richtet sich an Geflüchtete und ihre Familien, die neu oder schon länger in Leipzig sind.  Es werden neben Spiel- und Bastelmöglichkeiten auch kleine interaktive Theateraktionen für Kinder und Jugendliche angeboten. Erwachsene haben die Möglichkeit mit Leipziger*innen ins Gespräch zu kommen und sich zu vernetzen und auszutauschen. Im Theatercafé PAN gibt es kostenfrei Kaffee, Saft und Kuchen. Regelmäßig werden Initiativen vor Ort sein, die Sozialberatung, Hilfe bei der Wohnungssuche oder ähnliche Unterstützung anbieten. 

Heute sind wir vor Ort und gestalten mit euch Girlanden, Mobile und | oder Wimpelketten aus Papier. 

