---
id: 5835-1649498400-1649516400
title: Nähtreff - Herzenssache Nähen für Sternchen und Frühchen e.V.
start: 2022-04-09 10:00
end: 2022-04-09 15:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/naehtreff-herzenssache-naehen-fuer-sternchen-und-fruehchen-e-v/
teaser: "Schön, dass du dich für den Nähtreff interessierst. Für weitere Info’s
  bitte HIER entlang. "
isCrawled: true
---
Schön, dass du dich für den Nähtreff interessierst. Für weitere Info’s bitte HIER entlang. 

