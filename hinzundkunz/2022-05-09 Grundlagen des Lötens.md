---
id: 5882-1652116500-1652121900
title: Grundlagen des Lötens
start: 2022-05-09 17:15
end: 2022-05-09 18:45
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/grundlagen-des-loetens/
teaser: "In dem Workshop zeigen wir, worauf es im Umgang mit einem Lötgerät
  ankommt: Die Teilnehmer*innen bek"
isCrawled: true
---
In dem Workshop zeigen wir, worauf es im Umgang mit einem Lötgerät ankommt: Die Teilnehmer*innen bekommen viele interessante Infos und erlernen die Handhabung von Lötkolben, Lötzinn und Flußmittel um Elektrobauteile aus Platinen auszubauen und Drähte zusammen zu löten. Kabel zum Reparieren können gern mitgebracht werden. 

 

