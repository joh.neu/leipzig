---
id: 5907-1655910000-1655920800
title: Wenn aus Phantasie Realität wird - Wunschstofftiere nähen
start: 2022-06-22 15:00
end: 2022-06-22 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/wunschstofftiere-naehen/
teaser: Wenn aus Phantasie Realität wird. Du wolltest schon immer einmal mit
  deinen gemalten Figuren kusche
isCrawled: true
---
Wenn aus Phantasie Realität wird. 

Du wolltest schon immer einmal mit deinen gemalten Figuren kuscheln? Dann komm mit deinen Zeichnungen und deinen Eltern zu uns in den Laden und wir helfen euch, aus dem Entwurf ein richtiges Stofftier zu machen. 

Eure Anmeldung ist auf jeden Fall erforderlich!! Anmelden kannst du dich unter workshop@kunzstoffe.de oder 0163 4846916  

