---
id: 5745-1652281200-1652281200
title: Gürteltaschen nähen
start: 2022-05-11 15:00
end: 2022-05-11 15:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/guerteltaschen-naehen/
teaser: "Wichtig!! Bitte meldet euch an *per Mail an
  workshop@kunZstoffe.de  oder  *telefonisch/ SMS unter "
isCrawled: true
---
Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de  

oder  

*telefonisch/ SMS unter 0163 4846916 

Heute könnt ihr mit uns aus bunten Stoffen eine eigene Gürteltasche entwerfen und nähen. 

