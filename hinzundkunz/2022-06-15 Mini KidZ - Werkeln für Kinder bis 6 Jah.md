---
id: 5828-1655307000-1655314200
title: Mini KidZ - Werkeln für Kinder bis 6 Jahre
start: 2022-06-15 15:30
end: 2022-06-15 17:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/mini-kidz-werkeln-fuer-kinder-bis-6-jahre-3/
teaser: Immer am dritten Mittwoch im Monat öffnen wir unsere Werkstatt für
  Eltern mit Ihren Kinder bis 6 Jah
isCrawled: true
---
Immer am dritten Mittwoch im Monat öffnen wir unsere Werkstatt für Eltern mit Ihren Kinder bis 6 Jahren um sich einfach mal auszuprobieren. 

Unsere Termine: 

15.06.2022 … Völlig von der Rolle – Wir basteln aus Klopapier Rollen Autos, Tiere, fliegender Fisch 

20.07.2022 | 17.08.2022 | 21.09.2022 

  

Ihr sucht auch ein Angebot für ältere Kinder? Dann schaut doch einmal bei unserer UKW – Upcycling-Kinder-Werkstatt vorbei. 

