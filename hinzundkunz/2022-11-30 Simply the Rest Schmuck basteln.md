---
id: 6229-1669824000-1669831200
title: "Simply the Rest: Schmuck basteln"
start: 2022-11-30 16:00
end: 2022-11-30 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/simply-the-rest-schmuck-basteln/
teaser: Ohrringe, Kette, Armband, Anhänger aus Restmaterialien kreieren.
isCrawled: true
---
Ohrringe, Kette, Armband, Anhänger aus Restmaterialien kreieren.