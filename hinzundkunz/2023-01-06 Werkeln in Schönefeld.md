---
id: 6302-1673010000-1673019000
title: Werkeln in Schönefeld
start: 2023-01-06 13:00
end: 2023-01-06 15:30
address: Treffpunkt Schönefeld,, Waldbaurstraße 9, Leipzig, 04347
link: https://kunzstoffe.de/event/werkeln-in-schoenefeld/
teaser: "Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns
  in Schönefeld zum gemeinsamen "
isCrawled: true
---
Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns in Schönefeld zum gemeinsamen Basteln und Werkeln 



06.01.2022 findet statt (Projekt noch in der Planung)

13.01.2022 findet statt (Projekt noch in der Planung)

20.01.2022 findet statt (Projekt noch in der Planung)

27.01.2022 Lavendeldruck -Aufnäher selbst gemacht

03.02.2022 Schmuckbäumchen aus Draht

10.02.2022 Fototransfer auf Holz – individuelle Anhänger

17.02.2022 Korkuntersetzer

24.02.2022 aus alten T-Shirts werden Beutel



——————————————————————————————————————————————————————— 

Liebe Leute, 

für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 

Wir wünschen euch viel Spaß. Euer kunZstoffe-Team 

 