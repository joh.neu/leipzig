---
id: 6083-1660581000-1660588200
title: UKW - Upcycling-Kinder-Werkstatt
start: 2022-08-15 16:30
end: 2022-08-15 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcycling-kinder-werkstatt-10/
teaser: In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen
  kunterbunten Materialien experim
isCrawled: true
---
In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:30 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 

!!Bitte meldet euch an. Dazu reicht auch eine kurze Mail an workshop@kunzstoffe oder eine spontane SMS an 0163 4846916.!! 

