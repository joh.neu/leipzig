---
id: 6296-1669986000-1669995000
title: Zauberkugeln im Einwegglas
start: 2022-12-02 13:00
end: 2022-12-02 15:30
address: Treffpunkt Schönefeld,, Waldbaurstraße 9, Leipzig, 04347
link: https://kunzstoffe.de/event/zauberkugeln-im-einwegglas/
teaser: "Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns
  in Schönefeld zum gemeinsamen "
isCrawled: true
---
Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns in Schönefeld zum gemeinsamen Basteln und Werkeln 



02.12.2022 Zauberkugeln im Einwegglas

09.12.2022 Tannenbäumchen selbst gemacht

16.12.2022 Silvesterdeko – bunte Girlanden

06.01.2022 findet statt (Projekt noch in der Planung)

13.01.2022 findet statt (Projekt noch in der Planung)

20.01.2022 findet statt (Projekt noch in der Planung)

27.01.2022 Lavendeldruck -Aufnäher selbst gemacht

03.02.2022 Schmuckbäumchen aus Draht

10.02.2022 Fototransfer auf Holz – individuelle Anhänger

17.02.2022 Korkuntersetzer

24.02.2022 aus alten T-Shirts werden Beutel



——————————————————————————————————————————————————————— 

Liebe Leute, 

für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 

Wir wünschen euch viel Spaß. Euer kunZstoffe-Team 

 