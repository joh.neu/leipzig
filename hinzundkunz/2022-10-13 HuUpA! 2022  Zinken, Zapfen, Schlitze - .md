---
id: 6140-1665676800-1665676800
title: HuUpA! 2022 | Zinken, Zapfen, Schlitze -  Klassische Holzverbindungen
start: 2022-10-13 16:00
end: 2022-10-13 16:00
address: GRASSI Museum für Angewandte Kunst, Johannisplatz 5–11, Leipzig, 04103
link: https://kunzstoffe.de/event/huupa-2022-zinken-zapfen-schlitze-klassische-holzverbindungen/
teaser: Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück.
  Dieses Jahr ist die HuUpA! ein ge
isCrawled: true
---
Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück. Dieses Jahr ist die HuUpA! ein gemeinsames Projekt mit dem GRASSI Museum für Angewandte Kunst. An unterschiedlichen Orten veranstalten wir eine Vielzahl an Kursen zu den Themen Handwerk, Upcycling und Selbermachen. Mit dabei sind wieder bekannte und beliebte aber auch neue Themen sowie Handwerker:innen und kreative Menschen aus der Nachbarschaft und der Stadt Leipzig. Außerdem können Kurse auch von Gebärdensprachdolmetscher:innen begleitet werden. Du hast die Qual der Wahl zwischen mehrtägigen Kursen und einmaligen Veranstaltungen. Finde deinen Kurs und melde dich an!! 

Zu den Terminen und Informationen zu den Kursinhalten, Anmeldung sowie Teilnahmebetrag geht es hier entlang. 

