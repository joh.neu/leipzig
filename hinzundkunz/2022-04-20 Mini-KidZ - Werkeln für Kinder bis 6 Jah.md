---
id: 5822-1650468600-1650475800
title: Mini-KidZ - Werkeln für Kinder bis 6 Jahre
start: 2022-04-20 15:30
end: 2022-04-20 17:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/mini-kidz-werkeln-fuer-kinder-bis-6-jahre/
teaser: Immer am dritten Mittwoch im Monat öffnen wir unsere Werkstatt für
  Eltern und Kinder bis 6 Jahren um
isCrawled: true
---
Immer am dritten Mittwoch im Monat öffnen wir unsere Werkstatt für Eltern und Kinder bis 6 Jahren um sich einfach mal auszuprobieren. 

Unsere Termine: 

20.04.2022 – 

18.05.2022 – ökologische Knete selbst herstellen 

15.06.2022 – Basteln mit Klopapierrollen 

  

Ihr sucht auch ein Angebot für ältere Kinder? Dann schaut doch einmal bei unserer UKW – Upcycling-Kinder-Werkstatt vorbei. 

