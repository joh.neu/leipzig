---
id: 5885-1653327000-1653337800
title: Nähen mit der Nähmaschine - Grundlagen
start: 2022-05-23 17:30
end: 2022-05-23 20:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/naehen-mit-der-naehmaschine-grundlagen/
teaser: In dem Workshop werden die ersten Schritte im Nähen mit einer
  Nähmaschine gezeigt. Welche Vorbereitu
isCrawled: true
---
In dem Workshop werden die ersten Schritte im Nähen mit einer Nähmaschine gezeigt. Welche Vorbereitungen braucht es um die Maschine funktionsbereit zu machen und welche Einstellungen sind erforderlich. Im Kurs wird ein erstes kleines Projekt genäht, wie z.B. ein Stoffbeutel oder ein Täschchen. 

 

