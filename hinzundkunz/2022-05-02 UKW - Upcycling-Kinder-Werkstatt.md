---
id: 5865-1651509000-1651516200
title: UKW - Upcycling-Kinder-Werkstatt
start: 2022-05-02 16:30
end: 2022-05-02 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcycling-kinder-werkstatt-4/
teaser: In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen
  kunterbunten Materialien experim
isCrawled: true
---
In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:30 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 

Heute bringen wir unsere vielen schönen und  kunterbunten Stoffe in Form und Reihe und gestalten gemeinsam Wimpelketten.  

!!Bitte meldet euch, wenn möglich, an. Dazu reicht auch eine kurze Mail an workshop@kunzstoffe oder eine spontane SMS an 0163 4846916.!! 

Wir freuen uns auf euch! 

  

