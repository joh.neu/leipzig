---
id: 5730-1654095600-1654106400
title: Nähen für Anfänger*innen
start: 2022-06-01 15:00
end: 2022-06-01 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/naehen-fuer-anfaengerinnen-5/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Immer am 1. Mittwoch im Monat könnt ihr erste Einblicke in das Nähen bekommen. Ihr bekommt die ersten Schritte bei der Bedienung der Nähmaschine erklärt und könnt euch an einem kleinen Nähprojekt ausprobieren. 

Termine: 02.02.2022 | 02.03.2022 | 06.04.2022 | 04.05.2022 | 01.06.2022 

Uhrzeit: jeweils 15-18 Uhr 

  

