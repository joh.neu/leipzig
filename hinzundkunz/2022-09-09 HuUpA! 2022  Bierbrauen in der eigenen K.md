---
id: 6163-1662735600-1662757200
title: HuUpA! 2022 | Bierbrauen in der eigenen Küche (mehrtägig | 1. Termin von 3)
start: 2022-09-09 15:00
end: 2022-09-09 21:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/huupa-2022-bierbrauen-in-der-eigenen-kueche-mehrtaegig-1-termin-von-3/
teaser: Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück.
  Dieses Jahr ist die HuUpA! ein ge
isCrawled: true
---
Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück. Dieses Jahr ist die HuUpA! ein gemeinsames Projekt mit dem GRASSI Museum für Angewandte Kunst. An unterschiedlichen Orten veranstalten wir eine Vielzahl an Kursen zu den Themen Handwerk, Upcycling und Selbermachen. Mit dabei sind wieder bekannte und beliebte aber auch neue Themen sowie Handwerker:innen und kreative Menschen aus der Nachbarschaft und der Stadt Leipzig. Außerdem können Kurse auch von Gebärdensprachdolmetscher:innen begleitet werden. Du hast die Qual der Wahl zwischen mehrtägigen Kursen und einmaligen Veranstaltungen. Finde deinen Kurs und melde dich an!! 

Zu den Terminen und Informationen zu den Kursinhalten, Anmeldung sowie Teilnahmebetrag geht es hier entlang. 

