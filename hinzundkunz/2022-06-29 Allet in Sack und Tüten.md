---
id: 5905-1656514800-1656522000
title: Allet in Sack und Tüten
start: 2022-06-29 15:00
end: 2022-06-29 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/allet-in-sack-und-tueten/
teaser: "Wir nähen Brot- & Gemüsebeutel für den Einkauf im Supermarkt als
  Alternative zum Plastikbeutel.   "
isCrawled: true
---
Wir nähen Brot- & Gemüsebeutel für den Einkauf im Supermarkt als Alternative zum Plastikbeutel. 

  

Gerne anmelden unter: 0163 4846916 oder workshop@kunzstoffe.de 

