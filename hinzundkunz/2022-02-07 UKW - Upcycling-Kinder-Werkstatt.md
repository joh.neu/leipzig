---
id: 5701-1644251400-1644258600
title: UKW - Upcycling-Kinder-Werkstatt
start: 2022-02-07 16:30
end: 2022-02-07 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcycling-kinder-werkstatt/
teaser: "!!VERANSTALTUNG FINDET STATT!! In der UpcyclingKinderWerkstatt (UKW)
  kannst du mit den zahlreichen "
isCrawled: true
---
!!VERANSTALTUNG FINDET STATT!! 

In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:30 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 

Heute könnt ihr aus kleinen Holzresten Figuren einfach selber bauen. Egal ob Roboter, Tiere oder Fahrzeuge … durch zusammen nageln, kleben und bemalen können vielfältige und bunte Objekte entstehen. 

!!Bitte meldet euch an. Dazu reicht auch eine kurze Mail an workshop@kunzstoffe oder eine spontane SMS an 0163 4846916.!! 

Wir freuen uns auf euch! 

Termine und Themen 

07.02.2022 | Holzfiguren bauen 

14.02.2022 EXTRA-Ferien-Termin!! | Sprudelndes für den Badespaß 

21.02.2022 | Musikinstrumente bauen 

07.03.2022 | Nähen for Beginners – erste Näherfahrungen mit kleinen Nähprojekten 

21.03.2022 | Papier-Upcycling – Girlanden und Mobile 

