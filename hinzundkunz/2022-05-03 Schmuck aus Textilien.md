---
id: 5877-1651590000-1651600800
title: Schmuck aus Textilien
start: 2022-05-03 15:00
end: 2022-05-03 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/schmuck-aus-textilien/
teaser: Aus Restmaterialien (Stoffe, Garne, Bänder) entsteht unter Anwendung
  verschiedener Techniken (schnei
isCrawled: true
---
Aus Restmaterialien (Stoffe, Garne, Bänder) entsteht unter Anwendung verschiedener Techniken (schneiden, rollen, wickeln, nähen, kleben, sticken) Textilschmuck (Ketten, Anhänger). 

