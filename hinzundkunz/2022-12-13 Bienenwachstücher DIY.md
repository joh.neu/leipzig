---
id: 6274-1670949000-1670956200
title: Bienenwachstücher DIY
start: 2022-12-13 16:30
end: 2022-12-13 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/bienenwachstuecher-diy/
teaser: "… als nachhaltige Geschenkidee für unter den Weihnachtsbaum   Anmeldung
  erwünscht: unter workshop@"
isCrawled: true
---
… als nachhaltige Geschenkidee für unter den Weihnachtsbaum 

  

Anmeldung erwünscht: unter workshop@kunzstoffe.de oder im Laden (Kontaktdaten nicht vergessen!) 

————————————————————————————————————————————————————- 

Liebe Leute, 

für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 

Wir wünschen euch viel Spaß. Euer kunZstoffe-Team