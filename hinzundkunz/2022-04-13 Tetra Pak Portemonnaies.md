---
id: 5759-1649862000-1649872800
title: Tetra Pak Portemonnaies
start: 2022-04-13 15:00
end: 2022-04-13 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/tetra-pak-portemonnaies/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Aus alten Tetrapacks können Portemonnaies oder kleine Hefte entstehen. 

