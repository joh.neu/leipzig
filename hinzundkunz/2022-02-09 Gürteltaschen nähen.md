---
id: 5745-1644418800-1644429600
title: Gürteltaschen nähen
start: 2022-02-09 15:00
end: 2022-02-09 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/guerteltaschen-naehen/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Heute könnt ihr mit uns aus bunten Stoffen eine eigene Gürteltasche entwerfen und nähen. 

