---
id: 6093-1659709800-1659717000
title: Girlande und Co ( wir zu Gast bei der LeseLust im August)
start: 2022-08-05 14:30
end: 2022-08-05 16:30
address: Clara-Zetkin-Park (neben dem Musikpavillon)
link: https://kunzstoffe.de/event/6093/
teaser: Wir sind zu Besuch beim Vorlesefest für Kinder vom LeseLust Leipzig e.V.
  und gestalten Girlanden aus
isCrawled: true
---
Wir sind zu Besuch beim Vorlesefest für Kinder vom LeseLust Leipzig e.V. und gestalten Girlanden aus Zeitschriften. 

LeseLust im August: 04.08.-07.08.2022 | täglich 10-18 Uhr 

Das erwartet dich auf der Lesewiese:

An vier Tagen zu unterschiedlichen Themen kannst du auf der Lesewiese im Clara-Zetkin-Park (neben dem Musikpavillon) tolle Ferientage mit deinen Eltern, Großeltern und Freund:innen verbringen. An jedem Tag gibt es mehrere spannende Lesungen und Aktionen. Unsere Vorlesepat:innen sind wieder im Einsatz und dürfen unter freiem Himmel für groß und klein tolle Geschichten vorlesen! 

