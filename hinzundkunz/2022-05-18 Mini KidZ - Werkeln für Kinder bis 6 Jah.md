---
id: 5826-1652887800-1652898600
title: Mini KidZ - Werkeln für Kinder bis 6 Jahre
start: 2022-05-18 15:30
end: 2022-05-18 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/mini-kidz-werkeln-fuer-kinder-bis-6-jahre-2/
teaser: Immer am dritten Mittwoch im Monat öffnen wir unsere Werkstatt für
  Eltern und Kinder bis 6 Jahren um
isCrawled: true
---
Immer am dritten Mittwoch im Monat öffnen wir unsere Werkstatt für Eltern und Kinder bis 6 Jahren um sich einfach mal auszuprobieren. 

Unsere Termine: 

18.05.2022 – ökologische Knete selbst herstellen 

15.06.2022 – Basteln mit Klopapierrollen 

20.07.2022 | 17.08.2022 | 21.09.2022 

  

Ihr sucht auch ein Angebot für ältere Kinder? Dann schaut doch einmal bei unserer UKW – Upcycling-Kinder-Werkstatt vorbei. 

