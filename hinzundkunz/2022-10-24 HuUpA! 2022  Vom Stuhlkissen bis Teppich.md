---
id: 6189-1666630800-1666638000
title: HuUpA! 2022 | Vom Stuhlkissen bis Teppich - mit eigener Holznadel aus
  Reststoffen binden (mehrtägig | 5. Termin von 5)
start: 2022-10-24 17:00
end: 2022-10-24 19:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/huupa-2022-vom-stuhlkissen-bis-teppich-mit-eigener-holznadel-aus-reststoffen-binden-mehrtaegig-5-termin-von-5/
teaser: Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück.
  Dieses Jahr ist die HuUpA! ein ge
isCrawled: true
---
Unsere HuUpA! – die Handwerk- und Upcycling-Akademie – ist zurück. Dieses Jahr ist die HuUpA! ein gemeinsames Projekt mit dem GRASSI Museum für Angewandte Kunst. An unterschiedlichen Orten veranstalten wir eine Vielzahl an Kursen zu den Themen Handwerk, Upcycling und Selbermachen. Mit dabei sind wieder bekannte und beliebte aber auch neue Themen sowie Handwerker:innen und kreative Menschen aus der Nachbarschaft und der Stadt Leipzig. Außerdem können Kurse auch von Gebärdensprachdolmetscher:innen begleitet werden. Du hast die Qual der Wahl zwischen mehrtägigen Kursen und einmaligen Veranstaltungen. Finde deinen Kurs und melde dich an!! 

Zu den Terminen und Informationen zu den Kursinhalten, Anmeldung sowie Teilnahmebetrag geht es hier entlang. 

