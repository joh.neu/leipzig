---
id: 5838-1651948200-1651962600
title: Museumsnacht Leipzig - DIY Traumfänger
start: 2022-05-07 18:30
end: 2022-05-07 22:30
address: GRASSI Museum für Völkerkunde, Johannispl. 5-11, Leipzig, 04103, Deutschland
link: https://kunzstoffe.de/event/museumsnacht-leipzig-diy-traumfaenger/
teaser: Zur Museumsnacht in Leipzig sind wir im GRASSI Museum für Völkerkunde
  und gestalten mit euch einen D
isCrawled: true
---
Zur Museumsnacht in Leipzig sind wir im GRASSI Museum für Völkerkunde und gestalten mit euch einen DIY Traumfänger aus kunterbunten Materialien. 

Infos zu aktuellen Hinweisen und ggf. Anmeldung findet ihr auf der Seite des GRASSI Museums. 

 

