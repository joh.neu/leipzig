---
id: 5750-1646838000-1646848800
title: Schmuck gestalten
start: 2022-03-09 15:00
end: 2022-03-09 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/5750/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Heute werden aus verschiedene Materialien wie Fahrradschlauch, Perlen, Tetra Pak … Ohrringe, Armbänder, Ketten, Schlüsselanhänger … selbst entworfen und hergestellen. 

