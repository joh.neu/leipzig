---
id: 5869-1654533000-1654540200
title: UKW - Upcycling-Kinder-Werkstatt
start: 2022-06-06 16:30
end: 2022-06-06 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/ukw-upcycling-kinder-werkstatt-8/
teaser: "!!VERANSTALTUNG FINDET STATT!! In der UpcyclingKinderWerkstatt (UKW)
  kannst du mit den zahlreichen "
isCrawled: true
---
!!VERANSTALTUNG FINDET STATT!! 

In der UpcyclingKinderWerkstatt (UKW) kannst du mit den zahlreichen kunterbunten Materialien experimentieren und verschiedene Werkzeuge kennenlernen. Jeden 1. und 3. Montag im Monat zeigen wir euch eine neue Projektidee. Die UKW findet von 16:30 bis 18:30 Uhr statt und ist für Kinder im Alter von 7-12 Jahren gedacht. 

Heute stanzen, falten und kleben wir kunterbuntes Papier und Papierreste zu Girlanden und Mobiles.  

!!Bitte meldet euch an. Dazu reicht auch eine kurze Mail an workshop@kunzstoffe oder eine spontane SMS an 0163 4846916.!! 

Wir freuen uns auf euch! 

