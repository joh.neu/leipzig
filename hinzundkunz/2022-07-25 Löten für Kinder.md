---
id: 6074-1658766600-1658773800
title: Löten für Kinder
start: 2022-07-25 16:30
end: 2022-07-25 18:30
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/loeten-fuer-kinder/
teaser: Vorsicht heiß! Löten ist kein Zauberwerk. Mit Konzentration und etwas
  Fingerfertigkeit können auch K
isCrawled: true
---
Vorsicht heiß! Löten ist kein Zauberwerk. Mit Konzentration und etwas Fingerfertigkeit können auch Kinder mit Lötkolben und Lötzinn umgehen lernen. 

Für Kinder ab 12 Jahren. 

Anmeldung erforderlich unter: workshop@kunzstoffe.de 

