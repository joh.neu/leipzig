---
id: 6238-1668699000-1668706200
title: Tausch- und Verschenketreff
start: 2022-11-17 15:30
end: 2022-11-17 17:30
address: Treffpunkt Schönefeld,, Waldbaurstraße 9, Leipzig, 04347
link: https://kunzstoffe.de/event/tausch-und-verschenketreff/
teaser: Ob ausgelesene Bücher, längst vergessene Schallplatten im Regal, Cd’s
  und Kasetten die man nicht meh
isCrawled: true
---
Ob ausgelesene Bücher, längst vergessene Schallplatten im Regal, Cd’s und Kasetten die man nicht mehr hört, ungenutzte Haushaltswaren, Gesellschaftspiele…bitte nicht wegwerfen! 

Jemand anderes freut sich noch darüber, muss nichts Neues kaufen, kann deine Dinge weiter nutzen und wertschätzen. Ressourcenschutz und nachhaltiges Handeln sind heutzutage unumgänglich und geht uns alle an. 

Beim Tausch- und Verschenketreff  bekommen deine alten Schätze eine neue Chance. Kommt gerne mit euren ausgedienten Kostbarkeiten zu unseren Terminen Donnerstags 

