---
id: 5800-1645542000-1645552800
title: ARENA GOES FOR FUTURE
start: 2022-02-22 15:00
end: 2022-02-22 18:00
address: Offener Jugendfreizeittreff ARENA, Miltitzer Weg 8, 04205 Leipzig,
  Miltitzer Weg 8, Leipzig, 04205
link: https://kunzstoffe.de/event/arena-goes-for-future/
teaser: Nachhaltigkeitsprojekt im Offenen Jugendfreizeittreff ARENA
  [Grünau]wöchentlich dienstags, 15-18 Uh
isCrawled: true
---
Nachhaltigkeitsprojekt im Offenen Jugendfreizeittreff ARENA [Grünau]

wöchentlich dienstags, 15-18 Uhr 

Mehr Infos dazu hier. 

