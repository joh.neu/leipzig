---
id: 5770-1646065800-1646071200
title: Makramee Gürtel
start: 2022-02-28 16:30
end: 2022-02-28 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/makramee-guertel/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

