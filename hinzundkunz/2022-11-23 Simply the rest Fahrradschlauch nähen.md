---
id: 6227-1669215600-1669222800
title: "Simply the rest: Fahrradschlauch nähen"
start: 2022-11-23 15:00
end: 2022-11-23 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/kleine-taschen-aus-fahrradschlauch-naehen/
teaser: kleine Taschen aus Fahrradschlauch nähen
isCrawled: true
---
kleine Taschen aus Fahrradschlauch nähen