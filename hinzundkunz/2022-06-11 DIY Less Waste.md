---
id: 5897-1654941600-1654956000
title: DIY Less Waste
start: 2022-06-11 10:00
end: 2022-06-11 14:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/diy-less-waste/
teaser: "In diesem DIY-Workshop dreht sich alles um Abfallvermeidung und
  -reduzierung im Alltag, als Impluse "
isCrawled: true
---
In diesem DIY-Workshop dreht sich alles um Abfallvermeidung und -reduzierung im Alltag, als Impluse für einen nachhaltigeren Lebensstil. Dabei gehen wir die verschiedenen Bereiche des Lebens und der Wohnung durch. Neben Informationen, z.B. zur richtigen Mülltrennung, und dem Austausch von Erfahrungen, zeigt der Workshop praktische Handlungsmöglichkeiten von Life Hacks, die schnell und einfach im Alltag umsetzbar sind, über Einkaufsvorschläge bis zu Rezepten für selbstgemachte Kosmetik.  

BITTE MELDET EUCH FÜR DIESEN WORKSHOP AN!!! (workshop@kunzstoffe.de oder 0163 4846916) 

ES WIRD EINEN TEILNEHMENDENBETRAG GEBEN. BITTE INFORMIERT EUCH GGF. 

