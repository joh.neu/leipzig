---
id: 5831-1654700400-1654707600
title: "Tetra-Pak-Upcycling: Portemonnaies oder kleine Hefte"
start: 2022-06-08 15:00
end: 2022-06-08 17:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/tetra-pak-upcycling-portemonnaies-oder-kleine-hefte/
teaser: Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder
  *telefonisch/ SMS unter 0163
isCrawled: true
---
Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Aus alten Tetrapacks können Portemonnaies oder kleine Hefte entstehen. 

