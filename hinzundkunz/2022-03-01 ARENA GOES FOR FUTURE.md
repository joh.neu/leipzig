---
id: 5803-1646146800-1646157600
title: ARENA GOES FOR FUTURE
start: 2022-03-01 15:00
end: 2022-03-01 18:00
address: Offener Jugendfreizeittreff ARENA, Miltitzer Weg 8, 04205 Leipzig,
  Miltitzer Weg 8, Leipzig, 04205
link: https://kunzstoffe.de/event/arena-goes-for-future-2/
teaser: Nachhaltigkeitsprojekt im Offenen Jugendfreizeittreff ARENA
  [Grünau]wöchentlich dienstags, 15-18 Uh
isCrawled: true
---
Nachhaltigkeitsprojekt im Offenen Jugendfreizeittreff ARENA [Grünau]

wöchentlich dienstags, 15-18 Uhr 

Mehr Infos dazu hier. 

