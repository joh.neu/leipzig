---
id: 6314-1676638800-1676647800
title: Korkuntersetzer
start: 2023-02-17 13:00
end: 2023-02-17 15:30
address: Treffpunkt Schönefeld,, Waldbaurstraße 9, Leipzig, 04347
link: https://kunzstoffe.de/event/korkuntersetzer/
teaser: "Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns
  in Schönefeld zum gemeinsamen "
isCrawled: true
---
Wöchentlich freitags zwischen 13:00 Uhr und 15:30 Uhr treffen wir uns in Schönefeld zum gemeinsamen Basteln und Werkeln 



17.02.2022 Korkuntersetzer

24.02.2022 aus alten T-Shirts werden Beutel



——————————————————————————————————————————————————————— 

Liebe Leute, 

für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 

Wir wünschen euch viel Spaß. Euer kunZstoffe-Team 

 