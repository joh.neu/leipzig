---
id: 6276-1671030000-1671040800
title: "Simply the Rest: geknüpfter Korb aus Fahrradschlauch"
start: 2022-12-14 15:00
end: 2022-12-14 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/simply-the-rest-geknuepfter-korb-aus-fahrradschlauch/
teaser: wir zeigen euch wie man aus einem Fahrradschlauch einen praktischen Korb
  knüpfen könnt.   Anmeldun
isCrawled: true
---
wir zeigen euch wie man aus einem Fahrradschlauch einen praktischen Korb knüpfen könnt. 

  

Anmeldung erwünscht: unter workshop@kunzstoffe.de oder im Laden (Kontaktdaten nicht vergessen!) 

————————————————————————————————————————————————————- 

Liebe Leute, 

für die meisten unserer Angebote rufen wir keine Teilnahme-Gebühr auf, weil es uns wichtig ist, dass alle unabhängig von ihrem ökonomischen Status teilhaben können. Die andere Seite ist: auch wir haben laufende Kosten, die wir stemmen müssen und sind da auch auf eure Hilfe angewiesen. Daher freuen wir uns über jede als finanzielle Zuwendung als Unterstützung, als Dankeschön und als Ausdruck der Wertschätzung. 

Wir wünschen euch viel Spaß. Euer kunZstoffe-Team