---
id: 5757-1648652400-1648663200
title: Stempel selber machen
start: 2022-03-30 15:00
end: 2022-03-30 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/stempel-selber-machen/
teaser: Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen
  stattfinden können. !!BITTE  IN
isCrawled: true
---
Es bestehen nach wie vor Unsicherheiten, ob geplante Veranstaltungen stattfinden können. !!BITTE  INFORMIERT EUCH TAGESAKTUELL UND FRAGT BEI UNS NACH!! 

Wichtig!! Bitte meldet euch an *per Mail an workshop@kunZstoffe.de oder *telefonisch/ SMS unter 0163 4846916 

Kleine Stempel selber herstellen und damit können z.B. Geschenkpapiere farbig bedruckt werden. 

