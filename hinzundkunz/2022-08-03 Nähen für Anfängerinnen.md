---
id: 6087-1659538800-1659549600
title: Nähen für Anfänger:innen
start: 2022-08-03 15:00
end: 2022-08-03 18:00
address: krimZkrams Materialsammlung, Georg-Schwarz-Str. 7, Leipzig, Deutschland
link: https://kunzstoffe.de/event/naehen-fuer-anfaengerinnen-6/
teaser: Immer am 1. Mittwoch im Monat könnt ihr erste Einblicke in das Nähen
  bekommen. Ihr bekommt die erste
isCrawled: true
---
Immer am 1. Mittwoch im Monat könnt ihr erste Einblicke in das Nähen bekommen. Ihr bekommt die ersten Schritte bei der Bedienung der Nähmaschine erklärt und könnt euch an einem kleinen Nähprojekt ausprobieren. 

Du hast ein eigenes Projekt, an welches du dich gern einmal wagen möchtest und Unterstützung brauchst? Dann lass es uns gern wissen. 

Termine: 03.08.2022 | 07.09.2022 | 05.10.2022 

Uhrzeit: jeweils 15-18 Uhr 

