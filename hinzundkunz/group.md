---
name:  kunZstoffe e.V.
website: http://kunzstoffe.de/
email: buero@kunzstoffe.de
address: Georg-Schwarz-Str. 7, 04177 Leipzig
scrape:
    source: ical
    options:
        url: http://kunzstoffe.de/event/?ical=1
    exclude:
       - 650-1530489600-1533340799
       - 2772-1572098400-1572112800 # Löffenschnitzkurs (grund: nicht tauschlogikfrei)
---
Das hinZundkunZ, kurz huk, ist ein Raum für Kunst, Kultur und alles was da so rein passt.