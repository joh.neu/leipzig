---
id: "359324836218230"
title: Hans-Jürgen Krahl als Kritiker der kritischen Theorie
start: 2022-04-29 18:30
locationName: translib
address: Goetzstraße 7
link: https://www.facebook.com/events/359324836218230/
image: 278738270_3128212867434587_5130244605245862822_n.jpg
isCrawled: true
---
Sich heute mit Hans-Jürgen Krahls auseinanderzusetzen, bedeutet, sich den Weg zu einer Gestalt zu bahnen, um die sich nicht wenige Mythen ranken und die doch fast vergessen wurde. Sein Tod, den er 1970 mit nur 27 Jahren bei einem Autounfall fand, war ein zentrales Moment der Auflösung des SDS. Mit dem Ende des ›Sozialistischen deutschen Studentenbundes‹, der theoretisch wie organisatorisch zentralen Vereinigung der revoltierenden Studenten, ging die Studentenbewegung in Deutschland ihrem Ende entgegen.

Zugleich wurde das Unterfangen einer gehaltvollen Kritik der kritischen Theorie, die in Krahls hinterlassenen Fragmenten und Aufsätzen aufblitzt, kaum fortgesetzt. Krahl lernte die kritische Theorie in Frankfurt aus erster Hand durch Max Horkheimer und Theodor W. Adorno kennen. Hatten seine akademischen Lehrer der proletarisierten Masse teils durch die Erfahrung des Faschismus, teils durch idiosynkratische Vorbehalte abgeschworen, so unternahm Krahl den Versuch der „Vermittlung von Studenten- und Arbeiterbewegung“. Diese Rückorientierung auf das Proletariat, das Ende der 1960er europaweit zu einem großen Kampfzyklus ausholte, war begleitet durch eine avancierte Theoriebildung, welche die Möglichkeit einer anderen Rezeption der kritischen Theorie bereithielt. Mit seinem antiautoritären Marxismus erreichte Krahl intellektuell nicht nur große Eigenständigkeit, sondern entwickelte darüber hinaus Modelle, um die Hindernisse der Entwicklung emanzipatorischer Praxis zu benennen und diese organisationsstrategisch auf ein neues Niveau zu heben.

Aus diesen Elementen setzt sich das Bild eines Kritikers der kritischen Theorie zusammen, das Krahl weit besser beschreibt als die zahlreichen Mythen. Mit der Diskussionsveranstaltung wollen wir ihm nähertreten. Krahl für die Gegenwart in Erinnerung zu rufen, scheint gerade vor dem Hintergrund geboten, dass nicht wenige Teile der Linken in den letzten Jahren eine ähnliche Umschulung durchliefen wie er seinerzeit. Neben der Rückorientierung auf das Proletariat durch eine Kritik der kritischen Theorie sollen jedoch auch Krahls Schwach- und Leerstellen thematisiert werden. Kann sich eine Linke heute organisationsstrategisch tatsächlich an seinen Überlegungen orientieren?

Auf der Veranstaltung werden Thesen aus dem jüngst von Meike Gerber, Emanuel Kapfinger und Julian Volz im Mandelbaum Verlag herausgegebenen Sammelband ‚Für Hans-Jürgen Krahl‘ vorgestellt. 

Link zum Buch:
https://www.mandelbaum.at/buecher/meike-gerber-emanuel-kapfinger-julian-volz/fuer-hans-juergen-krahl/

Aufgrund der andauernden pandemischen Lage bitten wir euch, vorab einen Covid-19-Schnelltest zu machen. 


