---
name: translib
website: https://translibleipzig.wordpress.com/
email: translib@gmx.de
address: Lütznerstraße 30, 04177 Leipzig
scrape:
  source: facebook
  options:
    page_id: 1397618430494048
---
Diese Bibliothek soll zu einem Stützpunkt für Leute werden, die sich nicht mit ihrem „Schicksal“ abfinden wollen, ihr Dasein als Arbeitskraftbehälter für Kapital und Staat zu fristen – für alle, die an der alltäglichen Entfremdung leiden, eingezwängt in die herrschende zweigeschlechtliche Identitäts-Norm und eine soziale Hierarchie, immerfort nur die gigantische Profitmaschinerie am Laufen zu halten, die doch nicht nach unseren Bedürfnissen organisiert ist.