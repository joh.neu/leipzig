---
id: "1066693797599688"
title: Wider die Widrigkeiten – die translib nach zwei Jahren Covid. Mit einem
  Vortrag von Florian Bossert
start: 2022-06-03 18:30
locationName: translib
address: Goetzstraße 7
link: https://www.facebook.com/events/1066693797599688/
image: 281881088_3152725584983315_3131351409615672594_n.jpg
isCrawled: true
---
Am letzten Februartag des Jahres 2020 hatten wir einen Pfleger aus Brüssel zu Gast, der über Klassenkämpfe in belgischen Krankenhäusern berichtete. Vielleicht ahnten manche da bereits, dass das Thema Gesundheit bald überaus aktuell werden würde. Denn in derselben Woche trafen verwirrende Nachrichten aus Norditalien ein, wo das öffentliche Leben plötzlich in ungekanntem Maße von behördlichen Maßnahmen eingeschränkt wurde. 

Niemand hat jedoch vorausgesehen, dass dieser Abend für über zwei Jahre der letzte bleiben würde, an dem wir zu einem geselligen Austausch eingeladen haben. In diesen zwei Jahren haben wir unsere Arbeit fortgesetzt, wenn auch in anderen Formen. Relativ überschaubare, feste Gruppen, wie unser Plenum, die Lesezirkel oder themenbezogene Arbeitskreise verlegten ihre Tätigkeit rasch in den digitalen Raum. Eine scheinbar effiziente Lösung, die jedoch auch zu einer Vereinseitigung führte. Was derweil brachlag, war der öffentliche Charakter des Projekts in einem stärkeren Sinne. Es mangelte an Möglichkeiten, neue Beziehungen einzugehen und an der besonderen Anregung, die in spontanen Begegnungen entstehen kann. 

Als unsere eingeschliffene Praxis im Notstand der „ersten Welle“ abrupt abbrach, suchten einige Ersatz im Aktivismus und gründeten das Blogprojekt "Solidarisch gegen Corona". Mittelfristig setzte sich jedoch eine andere Tendenz durch. Die Reibung mit der Außenwelt nahm ab, und es folgte eine Wendung nach innen. Im Winter 2020/2021, in dem sich die Pandemie wie eine Glasglocke über das Leben legte und unsere Wirkmacht an einem Nullpunkt angekommen war, begannen wir, uns ausführlich mit unserer eigenen Organisation auseinanderzusetzen. Aus dieser Selbstverständigung ging eine neue Verfassung hervor, die wir zukünftig unserer Arbeit zugrunde legen.  
 
Nun, da sich die Pandemie langsam in ihre dritte Sommerpause verabschiedet, möchten wir erneut zusammenkommen. Um die Ergebnisse unserer bisherigen Auseinandersetzung vorzustellen; um mit unserem Gast Florian Bossert zu reflektieren, was zwei Jahre Pandemie mit der Denkfähigkeit gemacht haben; und nicht zuletzt, um zu feiern, dass Verbindungen zu anderen Menschen sich wieder leichter im öffentlichen Raum knüpfen lassen. 

Grober Fahrplan für den Abend:

18.30 Uhr | Sektempfang 
19.00 Uhr | Vorstellung unserer neuen Verfassung – Vorstellung der Lese- und Arbeitsgruppen
20.00 Uhr | Viraler Angriff auf fragile Subjekte. Eine Psychoanalyse der Denkfähigkeit in der Pandemie. Vortrag und Diskussion mit Florian Bossert (Berlin). 		
		 
Die Covid-19-Pandemie ist Ausdruck einer umfassenden ökologischen und gesellschaftlichen Krise. Den Nährboden für die globale Verheerung bereitete eine gesellschaftliche Praxis, die keine absoluten Grenzen des eigenen Fortschreitens kennt und die Natur als kostenlose Gabe behandelt. Vor diesem Hintergrund untersucht Florian Bossert die Auswirkungen der pandemischen Realität auf die menschliche Denkfähigkeit. Globale Krisen, unbewusste Phantasien, archaische Ängste, Bescheidwissen und Verschwörungsdenken werden im Rückgriff auf Kritische Theorie und Psychoanalyse näher bestimmt. Vor dem Hintergrund von Krise, Krieg und Revolte wirft der Vortrag die drängende Frage auf, wie man in einer chaotisierten Welt weder verzweifelt, noch im affektiven Furor den Kopf verliert, sondern die Umrisse einer anderen Welt antizipieren und dem kollektiven Denken verfügbar machen kann.

Florian Bossert ist Psychologe und lebt in Berlin. Er arbeitet u.a. zu Psychoanalyse, psychoanalytischer Sozialpsychologie und materialistischer Klassenanalyse. Sein Buch „Viraler Angriff auf fragile Subjekte. Eine Psychoanalyse der Denkfähigkeit in der Pandemie.“ ist 2022 im Psychosozial-Verlag erschienen.

22 Uhr | Bar & DJs (tba) 

