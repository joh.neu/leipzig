---
id: "749692839323449"
title: Lesekreis Marx, 'Das Kapital', Bd. 1 - erstes Treffen
start: 2022-01-13 19:00
address: Leipzig
link: https://www.facebook.com/events/749692839323449/
image: 271542845_3052944494961425_2675632145868474523_n.jpg
isCrawled: true
---
Ein neuer Lesekreis zu Karl Marx' 'Das Kapital', beginnend mit Band 1! Wir freuen uns über Mitstreiter:innen!

13.01.22, 19 Uhr
Bei Interesse meldet euch bei: translib@gmx.de

Zum Inhalt:
Es gibt keine Theorie der modernen kapitalistischen Produktionsweise und Weltgesellschaft ohne Karl Marx. Wer Phänomene wie die aktuelle Krise, die weltweit zunehmende Massenverelendung und die kapitalistische Zerstörung der ökologischen Lebensgrundlagen verstehen will, kommt um die von Marx entwickelten Begriffe von „abstrakter Arbeit“, Wert und Kapital nicht herum. Ohne Bezugnahme auf diese Grundkategorien der modernen Gesellschaftsformation kann es keine tatsächliche Kritik der herrschenden gesellschaftlichen Ideologien und der Herrschaftsverhältnisse selbst geben. Für jedes auf gesellschaftliche Befreiung zielende Denken und Handeln ist es daher unerlässlich, sich die Marx'sche Gesellschaftskritik und ihre Kategorien anzueignen. Um eine gängige Phrase zu bemühen: „Das Problem heißt Kapitalismus!“ Um es zu lösen, müssen wir es erst einmal begreifen.
In diesem Lesekreis werden wir uns gemeinsam dem ersten Band von Marx' Hauptwerk widmen und Das Kapital Band 1 (MEW 23) lesen. Im Mittelpunkt dabei steht die logische Entfaltung der Marx'schen Kategorien von abstrakter Arbeit, Wert und Kapital sowie der Begriff des sich selbst verwertenden Werts – der „Wirtschaft“ als Selbstzweck. Die Lektüre erfolgt anhand ausgewählter Textpassagen, die zur jeweiligen Sitzung gelesen und dann gemeinsam durchgegangen und besprochen werden. Über den unmittelbaren Text hinausgehende Diskussionen sind explizit erwünscht. Wir wollen alle an Gesellschaftskritik Interessierten und mit den bestehenden Verhältnissen nicht Einverstandenen ermutigen, an der Veranstaltung teilzunehmen, Nicht-Student:innen ebenso wie Student:innen.

Das Buch:
Das Kapital. Kritik der politischen Ökonomie. Bd. 1: Der Produktionsprozess des Kapitals

Organisatorisches:
Wir empfehlen allen Teilnehmer:innen, sich das 'Kapital' bis zur ersten Veranstaltung zuzulegen. Wir verwenden die allgemein übliche Ausgabe nach den Marx-Engels-Werken, Band 23 (MEW 23), erschienen im Dietz Verlag, erkennbar am blauem Kunstledereinband,  antiquarisch günstig erwerbbar. Andere Ausgaben (brauner Einband; verschiedene Taschenbuchausgaben; aus „Ausgewählten Werken“; etc.) weichen in den Seitenzahlen der Kapitel oder der Auflage ab und sind daher ungünstig.

In der ersten Sitzung wollen wir zunächst die Vorwörter (bis S. 49) diskutieren. Diese bitte nach Möglichkeit lesen.