---
name: StuRa Uni Leipzig
website: https://stura.uni-leipzig.de/
email: info@stura.uni-leipzig.de
scrape:
  source: facebook
  options:
    page_id: 136525366397328
---
Der Student_innenRat (StuRa) ist Organ der verfassten Student_innenschaft der Universität Leipzig und die zentrale Vertretung zur Erfüllung der Aufgaben der Student_innenschaft gemäß §24 (3) SächsHS"F"G.

