---
id: "1102302836978604"
title: Kennenlerntreffen
start: 2022-01-12 18:00
link: https://www.facebook.com/events/1102302836978604/
image: 271719883_1098538217657079_8983862761101301742_n.jpg
isCrawled: true
---
Die Klima- und Umweltkrise schreitet voran, und wird in 2022 nicht weniger aktuell sein. Jetzt ist die Zeit um aktiv zu werden! Am Mittwoch gibt es ein niedrigschwelliges Online-Kennenlerntreffen.
https://letsmeet.extinctionrebellion.de/b/xrl-gj4-lvr-uxk