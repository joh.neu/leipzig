---
name: Umweltbibliothek Leipzig
website: https://www.oekoloewe.de/umweltbibliothek-leipzig.html
email: info@umweltbibliothek-leipzig.de
address: Bernhard-Göring-Str. 152, 04277 Leipzig
scrape:
  source: facebook
  options:
    page_id: 583163818509582
---
Wir verstehen uns als lebendiger Ort der interkulturellen Begegnung und des generationsübergreifenden Lernens.ata