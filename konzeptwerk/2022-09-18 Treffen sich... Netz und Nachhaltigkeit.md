---
id: 18431-1663509600-1663515000
title: Treffen sich... Netz und Nachhaltigkeit
start: 2022-09-18 14:00
end: 2022-09-18 15:30
address: Zeitgeschichtliches Forum Leipzig, Grimmaische Straße 6, Leipzig, 04109
link: https://konzeptwerk-neue-oekonomie.org/termin/treffen-sich-netz-und-nachhaltigkeit/
teaser: "// Tandemrundgang durch die Ausstellung #deutschlanddigital mit Anja
  Höfner vom Konzeptwerk und Jose"
isCrawled: true
---
// Tandemrundgang durch die Ausstellung #deutschlanddigital mit Anja Höfner vom Konzeptwerk und Josefine Bauer (Zeitgeschichtliches Forum Leipzig) zu Digitalisierung & Nachhaltigkeit.

—