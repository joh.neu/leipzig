---
id: 18447-1664668800-1664755199
title: Ausflug des Gemeinsam.Gerecht.Global.-Projekts
start: 2022-10-02 00:00
end: 2022-10-03 00:00
address: Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/ausflug-des-gemeinsam-gerecht-global-projekts-4/
teaser: Zwischen August und Oktober 2022 laden wir zu vier Tagesausflügen zu
  verschiedenen Themen in das Uml
isCrawled: true
---
Zwischen August und Oktober 2022 laden wir zu vier Tagesausflügen zu verschiedenen Themen in das Umland von Berlin und Leipzig ein. Zusammen mit euch wollen wir verschiedene Orte erkunden und darüber ins Gespräch kommen. Der Kontakt mit der Natur sowie der Austausch untereinander werden bei den Ausflügen im Mittelpunkt stehen. Wir planen interaktive Einheiten ein, bei denen wir neue Perspektiven kennenlernen und verstehen wollen. Wir denken über globale Zusammenhänge nach und wollen unsere Kenntnisse vertiefen. Pro Ausflug gibt es 15 Plätze und wir bieten Kinderbetreuung an. Wir freuen uns darauf, euch bald persönlich kennenzulernen! 

Ausflug 4: So, 2. Oktober 2022 bei Leipzig 

Am 2. Oktober beschäftigen wir uns mit dem Thema Müll. Wie ist die Müll-Situation in Deutschland? Was passiert mit dem Müll nach der Mülltonne? Was verändert sich, wenn China keinen Müll mehr annimmt? In diesem Ausflug wollen wir uns zusammen darüber informieren, diskutieren und entdecken. Zur Anmeldung bitte das Formular ausfüllen (https://cloud.wechange.de/s/B57S9RSJF7LNa4Q) an die folgende E-Mail Adresse zurück schicken: gemeinsam.gerecht.global@knoe.org.