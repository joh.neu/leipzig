---
id: 16852-1656770400-1656806400
title: 10 Jahre Konzeptwerk Feier
start: 2022-07-02 14:00
end: 2022-07-03 00:00
address: heiter bis wolkig, Röckener Straße 44, Leipzig, 04229
link: https://konzeptwerk-neue-oekonomie.org/termin/10-jahre-konzeptwerk-feier/
teaser: 10 Jahre Konzeptwerk, das heißt:10 Jahre Lernräume für transformative
  Bildung.10 Jahre Vernetzen v
isCrawled: true
---
10 Jahre Konzeptwerk, das heißt:

10 Jahre Lernräume für transformative Bildung.

10 Jahre Vernetzen von sozialen Bewegungen, Wissenschaft und Gesellschaft.

10 Jahre Anschieben des sozial-ökologischen Wandels.

10 Jahre Alternativen ausprobieren, denn nichts ist alternativlos.

10 Jahre Visionen von einer gerechten Zukunft für alle.

10 Jahre Kampf für Klimagerechtigkeit. 

Ein Grund, inne zu halten und zu schauen: was war, was ist und was sein könnte.

Und ein Grund zu feiern – als Dankeschön für all unsere Unterstützer*innen, Förder*innen und Projektpartner*innen.

Weiter geht‘s! Bis unsere Ziele real werden, ist es noch ein weiter Weg. 

Ab 14 Uhr im heiter bis wolkig – nur mit persönlicher Einladung

– Kaffee und Kuchen und Sekt

– Bürobesichtigungen

– Ansprache

– Workshops

– Abendessen 

Ab 21 Uhr Party open end – offen für alle – Ort wird noch bekannt gegeben

– Musik und Tanzen