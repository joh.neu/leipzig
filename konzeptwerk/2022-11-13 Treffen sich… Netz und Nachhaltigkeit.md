---
id: 19391-1668348000-1668353400
title: Treffen sich… Netz und Nachhaltigkeit
start: 2022-11-13 14:00
end: 2022-11-13 15:30
address: Zeitgeschichtliches Forum Leipzig, Grimmaische Straße 6, Leipzig,
  04109, Deutschland
link: https://konzeptwerk-neue-oekonomie.org/termin/treffen-sich-netz-und-nachhaltigkeit/
teaser: "// Ein Tandemrundgang in der Ausstellung #deutschlandDigital mit Anja
  vom Konzeptwerk Es sind die S"
isCrawled: true
---
// Ein Tandemrundgang in der Ausstellung #deutschlandDigital mit Anja vom Konzeptwerk 

Es sind die Schwerpunktthemen des 21. Jahrhunderts: Die Digitalisierung und die Bewältigung des Klimawandels. Wie können sie zusammen funktionieren? Oft glauben wir, dass die technische Lösung immer die nachhaltigere ist: Wenn wir einen Videocall machen, statt live auf der Konferenz aufzutauchen, wenn wir unsere Daten in der Cloud speichern, statt sie auf Papier auszudrucken, wenn wir unsere Bücher auf dem E-Reader konsumieren, statt uns die Exemplare im Handel zu kaufen. Wir denken, dass wir Ressourcen schonen, wenn wir die digitale Alternative nutzen. Aber es gibt sogenannte Reboundeffekte. Wir sorgen für mehr Paketlieferungen, weil wir die gewünschte Hose in unterschiedlichen Größen bestellen und nicht passende Exemplare wieder zurücksenden oder wir erwerben häufiger neue Geräte, weil zwei Jahre nach dem letzten Kauf eine bessere Energieklasse zur Verfügung steht. 

In der Tandembegleitung geht Anja Höfner vom Konzeptwerk Neue Ökonomie darauf ein, wo der Einsatz digitaler Technologien sinnvoll ist und wo wir gut funktionierende Lösungen vielleicht nicht zwanghaft digitalisieren müssen. Welche digitalen Technologien vermindern die negativen Auswirkungen auf Menschen und Umwelt? Wo steht der Profitgedanke vor der Nachhaltigkeit? Wie könnte eine ökologisch und sozial gerechte digitale Zukunft aussehen? 

Der Rundgang ist Teil einer Reihe von Tandembegleitungen durch die Wechselausstellung #DeutschlandDigital mit je unterschiedlichem Schwerpunktthema. Eine Expertin oder ein Experte eines Fachgebiets, wie Psychologie oder Cyberkriminalität, und eine Museumsmitarbeiterin begleiten die Besucherinnen und Besucher durch die Ausstellung und verdeutlichen das Thema anhand einzelner Objekte. Der Rundgang dauert etwa 90 Minuten und bietet die Gelegenheit, mit der Expertin oder dem Experten ins Gespräch zu kommen.