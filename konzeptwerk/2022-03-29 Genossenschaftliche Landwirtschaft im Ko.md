---
id: 16675-1648576800-1648580400
title: Genossenschaftliche Landwirtschaft im Kontext der Klimakatastrophe
start: 2022-03-29 18:00
end: 2022-03-29 19:00
address: Pöge-Haus, Leipzig, Hedwigstr. 20, Leipzig, Sachsen, 04315, Deutschland
link: https://konzeptwerk-neue-oekonomie.org/termin/genossenschaftliche-landwirtschaft-im-kontext-der-klimakatastrophe/
teaser: // Eine Veranstaltung der KoLa Leipzig eG mit Lasse Thiele vom
  Konzeptwerk— Die „Kooperative Landw
isCrawled: true
---
// Eine Veranstaltung der KoLa Leipzig eG mit Lasse Thiele vom Konzeptwerk

— 

Die „Kooperative Landwirtschaft KoLa Leipzig eG“ wurde im Herbst 2020 aus der Motivation heraus gegründet, die Lebensmittelversorgung in einem kleinen Bereich konkret neu auszurichten: lokal, ökologisch, fair. Kann die Genossenschaft dem Klimawandel und anderen Krisen auf diese Weise begegnen?

Die aktuell über 1500 Genossenschafts-Mitglieder, Freunde und Interessierte sind eingeladen, in die Debatte einzusteigen: Wie können wir dem Klimawandel begegnen? Wie wirkt die Klimakatastrophe auf die landwirtschaftliche Erzeugung und was kann ein Gemüsebau-Betrieb für Klimagerechtigkeit tun? 

Lasse Thiele liefert zunächst einen Überblick über den aktuellen Stand der Wissenschaft und politischen Debatte. Was verbirgt sich hinter dem Begriff einer “sozial-ökologischen Marktwirtschaft” (Ampel-Regierung)? Verbreitet sind verschiedene Ideen eines “Green New Deal”, während Teile der Klimabewegung “Postwachstum” fordern. Wie unterscheiden sich diese Ansätze – und was bedeuten sie für ein Projekt wie KoLa Leipzig?