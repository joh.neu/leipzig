---
id: 17952-1656028800-1656287999
title: Degrowth in post-communist European countries No.2
start: 2022-06-24 00:00
end: 2022-06-27 00:00
address: Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/networking-and-exchange-meeting-no-2/
teaser: // Ein Kooperationsprojekt vom Konzeptwerk Neue Ökonomie, Re-set, NaZemi
  and the Department of Envir
isCrawled: true
---
// Ein Kooperationsprojekt vom Konzeptwerk Neue Ökonomie, Re-set, NaZemi and the Department of Environmental Studies Brno

-> Anmeldung per Mail

— 

The network and exchange meeting is part of the cooperation project between Konzeptwerk Neue Ökonomie, Re-set, NaZemi and the Department of Environmental Studies Brno aiming to create more space for exchange on socio-ecological transformation and degrowth in post-communist regions.