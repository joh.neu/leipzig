---
id: 19377-1666890000-1666900800
title: Raus aus der Klimakrise geht nur ohne Wachstum
start: 2022-10-27 17:00
end: 2022-10-27 20:00
address: HTWK Leipzig, Karl-Liebknecht Straße, , Geutebrück-Bau, Raum G238, Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/raus-aus-der-klimakrise-geht-nur-ohne-wachstum/
teaser: // Ein Workshop im Rahmen der Nachhaltigkeitswoche der HTWK Leipzig, mit
  Nora und Christoph vom Konz
isCrawled: true
---
// Ein Workshop im Rahmen der Nachhaltigkeitswoche der HTWK Leipzig, mit Nora und Christoph vom Konzeptwerk

Ort: Nieperbau der HTWK im Seminarraum N003 

Alternatives Wirtschaften für eine nachhaltige und global gerechte Zukunft für alle 

Aktueller Klimaschutz reicht nicht aus. Eine Hauptursache: Wirtschaftswachstum wird in unserer auf Ausbeutung basierenden Lebensweise nicht in Frage gestellt. In diesem Workshop wollen wir uns näher anschauen, warum Wachstum und Profite als Ziele gesellschaftlicher Anstrengungen verschiedene soziale und ökologische Krisen maßgeblich anfeuern. 

Vor allem wollen wir aber mit vielfältigen Methoden alternative Formen solidarischer Lebens- und Wirtschaftsweisen kennenlernen. Diese sollen nicht abstrakt bleiben. Wir beziehen aktuelle Vorschläge des Konzeptwerks mit in den Workshop ein, z.B. unsere „Bausteine für Klimagerechtigkeit – transformativ, solidarisch, machbar„. 

Wir lernen also Alternativen kennen, wo Menschen jetzt schon ganz praktisch an einer nachhaltigen Welt tüfteln, pflanzen und bauen. Und wir denken gemeinsam darüber nach, welche gesellschaftlichen Veränderungen es für einen nachhaltigen Wandel braucht, wer eigentlich zu diesem Wandel beiträgt und wo unsere Rolle darin ist. 

Fragen von gesellschaftlichen Machtverhältnissen und globalen Ungerechtigkeiten spielen für uns dabei immer eine Rolle.