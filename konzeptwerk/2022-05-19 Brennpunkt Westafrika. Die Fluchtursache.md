---
id: 17412-1652986800-1652994000
title: Brennpunkt Westafrika. Die Fluchtursachen und was Europa tun sollte.
start: 2022-05-19 19:00
end: 2022-05-19 21:00
address: Pöge-Haus, Leipzig, Hedwigstr. 20, Leipzig, Sachsen, 04315, Deutschland
link: https://konzeptwerk-neue-oekonomie.org/termin/brennpunkt-westafrika-die-fluchtursachen-und-was-europa-tun-sollte/
teaser: // Eine Veranstaltung vom Konzeptwerk in Kooperation mit
  afrique-europe-interact— Die Bekämpfung v
isCrawled: true
---
// Eine Veranstaltung vom Konzeptwerk in Kooperation mit afrique-europe-interact

— 

Die Bekämpfung von Fluchtursachen ist in Europa spätestens seit 2015 zu einer Art Mantra avanciert. Viele Politiker:innen versprechen sich davon eine deutliche Reduzierung der Ankunftszahlen afrikanischer Migrant:innen, auch in Verbindung mit einer immer stärkeren Überwachung der EU-Außengrenzen. Der Aktivist und Autor Olaf Bernau widerspricht dieser Haltung vehement. In seinem Buch „Brennpunkt Westafrika. Die Fluchtursachen und was Europa tun sollte“ zeigt er, warum Menschen in Westafrika aufbrechen. Dafür spannt er einen Bogen vom kolonialen Erbe über die ersten Jahrzehnte nach der Unabhängigkeit bis in die Gegenwart. Eine besondere Rolle spielt die systematische Benachteiligung der bäuerlichen Bevölkerung, einschließlich der Klimakrise. In der Veranstaltung soll auch der Frage nachgegangen werden, welchen Beitrag soziale Bewegungen in Europa zur Veränderung der Nord-Süd-Beziehungen leisten können.  

Olaf Bernau ist seit 2010 im transnationalen Netzwerk Afrique-Europe-Interact aktiv, er hält sich regelmäßig in Mali auf.  

Der Eintritt ist frei, Spenden sind willkommen.