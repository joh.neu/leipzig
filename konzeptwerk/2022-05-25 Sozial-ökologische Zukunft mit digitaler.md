---
id: 17466-1653505200-1653512400
title: Sozial-ökologische Zukunft mit digitaler Technik gestalten?
start: 2022-05-25 19:00
end: 2022-05-25 21:00
address: Galerie KUB, Kantstr. 18, Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/sozial-oekologische-zukunft-mit-digitaler-technik-gestalten/
teaser: // Eine Veranstaltung im Rahmen des Projekts „digital bewegt“ vom
  Konzeptwerk -> Zum Youtube-Livest
isCrawled: true
---
// Eine Veranstaltung im Rahmen des Projekts „digital bewegt“ vom Konzeptwerk 

-> Zum Youtube-Livestream 

Wie sieht eine Zukunft aus, in der gesellschaftliche Herausforderungen genau dann mit digitaler Technik gelöst werden, wenn es das sinnvollste Mittel ist? Wie kann mit der Klimakrise global gerecht umgegangen werden, wenn wir wirklich abwägen, wann digitale Technik mehr Probleme schafft als sie löst? 

Dass eine tiefgreifende sozial-ökologische Transformation unserer Wirtschaft und Gesellschaft nötig ist, machen vielfältige Krisen deutlich. Die Klimakrise ist dabei untrennbar mit Krisen globaler und sozialer Ausbeutung und Ungleichheit verbunden. In der aktuellen öffentlichen und politischen Debatte spielt digitale Technik als Lösung für diese Krisen eine zentrale Rolle: Über Mobilitätsplattformen und algorithmisch gesteuerte Lieferketten bis hin zu digitalen Medien sollen Wirtschaft und Gesellschaft effizienter und damit – so die Hoffnung – grüner gemacht werden. Die Zukunft, die dabei entworfen wird, ist meist ein „weiter wie bisher“ nur mit mehr Technik. Zentrale Herausforderungen wie Rebound-Effekte, Ressourcen- und Energieverbrauch digitaler Technik und die damit einhergehenden Veränderungen von Arbeitsbedingungen und gesellschaftlichen Machtverhältnissen werden dabei häufig vernachlässigt. 

Im Rahmen der Podiumsdiskussion werfen wir einen Blick darauf, wie eine sozial-ökologische Zukunft aussehen kann, wenn wir uns erlauben den Fokus zu weiten. Mit Perspektiven aus Wissenschaft, Klima- und Tech-Bewegung diskutieren wir wie digitale Technik gestaltet und genutzt werden müsste, damit sie wirklich zu mehr Klimaschutz, sozialer Gerechtigkeit und Selbstbestimmung beiträgt – und wo deren Grenzen liegen. Konkrete Beispiele dafür gibt es bereits und können als Kompass dafür dienen, wie wir als (Zivil-)Gesellschaft zu einem reflektierten und demokratischen Umgang mit dem großen Projekt „Digitalisierung“ kommen. 

Die Veranstaltung findet in den Räumen der Galerie KUB in Leipzig statt. Ein sensibler Umgang mit coronabedingte Vorsichtsmaßnahmen ist nach wie vor Teil unseres Veranstaltungskonzepts. Im Veranstaltungsraum werden Masken getragen und es wird regelmäßig gelüftet. 

Referentinnen: 



Andrea Vetter (arbeitet wissenschaftlich und journalistisch zu sozial-ökologischen Zukunftsversionen und konvivialer Technik)

Claudia Henke (Mitbegründerin der Plattform-Kooperative SUPERMARKT)

Karolin Varner (Software Ingenieurin und Haeckse vom Chaos Computer Club)



Moderation:  



Nina Treu, Konzeptwerk Neue Ökonomie