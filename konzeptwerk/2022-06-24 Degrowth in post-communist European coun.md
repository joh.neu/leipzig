---
id: 17952-1656090000-1656248400
title: Degrowth in post-communist European countries No.2
start: 2022-06-24 17:00
end: 2022-06-26 13:00
address: Karl-Heine-Straße 93, Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/networking-and-exchange-meeting-no-2/
teaser: // A cooperation project between Konzeptwerk Neue Ökonomie, Re-set,
  NaZemi and the Department of Env
isCrawled: true
---
// A cooperation project between Konzeptwerk Neue Ökonomie, Re-set, NaZemi and the Department of Environmental Studies Brno

-> Registration by email

— 

The network and exchange meetings aim to create more space for exchange on socio-ecological transformation and degrowth in post-communist regions. You can find a report from the first network meeting here:

https://konzeptwerk-neue-oekonomie.org/degrowth-in-post-communist-european-countries/ 

SCHEDULE 

Friday 24.6. 

5pm Welcome

Short introduction of the project „Partnership for Dialogue on Degrowth and Socio-Ecological Transformation“. Getting to know each other. 

6pm Presentations of the organizations, Networking time 

7pm Dinner (vegan catering) 

8pm Guided office tour in Konzeptwerk Neue Ökonomie 

informal evening end (probably at „Heiter bis Wolkig“, not part of the official program) 

  

Saturday 25.6. 

9am Introduction to needs-based payment on example of Konzeptwerk Neue Ökonomie (Input & Diskussion)

Konzeptwerk Neue Ökonomie operates on a non-profit basis. Konzeptwerk does not only develop concepts for a new economy, they also try them out. In the area of finances, this means aligning their payment with the needs of people working there regardless of the functions or the length of time they have worked at Konzeptwerk. At this slot Konzeptwerk gives an inside look on its distribution of money within the organization and its redistribution policy. 

10.30am Break 

11am Introduction of the Book Chapter “Degrowth enthusiasm and the transformation blues of the East” by Lilian Pungas (Input & Diskussion) 

12.30am Lunch (vegan catering) 

2pm Introduction to organizational structures of Konzeptwerk Neue Ökonomie (Input & Diskussion)

Konzeptwerk Neue Ökonomie has been working as a collective for a social, ecological and democratic economy and society for 10 years. What does grassroots democracy look like in practice? What experiences have we gathered over the past years? 

3.30pm Break 

4pm Introduction to plural economics by Andreas Jaumann (Workshop)

The Network for Pluralist Economics promotes self-reflection, constructive criticism as well as theoretical and methodological openness in the education of and research on economics. Hence, students and researchers will more likely be enabled to solve our current multiple crises in a problem-oriented and future-fit way. For this purpose, an overlap with climate justice and degrowth movement groups can be assumed. Based thereon, frictions and synergies between these movements will be discussed and elaborated on. Furthermore, the challenges and potentials of their institutionalization in (academic) education shall be discerned. 

7pm Dinner (vegan catering) 

informal evening end (not part of the official program) 

  

Sunday 26.6. 

All participants from Friday/Saturday are welcome to come on Sunday as well. However the focus lies in an organizational part of the project – planning the Conference on Degrowth and Socio-ecological Transformation which takes place in September 8-11th in Brno, Czech republic. There will be place to have an inside look in a collaborative organizational process as well as taking part on discussion groups concerning the topics of degrowth and socio-ecological transformation. 

9am Open space (topics left from Saturday)



10am Introduction of the conference + organizing (work in small groups – networking/discussion/organizational groups) 

12am Lunch (vegan catering) 

13am Clean up, Departure