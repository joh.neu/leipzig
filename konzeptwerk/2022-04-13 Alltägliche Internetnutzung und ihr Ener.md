---
id: 16912-1649876400-1649883600
title: Alltägliche Internetnutzung und ihr Energieverbrauch – Leipziger Energietour
start: 2022-04-13 19:00
end: 2022-04-13 21:00
address: Café im Haus d. Demokratie, B.-Göring-Str. 152, Leipzig, 04277
link: https://konzeptwerk-neue-oekonomie.org/termin/alltaegliche-internetnutzung-und-ihr-energieverbrauch-leipziger-energietour/
teaser: "// Eine Veranstaltung vom BUND Leipzig mit Mascha vom Konzeptwerk— Wir
  alle tun es: Mal schnell et"
isCrawled: true
---
// Eine Veranstaltung vom BUND Leipzig mit Mascha vom Konzeptwerk

— 

Wir alle tun es: Mal schnell etwas googeln, mailen, liken oder Inhaltestreamen.

Weltweit werden täglich ca. 320 Milliarden E-Mails verschickt und über 1 Milliarde Stunden Videos auf Youtube geschaut! 

Der AK Klima und Energie veranstaltet einen Vortragsabend zu folgenden Fragen: Was bedeutet der zunehmende Datenverkehr für die Umwelt? Welche Alternativen gibt es? Jede*r ist eingeladen mitzudiskutieren und Ideen für den umweltfreundlicheren Umgang mit dem Internet zu sammeln. 

Die Veranstaltung wird hybrid angeboten, was bedeutet, dass Teilnehmer*innen an der Veranstaltung entweder live im Café des Hauses der Demokratie oder online teilnehmen können. 

Mehr Infos zur Veranstaltung 

Programmflyer vom BUND