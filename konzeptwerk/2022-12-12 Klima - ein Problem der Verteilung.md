---
id: 19670-1670859000-1670868000
title: Klima - ein Problem der Verteilung
start: 2022-12-12 15:30
end: 2022-12-12 18:00
address: Konzeptwerk Neue Ökonomie, Klingenstr. 22, Leipzig, Sachsen, 04177, Deutschland
link: https://konzeptwerk-neue-oekonomie.org/termin/klima-ein-problem-der-verteilung/
teaser: // Eine Veranstaltung mit Anne Rabin von „Arbeit und Leben Sachsen“
  sowie Esther und Nora vom Konzep
isCrawled: true
---
// Eine Veranstaltung mit Anne Rabin von „Arbeit und Leben Sachsen“ sowie Esther und Nora vom Konzeptwerk

// Bitte hier vorab anmelden

— 

Die Klimakrise gilt als die globale Herausforderung schlechthin – Hitzewellen, Überschwemmungen und Dürren erschweren bereits jetzt ein gutes Leben auf der Erde. Doch weder sind alle Menschen gleich verantwortlich für die Klimakrise, noch sind alle gleich davon betroffen. Während besonders Länder des globalen Nordens wie Deutschland zur Erderwärmung beigetragen haben, sind die Folgen in      den Ländern des globalen Südens wie Bangladesch oder Uganda besonders spürbar.  

Woher kommen diese globalen Unterschiede? Was bedeuten sie für den Umgang mit der Klimakrise, für Verantwortung und Solidarität? Wie kann eine (klima-)gerechtere Welt aussehen und welche Wege      führen dorthin?  

In diesem Workshop erkunden wir gemeinsam die verschiedenen Aspekte von Klimagerechtigkeit und diskutieren, wie eine solidarische Welt aussehen könnte.