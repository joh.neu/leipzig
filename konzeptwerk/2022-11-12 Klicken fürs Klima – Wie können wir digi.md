---
id: 19386-1668261600-1668272400
title: Klicken fürs Klima – Wie können wir digital nachhaltig sein?
start: 2022-11-12 14:00
end: 2022-11-12 17:00
address: Zeitgeschichtliches Forum Leipzig, Grimmaische Straße 6, Leipzig,
  04109, Deutschland
link: https://konzeptwerk-neue-oekonomie.org/termin/klicken-fuers-klima-wie-koennen-wir-digital-nachhaltig-sein-2/
teaser: "// Ein offenes Gespräch als Teil der Formatreihe OpenSpace in der
  Wechselausstellung #DeutschlandDig"
isCrawled: true
---
// Ein offenes Gespräch als Teil der Formatreihe OpenSpace in der Wechselausstellung #DeutschlandDigital mit Anja vom Konzeptwerk

Eintritt frei. Keine Anmeldung erforderlich.  

Digitalisierung wird oft als die große Lösung für Klimaprobleme gesehen. Technik erweckt zunächst vielfach den Eindruck, ökologischer und nachhaltiger zu sein. Dabei gibt es aber zwei Seiten der Medaille und digitale Technik hat immer auch ökologische Auswirkungen – sie sind nur meist nicht direkt sichtbar. So treiben die Vernetzung mit digitalen Geräten und die Nutzung von Onlinediensten den Energieverbrauch von Haushalten in die Höhe. Großkonzerne sammeln Daten und Lieferdienste zeigen auf, dass etwa digitales Tracking Arbeitsverhältnissen verschlechtern kann. Daher stellt sich die Frage, wo digitale Technik der Nachhaltigkeit zuträglich sein kann. 

Das Konzeptwerk Neue Ökonomie e. V. bietet Antworten auf diese Frage und zeigt auf, wie Digitalisierung nachhaltig möglich ist. Zusammen mit den Besucherinnen und Besuchern gehen sie darauf ein, welchen Einfluss einzelne Nutzerinnen und Nutzer, die Politik und die Tech-Großkonzerne haben. Dabei gibt eine Vertreterin des Vereins praktische Tipps und regt dazu an, das eigene Nutzungsverhalten zu reflektieren. 

Die Veranstaltung ist Teil der Formatreihe OpenSpace in der Wechselausstellung #DeutschlandDigital. Dabei läd das Zeitgeschichtliche Forum gemeinsam mit Kooperationspartnerinnen und -partnern zu kleinen Aktionen in der Ausstellung ein. Die OpenSpace-Angebote finden während der Laufzeit der Ausstellung an ausgewählten Mittwochen und Samstagen zwischen 14 und 17 Uhr statt.