---
id: 18870-1662890400-1662890400
title: Action für eine gerechte Wirtschaft
start: 2022-09-11 10:00
end: 2022-09-11 10:00
address: Haus der Begegnung, Arno-Nitzsche-Str. 37, 04277 Leipzig
link: https://konzeptwerk-neue-oekonomie.org/termin/action-fuer-eine-gerechte-wirtschaft/
teaser: Die wirtschaftlichen Folgen der anhaltenden Covid-19-Pandemie und
  steigende Preise für Energie und L
isCrawled: true
---
Die wirtschaftlichen Folgen der anhaltenden Covid-19-Pandemie und steigende Preise für Energie und Lebensmittel befeuern Armut und soziale Ungleichheit. Während mehr als eine Viertelmilliarde Menschen gefährdet sind, im Jahr 2022 in extreme Armut abzurutschen, machen Konzerne und die dahinterstehenden Superreichen gigantische Gewinne. Unsere Wirtschaft ist nicht gerecht – doch das muss nicht so bleiben! Es gibt bereits viele inspirierende Beispiele, wie es anders gehen kann. 

Du willst Politik bewegen, Kampagnen starten und gemeinsam mit anderen unsere Welt verändern? Dann sei bei unserem ganztägigen Workshop in Leipzig dabei!

Gemeinsam mit Expert*innen vom Konzeptwerk Neue Ökonomie entdeckst Du verschiedene Ansätze für eine sozialere und ökologischere Wirtschaft. Neben viel Raum zum Austausch und der Möglichkeit zur Diskussion, wollen wir gemeinsam eine öffentlichkeits- und medienwirksame Aktion für eine gerechtere Wirtschaft entwickeln und diese im Oktober mit Euch umsetzen! Mit Bummi Hammer (ehemals Peng-Kollektiv) untersuchen wir gelungene Beispiele subversiver Medienarbeit und aufsehenerregender Aktionen im öffentlichen Raum und finden heraus, was sie erfolgreich macht. Anschließend entwickeln wir Ideen für einen Pressestunt, um einen sozialen und ökologischen Umbau der Wirtschaft einzufordern.

Melde Dich jetzt an für den kostenlosen Workshop „Action – für eine gerechte Wirtschaft!“ – und setz Dich für Veränderung ein!