---
id: "634333397634211"
title: Turn left! Gegen den neuen Faschismus und den Nationalismus der Mitte
start: 2022-04-07 19:00
locationName: Conne Island
address: Koburger Str. 3, 04277 Leipzig-Connewitz
link: https://www.facebook.com/events/634333397634211/
image: 275487067_10159438074935862_7869788636278527348_n.jpg
isCrawled: true
---
Anlass zum praktischen Antifaschismus gibt es (nicht nur in Sachsen) bekanntlich genug: von alt eingesessenen Kameradschaftsstrukturen, rechten Kampfsportlern, über die AfD als den parlamentarischen Arm und bürgerlichen Türöffner der völlkischen Bewegung, rassistischen Staats-Akteur*innen bis hin zu von organisierten Faschos angeführten und koordinierten Corona-Protesten.  An fundierten Analysen der zugrundeliegenden Verhältnisse und darüber geführten Debatten mangelt es jedoch in jüngster Zeit erheblich. Eine emanzipatorische, antinationale (radikale) Linke kann sich das auf Dauer nicht leisten, zumal sie auch in Zukunft, wie es aussieht, nicht weniger Abwehrkämpfe gegen reaktionäre und menschenverachtende Akteur*innen führen werden muss.

Wir haben Vertreter*innen von der Kampagne "Nationalismus ist keine Alternative" und T.O.P Berlin eingeladen, uns ihre neue Broschüre "TURN LEFT. Gegen den neuen Faschismus und den Nationalismus der Mitte" vorzustellen. Sie richtet sich an alle, denen die Verhältnisse zuwider sind: Egal ob Fridays for Future oder abgegessene Altantifas, alle denen Memes und Deutschlandfunk nicht reichen, alle, denen es in den Fingern juckt, wenn sie Gauland reden hören. Die Broschüre soll an die Aufgabe antifaschistischer Politik schlechthin erinnern: Aufgabe ist nicht, die bürgerlich-kapitalistische Gesellschaft vor ihr selbst zu retten, vor ihrem Umschlag in autoritäre, reaktionäre und faschistische Ideologie und Politik, sondern genau dieses Umkippen zu thematisieren und die Ursachen zu bekämpfen und abzuschaffen, so die Verfasser*innen.