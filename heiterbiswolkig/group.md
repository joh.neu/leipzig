---
name: heiter bis wolkig
website: https://buergerbahnhof-plagwitz.de/cafe
email: agnesreuter@gmx.de
scrape:
  source: facebook
  options:
    page_id: heiterbiswolkigcafe
---
Heiter bis Wolkig heißt das Freiluft-Café auf dem Bürgerbahnhof Plagwitz gegenüber dem Bauspielplatz Wilder Westen.

Heiter bis Wolkig ist ein Ort zum Entspannen, frischen Kaffee trinken, frei und kreativ sein -  wo man die Welt an sich vorbeiziehen lassen kann, zwischen Stadt und Wildnis. Wir fördern Kultur und nachbarschaftliche Begegnung. Ein Café an dem sich jeder wohlfühlt und Natur und Freiraum genießen kann.