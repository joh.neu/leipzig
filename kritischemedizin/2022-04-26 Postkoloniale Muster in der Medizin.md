---
id: "2200543926787853"
title: Postkoloniale Muster in der Medizin
start: 2022-04-26 19:00
locationName: online
link: https://www.facebook.com/events/2200543926787853/
image: 279057437_3065316520398907_6754658134483045462_n.jpg
isCrawled: true
---
Postkoloniale Muster in der Medizin - ein Vortrag von Vivien-Lee Greiwe

Welche Rolle spielte die Medizin in der Schaffung von Kolonialverhältnissen? Welche (pseudo-)wissenschaftliche Legitimierung für koloniale Ausbeutung und welche aktuellen postkolonialen Muster lassen sich in der medizinschen Versorgung finden?
Diesen Fragen wollen wir mit der Politikwissenschaftlerin und Medizinerin Vivien-Lee Greiwe nachgehen