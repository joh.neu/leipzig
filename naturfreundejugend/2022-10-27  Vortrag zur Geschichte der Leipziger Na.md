---
id: 20221002T1100Z-1664708429.5884-EO-1181-1
title: " Vortrag zur Geschichte der Leipziger Naturfreunde"
start: 2022-10-27 19:30
end: 2022-10-27 21:00
link: https://naturfreundejugend-leipzig.de/events/event/vortrag-zur-geschichte-der-leipziger-naturfreunde/
teaser: " Unsere Freund:innen vom AK Hundestart beschäftigen sich mit der
  Geschichte der Leipziger Meuten, Ju"
isCrawled: true
---
 Unsere Freund:innen vom AK Hundestart beschäftigen sich mit der Geschichte der Leipziger Meuten, Jugendliche und junge Erwachsene die antifaschistischen Widerstand gegen den NS geleistet haben. Die Naturfreunde waren ebenfalls Teil davon und Jürgen Lorenz, mit über 80 eines der Gründungsmitglieder der Naturfreunde Leipzig, setzt sich schon lange mit der Geschichte der Leipziger Naturfreunde auseinander und wird dazu einen Vortrag halten.… weiterlesen  “Vortrag zur Geschichte der Leipziger Naturfreunde”