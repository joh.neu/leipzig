---
id: 20221122T1523Z-1669130583.4982-EO-1187-1
title: " nationalism vs. national state"
start: 2022-11-28 19:00
end: 2022-11-28 21:00
address: Meuterei
link: https://naturfreundejugend-leipzig.de/events/event/nationalism-vs-national-state/
teaser: " Dieser Vortrag findet in englischer Sprache statt • This event will be
  held in EnglishNation state"
isCrawled: true
---
 Dieser Vortrag findet in englischer Sprache statt • This event will be held in English

Nation states appear somewhere between free will, solidarity, loyalty on one side – and fear, hierarchy, capitulation on the other side.

Political systems of eastern European countries might seem too complicated and unusual for a foreigner – yet they tend to just more dramatically illustrate the classical tension: nationalism vs.… weiterlesen  “nationalism vs. national state”