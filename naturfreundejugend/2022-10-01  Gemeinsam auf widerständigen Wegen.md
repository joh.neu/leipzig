---
id: 20220905T2100Z-1662411629.6558-EO-1163-1
title: " Gemeinsam auf widerständigen Wegen"
start: 2022-10-01 10:00
end: 2022-10-01 18:00
link: https://naturfreundejugend-leipzig.de/events/event/gemeinsam-auf-widerstaendigen-wegen/
teaser: " Gemeinsam mit ortskundigen Referent_innen des AKuBiZ e.V. aus Pirna
  wandern wir am 1. Oktober 2022 "
isCrawled: true
---
 Gemeinsam mit ortskundigen Referent_innen des AKuBiZ e.V. aus Pirna wandern wir am 1. Oktober 2022 zur Burg Hohnstein in der Sächsischen Schweiz, um uns mit der wechselhaften Geschichte des Ortes zu befassen, der vor 1933 eine der größten und schönsten Jugendherbergen Deutschlands war und nach der Besetzung durch die SA als frühes Konzentrationslager genutzt wurde.… weiterlesen  “Gemeinsam auf widerständigen Wegen”