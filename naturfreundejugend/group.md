---
name: Naturfreundejugend Leipzig
website: https://www.naturfreundejugend.de/
email: leipzig@naturfreundejugend.de
scrape:
  source: ical
  options:
    url: https://naturfreundejugend-leipzig.de/feed/eo-events/
---
Die NATURFREUNDEJUNGEND LEIPZIG ist eine Gruppe im Aufbau. Wir sind ein parteiunabhängiger, linker Jugendverband, in dem Jugendliche selbstorganisiert herrschaftskritische Politik machen, Veranstaltungen und Aktionen organisieren oder zusammen verreisen.