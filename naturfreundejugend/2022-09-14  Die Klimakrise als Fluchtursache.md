---
id: 20220905T2100Z-1662411629.6513-EO-1164-1
title: " Die Klimakrise als Fluchtursache"
start: 2022-09-14 18:00
end: 2022-09-14 21:00
address: Stadtgarten H17
link: https://naturfreundejugend-leipzig.de/events/event/die-klimakrise-als-fluchtursache/
teaser: " Ende 2020 waren 82,4 Millionen Menschen weltweit auf der Flucht. Dies
  entspricht etwa einem Prozent"
isCrawled: true
---
 Ende 2020 waren 82,4 Millionen Menschen weltweit auf der Flucht. Dies entspricht etwa einem Prozent der Weltbevölkerung und ist die höchste Zahl, die jemals vom Flüchtlingshilfswerk der Vereinten Nationen (UNHCR) verzeichnet wurde. Parallel dazu steigen die globale Durchschnittstemperatur, die CO2- und Methankonzentration sowie der Meeresspiegel Jahr für Jahr an. Doch wie hängen diese beiden Entwicklungen im Detail zusammen?… weiterlesen  “Die Klimakrise als Fluchtursache”