---
id: 20220323T1358Z-1648043883.6929-EO-1085-1
title: " Sommercamp"
start: 2022-07-27 00:00
end: 2022-08-02 00:00
address: Nahe Berlin
link: https://naturfreundejugend-leipzig.de/events/event/sommercamp/
teaser: " Vom Mittwoch 27. Juli bis Montag 1. August findet wieder unser Camp
  statt. Auf einem wunderschönen "
isCrawled: true
---
 Vom Mittwoch 27. Juli bis Montag 1. August findet wieder unser Camp statt. Auf einem wunderschönen Gelände ganz in der Nähe von Berlin. Mit Workshopprogramm von Donnerstag bis Sonntag und auf Spendenbasis.

Für Anmeldung und Anfragen aller art, wendet euch Bitte per Mail an camp(at)naturfreundejugend.de