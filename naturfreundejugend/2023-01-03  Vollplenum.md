---
id: 20230102T1423Z-1672669382.4723-EO-1192-1
title: " Vollplenum"
start: 2023-01-03 19:00
end: 2023-01-03 21:00
address: Ort auf Anfrage
link: https://naturfreundejugend-leipzig.de/events/event/vollplenum-8/
teaser: " Auch 2023 wieder: Alle zwei Wochen Orgaplenum für alle. Neu dabei?
  Hier ist dein Startpunkt bei uns"
isCrawled: true
---
 Auch 2023 wieder: Alle zwei Wochen Orgaplenum für alle. Neu dabei? Hier ist dein Startpunkt bei uns mit zu machen, schreib‘ uns einfach eine Mail.