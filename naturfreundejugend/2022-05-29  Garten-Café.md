---
id: 20220426T1900Z-1650999638.8121-EO-1133-1
title: " Garten-Café"
start: 2022-05-29 15:00
end: 2022-05-29 18:00
address: Stadtgarten H17
link: https://naturfreundejugend-leipzig.de/events/event/garten-cafe-2/
teaser: " Wir laden zu Kaffe, Limo und Kuchen in den schönen Stadtgarten H17
  ein. Kommt mit uns ins Gespräch,"
isCrawled: true
---
 Wir laden zu Kaffe, Limo und Kuchen in den schönen Stadtgarten H17 ein. Kommt mit uns ins Gespräch, ins Diskutieren, lernt uns kennen oder nehmt euch einfach einen Tisch mit euren Friends und genießt den aufkommenden Sommer.