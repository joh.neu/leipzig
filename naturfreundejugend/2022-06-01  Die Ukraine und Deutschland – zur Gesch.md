---
id: 20220510T1700Z-1652202023.5833-EO-1136-1
title: " Die Ukraine und Deutschland – zur Geschichte eines asymetrischen
  Verhältnisses in historischer Perspektive"
start: 2022-06-01 19:00
end: 2022-06-01 21:00
address: Galerie KUB
link: https://naturfreundejugend-leipzig.de/events/event/die-ukraine-und-deutschland-zur-geschichte-eines-asymetrischen-verhaeltnisses-in-historischer-perspektive/
teaser: " Mit Johannes Spohr (Historiker) und angefragt: Irina
  Bondes (Autorin)Eine Veranstaltung der VVN-Bd"
isCrawled: true
---
 Mit Johannes Spohr (Historiker) und angefragt: Irina Bondes (Autorin)

Eine Veranstaltung der VVN-BdA Leipzig, der Naturfreundejugend Leipzig und der RLS Sachsen

Der genaue Veranstaltungsort wird noch bekannt gegeben



Infolge des Krieges der Russischen Föderation gegen die Ukraine seit dem 24. Februar 2022 entwickeln sich auch deutsch-ukrainische Beziehungen unter neuen Vorzeichen. Dies drückt sich auf politisch-repräsentativer Ebene, aber auch in der Berichterstattung oder in den Hilfsaktionen für Geflüchtete aus.… weiterlesen  “Die Ukraine und Deutschland – zur Geschichte eines asymetrischen Verhältnisses in historischer Perspektive”