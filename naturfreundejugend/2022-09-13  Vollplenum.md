---
id: 20220621T1900Z-1655838038.1263-EO-1154-1
title: " Vollplenum"
start: 2022-09-13 19:00
end: 2022-09-13 21:00
address: Ort auf Anfrage
link: https://naturfreundejugend-leipzig.de/events/event/vollplenum-7/
teaser: " Unser öffentliches Vollplenum, zurück aus der Sommerpause 🙂"
isCrawled: true
---
 Unser öffentliches Vollplenum, zurück aus der Sommerpause 🙂