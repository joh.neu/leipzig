---
id: 20220511T0723Z-1652253782.6777-EO-1138-1
title: " Vollplenum"
start: 2022-05-24 19:00
end: 2022-05-24 21:00
address: Ort auf Anfrage
link: https://naturfreundejugend-leipzig.de/events/event/vollplenum-6/
teaser: " Unser öffentliches Vollplenum, endlich wieder in Person"
isCrawled: true
---
 Unser öffentliches Vollplenum, endlich wieder in Person