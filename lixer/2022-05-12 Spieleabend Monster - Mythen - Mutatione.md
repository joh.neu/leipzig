---
id: 838-1652382000-1652394600
title: "Spieleabend: Monster - Mythen - Mutationen"
start: 2022-05-12 19:00
end: 2022-05-12 22:30
link: https://zschocher.com/event/spieleabend-monster-mythen-mutationen-2/
teaser: monster – mythen – mutationen am donnerstag, 12.5., findet um 19 uhr wie
  jeden 2. donnerstag im mon
isCrawled: true
---
monster – mythen – mutationen 

am donnerstag, 12.5., findet um 19 uhr wie jeden 2. donnerstag im monat der spieleabend im lixer statt. schwerpunkt auf komlexen und länger dauernden brettspielen. 

lixer    pörstener Straße 9   leipzig-kleinzschocher 

***3G***