---
id: 729-1647457200-1647464400
title: "Lesung im Rahmen von Leipzig Liest: Cornelia Lotter: Himmel über den Mauern"
start: 2022-03-16 19:00
end: 2022-03-16 21:00
address: Stadtteilladen Lixer, Pörstner Straße, Leipzig, Sachsen, 04229, Deutschland
link: https://zschocher.com/event/lesung-im-rahmen-von-leipzig-liest-cornelia-lotter-himmel-ueber-den-mauern/
teaser: Lesung im Lixer e.V. um 19:00 und Stream  auf zschocher.com     Für die
  Präsenzveranstaltung gilt
isCrawled: true
---
Lesung im Lixer e.V. um 19:00 und Stream  auf zschocher.com 

  

  

Für die Präsenzveranstaltung gilt  2g+ ! Bitte bringt einen tagesaktuellen Test oder testet euch gegen Spende ab 18:30  vor dem Lixer e.V.  

 

  

  

  

Cornelia Lotter liest aus ihrem Roman: Himmel über den Mauern 

Eine packende Geschichte über das ehemalige Wehrmachtsgefängnis und  späteren Jugendwerkhof in Torgau. 

Simone findet bei der Auflösung des Hausstandes ihrer Mutter Dokumente, die nahelegen, dass ihr Großvater, dessen Schicksal in der Familie stets totgeschwiegen wurde, während des Zweiten Weltkrieges im Wehrmachtgefängnis in Torgau inhaftiert gewesen ist. 

Dieser Ort im Sächsischen, von dem Simone noch nie etwas gehört hat, ist für Matthias mit traumatischen Erlebnissen im einzigen Geschlossenen Jugendwerkhof der DDR verbunden. Noch Jahrzehnte danach leidet er unter Albträumen und befindet sich in therapeutischer Behandlung. 

Die Bankangestellte aus Stuttgart und der arbeitslose Mann aus Leipzig begegnen sich in dem malerischen Städtchen an der Elbe und tauchen ein in das, was sie ganz persönlich mit diesem Ort verbindet. Einem Ort, an dem wehrlose Menschen aus politischen Gründen gebrochen werden sollten. 

Am Ende erhält Simone die Antwort darauf, welch ungeheuren Frevel ihr Großvater in den Augen ihrer Familie begangen hat.