---
id: 870-1655823600-1655838000
title: Bürokratiesprechstunde
start: 2022-06-21 15:00
end: 2022-06-21 19:00
link: https://zschocher.com/event/buerokratiesprechstunde-3/
teaser: Brauchst du Unterstützung beim Ausfüllen von Anträgen? Oder Hilfe beim
  Formulieren von Ämterschreibe
isCrawled: true
---
Brauchst du Unterstützung beim Ausfüllen von Anträgen? Oder Hilfe beim Formulieren von Ämterschreiben und Widersprüchen? Wir bieten in unserer Bürokratie-Sprechstunde Orientierung im Umgang mit Ämtern sowie dem allgemeinen Bürokratiedschungel. Bei besonders komplizierten Fällen vermitteln wir an Stellen, die sich auf dein Anliegen spezialisiert haben. 

Wann?

Jeden Dienstag von 15:00 Uhr – 19:00 Uhr

Wo?

Im Lixer Laden in der Pörstener Straße 9 

Die Bürokratie-Sprechstunde umfasst zum Beispiel Hilfen zu den Themen Jugendamt, Jobcenter, Wohnthemen (z.B. Wohngeld, Streitigkeiten mit Vermieter*innen etc.), Kranken-, Renten- und Pflegekasse. 

 