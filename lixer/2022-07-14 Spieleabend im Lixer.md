---
id: 874-1657825200-1657836000
title: Spieleabend im Lixer
start: 2022-07-14 19:00
end: 2022-07-14 22:00
link: https://zschocher.com/event/spieleabend-im-lixer-3/
teaser: "_.------.                        .----.__           /         \\_.       \
  ._           /---.__  \\   "
isCrawled: true
---
_.------.                        .----.__

           /         \_.       ._           /---.__  \

          |  O    O   |\___  //|          /       `\ |

          |  .vvvvv.  | )   `(/ |         | o     o  \|

          /  |     |  |/      \ |  /|   ./| .vvvvv.  |\

         /   `^^^^^'  / _   _  `|_ ||  / /| |     |  | \

       ./  /|         | O)  O   ) \|| //' | `^vvvv'  |/\

      /   / |         \        /  | | ~   \          |  \

      \  /  |        / \ Y   /'   | \     |          |   ~

       `'   |  _     |  `._/' |   |  \     7        /

         _.-'-' `-'-'|  |`-._/   /    \ _ /    .    |

    __.-'            \  \   .   / \_.  \ -|_/\/ `--.|_

 --'                  \  \ |   /    |  |              `-

                       \uU \UU/     |  /   :F_P:

mit kleinen, gefährlichen plastikmonstern die welt beherschen – oder  untergehen 

Spieleabend im Lixer am 14.7. um 19 Uhr