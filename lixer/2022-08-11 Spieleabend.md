---
id: 916-1660244400-1660255200
title: Spieleabend
start: 2022-08-11 19:00
end: 2022-08-11 22:00
link: https://zschocher.com/event/spieleabend/
teaser: "_.------.                        .----.__           /         \\_.       \
  ._           /---.__  \\   "
isCrawled: true
---
_.------.                        .----.__

           /         \_.       ._           /---.__  \

          |  O    O   |\___  //|          /       `\ |

          |  .vvvvv.  | )   `(/ |         | o     o  \|

          /  |     |  |/      \ |  /|   ./| .vvvvv.  |\

         /   `^^^^^'  / _   _  `|_ ||  / /| |     |  | \

       ./  /|         | O)  O   ) \|| //' | `^vvvv'  |/\

      /   / |         \        /  | | ~   \          |  \

      \  /  |        / \ Y   /'   | \     |          |   ~

       `'   |  _     |  `._/' |   |  \     7        /

         _.-'-' `-'-'|  |`-._/   /    \ _ /    .    |

    __.-'            \  \   .   / \_.  \ -|_/\/ `--.|_

 --'                  \  \ |   /    |  |              `-

                       \uU \UU/     |  /   :F_P:

mit kleinen, gefährlichen plastikmonstern die welt beherschen – oder  untergehen 

Spieleabend im Lixer am 11.8. um 19 Uhr