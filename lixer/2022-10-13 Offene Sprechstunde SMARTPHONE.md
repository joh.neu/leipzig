---
id: 925-1665669600-1665676800
title: Offene Sprechstunde SMARTPHONE
start: 2022-10-13 14:00
end: 2022-10-13 16:00
link: https://zschocher.com/event/offene-sprechstunde-smartphone/
teaser: Offene Sprechstunde für Senioren und andere Altersklassen.In gemütlicher
  Atmosphäre, bei Käffchen u
isCrawled: true
---
Offene Sprechstunde für Senioren und andere Altersklassen.

In gemütlicher Atmosphäre, bei Käffchen und Knabbereien die Hürden des praktischen Helfers meistern. 

Kostenfrei und ohne Anmeldung. 2G+ beachten