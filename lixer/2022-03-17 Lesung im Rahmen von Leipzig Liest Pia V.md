---
id: 737-1647543600-1647550800
title: 'Lesung im Rahmen von Leipzig Liest: Pia Volk: "Deutschlands schrägste Orte"'
start: 2022-03-17 19:00
end: 2022-03-17 21:00
address: Stadtteilladen Lixer, Pörstner Straße, Leipzig, Sachsen, 04229, Deutschland
link: https://zschocher.com/event/lesung-im-rahmen-von-leipzig-liest-pia-volk-die-schraegsten-orte-deutschlands/
teaser: "Lesung im Lixer e.V. um 19:00 und Stream  auf zschocher.com Für die
  Präsenzveranstaltung gilt  2g+ "
isCrawled: true
---
Lesung im Lixer e.V. um 19:00 und Stream  auf zschocher.com 

Für die Präsenzveranstaltung gilt  2g+ ! Bitte bringt einen tagesaktuellen Test oder testet euch gegen Spende ab 18:30  vor dem Lixer e.V.  

 

  

  

  

  

  

  

  

  

  

  

  

  

  

  

Pia Volk ist einen Pfad entlanggewandert, der über das Gelände eines Atomkraftwerks führt, und hat einen Truppenübungsplatz durchquert auf dem Weg zu mächtigen Gräbern, von denen niemand weiß, wie sie gebaut wurden. Sie ist über eine mit Hohlräumen durchsetzte Felslandschaft gesprungen, in der alles Wasser verschwindet, und hat sich erklären lassen, wie man von einem Kirchturm auf das wohl gigantischste Ereignis der deutschen Erdgeschichte schließen kann. Sie hat sich sorbische Märchen angehört. saterfriesische Sprichwörter, sowie Töne, die Jahre anhalten. Über all diese bizarren Landschaften, exzentrischen Welten und obskuren Objekte berichtet sie. Spannend und unterhaltsam führt sie zu geographischen und historischen Kuriositäten und lehrt uns, das eigene Land mit neuen Augen zu sehen. 

Pia Volk schreibt. Meist stolpert sie eher zufällig über Themen, trifft Menschen, die sie außergewöhnlich findet, hört von Orten, die seltsam sind, oder ihr werden Geschichten erzählt, die sie innehalten lassen. Ihre Texte sind in der Frankfurter Allgemeinen Sonntagszeotung, der Süddeutsche Zeitung, der Zeit und in vielen verschiedenen Magazinen erschienen. Als Erzählerin hört man sie bei Deutschlandfunk Nova. Pia Volk hat Geographie und Ethnologie studiert und nach ihrem Abschluss ein Journalistikstudium in Leipzig absolviert. Von Leipzig aus erkundet sie die große, weite Welt. 

  

 