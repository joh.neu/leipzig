---
id: 922-1662663600-1662678000
title: Spieleabend
start: 2022-09-08 19:00
end: 2022-09-08 23:00
link: https://zschocher.com/event/spieleabend-2/
teaser: "mit plastikmonstern unheil anrichten! jeder 2. donnerstag im monat um
  19 uhr: fokus auf nerdige, k"
isCrawled: true
---
mit plastikmonstern unheil anrichten! 

jeder 2. donnerstag im monat um 19 uhr: 

fokus auf nerdige, komplexe brettspiele 

Der Spieleabend im Lixer ist ein Treffpunkt für anspruchsvollen Spaß.

Die Spiele können schon mal gute vier Stunden dauern – das Universum zu

erobern ist halt keine leichte Aufgabe… . 

Wenn ihr mal die kleine, bösartige Weltbeherrscherin in euch ausleben

wollt, ist das die Gelegenheit. Aber es gibt nur eine Gewinnerin.