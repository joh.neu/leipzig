---
name: Lixer - der Demokratieladen für Zschocher
website: https://www.facebook.com/pg/LixerinZschocher
email: lixer@riseup.net
address: null
scrape:
  source: ical
  options:
    url: https://zschocher.com/events/?ical=1
---
Der Lixer e.V. ist eine junge Initiative, die mit einem Stadtteilladen in Kleinzschocher zu einem lebenswerten Viertel beitragen will. Wir wollen uns gegen rechte Raumnahme wehren und einen solidarischen Freiraum für Alle schaffen.