---
id: 930-1669298400-1669305600
title: Offene Sprechstunde SMARTPHONE
start: 2022-11-24 14:00
end: 2022-11-24 16:00
address: Sachsen
link: https://zschocher.com/event/offene-sprechstunde-smartphone-3/
teaser: Offene Sprechstunde für Senioren und andere Altersklassen.In gemütlicher
  Atmosphäre, bei Käffchen u
isCrawled: true
---
Offene Sprechstunde für Senioren und andere Altersklassen.

In gemütlicher Atmosphäre, bei Käffchen und Knabbereien die Hürden des praktischen Helfers meistern. 

Kostenfrei und ohne Anmeldung. 2G+ beachten