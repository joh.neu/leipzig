---
id: 743-1647716400-1647716400
title: "Lesung im Rahmen von Leipzig Liest: Ronya Othmann über den Genozid an
  den Jesid:innen"
start: 2022-03-19 19:00
end: 2022-03-19 19:00
address: Stadtteilladen Lixer, Pörstner Straße, Leipzig, Sachsen, 04229, Deutschland
link: https://zschocher.com/event/lesung-im-rahmen-von-leipzig-liest-ronya-othmann-ueber-den-genozid-an-den-jesidinnen/
teaser: Lesung im Lixer e.V. um 19:00 und Stream  auf zschocher.com     Für die
  Präsenzveranstaltung gilt
isCrawled: true
---
Lesung im Lixer e.V. um 19:00 und Stream  auf zschocher.com 

  

  

Für die Präsenzveranstaltung gilt  2g+ ! Bitte bringt einen tagesaktuellen Test oder testet euch gegen Spende ab 18:30  vor dem Lixer e.V.  

 

Ronya Othmann schreibt Prosa, Gedichte und Essays. Erste Arbeiten von ihr erschienen unter anderem in BELLA triste und im Jahrbuch der Lyrik. Gemeinsam mit Yevgeniy Breyger, Özlem Özgül Dündar und anderen gab sie 2017 die Lyrikanthologie Ansicht der leuchtenden Wurzeln von unten heraus. Im August 2020 erschien im Hanser Verlag ihr Debütroman Die Sommer, der anhand einer Familiengeschichte den Bürgerkrieg in Syrien und die Ermordung der Jesiden durch den Islamischen Staat reflektiert.  Das Werk gelangte auf die Shortlist des aspekte-Literaturpreises. In ihrem ersten Gedichtband die verbrechen (2021) bezieht sich Othmann auf ein müdes, müde Land, das Kritiker mit Kurdistan identifizierten. Geschichtliche und ideologische Spuren eines Jahrhunderts verwebt sie darin mit den Erinnerungen der Sprecherin. In Medien wie Der Spiegel, taz oder Zeit Online veröffentlichte Othmann auch journalistische Texte. 

Auf Einladung von Insa Wilke nahm Ronya Othmann im Juni 2019 an den 43. Tagen der deutschsprachigen Literatur (Ingeborg-Bachmann-Preis) in Klagenfurt teil und las dort ihren Text Vierundsiebzig, der den Genozid an den Jesiden zum Gegenstand hat. Ihr Beitrag gewann den Publikumspreis. Damit wurde Ronya Othmann für sechs Monate auch Klagenfurter Stadtschreiberin. Das Stipendium begann im Mai 2020.