---
id: 932-1666899000-1666904400
title: geschichte der naturfreunde – vortrag von jürgen lorenz
start: 2022-10-27 19:30
end: 2022-10-27 21:00
address: Sachsen
link: https://zschocher.com/event/geschichte-der-naturfreunde-vortrag-von-juergen-lorenz/
teaser: geschichte der naturfreunde vortrag von jürgen lorenzdonnerstag,
  27.10.2219:30 im lixerpörstener
isCrawled: true
---
geschichte der naturfreunde 

vortrag von jürgen lorenz

donnerstag, 27.10.22

19:30 im lixer

pörstener straße 9 leipzig kleinzschocher 

1895 wurde die naturfreunde als arbeitersportverein in österreich

gegründet. in den 20ern spalteten sich die naturfreunde wie die meisten

arbeiterinnenorganisationen in einen spd nahen und einen kpd nahen

flügel. obwohl in 30ern verboten, waren viele naturfreundInnen in

verschiedene widerstandsaktivitäten verstrickt. 

inzwischen haben die naturfreundInnen international 350 000 mitglieder.