---
id: 746-1647792000-1647799200
title: "Lesung im Rahmen von Leipzig Liest: Volkmar Wölk über die Neue Rechte"
start: 2022-03-20 16:00
end: 2022-03-20 18:00
address: Stadtteilladen Lixer, Pörstner Straße, Leipzig, Sachsen, 04229, Deutschland
link: https://zschocher.com/event/lesung-im-rahmen-von-leipzig-liest-volkmar-woelk-ueber-die-neue-rechte/
teaser:   Lesung im Lixer e.V. um 16:00 und Stream  auf zschocher.com     Für
  die Präsenzveranstaltung g
isCrawled: true
---
  

Lesung im Lixer e.V. um 16:00 und Stream  auf zschocher.com 

  

  

Für die Präsenzveranstaltung gilt  2g+ ! Bitte bringt einen tagesaktuellen Test oder testet euch gegen Spende ab 15:30  vor dem Lixer e.V.  

 

  

Volkmar Wölk, Jg. 1957, Mitarbeiter der Zeitschrift der Rechte Rand. Themenschwerpunkte: ‚Neue‘ Rechte und Nationalrevolutionäre, Ideologieentwicklung der extremen Rechten, Europakonzeption der extremen Rechten. 

Im Lixer e.V. wird er aus seinen neuesten Texten lesen.