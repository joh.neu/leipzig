---
id: 997-1667588400-1667595600
title: Das Transitghetto Bełżyce in der “Aktion Reinhardt - Vortrag und Diskussion
start: 2022-11-04 19:00
end: 2022-11-04 21:00
address: Stadtteilladen Lixer, Pörstner Straße, Leipzig, Sachsen, 04229, Deutschland
link: https://zschocher.com/event/das-transitghetto-belzyce-in-der-aktion-reinhardt-vortrag-und-diskussion/
teaser: Eine Veranstaltung der VVN-BdA Leipzig und der RLS Sachsen Mit Steffen
  Hänschen (Historiker, Bildun
isCrawled: true
---
Eine Veranstaltung der VVN-BdA Leipzig und der RLS Sachsen 

Mit Steffen Hänschen (Historiker, Bildungswerk Stanisław Hantz e.V.) 

Die große Mehrheit der Juden und Jüdinnen, die Ende 1941 noch auf dem Gebiet des Deutschen Reichs lebten, wurde 1942 “nach Osten” deportiert und ermordet. Auch aus dem Protektorat Böhmen und Mähren, der Slowakei und Luxemburg verschleppten die Nationalsozialisten weite Teile der jüdischen Bevölkerung in die Ghettos und Lager im besetzten Polen. Ein Hauptziel war der Distrikt Lublin, wo die Deportierten nicht sofort getötet, sondern zunächst auf Transitghettos verteilt wurden. Eines davon war das Ghetto Bełżyce.

Am 10. Mai 1942 verließ ein Deportationstransport mit 1002 Jüdinnen und Juden die Stadt Leipzig. Am 12. Mai erreichte der Transport die Stadt Bełżyce. 1942 begannen die “Aussiedlungen” in die Mordlager der “Aktion Reinhardt”: nach Bełżec, Sobibór und Treblinka. Die große Verschleppung von 2500 Jüdinnen und Juden aus Bełżyce fand am 13. Oktober wahrscheinlich in das Mordlager Sobibór statt. Andere kamen in Arbeitslager der Region. Die letzte kleine Gruppe jüdischer Bewohner*innen wurde schließlich im April 1943 auf dem jüdischen Friedhof erschossen.

Im Zentrum des Vortrags stehen die Deportationen nach Bełżyce und das Geschehen am Ort. 

—

Dr. Steffen Hänschen engagiert sich im Rahmen seiner Tätigkeit im Bildungswerk Stanisław Hantz e.V. in der Lubliner Region. 

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.