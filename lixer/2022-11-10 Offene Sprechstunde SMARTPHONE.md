---
id: 928-1668088800-1668096000
title: Offene Sprechstunde SMARTPHONE
start: 2022-11-10 14:00
end: 2022-11-10 16:00
address: Sachsen
link: https://zschocher.com/event/offene-sprechstunde-smartphone-2/
teaser: Offene Sprechstunde für Senioren und andere Altersklassen.In gemütlicher
  Atmosphäre, bei Käffchen u
isCrawled: true
---
Offene Sprechstunde für Senioren und andere Altersklassen.

In gemütlicher Atmosphäre, bei Käffchen und Knabbereien die Hürden des praktischen Helfers meistern. 

Kostenfrei und ohne Anmeldung. 2G+ beachten