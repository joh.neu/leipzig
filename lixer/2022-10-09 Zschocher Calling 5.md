---
id: 973-1665324000-1665342000
title: Zschocher Calling 5
start: 2022-10-09 14:00
end: 2022-10-09 19:00
link: https://zschocher.com/event/zschocher-calling-5/
teaser: Unser jährliches Stadtteilfest Zschocher Calling geht nun schon in die
  5. Runde. Am Sonntag 9. Oktob
isCrawled: true
---
Unser jährliches Stadtteilfest Zschocher Calling geht nun schon in die 5. Runde. Am Sonntag 9. Oktober verwandeln wir den Schwartzepark in einen gemütlichen Treffpunkt um Nachbar*innen sowie ortsansässige Vereine und Initiativen kennenzulernen. Neben Infos und Musik gibt es eine Kinderbetreuung und ~ptogramm sowie Speis&Trank (gegen Spende)

Kommt gern vorbei wir freuen uns auf euch 🙂 

Zeitgleich dürft ihr im LIXER die Ausstellung einer lokalen Künstlerin begutachten uns mit dieser ins Gespräch kommen 

Dort gibt es direkt Im Anschluss ab 19Uhr wieder KüfA (Küche für Alle)

diesmal mit Musik von „La Poche à trou“ zum gemütlichen Ausklang des Tages