---
id: 865-1654614000-1654628400
title: Bürokratiesprechstunde
start: 2022-06-07 15:00
end: 2022-06-07 19:00
link: https://zschocher.com/event/buerokratiesprechstunde/
teaser: Brauchst du Unterstützung beim Ausfüllen von Anträgen? Oder Hilfe beim
  Formulieren von Ämterschreibe
isCrawled: true
---
Brauchst du Unterstützung beim Ausfüllen von Anträgen? Oder Hilfe beim Formulieren von Ämterschreiben und Widersprüchen? Wir bieten in unserer Bürokratie-Sprechstunde Orientierung im Umgang mit Ämtern sowie dem allgemeinen Bürokratiedschungel. Bei besonders komplizierten Fällen vermitteln wir an Stellen, die sich auf dein Anliegen spezialisiert haben. 

Wann?

Jeden Dienstag von 15:00 Uhr – 19:00 Uhr

Wo?

Im Lixer Laden in der Pörstener Straße 9 

Die Bürokratie-Sprechstunde umfasst zum Beispiel Hilfen zu den Themen Jugendamt, Jobcenter, Wohnthemen (z.B. Wohngeld, Streitigkeiten mit Vermieter*innen etc.), Kranken-, Renten- und Pflegekasse. 

Kontaktiere uns gern unter lixer_buerokratie@riseup.net oder komm einfach direkt vorbei! Komm bitte getestet.