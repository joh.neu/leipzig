---
id: 752-1647804600-1647811800
title: Buchvorstellung im Rahmen von Leipzig Liest Eva von Redecker "Revolution
  für das Leben" Diskussion und Autor:innengespräch
start: 2022-03-20 19:30
end: 2022-03-20 21:30
address: Stadtteilladen Lixer, Pörstner Straße, Leipzig, Sachsen, 04229, Deutschland
link: https://zschocher.com/event/buchvorstellung-im-rahmen-von-leipzig-liest-eva-von-redecker-revolution-fuer-das-leben-diskussion-und-autorinnengespraech/
teaser: Lesung im Lixer e.V. um 19:30 und Stream  auf zschocher.com     Für die
  Präsenzveranstaltung gil
isCrawled: true
---
Lesung im Lixer e.V. um 19:30 und Stream  auf zschocher.com 



  

  

Für die Präsenzveranstaltung gilt  2g+ ! Bitte bringt einen tagesaktuellen Test oder testet euch gegen Spende ab 19:00 vor dem Lixer e.V.  





Revolution für das Leben

Philosophie der neuen Protestformen







Eine neue Kapitalismuskritik – und eine Liebeserklärung an menschliches Handeln 

In Zeiten der Krise entzündet sich politisches Engagement. Protestbewegungen wie Black Lives Matter, Fridays for Future und NiUnaMenos kämpfen derzeit weltweit gegen Rassismus, Klimakatastrophe und Gewalt gegen Frauen.

So unterschiedlich sie scheinen mögen, verfolgen diese Widerstandskräfte doch ein gemeinsames Ziel: die Rettung von Leben. Im Kern richtet sich ihr Kampf gegen den Kapitalismus, der unsere Lebensgrundlagen zerstört, indem er im Namen von Profit und Eigentum lebendige Natur in toten Stoff verwandelt: Der Kapitalismus verwertet uns und unseren Planeten rücksichtslos. In autoritären Tendenzen und rassistischen Ausschreitungen, in massiven Klimaveränderungen und einer globalen Pandemie zeigt er seine verheerendsten Seiten.

In den neuen Protestformen erkennt Eva von Redecker, die als Philosophin zu Fragen der Kritischen Theorie forscht und auf einem Biohof aufgewachsen ist, die Anfänge einer Revolution für das Leben, die die zerstörerische kapitalistische Ordnung stürzen könnte und unseren grundlegenden Tätigkeiten eine neue solidarische Form verspricht: Wir könnten pflegen statt beherrschen, regenerieren statt ausbeuten, teilhaben statt verwerten.

Die erste philosophische Analyse des neuen Aktivismus. 

»Eine der aufregendsten Nachwuchsphilosophinnen des Landes.« Philosophie Magazin