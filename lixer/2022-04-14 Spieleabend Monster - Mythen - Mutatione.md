---
id: 832-1649962800-1649975400
title: "Spieleabend: Monster - Mythen - Mutationen"
start: 2022-04-14 19:00
end: 2022-04-14 22:30
link: https://zschocher.com/event/spieleabend-monster-mythen-mutationen/
teaser: monster – mythen – mutationen am donnerstag, 14.4., findet um 19 uhr wie
  jeden 2.donnerstag im mona
isCrawled: true
---
monster – mythen – mutationen 

am donnerstag, 14.4., findet um 19 uhr wie jeden 2.donnerstag im monat der spieleabend im lixer statt. schwerpunkt auf komlexen und länger dauernden brettspielen. 

lixer    pörstener Straße 9   leipzig-kleinzschocher 

***3G*** 

 