---
id: 901-1660557600-1660932000
title: Hundestart on tour
start: 2022-08-15 10:00
end: 2022-08-19 18:00
address: Leipziger Naturfreundehaus Grethen e.V.
link: https://zschocher.com/event/hundestart-on-tour/
teaser: Auf den Spuren der Kleinzschocherer Meute „Hundestart“Die „Leipziger
  Meuten“ waren Jugendliche, die
isCrawled: true
---
Auf den Spuren der Kleinzschocherer Meute „Hundestart“

Die „Leipziger Meuten“ waren Jugendliche, die versucht haben, Widerstand gegen den Nationalsozialismus zu leisten. In ganz Leipzig gab es verschiedene Gruppen. Eine der Gruppen in Kleinzschocher hieß „Hundestart“. Wir wollen von Leipzig aus auf den Spuren der Meute Hundestart mit demFahrrad eine Bildungsreise nach Grethen machen. 

* Bildungsreise – aber auch viel Freizeit 😉 – für ca. 15 Jugendliche zwischen 14-18 Jahren 

** 15.-19. August 2022 zum Naturfreundehaus Grethen mit dem Fahrrad 

*** Die Teilnahme ist kostenlos. Anmeldeschluss ist der 01.08. 

**** Details auf dem Flyer und per Mail (Mailadresse siehe Infoflyer!) 

***** Eine Veranstaltung des Arbeitskreises Hundestart in Kooperation mit dem Lixer e.V. (Stadtteilverein in Kleinzschocher) und KiWest e.V. (Bau- und Aktivspielplatz Leipzig) 

Und Werbung im Radio dazu 

  