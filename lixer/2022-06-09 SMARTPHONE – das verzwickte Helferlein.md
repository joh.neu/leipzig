---
id: 857-1654783200-1654792200
title: SMARTPHONE – das verzwickte Helferlein
start: 2022-06-09 14:00
end: 2022-06-09 16:30
link: https://zschocher.com/event/smartphone-das-verzwickte-helferlein/
teaser: Wir schauen in gemütlicher Atmosphäre vor welche Hindernisse uns das
  Smartphone bei der täglichen An
isCrawled: true
---
Wir schauen in gemütlicher Atmosphäre vor welche Hindernisse uns das Smartphone bei der täglichen Anwendung stellt und finden wir gemeinsam Lösungen. Der Workshop ist kostenfrei und findet unter 2G+ Bedingungen statt. Eine Anmeldung ist nicht erforderlich! 

Für Kaffee, Getränke und Kekse/Kuchen ist gesorgt. Smartphone aufladen und los geht’s.