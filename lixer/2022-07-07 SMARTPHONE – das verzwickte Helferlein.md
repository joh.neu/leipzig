---
id: 862-1657202400-1657211400
title: SMARTPHONE – das verzwickte Helferlein
start: 2022-07-07 14:00
end: 2022-07-07 16:30
link: https://zschocher.com/event/smartphone-das-verzwickte-helferlein-3/
teaser: Wir schauen in gemütlicher Atmosphäre vor welche Hindernisse uns das
  Smartphone bei der täglichen An
isCrawled: true
---
Wir schauen in gemütlicher Atmosphäre vor welche Hindernisse uns das Smartphone bei der täglichen Anwendung stellt und finden wir gemeinsam Lösungen. Der Workshop ist kostenfrei und findet unter 2G+ Bedingungen statt. Eine Anmeldung ist nicht erforderlich! 

Für Kaffee, Getränke und Kekse/Kuchen ist gesorgt. Smartphone aufladen und los geht’s.