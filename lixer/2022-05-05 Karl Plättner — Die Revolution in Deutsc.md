---
id: 829-1651779000-1651784400
title: Karl Plättner — Die Revolution in Deutschland und der mitteldeutsche
  Bandenführer
start: 2022-05-05 19:30
end: 2022-05-05 21:00
link: https://zschocher.com/event/karl-plaettner-die-revolution-in-deutschland-und-der-mitteldeutsche-bandenfuehrer/
teaser: Karl Plättner— Die Revolution in Deutschland und der mitteldeutsche
  Bandenführer In der weitgehend
isCrawled: true
---
Karl Plättner

— Die Revolution in Deutschland und der mitteldeutsche Bandenführer 

In der weitgehend vergessenen und verbogenen Geschichte der deutschen Revolutionsjahre nach dem Ersten Weltkrieg stecken ebenso verschüttete, verzerrte, verkürzte Einzelgeschichten, die meisten heute fast unbekannt, darunter auch die von Karl Plättner, den Daniel Kulla im Rahmen seiner Vorträge über die damalige Revolutionsperiode vorstellen will. 

Plättner ist bereits als Jugendlicher sozialistisch aktiv und während des Weltkriegs politischer Gefangener, dann Beteiligter an fast allen Kämpfen der Revolution, 1920 Mitbegründer der KAPD, 1921 Theoretiker und Praktiker des „revolutionären Bandenkampfs“. Nach den Jahren im Zuchthaus schreibt er über die Haft und besonders über die Sexualität unter Gefangenen, hält dazu Veranstaltungen ab, lebt währenddessen in Leipzig-Leutzsch bis zu seiner Gefangenschaft im NS-Lagersystem: Sachsenburg, Buchenwald, Majdanek, Auschwitz, Mauthausen – befreit durch die Enden beider Weltkriege, überlebt er den Heimweg nicht. 

Es wird um Leipzig, Sachsen und Mitteldeutschland vor, während und nach der Revolution vor hundert Jahren gehen, und darum, wie später im DDR-Kino Plättner mit Max Hoelz zum Anarchisten „Wolz“ verschmolz. Daniel Kulla kommt politisch und geographisch daher, wo der Plättner herkam, und ist im Internet auf classless.org zu finden.