---
id: 826-1651255200-1651266000
title: Verleihung des Big Brother Awards 2022
start: 2022-04-29 18:00
end: 2022-04-29 21:00
link: https://zschocher.com/event/verleihung-des-big-brother-awards-2022/
teaser: Verleihung des Big Brother Awards 2022live Übertragung der
  Preisverleihung von Digitalcourage in Bi
isCrawled: true
---
Verleihung des Big Brother Awards 2022

live Übertragung der Preisverleihung von Digitalcourage in Bielefeld 

Bürgerrechtler.innen verleiht jährlich diesen Datenschutz-Negativpreis an Firmen, Organisationen und Politiker. Alle großen Medien berichten von dem Anlass – der Bielefelder Verein Digitalcourage setzt damit das Thema auf die politische Agenda. 

Die BigBrotherAwards bewirken viel: Sie machten zum Beispiel Rabattkarten, Scoring, Mautkameras, Farbkopierer und Handyüberwachung als Gefahr für Grundrechte und Privatsphäre bekannt. Sie warnten schon früh vor der Gesundheitskarte, der Steuer-ID und der Vorratsdatenspeicherung. Schon lange vor den Skandalen bei Lidl, Telekom, Bahn und Co. wurden die BigBrotherAwards an diese Konzerne verliehen. Ein Abend der gelebten Politik mit den Mitteln des Theaters, geschliffener Rede und Musik. 

Die Veranstaltung wird live aus dem Internet übertragen.