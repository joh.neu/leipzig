---
id: "642981970468085"
title: Ein Prozess in Prag – Das Volk gegen Rudolf Slánský und Genossen
start: 2022-01-20 18:00
locationName: Universitätsbibliothek Leipzig
address: Universitätsbibliothek Leipzig
link: https://www.facebook.com/events/642981970468085/
image: 257596363_5012187202166687_8687201157172709615_n.jpg
isCrawled: true
---
Vortrag und Diskussion mit PD Dr. Jan Gerber
20.01.2022, 18:00 Uhr

Angesichts der aktuellen Lage verlegen wir die Veranstaltung ins Digitale. Bei Interesse bitte eine kurze E-Mail schreiben (dgb-hochschulgruppe@stura.uni-leipzig.de), daraufhin werden wir die Zugangsdaten übermitteln. Wir freuen uns auf eine anregende Veranstaltung und lebhafte Diskussion!

~

Vor siebzig Jahren, im November 1952, fand in Prag der letzte stalinistische Schauprozess statt. Vierzehn hochrangige Funktionäre des Staats- und Parteiapparats der Tschechoslowakei, darunter Rudolf Slánský, der vormalige Generalsekretär der Kommunistischen Partei, wurden beschuldigt, sich gegen die volksdemokratische Ordnung verschworen zu haben. Elf von ihnen wurden zum Tode verurteilt und kurz darauf gehenkt. Der Slánský-Prozess unterschied sich nicht nur in seiner Größendimension von den anderen politischen Prozessen des beginnenden Kalten Kriegs, sondern auch durch seinen offen antisemitischen Charakter. Die Mehrheit der Angeklagten war jüdischer Herkunft; parallel zum Tribunal wurden Juden aus allen mittleren und höheren Positionen des Staats- und Parteiapparats der Tschechoslowakei und vieler anderer Staaten des sowjetischen Machtbereichs verbannt. Aus der DDR flohen hunderte Jüdinnen und Juden in den Westen, weil sie Angst vor erneuter Verfolgung hatten.

Dennoch ging der Slánský-Prozess nicht allein auf die Initiative der Sowjetunion und ihren Interessenwandel im Nahen Osten – weg von Israel, hin zu seinen arabischen Nachbarstaaten – zurück. In ihm verschafften sich zugleich die Nationalitätenkämpfe der Zwischenkriegszeit, die komplizierte Akkulturationsgeschichte der böhmischen und mährischen Juden und die Erfahrung der deutschen Okkupation Ausdruck. Auf einer verborgenen Ebene steht der Slánský-Prozess zudem für das Ende der Geltungskraft des Begriffs der Klasse, der in der Zwischenkriegszeit entscheidend zur Anziehungskraft des Kommunismus auf junge Jüdinnen und Juden beigetragen hatte.

~

PD Dr. Jan Gerber ist leitender wissenschaftlicher Mitarbeiter des Leibniz-Instituts für jüdische Geschichte und Kultur – Simon Dubnow in Leipzig. Zuletzt von ihm erschienen: Karl Marx in Paris. Die Entdeckung des Kommunismus, München: Piper 2018; Ein Prozess in Prag. Das Volk gegen Rudolf Slánský und Genossen, 2. Auflage, Göttingen/Bristol: Vandenhoeck & Ruprecht 2017.