---
name: DGB Hochschulgruppe
website: https://www.facebook.com/pg/dgbhsgleipzig/
email: dgb-hochschulgruppe@stura.uni-leipzig.de
scrape:
  source: facebook
  options:
    page_id: dgbhsgleipzig
---