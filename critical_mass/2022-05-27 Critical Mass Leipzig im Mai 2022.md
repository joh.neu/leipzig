---
id: "1703541466648459"
title: Critical Mass Leipzig im Mai 2022
start: 2022-05-27 18:00
locationName: Augustusplatz
link: https://www.facebook.com/events/1703541466648459/
image: 282151756_1497890830609007_498120631533055871_n.jpg
isCrawled: true
---
Am letzten Freitag im Monat trifft sich die Critical Mass in Leipzig zum Rad fahren.