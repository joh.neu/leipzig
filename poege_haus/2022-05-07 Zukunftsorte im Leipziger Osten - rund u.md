---
id: "475239551062500"
title: Zukunftsorte im Leipziger Osten - rund um die Eisenbahnstraße
start: 2022-05-07 14:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20
link: https://www.facebook.com/events/475239551062500/
image: 278843922_5120871841285035_667271263669883369_n.jpg
isCrawled: true
---
Jährlich zu Anfang Mai findet weltweit das Jane's Walk Festival statt. Einer dieser Walks startet am Pöge-Haus und erkundet Zukunftsorte im Werden: Superblocks und Stadtteilpark Volkmarsdorf, Kino der Jugend, Ostwache und Parkbogen Ost, Quartiersschule Ihmelstraße und Schwimmhalle am Otto-Runki-Platz. 