---
id: "321927479943764"
title: GEFÜHLE-FIGUREN (Schaufenster-Ausstellung)
start: 2022-01-11 00:00
end: 2022-01-11 23:59
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20, 04315 Leipzig
link: https://www.facebook.com/events/321927479943764/
image: 271276041_4785814354790787_4428511900887367257_n.jpg
isCrawled: true
---
Kunst im Kiez“ - Gefühle-Workshop 

Wir – das "Kunst im Kiez"- Kollektiv – trafen uns im Dezember mit Kindern der Fröbel-Kita Spielhaus – ein Austausch über Gefühle und Geschichten der Kinder-Künstler:innen.

Unser Gast war Kobold Knick Knack – eine Stabfigur. 
Kobold Knick Kack wohnt in der Spielhaus-Kita und ist die Figur des KITA-Alltages. Diese Figur und ein kurzer Film mit Figuren der Kinder-Künstler:innen des Spielhauses entstand im Frühjahr 2021 im Rahmen eines Fröbel-Projektes KulturJetzt! mit der Künstlerin Sophie Stephan.

Der Film heißt „Überraschung“
Link:
https://vimeo.com/543127308


Diesmal entstanden bei „Kunst im Kiez“ entstanden Gefühle-Figuren in Körpergröße.  Zu sehen sind die Figuren in der Fensterausstellung im Saal des Pöge-Hauses. Besonders schön ist es, wenn das Haus abends zu leuchten beginnt - ein kleines Leuchten in der grauen Zeit. Die Figuren und verschiedenen Gefühle funkeln gleich mit. Im kleineren Fenster sind Gefühle-Plakate ausgestellt - Papierstreifen jedes einzelnen Gefühls eines jeden Kindes und deren gewählte Farbe wurden zu einem Plakat zusammengefügt.

Die Fenster-Ausstellung ist noch bis zum 15.01.2022 zusehen.
Und wie fühlst du dich gerade so? 