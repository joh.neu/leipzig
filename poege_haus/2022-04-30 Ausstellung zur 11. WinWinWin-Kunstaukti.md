---
id: "3733359036788733"
title: Ausstellung zur 11. WinWinWin-Kunstauktion
start: 2022-04-30 12:00
end: 2022-04-30 15:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20
link: https://www.facebook.com/events/3733359040122066/
image: 277740396_5075770352461851_8782896798917699950_n.jpg
isCrawled: true
---
Der Pöge-Haus e.V. läd in diesem Jahr bereits im Frühling zur 11. Kunstauktion am 30. April 2022.

Präsentiert werden Werke von 36 renomierten und aufstrebenden Künstler*innen der zeitgenössischen Bildenden Kunst aus den Sparten Malerei, Grafik, Fotografie und Skulptur - versteigert werden sie erneut vom redegewandten Künstler und HGB-Dozenten Paule Hammer als Auktionator!

Ab dem 27.04.2022 gibt die Gelegenheit, die Arbeiten in der kuratierten Auktionsausstellung zu besichtigen und in Ruhe auf sich wirken zu lassen. Die Vernissage lockt ab 18 Uhr, um die 11. WinWinWin festlich einklingen zu lassen und mit Künstler*innen und Gästen ins Gespräch zu kommen.

Teilnehmende Künstler*innen 2022:

John Albrecht, Aqeel A. Abdulhussein, Hjördis Baacke, Larissa Barth, Käthe Bauer, Cihan Cakmak, Katja Enders-Plate, Patrick Fauck,  Ruth Habermehl, Kathrin Henschler, Barnabas Herrmann, Fabian Heublein, Madeleine Heublein, Astrid Homuth, Elisabeth Howey, Manuela Kasemir, Caroline Kober, Gabriela Kobus, Alexander König, Annette Krisper-Beslic, Mandy Kunze, Verena Landau, Paula Muhr, Bea Nielsen, Adam Noack, Inka Perl, Julia Peters, André Radke, Max Roßner, Daniela Schönemann, Daniel Skowronek, Sophie Stephan, Elisabeth Stiebritz, Sandra Wehlisch, Alexandra Weidmann, FranziskaWicke

 

Alle Daten auf einen Blick:

Öffnungszeiten der Ausstellung: Mi: 18-21 Uhr, Do+Fr: 16-20.30 Uhr, Sa: 12-15 Uhr

Vernissage: 27.04. 18 Uhr

Auktion: 30.04. 16 Uhr

Foto: Fabian Heublein