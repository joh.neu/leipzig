---
id: "2396126213861296"
title: Etablierte Provisorien. Leipzig und der lange Sommer der Migration.
start: 2022-06-01 18:30
end: 2022-06-01 20:00
locationName: Pöge-Haus e.V.
address: Hedwigstraße 20
link: https://www.facebook.com/events/2396126213861296/
image: 281064894_5198104526895099_730906566573975921_n.jpg
isCrawled: true
---
Leipzig und der lange Sommer der Migration 2015. Überall in der Stadt eröffneten und schlossen behelfsmäßige Notunterkünfte. Notstandsargumente dirigierten die Politik und der Umgang mit den zahlreichen Geflüchteten war Gegenstand mitunter heftiger Auseinandersetzungen. Diese allgegenwärtigkeit des Provisorischen und Konflikthaften, so Philipp Schäfer, war jedoch kein Produkt einer jähen Krise, sondern das Ergebnis andauernder Aushandlungsprozesse. Der Soziologe zeigt, wie lokale Praktiken und Politiken des Regierens von Flucht und Geflüchteten über Jahre provisorische Zustände etablierten, die es erlaubten, asylsuchende Personen räumlich, zeitlich und moralisch in einem Schwebezustand des Noch-nicht-ganz-Angekommenseins festzuhalten. Am 1. Juni kommt Philipp Schäfer mit Julia Leser ins Gespräch. „Etablierte Provisorien“ erscheint Open Access im Campus-Verlag und kann kostenlos heruntergeladen werden.

Philipp Schäfer ist promovierter Soziologe und wissenschaftlicher Mitarbeiter in der Nachwuchsgruppe „Die wissenschaftliche Produktion von Wissen über Migration“ am Institut für Migrationsforschung und Interkulturelle Studien (IMIS) der Universität Osnabrück. Seine Dissertation „Etablierte Provisorien. Leipzig und der lange Sommer der Migration“ ist 2022 Open Access im Campus-Verlag erschienen.

Julia Leser ist promovierte Politikwissenschaftlerin und wissenschaftliche Mitarbeiterin im Projekt “Challenging Populist Truth-Making in Europe (CHAPTER)” an der Humboldt Universität zu Berlin. 2021 erschien ihr Buch “The Wolves are Coming Back: The Politics of Fear in Eastern Germany” (zusammen mit Rebecca Pates) bei Manchester University Press.

Eintritt gegen Spende

Twitter: @PhiSchaefer

Homepage: https://www.campus.de/e-books/wissenschaft/soziologie/etablierte_provisorien-17277.html