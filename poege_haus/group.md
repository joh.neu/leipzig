---
name: Pöge-Haus e.V.
website: https://www.pöge-haus.de
address: Hedwigstraße 20
email: kontakt@verein.poege-haus.de
scrape:
  source: facebook
  options:
    page_id: PoegeHausLeipzig
---
Das Pöge-Haus ist ein offener Raum für vielfältige kulturelle und gesellschaftliche Projekte. Wir unterstützen gesellschaftliches Engagement und setzen uns für ein offenes, vielfältiges, ökologisches und soziales Zusammenleben im Stadtteil, in der Stadtgesellschaft und darüber hinaus ein. Dabei möchten wir gesellschaftliche Konfliktlinien offen legen und diskutieren. Verankert in der Leipziger Neustadt begleiten wir kritisch die Wandlung von Gesellschaft und Stadt, indem wir Kultur, Politik und Stadtteilarbeit verknüpfen. In unseren Projekten setzen wir einen Schwerpunkt auf selbstbestimmte Beteiligung und Bildung. Hier sollen unterschiedliche Formate zur Auseinandersetzung mit Vielfalt und kollektiver Ermächtigung ausprobiert und getestet werden.