---
id: "487414533004061"
title: "Leipzig liest: Aktuelles zur nd.Genossenschaft"
start: 2022-03-16 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/487414533004061/
image: 275144211_4924370880974925_2138308728020372303_n.jpg
isCrawled: true
---
Mit Ines Wallrodt (Genossenschaftsvorstand) und Rouzbeh Taheri (nd-Verlagsleiter)

Im August 2021 wurde von Mitarbeitenden der sozialistischen Tageszeitung »neues deutschland« die nd.Genossenschaft gegründet. Sie ist offen auch für Leserinnen und Leser der Zeitung, von denen bereits weit über 500 Genossenschaftsanteile gezeichnet haben. Inzwischen hat die nd.Genossenschaft eG die Herausgabe und wirtschaftliche Verantwortung für die Zeitung übernommen. Der neue Verlagsleiter Rouzbeh Taheri und Ines Wallrodt als Mitglied des Genossenschaftsvorstandes geben Auskunft über die »Mühen der Ebenen« und den Stand der Dinge.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail interim@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig


