---
id: "346143857496396"
title: Planwirtschaft Reloaded – Brauchen wir einen Plan?
start: 2022-05-16 19:00
locationName: linXXnet
link: https://www.facebook.com/events/346143857496396/
image: 278802779_5035137273238692_2358546626294258160_n.jpg
isCrawled: true
---
Die Planwirtschaft der realexistierenden Sozialismen wird heute als gescheitert betrachtet. Ihr Scheitern ist Ursache, dass heute selbst Linke beim Begriff „Planwirtschaft“ Schnappatmung bekommen.

Vergessen scheinen die revolutionären Ideen die Planwirtschaft mittels modernster Technik von ihren Unzulänglichkeiten zu befreien. Mit dem Projekt Cybersyn sollte im sozialistischen Chile die Produktion entsprechend der Bedürfnisse quasi in Echtzeit gesteuert werden. In der Sowjetunion gab es mit OGAS ein ähnliches Experiment. Und selbst in der DDR gab es, zumindest eine kurze Zeit lang, die Bestrebung das Dogma von der Kybernetik als klassenfeindliche Erfindung der Kapitalisten über Bord zu werfen und ihr Potenzial für wirtschaftliche Reformen anzuwenden.

Doch während viele Linke vom Plan heute nichts hören wollen, greifen Konzerne auf planwirtschaftliche Elemente zurück, um ihre inneren Abläufe zu optimieren. So hat beispielsweise Amazon ein Patent auf ein Verfahren angemeldet, um seine Produkte in die richtigen Regionen zu schicken, noch bevor dessen Bevölkerung weiß, dass es diese überhaupt kaufen will.

Gemeinsam mit Timo Daum und Dr. Eva Bockenheimer wollen wir darüber diskutieren, ob eine geplante Wirtschaft eine linke Alternative zur Anarchie des kapitalistischen Marktes sein kann. Was sind die Potenziale und Chancen, was die Risiken? Welche Herausforderungen bestehen für eine moderne Planwirtschaft? Wir wollen auch darüber reden, warum alleine der Gedanke an die Planwirtschaft bei vielen Schüttelfost auslöst und ob wir uns davon abschrecken lassen sollten.
Die Veranstaltung wird gefördert aus Mitteln des Freistaates Sachsen.

Unsere Referent:innen:
Dr. Eva Bockenheimer ist Philosophin und beschäftigt sich mit der Aktualität von Marx und Hegels.

Timo Daum ist Autor und Mitherausgeber des Buchs „Die Unsichtbare Hand des Plans“.