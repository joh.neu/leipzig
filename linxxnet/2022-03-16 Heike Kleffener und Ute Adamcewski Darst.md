---
id: "895842294427075"
title: Heike Kleffner und Henrik MerkerWie kann über Rechte berichtet werden?
start: 2022-03-16 20:15
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/895842294427075/
image: 274605741_4878902168862204_3633642710382417646_n.jpg
isCrawled: true
---
Heike Kleffner (u.a. taz/Tagesspiegel) und Henrik Merker (u.a. ZEIT Online) nd-Redakteur Robert Meyer, zu dessen Arbeitsschwerpunkten u.a. die Berichterstattung über, Analyse und Kommentierung rechtsradikaler Tendenzen, Entwicklungen und Bewegungen gehört, diskutiert mit Aktivistinnen von #verlagegegenrechts darüber, wie man publizistisch mit rechten Bewegungen umgesehen sollte. 
Wie kann über Rechte berichtet werden? Wie neutral kann oder soll das Dokumentieren rechtsradikalen Gedankenguts sein? 

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig