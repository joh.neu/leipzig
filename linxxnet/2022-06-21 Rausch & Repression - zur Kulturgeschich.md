---
id: "322624283378496"
title: Rausch & Repression - zur Kulturgeschichte der Prohibition
start: 2022-06-21 19:00
locationName: linXXnet
address: Brandstr. 15
link: https://www.facebook.com/events/322624283378496/
image: 281719243_5126035680815517_178107946650444386_n.jpg
isCrawled: true
---
Teil 1 unserer 3-teiligen Veranstaltungsreihe "Progressive Drogenpolitk zwischen Hedonismus und Gesundheitsprävention"

Nach langen Jahren einer dogmengetriebenen Drogenpolitik scheint es nach dem Antritt der neuen Bundesregierung wieder vorsichtige Bemühungen zu geben, mit illegalisierten Substanzen einen pragmatischen Umgang zu finden. Doch die Frage der Legalisierung von Cannabis tangiert nur die Oberfläche eines jahrhundertealten Streitpunktes: der staatlichen Normsetzung von erlaubten und erwünschten Formen des Substanzkonsums bei gleichzeitig scharfer Abgrenzung zum "schädlichen und kriminellen Drogenmissbrauch". 
Ein Blick in die Kulturgeschichte des Rausches zeigt, dass diese eigentlich schon immer vielmehr eine Geschichte der Prohibition war. Unter dem Deckmantel der Gesundheitsprävention und der Betonung der Gefahren des Rausches von Kontrollverlust, Sucht und körperlichem Verfall, ist die Historie der Illegalisierung berauschender Substanzen damit vor allem eine Geschichte des Rassismus und der Unterdrückung von Jugendkulturen. Die Polizei spielt dabei in der exekutiven Durchsetzung staatlicher Drogenpolitik eine besonders große Rolle, und setzt deren repressiven, stigmatisierenden und rassistischen Subtext letztendlich in die Praxis um. 
Damit der gesellschaftliche Umgang mit berauschenden Substanzen progressiv und neu gedacht werden kann, braucht es zuallererst eine klare Analyse, auf welchem Fundament die Misere der Verbotsdoktrin errichtet worden ist - Robert Feustel wird sie in seinem Vortrag "Rausch & Repression - zur Kulturgeschichte der Prohibition"  eindrücklich bieten.

Robert Feustel beschäftigt sich mit politischer Theorie, Kultursoziologie sowie Wissenschaftsgeschichte. Er studierte Politikwissenschaft und Geschichte in Leipzig und Madrid, promovierte zum Thema “Grenzgänge. Kulturen des Rauschs seit der Renaissance” in Leipzig und habilitierte sich im Fach Soziologie in Jena. Robert Feustel lehrt an den Universitäten in Jena und Leipzig.