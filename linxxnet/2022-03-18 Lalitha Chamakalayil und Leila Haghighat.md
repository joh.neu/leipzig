---
id: "681207343242150"
title: "Lalitha Chamakalayil und Leila Haghighat: Bildung - ein Postkoloniales
  Maifest"
start: 2022-03-18 19:00
locationName: linXXnet
address: Brandstr. 15, 04277 Leipzig-Connewitz
link: https://www.facebook.com/events/681207343242150/
image: 274539820_4878678245551263_2509144992085562229_n.jpg
isCrawled: true
---
Was ist Bildung? Die meisten Publikationen, die vorgeben, diese Frage beantworten zu können, führen in der Regel eine Reihe weißer europäischer Männer an, die an Universitäten gelehrt haben oder das immer noch tun. Das bildungsLab* interveniert in diese Selbstverständlichkeit und legt – als Auftakt einer mehrteiligen Reihe unter dem Titel resistance & desire – ein Manifest vor, das dazu anregen möchte, Bildung und Erfahrung zu verknüpfen.

Akademikerinnen* und Künstlerinnen* of Color und/oder mit Migrationsgeschichten reflektieren über Bildung aus unterschiedlichsten Blickwinkeln: mal persönlich, mal theoretisch, mal poetisch, mal witzig, mal ernst – immer aber präzise und klug. Sie fordern das Überdenken der eurozentrisch-humanistischen Bildungsvorstellungen und ein Nachdenken über eine postkoloniale Bildung in einer pluralen Gesellschaft.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.