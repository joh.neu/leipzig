---
id: "683294756239653"
title: Solidarische Landwirtschaft – der Traktor der Revolution?
start: 2022-05-31 19:00
locationName: Demmeringstraße 32, 04177 Leipzig, Deutschland
link: https://www.facebook.com/events/683294756239653/
image: 278976363_4996741220445574_5590513606477082635_n.jpg
isCrawled: true
---
Die solidarische Landwirtschaft ist ein Versuch landwirtschaftliche Produktion kollektiv, demokratisch und nachhaltig zu gestalten. Vielen Linken gilt sie als Projekt der Selbstermächtigung. Aus Kunden und Produzenten werden gleichberechtigte Mitglieder. Der Preis wird durch die Produktionskosten bestimmt, nicht durch Angebot und Nachfrage. Wird mehr produziert, wird auch mehr verteilt. Fällt die Ernte mies aus, gibt es eben weniger für Alle. Ohne Geldzurückgarantie.
Für den Acker bedeutet solidarische Landwirtschaft mehr biologische Vielfalt. Für die Mitarbieter:innen demokratische Mitbestimmung und eine faire Bezahlung. Für die Mitglieder der solidarischen Landwirtschaft gute Lebensmittel und die Aufhebung des klassischen Kunde-Verkäufer-Verhältnisses.
Doch bislang zeigt sich der Kapitalismus unbeeindruckt vom Versuch die agrarische Produktion im basisdemokratischen Plenum zu organisieren. Wir wollen uns deswegen die Frage stellen wie die solidarische Landwirtschaft aus der Vogelperspektive und im Kampf für eine antikapitalistische Alternative zu betrachten ist.
Welche unterschiede gibt es zwischen den verschiedenen Formen der Solawis?
Ist sie ein Erfahrungsraum für Selbstermächtigung aus dem das Bewusstsein hervorgeht, dass der Kapitalismus nicht alternativlos ist?
Ist sie ein Versuch antikapitalistische Freiräume zu erkämpfen und ein Experimentierfeld für alternative Gesellschaftsformen?
Ist sie trauriger Ausdruck, dass die Linke den Kampf um die ganze Bäckerei aufgegeben hat und sich nun mit dem Acker zu Frieden gibt?
Oder ist sie Spielwiese für privilegierte linke Rich Kids und eskapistische Hippies?
 
Auf dem Podium werden sitzen:
Hanno (KOLA)
Karl (Rote Beete)
Wenke (Translib)

https://youtu.be/ON3REz01lxA