---
id: "426929405895470"
title: "Valeria Bruschi und  Moritz Zeiler: Klima-Kapital-Naturverhältnisse
  Buchvorstellung"
start: 2022-03-19 16:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/426929405895470/
image: 274641928_4878887752196979_2459709004167060276_n.jpg
isCrawled: true
---
Der Zwang zur Profitmaximierung untergräbt nach Marx permanent die Quellen allen gesellschaftlichen Reichtums: Natur und Arbeit. Exzessiver Raubbau durch kommerzielle Landwirtschaft, massive Verschnmutzung von Luft, Boden und Wasser durch die Industrie und rapide ansteigende Erderwärmung infolge enormen Energieverbrauchs haben fatale Folgen für das globale Klima. Ein Green New Deal wird bestenfalls zur Modernisierung des Kapitalismus führen, die Klimakrise aber nicht lösen können. Die Autoren des Buches plädieren für Produktion, Verteilung und Konsum nach menschlichen Bedürfnissen wie auch für einen respektvollen Umgang mit der Natur und fordern einen Bruch mit der kapitalistischen Logik.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig
