---
id: "496339218862718"
title: "Leipzig liest: Buchvorstellung von »Das Klima des Kapitals«"
start: 2022-03-19 16:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/496339218862718/
image: 275557253_4943323619079651_8478767943041724814_n.jpg
isCrawled: true
---
Buchvorstellung mit den Herausgeber*innen Valeria Bruschi und Moritz Zeiler

Der Zwang zur Profitmaximierung untergräbt nach Marx permanent die Quellen allen gesellschaftlichen Reichtums: Natur und Arbeit. Exzessiver Raubbau durch kommerzielle Landwirtschaft, massive Verschmutzung von Luft, Boden und Wasser durch die Industrie und rapide ansteigende Erderwärmung infolge enormen Energieverbrauchs haben fatale Folgen für das globale Klima.

Ein Green New Deal wird bestenfalls zur Modernisierung des Kapitalismus führen, jedoch keinen Ausweg aus der Klimakrise. Produktion, Verteilung und Konsum nach menschlichen Bedürfnissen wie auch ein respektvoller Umgang mit der Natur erfordern daher einen Bruch mit der kapitalistischen Logik. Marx konnte zwar die aktuelle Klimakrise nicht vorhersehen, aber sein Werk liefert wichtige Anregungen für aktuelle Diskussionen um einen wünschenswerten Stoffwechsel von Mensch und Natur.

Mit Beiträgen von Martina Backes, Maximilian Becker, Peter Bierl, Valeria Bruschi, Mike Davis, direction f, Alexander Dunlap, Silvia Federici, Julia Fritzsche, Ehrenfried Galander, Jan Hoff, Daniel Hofinger, Stefanie Hürtgen, Guillaume Pitron, Nora Räthzel, Christian Schmidt, Rainer Trampert und Markus Wissen.

»Das Klima des Kapitals. Gesellschaftliche Naturverhältnisse und Ökonomiekritik« Sachbuch.
Erschienen im Verlag Dietz Berlin.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail: tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar: https://www.youtube.com/c/linXXnetLeipzig