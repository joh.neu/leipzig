---
id: "216658550641947"
title: "Leipzig liest: Vorstellung der Initiative #verlagegegenrechts /
  Gesprächsrunde"
start: 2022-03-16 20:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/216658550641947/
image: 275024900_4924394444305902_7135907427939955520_n.jpg
isCrawled: true
---
Leipzig liest: Vorstellung der Initiative #verlagegegenrechts und Gesprächsrunde zur medialen Darstellung rechter Strömungen

#verlagegegenrechts ist ein Aktionsbündnis gegen rassistisches, antifeministisches und homofeindliches Gedankengut. Mittlerweile haben sich über 80 Verlage und 200 Einzelpersonen und Initiativen dem Bündnis angeschlossen. #vgr organisiert u.a. Veranstaltungen auf den Buchmessen in Frankfurt und Leipzig und diskutiert das Jahr über auf Podien. Eine Arbeitsgruppe trifft sich regelmäßig in Berlin, um weitere Aktionen und Positionen zu diskutieren.

Ab 20.15 Uhr: Gesprächsrunde zur medialen Darstellung rechter Strömungen

Wie kann über Rechte berichtet werden? Wie neutral kann oder soll das Dokumentieren rechtsradikalen Gedankenguts sein?

Gesprächspartner: Heike Kleffner (u. a. taz/Tagesspiegel) und Henrik Merker (u. a. ZEIT Online)

Moderation: Robert D. Meyer (nd)

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig