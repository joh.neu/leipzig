---
id: "362822818996100"
title: "Leipzig liest: Buchvorstellung »Der Satz«"
start: 2022-03-17 16:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/362822818996100/
image: 274938715_4924471300964883_7499145941847883818_n.jpg
isCrawled: true
---
Eine Reise in die frühere Heimat, Erinnerungen an eine wilde Kindheit in der Lüneburger Heide, Abenteuer mit den Brüdern Mick und Joon und der Freundin Tati, das immer unerklärliche Verhalten der Erwachsenen, versteckte Hütten im Wald, spleenige Dorfbewohner, Sommertage in der Kiesgrube und Erkundungstouren in der verfallenen Ziegelei, die erste Liebe, vor den Erwachsenen geheim gehaltene Erlebnisse – und ein tragisches Ereignis, das damals, vor dreißig Jahren, alles veränderte und das Stück für Stück in das Gedächtnis von Linda, der Ich-Erzählerin, zurückkehrt. Soll sie sich zumuten, ein bestimmtes inneres Kästchen mit Erinnerungen zu öffnen oder soll sie es geschlossen lassen?
Isobel Markus’ Romandebüt handelt von Verdrängung, Sprachlosigkeit und Schuldgefühlen, von Zumutungen und Entscheidungen, die, so klein sie auch zu sein scheinen, einen folgenreichen Einfluss auf Lebenswege haben können.

»Der Satz« ist erschienen im Quintus-Verlag.

Ein Abend mit Isobel Markus.
Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig