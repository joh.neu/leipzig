---
id: "641442703827019"
title: "Leipzig liest: Buchvorstellung »Du küsst wie Rachmaninow«"
start: 2022-03-19 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/641442703827019/
image: 275436266_4943330129079000_8231247511425454776_n.jpg
isCrawled: true
---
Buchvorstellung mit Siegfried Kühn Christian Steyer als Vorleser

In Potsdam gibt es das von düsteren Gerüchten umwaberte »Militärstädtchen« der Sowjetarmee. Die hält enge Verbindung zum Geheimdienst des Landes. In Potsdam, so legt es das Abkommen fest, residieren auch die Militärmissionen der Westmächte und dürfen sich fast frei im Land bewegen, wobei sie streng vom selben Geheimdienst beobachtet werden. Genauso wie die Potsdamer selbst.
»Wenn man etwas zu bereden hatte, traf man sich an der Grillbar im Interhotel.« So halten es auch Wanessa und ihre Freundin. An diesem Abend lernen die jungen Frauen zwei Männer kennen, Männer in Uniform. »Wanessa tippte zuerst auf Feuerwehr, doch dann hörte man sie Französisch sprechen. ›Die sind von der Militärmission‹, sagte Vera.
›Sind das nicht Spione?‹, fragte Wanessa. ›Aber süß die beiden‹, sagte Vera.« Wanessa verliebt sich in den Franzosen Claude. Nach einem gescheiterten Fluchtversuch findet sich Wanessa in einer Zelle im »Militärstädtchen« wieder. Doch einer der Geheimdienstler hält schützend die Hand über sie – und steht sich bald mit Claude auf Leben und Tod gegenüber.

»Du küsst wie Rachmaninow. Eine Liebesgeschichte« Roman
Erschienen Eulenspiegel Verlag.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail: tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar: https://www.youtube.com/c/linXXnetLeipzig

Foto: Felix Landbeck