---
id: "1012658579603300"
title: "Leipzig liest: Buchvorstellung von »Die Schönheit der Differenz«"
start: 2022-03-17 20:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/1012658579603300/
image: 274983363_4924566357622044_4043969118119456027_n.jpg
isCrawled: true
---
Buchvorstellung mit Hadija Haruna-Oelker

Hadija Haruna-Oelker, Journalistin, Politikwissenschaftlerin und Moderatorin beschäftigt sich seit langem mit Rassismus, Intersektionalität und Diskriminierung. Sie ist davon überzeugt, dass wir alle etwas von den Perspektiven anderer in uns tragen. Dass wir voneinander lernen können. Und einander zuhören sollten. In ihrem Buch erzählt sie ihre persönliche Geschichte und verbindet sie mit gesellschaftspolitischem Nachdenken. Sie erzählt von der Wahrnehmung von Differenzen, von Verbündetsein, Perspektivwechseln, Empowerment und von der Schönheit, die in unseren Unterschieden liegt.

Ein hochaktuelles Buch, das drängende gesellschaftspolitische Fragen stellt und Visionen davon entwickelt, wie wir Gelerntes verlernen und Miteinander anders denken können: indem wir einander Räume schaffen, Sprache finden, mit Offenheit und Neugier begegnen.

Nominiert für den Preis der Leipziger Buchmesse 2022 in der Kategorie Sachbuch/Essayistik.

»Die Schönheit der Differenz. Miteinander anders denken« Sachbuch. Erschienen im Verlag btb (Randomhouse).

Moderation: Katharina Warda

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail interim@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar: https://www.youtube.com/c/linXXnetLeipzig