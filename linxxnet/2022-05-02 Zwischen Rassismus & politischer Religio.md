---
id: "4944649972288909"
title: "Zwischen Rassismus & politischer Religionskritik: Zur Aufarbeitung des
  Angriffs auf eine Moschee"
start: 2022-05-02 18:00
locationName: Ost-Passage Theater
address: Konradstr. 27
link: https://www.facebook.com/events/4944649972288909/
image: 277223935_4962778743807879_5395867418893500697_n.jpg
isCrawled: true
---
Im Dezember 2021 wurde die Eyüp Sultan Moschee in der Hermann-Liebmann-Straße in Leipzig angegriffen, aus einer linken Spontandemonstration heraus. Legitim, meinten die eine, da diese zum DITIB-Verband gehört und damit vom autoritären türkischen Staat aus gelenkt werde und mit Faschisten kooperiere. Ein no go, da es um einen Schutzraum von vor allem migrantisierten Menschen in einem rassistisch aufgeladenen gesellschaftlichen Klima geht, meinen die anderen. Der Umgang mit dem Islam und den Angehörigen einer der Weltreligionen changiert in einem Spannungsfeld zwischen rassistischer Aufladung, Vorurteilen, Religionskritik, Annäherungsversuchen und dem Versuch der institutionellen Einbindung. 

Der Blick auf muslimische Gemeinden in Leipzig, Sachsen und Deutschland muss geöffnet und geschärft werden. Islamische Religionsgemeinschaften entfalten für Muslim*a nicht nur als Gebetsstätten eine große Relevanz, sondern auch als Orte von Kommunikation, Lebenshilfe und Sicherheit. Und dies auch, weil die weiße, deutsche Gesellschaft sich gegenüber migrantischen Muslim*a und ihren Lebensrealitäten und Bedürfnissen weiter verschließt. Wir wollen Diskussionsräume öffnen und verschiedenen Perspektiven auf muslimisch-sein, antimuslimischem Rassismus, aber auch Ansprüchen nach Religionsfreiheit und -kritik eine Bühne geben. 

Im Rahmen der ersten Veranstaltung wollen wir einen Raum für die Debatte um die Ereignisse am Abend des 13.12.21 öffnen. Dafür gibt es kurze, prägnante Inputs und im Anschluss eine moderierte Diskussion.

Veranstaltet von der Initiative Offene Debatte 
