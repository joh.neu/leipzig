---
id: "962521764653294"
title: "Leipzig liest: Vorstellung des Erzählbandes »Supermilch«"
start: 2022-03-19 20:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/962521764653294/
image: 275483280_4943337929078220_2416505641207947663_n.jpg
isCrawled: true
---
Buchvorstellung mit Philipp Böhm

In einem Start-up-Büro verliert ein Werbetexter den Verstand. Unter der Stadt verstopfen Fettberge die Kanalisation, während sich in einer Sozialbausiedlung ein unerwünschter Mitbewohner in eine Kröte verwandelt. Der berühmteste Elvis-Imitator des World Wide Webs nimmt sein letztes YouTube-Video auf und jeden Monat gehen elternlose Jugendliche mit Fahrrädern und Kanthölzern auf Menschenjagd. Und immer wieder taucht eine bedrohliche, stetig wachsende Untergrundbewegung auf, die die Sozialen Medien mit einer einzigen Frage flutet: »Do you like scary movies?«
Die Geschichten in »Supermilch« erzählen von einer unruhigen, nervösen Zeit: von der Transformation der Arbeitswelt, von digitalem Alltag und der Zerstörung der Natur. Die Menschen sind überfordert von ihrer Lohnarbeit, die doch angeblich mehr sein soll als nur Arbeit. Sie sind ermüdet von der beständigen Suche nach der besten Version ihrer selbst und können doch nicht davon lassen. Sie haben Angst, aber können nicht sagen wovor. Einen normalen Tag herumzubringen, scheint in dieser Welt das Einfachste und Schwerste zugleich zu sein. Also stürzen sich ihre Bewohner in Privatobsessionen, suchen ihr Glück im Ausstieg, steigern sich in obskure Internet-Phänomene hinein oder wählen sinnlose Gewalt als letztes Mittel.
»Supermilch« wirft einen Blick in die Zukunft – und die ist bedrohlich und flimmernd.

»Supermilch. Erzählungen«. Erschienen im Verbrecher Verlag.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail: tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar: https://www.youtube.com/c/linXXnetLeipzig

Foto: Nane Diehl