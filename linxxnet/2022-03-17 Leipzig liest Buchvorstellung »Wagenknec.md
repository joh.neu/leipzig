---
id: "1132137167597371"
title: "Leipzig liest: Buchvorstellung »Wagenknecht – Nationale Sitten &
  Schicksalsgemeinschaft«"
start: 2022-03-17 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/1132137167597371/
image: 275071289_4924569987621681_1130723156812113097_n.jpg
isCrawled: true
---
Buchvorstellung mit Klaus Weber
Band 2 der Buchreihe »gestalten der faschisierung« versammelt Texte zur Kritik an Sahra Wagenknechts ökonomischen, politischen sowie kulturellen Diagnosen und Perspektiven. Den im gewerk­schaftlichen Kontext aktiven Autoren geht es nicht um die Strategiebildung der Partei, sondern um eine inhaltliche Auseinandersetzung.

»Wagenknecht – Nationale Sitten & Schicksalsgemeinschaft« Sachbuch. Erschienen im Verlag Argument.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig