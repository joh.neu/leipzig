---
id: "246780117663801"
title: "Leipzig liest: Buchvorstellung »Die Zukunft der Außenpolitik ist
  feministisch«"
start: 2022-03-18 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/246780117663801/
image: 275496019_4943303539081659_5740662584943747955_n.jpg
isCrawled: true
---
Buchvorstellung mit Kristina Lunz

Kein Frieden ohne Feminismus

Immer noch dominieren alte, weiße, westliche Männer die Politik sowie Theorie und Praxis der internationalen Beziehungen. Dadurch werden die Bedürfnisse von Frauen und Minderheiten permanent ignoriert. Die Welt ist voller Kriege, Krisen und Unrecht.

Kristina Lunz tritt mit ihrem »Centre for Feminist Foreign Policy« an, das zu ändern. Die Politikwissenschaftlerin, Aktivistin und Entrepreneurin denkt Frieden, Menschenrechte und Gerechtigkeit mit Außenpolitik zusammen und will so einen Paradigmenwechsel einleiten: Machtgebaren und militärischen Muskelspielen setzt sie Mediation in Friedensverhandlungen, feministische Machtanalysen und Klimagerechtigkeit entgegen. Realpolitik wird gegen Utopien ausgetauscht, und Botschafterinnen gibt es genauso viele wie Botschafter. So kann das Gegeneinander der Nationen endlich abgelöst werden, und alle werden in größerer Sicherheit und mit weniger Konflikten leben können.

»Mit ihrem Buch deckt Kristina Lunz in brillanter Weise die brutalen Muster der männlichen Dominanz auf globaler Ebene auf. Anhand von konkreten Beispielen zeigt sie, wie patriarchale Strukturen unsere Welt durch Gier, Gewalt und Machtmissbrauch allmählich zerstören. Lunz zeigt die Möglichkeit einer gerechteren und sicheren Welt - und somit einen Weg aus der gegenwärtigen Krise.« - Emilia Roig

»Mit diesem Buch legt Kristina Lunz den Finger in die Wunde und führt eloquent und scharfsinnig vor: Eine feministische Außenpolitik ist dringlich und notwendig. Und zwar nicht zur Konstruktion kolonialer Überlegenheit, sondern zur Dekonstruktion ebendieser.« - Kübra Gümüsay

»Dieses Buch wird Sie dazu bringen, den Status quo der Sicherheits- und Außenpolitik zu hinterfragen und ihn humaner, effektiver und inklusiver neu zu denken.« - Madeleine Rees, Generalsekretärin der Women’s International League for Peace and Freedom

»Die Zukunft der Außenpolitik ist feministisch. Wie künftig Krisen gelöst werden müssen« Sachbuch. Erschienen im Verlag Econ (Ullstein).

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail: tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar: https://www.youtube.com/c/linXXnetLeipzig

Foto: F. Castro