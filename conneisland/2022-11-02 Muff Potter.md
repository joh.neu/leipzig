---
id: "6263"
title: Muff Potter
start: 2022-11-02 19:00
end: 2022-11-02 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6263.html
teaser: Muff PotterWir haben ein neues Album aufgenommen. Es heißt »Bei aller
  Liebe« und erscheint am 26.0
isCrawled: true
---
Muff Potter



Wir haben ein neues Album aufgenommen. Es heißt »Bei aller Liebe« und erscheint am 26.08. auf Hucks Plattenkiste. Bereits am 25.03. (diesen Freitag!) erscheint die erste Single »Ich will nicht mehr mein Sklave sein«. Im Herbst gehen wir mit dem Album auf große Tour. Der Vorverkauf für Tour und Album beginnt am Freitag um 10:00, exklusiv über unseren Bandshop auf www.muff-potter.de. Das wird was.



http://www.conne-island.de/termin/nr6263.html