---
id: "6280"
title: B R U N C H - Kaffee und Kuchen für den Feminismus
start: 2022-05-26 14:00
end: 2022-05-26 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6280.html
teaser: B R U N C H - Kaffee und Kuchen für den FeminismusDie Tradition am
  Feiertag Ende Mai:Der afbl-NoM
isCrawled: true
---
B R U N C H - Kaffee und Kuchen für den Feminismus



Die Tradition am Feiertag Ende Mai:

Der afbl-NoMännertags-Brunch!



14 - 18 Uhr



Mit Musik von:



Enni X

Jenny Dopezz b2b Jess Passeri



Spread the word, kommt vorbei und kauft reichlich Kuchen, Kaffee und Piccolos! Für den Feminismus!





http://www.conne-island.de/termin/nr6280.html