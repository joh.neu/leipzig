---
id: "6248"
title: FEMINIST-HANG-AROUND
start: 2022-03-08 17:00
end: 2022-03-08 21:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6248.html
teaser: "FEMINIST-HANG-AROUNDDer 8. März steht bevor. Das heißt: Kundgebungen,
  Demos und für den Feminismus"
isCrawled: true
---
FEMINIST-HANG-AROUND



Der 8. März steht bevor. Das heißt: Kundgebungen, Demos und für den Feminismus kämpfen. Doch damit nicht alle einfach alleine nach Hause gehen, haben wir ein Programm für danach im Angebot. Bei uns könnt ihr ab 17 Uhr mit einem Getränk und DJ-Mukke den Abend ausklingen lassen, Gespräche führen, den ein oder anderen Schnaps trinken und die feministische Revolution planen. Kommt vorbei und bringt eure Freund:innen und Impfausweise mit! 2G+



FEMINIST-HANG-AROUND



http://www.conne-island.de/termin/nr6248.html