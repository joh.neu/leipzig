---
id: "6181"
title: <s>Turnstile</s>
start: 2022-02-10 19:00
end: 2022-02-10 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6181.html
teaser: "TurnstileAufgrund der aktuellen Situation müssen Turnstile ihre für
  Februar geplanten Konzerte in "
isCrawled: true
---
Turnstile



Aufgrund der aktuellen Situation müssen Turnstile ihre für Februar geplanten Konzerte in Köln, Berlin und Hamburg verschieben. Bereits gekaufte Tickets behalten ihre Gültigkeit für die neuen Termine. Die letzten Tickets für Köln bekommt ihr weiterhin unter www.eventim.de.

Das Konzert am 10.02.2022 in Leipzig muss ersatzlos abgesagt werden. Tickets können bei den jeweiligen Vorverkaufstellen erstattet werden.



Abgesagt



http://www.conne-island.de/termin/nr6181.html