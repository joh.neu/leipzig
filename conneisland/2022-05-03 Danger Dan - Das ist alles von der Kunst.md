---
id: "6018"
title: Danger Dan - Das ist alles von der Kunstfreiheit gedeckt (ausverkauft)
start: 2022-05-03 19:00
end: 2022-05-03 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6018.html
teaser: Danger Dan - Das ist alles von der Kunstfreiheit gedeckt
  (ausverkauft)Danger Dan"Das ist alles vo
isCrawled: true
---
Danger Dan - Das ist alles von der Kunstfreiheit gedeckt (ausverkauft)



Danger Dan

"Das ist alles von der Kunstfreiheit gedeckt" - Tour 2022

03.05.2022

Leipzig - Conne Island

Einlass: 19:00 Uhr

Beginn: 20:00 Uhr



Bitte beachtet die Maskenpflicht im Einlassbereich, an den Bars, der Garderobe und den Toiletten! 



Tickets gibt es ab dem 31.03.21 um 10:00 Uhr unter https://www.antilopengang.de/



Präsentiert von TAZ und DIFFUS



Das ganz große Kino, Lasershows und Blitzgewitter, Explosionen und Feuerwände, fette Bässe und wackelnde Wände, Pogo und Randale, ein Fitness-Animationsprogramm, tausende Handys in der Luft und die absolute Spitze des hiesigen Live Show-Wettrüstens. All das gibt es auf dieser Tour nicht. Denn die "Das ist alles von der Kunstfreiheit"-Tour von Danger Dan wird ganz anders und verpasst den Puls der Zeit um mehrere Jahrzehnte. Es wird eher so wie in den Erzählungen deiner Eltern, diese Erzählungen aus einer Zeit, als sie in ihrer Studenten-WG nächtelang am Küchentisch saßen, eine Kerze anzündeten, über Politik diskutierten und all die merkwürdigen Lieder sangen, die du noch aus deiner Kindheit kennst. Keine Sorge, die merkwürdigen Lieder deiner Eltern wird Danger Dan nicht singen. Dafür hat er ganz viele neue mitgebracht, die jedoch stilistisch auch schon damals, in der Blütezeit der Liedermacher, entstehen hätten könnten. Auf der "Das ist alles von der Kunstfreiheit gedeckt"-Tour kannst du dich auf den Boden setzen, oder, falls deine Knie das nicht möchten, auf einen Stuhl. Du kannst mit deinen Freunden kuscheln und dabei zuhören, wie Danger Dan Klavier spielt, seine Liedchen singt und bestimmt die eine oder andere Geschichte aus seinem Leben erzählt. Vermutlich bringt er sogar wirklich ein paar Kerzen mit, er ist sich ja für nichts zu schade. Und vielleicht bringst du sogar deine Eltern mit, denn endlich hörst du mal Musik, die ihnen auch gefällt. In jedem Fall wirst du, wenn du sie nicht eh schon dabei hast, deinen Kindern später davon erzählen, wie du bei Kerzenschein am Boden mit deinen Freunden gekuschelt und zusammen Lieder gesungen hast und sie werden kurz von ihren Smartphones zu dir aufschauen und die Stirn runzeln. Das ist aber nicht so schlimm, man muss halt dabei gewesen sein, um es zu verstehen.



http://www.conne-island.de/termin/nr6018.html