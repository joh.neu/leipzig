---
id: "6234"
title: Angry Workers
start: 2022-03-12 18:30
end: 2022-03-12 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6234.html
teaser: "Vortrag und Lesung: Angry Workers Als kleines Kollektiv haben wir über
  sechs Jahre versucht uns in"
isCrawled: true
---
Vortrag und Lesung: Angry Workers 



Als kleines Kollektiv haben wir über sechs Jahre versucht uns in einem industriellen Vorort Londons zu verankern. Uns ging es dabei um eine Neufindung von revolutionaerer Klassenpolitik und ArbeiterInnenautonomie. Wir haben in verschiedenen Fabriken und Warenlagern gearbeitet, ein lokales Soli-Netzwerk an den Start gebracht und uns innerhalb und ausserhalb der Gewerkschaften rumgeschlagen. Wir wollen unsere Erfahrungen mit Euch teilen und weitergehend ueber die Fragen der ArbeiterInnenuntersuchung und Klassenorganisation diskutieren. Fuer die Vorbereitung könnt Ihr diesen Text lesen. https://communaut.org/de/class-power



Wir werden Passagen aus unserem Buch vorstellen, zusammen mit Bildern aus dem proletarischen Alltag in West-London.



Moderation: Marco Hamann // communaut.org



Die Lesung findet unter der 3G Regelung statt. Bitte tragt eine Maske, am Platz könnt ihr sie abnehmen.



http://www.conne-island.de/termin/nr6234.html