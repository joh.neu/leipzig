---
id: "6243"
title: "Online Versteigerung: Testpressung 30 YRS Conne Island Sampler"
start: 2022-02-19 16:00
end: 2022-02-19 20:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6243.html
teaser: Testpressung 30 YRS Conne Island SamplerSpecials! Specials! Specials!Wir
  versteigern 5 Exemplare
isCrawled: true
---
Testpressung 30 YRS Conne Island Sampler



Specials! Specials! Specials!



Wir versteigern 5 Exemplare der Testpressung '30 YRS Conne Island Sampler', ein 3-fach Vinyl mit über 30 Interpret:innen. Der Startpreis liegt bei 40, das höchste Gebot gewinnt. Es handelt sich um eine Online Versteigerung der Testpressung am 19.02.2022 um 16 Uhr auf unserem Instagram Profil. Abholung dann individuell im Conne Island möglich.



>> 30 Jahre Conne Island, das sind auch 30 Jahre laute Konzerte, tanzen, träumen, singen, schwitzen und Gänsehaut im Saal. Wie gerne hätten wir euch und uns viele viele Live Kozerte zum Geburtstag geschenkt, aber wie ihr wisst ist das leider immer noch nicht in Gänze möglich. Trotz allem gibt es etwas tolles für euch, von uns und dem Conne Island nahestehenden Künstler:innen. Wir haben es mit sehr viel Liebe zum Detail geschafft einen 30 YRS Sampler zusammen zu stellen, der versucht den unterschiedlichsten Genres und Menschen gerecht zu werden. Die dreifach Vinyl-LP mit Booklet zu allen Musiker:innen wird im Frühjahr 2022 erscheinen. 