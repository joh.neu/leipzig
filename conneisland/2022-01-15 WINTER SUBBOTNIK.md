---
id: "6226"
title: WINTER SUBBOTNIK
start: 2022-01-15 10:00
end: 2022-01-15 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6226.html
teaser: WINTER SUBBOTNIK +++CARE+++ KEEP UR ISLANDHallo liebe Inselgemeinde,Auch
  wenn die Kälte in den K
isCrawled: true
---
WINTER SUBBOTNIK +++CARE+++ KEEP UR ISLAND



Hallo liebe Inselgemeinde,



Auch wenn die Kälte in den Knochen zieht und die Plakate der nicht stattfindenden Konzerte übers Islandgelände wehen, gibt es einiges zu tun.



Das Islandgelände muss trotz oder gerade wegen der fehlenden Feiernden und Partygehenden Winter- und Feuerfest gemacht werden, damit die Insel weder zufriert noch abbrennt!



Also muss ein Wintersubbotnik her!



Wir fangen um frühe 10 Uhr an und werden uns bis in die Abendstunden mit spaßigen Sport- und Outdooraktivitäten die Zeit vertreiben, also packt die Trekkingschuhe und Outdoorjacke ein, es wird dreckig!



Damit keiner auf der Strecke bleibt wird es Essen und Getränke geben, die das leibliche Wohl erhalten, wohingegen Glühwein für die Motivation sorgt.



Das Licht am Ende des Freisitzes wird ein Lagerfeuer sein, das auch die Gliedmaßen wieder auftaut, die der Glühwein noch nicht erreicht hat.



Wir freuen uns auf euch!



http://www.conne-island.de/termin/nr6226.html