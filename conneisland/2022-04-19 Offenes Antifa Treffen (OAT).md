---
id: "6273"
title: Offenes Antifa Treffen (OAT)
start: 2022-04-19 18:30
end: 2022-04-19 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6273.html
teaser: "Offenes Antifa Treffen (OAT)Am 19. April 2022 um 19 Uhr wird es das
  nächste Offene Antifa Treffen "
isCrawled: true
---
Offenes Antifa Treffen (OAT)



Am 19. April 2022 um 19 Uhr wird es das nächste Offene Antifa Treffen (OAT) im Conne Island im Vorderhaus geben. Am Anfang wird es wie immer einen allgemeinen Überblick geben, was es in der Stadt so gibt. Danach wollen wir über kommende Termine sprechen. Da wäre der antifaschistische Frühjahrsputz und das Antifa Weekend diesen Monat noch.



Was ist das Offene Antifa Treffen?



Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.



Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).



http://www.conne-island.de/termin/nr6273.html