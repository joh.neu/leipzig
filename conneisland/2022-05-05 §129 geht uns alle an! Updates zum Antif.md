---
id: "6281"
title: §129 geht uns alle an! Updates zum Antifa Ost-Verfahren
start: 2022-05-05 18:30
end: 2022-05-05 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6281.html
teaser: §129 geht uns alle an! Updates zum Antifa Ost-VerfahrenUnsere Freundin
  und Genossin Lina sitzt sei
isCrawled: true
---
§129 geht uns alle an! Updates zum Antifa Ost-Verfahren



Unsere Freundin und Genossin Lina sitzt seit über 500 Tagen in U-Haft, der Prozess gegen sie und drei weitere Angeklagte zieht sich bereits seit über 8 Monaten. Der Vorwurf: Organisierte Angriffe auf Nazis und die Gründung und Mitgliedschaft in einer kriminellen Vereinigung nach § 129.



Das Solidaritätsbündnis Antifa Ost begleitet und dokumentiert diesen Prozess, dessen Ausgang die Maßstäbe politischer Strafverfolgung nach § 129 neu setzen könnte und somit einen Einfluss auf die gesamte Bewegung haben wird.



Im Input berichten wir vom bisherigen Prozessverlauf und beleuchten die Hintergründe. Die anschließende Diskussion wird offen gestaltet.



Eine Veranstaltung der Soli Antifa Ost.



http://www.conne-island.de/termin/nr6281.html