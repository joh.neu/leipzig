---
id: "6269"
title: Popdisco
start: 2022-05-06 18:00
end: 2022-05-06 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6269.html
teaser: Popdisco Kleine KlinkeApæthie:tTwerking ClassFür den Abend gilt 2G+ und
  ein negativer Schnellt
isCrawled: true
---
Popdisco 



Kleine Klinke

Apæthie:t

Twerking Class



Für den Abend gilt 2G+ und ein negativer Schnelltest, bitte denkt an eure Nachweise. Tragt weiterhin eine Maske am Einlass, Toiletten, Bar und an der Garderobe.



http://www.conne-island.de/termin/nr6269.html