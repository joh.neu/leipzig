---
id: "6279"
title: "Halftime: Proberaum Mentoring"
start: 2022-05-25 18:00
end: 2022-05-25 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6279.html
teaser: "Halftime: Proberaum Mentoring18-24 UhrFLINTA Proberaum Mentoring
  Halftime! Noch nicht mal ein Jah"
isCrawled: true
---
Halftime: Proberaum Mentoring



18-24 Uhr

FLINTA Proberaum Mentoring Halftime! Noch nicht mal ein Jahr FLINTA-Mentoring im Proberaum, und schon gab es über 40 DJ Mentorings. Die rising Stars bei der Halftime!



Lecat & Kali Avaaz

n:oem & Lax303

DJ Team Crybabies



Der Einlass ist begrenzt. Daher haben wir einen Telegram Channel eingerichtet, über den wir euch informieren, ob der Freisitz voll ist. Wenn das der Fall ist, vermeidet bitte lange Schlangen und kommt später wieder. Wir sagen bescheid, wenn wieder Platz ist!

https://t.me/joinchat/ME7JEF1Gs4Y3Mjhi

Spendenempfehlung 2-5

Layout by: https://www.instagram.com/leonleon.mi/

x Die Veranstaltung findet unter 2G+ Bedingungen statt. Nachweise bitte am Einlass bereithalten x

x no dogs allowed x 

x good weather only x 



http://www.conne-island.de/termin/nr6279.html