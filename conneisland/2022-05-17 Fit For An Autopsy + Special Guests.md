---
id: "6212"
title: Fit For An Autopsy + Special Guests
start: 2022-05-17 18:00
end: 2022-05-17 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6212.html
teaser: Fit For An Autopsy + Special GuestsBitte beachtet die Maskenpflicht im
  Einlassbereich, an den Bars
isCrawled: true
---
Fit For An Autopsy + Special Guests



Bitte beachtet die Maskenpflicht im Einlassbereich, an den Bars, der Garderobe und den Toiletten!

Es wird keine G-Regelung geben für den Abend.



FIT FOR AN AUTOPSY · 2022 EU/UK TOUR

Special Guests:

ENTERPRISE EARTH

INGESTED

GREAT AMERICAN GHOST

SENTINELS



http://www.conne-island.de/termin/nr6212.html