---
id: "6254"
title: Laura Jane Grace
start: 2022-06-29 19:00
end: 2022-06-29 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6254.html
teaser: Laura Jane GraceBitte beachtet die Maskenpflicht im Einlassbereich, an
  den Bars, der Garderobe und
isCrawled: true
---
Laura Jane Grace



Bitte beachtet die Maskenpflicht im Einlassbereich, an den Bars, der Garderobe und den Toiletten!

Es wird keine G-Regelung geben für den Abend.



Laura Jane Grace

+ Cold Years

+ Cuir



https://www.eventim.de/artist/laura-jane-grace/



http://www.conne-island.de/termin/nr6254.html