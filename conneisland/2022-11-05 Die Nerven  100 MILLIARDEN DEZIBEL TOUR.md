---
id: "6295"
title: Die Nerven  "100 MILLIARDEN DEZIBEL TOUR"
start: 2022-11-05 20:00
end: 2022-11-06 00:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6295.html
teaser: 'Die Nerven  "100 MILLIARDEN DEZIBEL TOUR"Seit 2010 gibt es DIE NERVEN,
  gegründet in Esslingen und '
isCrawled: true
---
Die Nerven  "100 MILLIARDEN DEZIBEL TOUR"



Seit 2010 gibt es DIE NERVEN, gegründet in Esslingen und mittlerweile verteilt auf Stuttgart und Berlin, und sie gelten heute, zwölf Jahre später, nicht nur als eine der besten Live-Bands des Landes: Nach einer an kreativem Schöpfungswillen unvergleichlichen Flut an Digital-Releases erschien 2012 FLUIDUM (Fin Du Monde/This Charming Man Records), 2014 gefolgt von FUN (ebd.), von Jan Wigger (Spiegel Online) als "eine der wichtigsten und besten deutschsprachigen Platten dieses Jahrzehnts" verstanden. Stets glaubwürdig und dem Underground verbunden geblieben spielten sich DIE NERVEN in den folgenden Jahren durch die Clubs und Festivals (Roskilde, Melt!, Appletree Garden, Sziget, Eurosonic), unternahmen eine Israel-Tour und veröffentlichten als erste deutschsprachige Band auf dem stilprägenden US-amerikanischen Label Amphetamine Reptile Records. 2015 veröffentlichten sie ihr drittes Album OUT (Glitterhouse Records) und gingen auf ausgiebige Tour weit über die Grenzen Deutschlands hinaus, was im Juni 2017 zu ihrer ersten Live-Platte LIVE IN EUROPA führte. FAKE dann, mit Platz 13 ein Chart-Erfolg für die Band, markierte spätestens den Durchbruch. DIE NERVEN sind 2022, das Jahr, in dem ihr fünftes, selbstbetiteltes Album erscheint und umfangreich betourt wird, nicht mehr wegzudenken aus den lebendigen Regionen der Musikwelt  sie befeuern und prägen sie! Und das nicht nur durch die Band selbst: Kevin Kuhn, einer der melodischsten und eigenständigsten Schlagzeuger der Gegenwart, trommelt auch bei Gordon Raphael und Wolf Mountains, Julian Knoth legt mit dem Peter Muffin Trio und Yum Yum Club beindruckende, wuchtige und der Avantgarde nahe Platten vor, und Max Rieger (u.a. All diese Gewalt) produziert sich mit Casper, Drangsal, Die Selektion, Friends Of Gas, Ilgen-Nur, Jungstötter, Stella Sommer etc. an die Spitze der deutschen Musikindustrie jenseits vorhersehbarer Plastikwerke.



http://www.conne-island.de/termin/nr6295.html