---
id: "6270"
title: "Halftime // Season Opening "
start: 2022-04-13 18:00
end: 2022-04-13 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6270.html
teaser: Halftime // Season OpeningDie Halftime Crew eröffnet den mittwöchlichen
  Hangout! Bunny Tsukino(
isCrawled: true
---
Halftime // Season Opening



Die Halftime Crew eröffnet den mittwöchlichen Hangout! 



Bunny Tsukino

(BLADE, Halftime Crew)

https://soundcloud.com/bunny_tsukino



Goody

(Kollektiv99, Halftime Crew)

https://soundcloud.com/goodyyyy



Leeza

(G-Edit, BLADE, Halftime Crew)

https://soundcloud.com/l_eeza



Malte Magnum

(ORGIA, Halftime Crew)

https://soundcloud.com/malte_magnum



Der Einlass ist begrenzt. Daher haben wir einen Telegram Channel eingerichtet, über den wir euch informieren, ob der Freisitz voll ist. Wenn das der Fall ist, vermeidet bitte lange Schlangen und kommt später wieder. Wir sagen bescheid, wenn wieder Platz ist!

https://t.me/joinchat/ME7JEF1Gs4Y3Mjhi



Spendenempfehlung 2-5



Layout by: https://www.instagram.com/leonleon.mi/



x Die Veranstaltung findet unter 2G+ Bedingungen statt. Nachweise bitte am Einlass bereithalten x

x no dogs allowed x 

x good weather only x 



http://www.conne-island.de/termin/nr6270.html