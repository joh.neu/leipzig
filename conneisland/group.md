---
name:  Conne Island
website: https://conne-island.de/
email: info@conne-island.de
address: Koburger Str. 3, 04277 Leipzig
scrape:
  source: ical
  options:
    url: https://www.conne-island.de/ical.php?genre=conneisland
---
Das Conne Island ist ein Zentrum von und für Linke, Jugend-, Pop- und Subkulturen.