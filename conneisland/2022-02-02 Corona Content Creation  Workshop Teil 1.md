---
id: "6230"
title: "Corona Content Creation  Workshop Teil 1: Kameras und Videomischer,
  Liveschnitt und beste Contentgrundlage"
start: 2022-02-02 18:00
end: 2022-02-02 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6230.html
teaser: "Conne Island Corona Content Creation  Workshop Teil 1: Kameras und
  Videomischer, Liveschnitt und be"
isCrawled: true
---
Conne Island Corona Content Creation  Workshop Teil 1: Kameras und Videomischer, Liveschnitt und beste Contentgrundlage



Ihr habt keine Lust mehr Streams zu gucken, sondern selber welche zu machen?

Dann kommt vorbei und lasst euch zeigen wie das alles funktioniert und seid beim nächsten Stream selbst dabei!

Kameras und Videomischer, Liveschnitt und beste Contentgrundlage.

Und wenn ihr Spaß habt könnt ihr euch gleich ausprobieren!



Ort: Conne Island Saal 2G+



Anmeldung unter:



Anmeldung@conne-island.de







http://www.conne-island.de/termin/nr6230.html