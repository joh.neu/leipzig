---
id: "6293"
title: Offenes Antifa Treffen (OAT)
start: 2022-05-24 18:30
end: 2022-05-24 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6293.html
teaser: Offenes Antifa Treffen (OAT)Am 24. Mai 2022 um 19 Uhr wird es das
  nächste Offene Antifa Treffen (O
isCrawled: true
---
Offenes Antifa Treffen (OAT)



Am 24. Mai 2022 um 19 Uhr wird es das nächste Offene Antifa Treffen (OAT) geben. Am Anfang wird es wie immer einen allgemeinen Überblick geben, was es in der Stadt so gibt. Danach wird es eine Veranstaltung mit dem Ermittlungsausschuss Leipzig geben.



Staying clean  Repression bei Demos vermeiden  mit dem Ermittlungsausschuss Leipzig und eine*m Strafverteidiger*in



Oft stehen polizeiliche Ermittlungen im Zusammenhang mit Demos und Aktionen  Es gibt also einiges wichtiges zu beachten, um Repression oder wenigstens ihre Folgen zu vermeiden.

Wir wollen euch zusammen mit einem erfahrenen Rechtsanwalt für Strafrecht hierzu einige Tipps geben. Wir wollen unter anderem besprechen, wie wir uns gut auf eine Aktion/Demo vorbereiten können, was für Szenarien eintreten können, wenn die Cops gegen Aktivist:innen vorgehen und wie wir damit umgehen können. Auch nach der Demo/Aktion gibt es wichtiges zu beachten. Für Minderjährige und Leute ohne deutschen Pass gibt es wiederum spezifische Formen und Konsequenzen von Repression.

Je besser wir vorbereitet sind auf das, was auf uns zukommt, und wir wissen, wie wir uns wehren können, desto besser können wir uns und andere schützen. Wir  der Ermittlungsausschuss Leipzig  beraten und unterstützen Menschen, die wegen ihres politischen Engagements Ärger mit Polizei und Justiz bekommen.

Wir freuen uns auf euch, eure Fragen und Diskussionen!



http://www.conne-island.de/termin/nr6293.html