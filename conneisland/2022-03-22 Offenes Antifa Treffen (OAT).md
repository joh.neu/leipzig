---
id: "6260"
title: Offenes Antifa Treffen (OAT)
start: 2022-03-22 19:00
end: 2022-03-22 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6260.html
teaser: Offenes Antifa Treffen (OAT)Am Anfang wird es wie immer einen
  allgemeinen Überblick geben, dann fo
isCrawled: true
---
Offenes Antifa Treffen (OAT)



Am Anfang wird es wie immer einen allgemeinen Überblick geben, dann folgt der Vortrag, der im November angedacht war:



Seit 1997 gedenken Neonazis aus etlichen europäischen Ländern am sog. Tag der Ehre dem gescheitertern Ausbruchsversuch im Februar 1944 ungarischer und deutscher Faschisten aus dem Budapester Kessel  die Einkreisung und Belagerung durch die Rote Armee.



Jedes Jahr um den 13. Februar, findet in Budapest am sog. Tag der Ehre das größte Vernetzungsevent des militanten Faschismus in Europa statt. Das Gedenken besteht aus zwei Veranstaltungen: Einem NS-Heldengedenken mit Kranzniederlegungen und einer nächtlichen Wehrsportübung, die als familienfreundliche Wanderung beworben wird. Strukturen wie Blood&Honour, Hammerskins und Kameradschaften aber auch völkische Siedler beteiligen sich jedes Jahr an diesem Event.



Was ist das Offene Antifa Treffen?



Es ist wichtig, sich zu vernetzen und Strukturen aufzubauen, um den Widerstand zu organisieren  nicht nur gegen Rassismus und Faschismus, sondern auch gegen Nationalismus, Sexismus, Antisemitismus, Homosexuellenfeindlichkeit und die kapitalistische Gesamtscheiße.



Du willst endlich was dagegen tun, wusstest bisher aber nicht wo, wie und mit wem? Dann komm zum Offenen Antifa Treffen (OAT).



Es gilt die 3G-Regelung geimpft, genesen, getestet. Tragt bitte eine Maske, wenn ihr euch nicht gut fühlt, bleibt bitte zu Hause.



http://www.conne-island.de/termin/nr6260.html