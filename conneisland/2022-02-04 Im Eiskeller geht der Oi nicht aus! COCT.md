---
id: "6233"
title: Im Eiskeller geht der Oi nicht aus! COCTAILS MIXEN MIT LOIKAEMIE
start: 2022-02-04 18:00
end: 2022-02-04 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6233.html
teaser: "Im Eiskeller geht der Oi nicht aus! COCTAILS MIXEN MIT LOIKAEMIEDie
  Zeiten sind hart und manchmal "
isCrawled: true
---
Im Eiskeller geht der Oi nicht aus! COCTAILS MIXEN MIT LOIKAEMIE



Die Zeiten sind hart und manchmal bleibt nichts weiter als die Freude an unserer Trinkfestigkeit. Deshalb:

Holt die Stiefel aus dem Schrank, am 4.2.22 mixen LOIKAEMIE Cocktails.

Ab 18 Uhr streamen wir das Spektakel aus dem Conne Island Café  und das sogar zu euch nach Hause, dann braucht ihr die Hemden nicht extra bügeln. Die Übertragung erfolgt über Facebook, Twitch und Youtube, der Link dazu kommt am Donnerstag.

Seid live dabei, wenn unser guter Freund Ecke mit einem prickelndem Bier in der Hand eure Lieblingsband mit schalen Fragen löchert.

Wer will, kann sich per Live-Chat oder Fernsprechapparillo zuschalten, sich mit anrüchigen Fragen ans Ende der Redner:innenliste setzen lassen oder Drinks ordern.

Letztere könnt ihr euch dann ab 20 Uhr abholen, mit Maske, Thermobecher und Impf- oder Genesenennachweis.

Kippt euch zwei, drei Cocktails rein, nehmt noch paar Drinks für den Weg mit und haut dann wieder ab.

Schaltet ein & join the Saufgelage.

Oi! Oi! Oi!



http://www.conne-island.de/termin/nr6233.html