---
id: "6227"
title: Bücher und Bechern am Fenster
start: 2022-01-29 14:00
end: 2022-01-29 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6227.html
teaser: "Bücher und Bechern am FensterWer will, wer will, wer hat noch nicht? Am
  29.1. gibt es noch einmal "
isCrawled: true
---
Bücher und Bechern am Fenster



Wer will, wer will, wer hat noch nicht? Am 29.1. gibt es noch einmal in geselligem Sein unser 30-Jahre-Buch "Auf dem Klo habe ich noch nie einen Schwan gesehen" sowie die frisch erschienene Broschüre der Gruppe Utopie und Praxis "nine to five?  Perspektiven auf Arbeit" zu erwerben. 



Heißgetränke und Futter aus der Fritte warten auf euch. 14 Uhr geht's los und bis 18 Uhr halten wir die Fenster für euch offen.



http://www.conne-island.de/termin/nr6227.html