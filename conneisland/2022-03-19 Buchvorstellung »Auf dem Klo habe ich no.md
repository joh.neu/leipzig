---
id: "6235"
title: "Buchvorstellung: »Auf dem Klo habe ich noch nie einen Schwan gesehen«"
start: 2022-03-19 18:30
end: 2022-03-19 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6235.html
teaser: Buchvorstellung mit Protagonist:innen und RedaktionAuf dem Klo habe ich
  noch nie einen Schwan gese
isCrawled: true
---
Buchvorstellung mit Protagonist:innen und Redaktion



Auf dem Klo habe ich noch nie einen Schwan gesehen

Erinnerungen aus 30 Jahren Conne Island



+++ Aufgrund der aktuellen Situation bitten wir alle Besucher:innen um einen Tagesaktuellen Test +++ 



Kreuz und quer durch 30 Jahre Conne Island führen die biografischen Erzählungen in Auf dem Klo habe ich noch nie einen Schwan gesehen. Dabei wird nicht nur auf die Geschichte des linken und nicht mehr ganz jugendlichen Kulturzentrums in Leipzig-Connewitz geblickt, sondern auch auf den Wandel von verschiedenen Jugend- und Subkulturen seit den 1980er Jahren. An vielen Stellen bieten die persönlichen Erzählungen dabei Perspektiven, die deutlich über das Conne Island hinausgehen. Es sind exemplarische Beschreibungen der Geschichte linker Gegenkultur im wiedervereinigten Deutschland. Außerdem bringen die über 20 Interviewten verschiedene linke Debatten zur Sprache, für die das Conne Island nicht selten ein Ausgangspunkt war. Schlussendlich ist es auch ein Buch über die Leipziger Polit- und Kulturszene, ihre Strahlkraft, ihren Wandel und ihre Eigenheiten.



Verbrecher Verlag 2021





Einlass nur mit Tagesaktuellem Test. Bitte tragt währen der Veranstaltung eine Maske.



http://www.conne-island.de/termin/nr6235.html