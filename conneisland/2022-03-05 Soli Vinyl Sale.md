---
id: "6241"
title: Soli Vinyl Sale
start: 2022-03-05 14:00
end: 2022-03-05 18:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6241.html
teaser: Soli Vinyl Sale30 Jahre Conne Island - Das möchten wir nun auch
  musikalisch mit Euch teilen.Feier
isCrawled: true
---
Soli Vinyl Sale



30 Jahre Conne Island - Das möchten wir nun auch musikalisch mit Euch teilen.

Feiert mit farbigem Vinyl an euren Plattentellern, schmökert in 68seitigem Booklet und unterstützt

uns somit auch in beispiellosen Zeiten.

Vorbestellung: vinyl@conne-island.de

Abholung dann Vorort in der  Koburger Str. 3





http://www.conne-island.de/termin/nr6241.html