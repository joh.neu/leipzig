---
id: "6240"
title: Girls'Day 2022
start: 2022-04-28 09:00
end: 2022-04-28 13:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6240.html
teaser: Girls'Day 2022Schnupperkurs Tontechnik und DJing im Conne Island9 - 13
  UhrIm Conne Island find
isCrawled: true
---
Girls'Day 2022



Schnupperkurs Tontechnik und DJing im Conne Island



9 - 13 Uhr



Im Conne Island finden Konzerte und Tanzveranstaltungen verschiedenster Genre wie Hip Hop, Pop, House, Punk und Hardcore statt. Bei jeder Veranstaltung muss die Anlage betreut werden, oder draußen auf dem Freisitz erst Boxen, Mischpult, Monitore und DJ-Technik aufgebaut werden. Im Workshop zeigen wir Dir, wie eine kleine Aktiv-PA (Public Address-Beschallungsanlage) verkabelt wird und welches Mischpult für eine Band, eine Lesung oder ein DJ-Set gebraucht wird. Außerdem könnt ihr an digitalen CDJs die Technik des auflegens ausprobieren. DJs verwenden diese Player, um ihre Tracks und Songs übergangslos ineinander zu mischen.



Für den DJ-Part könnt ihr eigene Musik mitbringen (.mp3 auf einem USB-Stick oder CDs).



Marlene und Anna, die den Workshop halten, veranstalten selbst im Conne Island und in anderen Spielstätten Konzerte und Tanzveranstaltungen und legen als DJ-Team auf.



-

Barrierefreiheit

Wenn Du begleitende Unterstützung benötigst, melde Dich bitte bei uns. Wir können deinen Besuch vor Ort gemeinsam planen.



Anmeldung über den Radar auf https://www.girls-day.de



Das Angebot wird gefördert durch das Amt für Jugend und Familie der Stadt Leipzig



http://www.conne-island.de/termin/nr6240.html