---
id: "6299"
title: "Halftime: Fantifa Zine Release X nice4what "
start: 2022-06-01 18:00
end: 2022-06-01 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6299.html
teaser: "Halftime: Fantifa Zine Release X nice4what 1. Juni 202218 - 24Halftime:
  Fantifa Zine Release X "
isCrawled: true
---
Halftime: Fantifa Zine Release X nice4what 



1. Juni 2022

18 - 24



Halftime: Fantifa Zine Release X nice4what 



Liebeskrise? Steuerhinterziehung? Trennung trotz Baby? Kratzer am Ferrari?



Wir feiern die 2. Ausgabe des fit4fantifa Magazins und laden zur Releaseparty am 01.06. ins Island ein. Motto: das F steht für Vogue &9825;

Wie möchten mit euch zur Musik von nice4what tanzen, uns an der Solisektbar richtig einen reinorgeln und die Tränen des politischen Alltags mit den 40 Seiten des neuen Magazins trocknen.

Bringt die Crew mit!



Der Einlass ist begrenzt. Daher haben wir einen Telegram Channel eingerichtet, über den wir euch informieren, ob der Freisitz voll ist. Wenn das der Fall ist, vermeidet bitte lange Schlangen und kommt später wieder. Wir sagen bescheid, wenn wieder Platz ist!



https://t.me/joinchat/ME7JEF1Gs4Y3Mjhi

Spendenempfehlung 2-5

Layout by: https://www.instagram.com/leonleon.mi/

x Die Veranstaltung findet unter 2G+ Bedingungen statt. Nachweise bitte am Einlass bereithalten x

x no dogs allowed x 

x good weather only x 



http://www.conne-island.de/termin/nr6299.html