---
id: "6258"
title: Die Untiefen des Postkolonialismus
start: 2022-04-21 18:30
end: 2022-04-21 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6258.html
teaser: Roter Salon im Conne Island und Gruppe Minus im Gespräch mit Jan
  GerberDie zentralen Herausforderu
isCrawled: true
---
Roter Salon im Conne Island und Gruppe Minus im Gespräch mit Jan Gerber



Die zentralen Herausforderungen für die Erinnerung an den Holocaust scheinen inzwischen weniger von rechts als von links zu kommen. Insbesondere von postkolonialer Seite wird die Vernichtung der europäischen Juden regelmäßig relativiert. Während die rechten Versuche, die Präzedenzlosigkeit des Holocaust in Frage zu stellen, völlig zu Recht große Empörung nach sich ziehen, sind die postkolonialen Anwürfe akzeptierter Bestandteil der Debattenkultur. In den Beiträgen des Schwerpunktes im ersten Band der Hallischen Jahrbücher wird dieser Entwicklung sowie den Hintergründen, Ursachen und Dynamiken der postkolonialen Auseinandersetzung mit dem Holocaust nachgegangen.



Mit dem Herausgeber des Schwerpunktes und Mitherausgeber der Jahrbücher, Jan Gerber, wird der Rote Salon sich in die Untiefen des Postkolonialismus begeben.



http://www.conne-island.de/termin/nr6258.html