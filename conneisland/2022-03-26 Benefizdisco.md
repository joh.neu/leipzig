---
id: "6250"
title: Benefizdisco
start: 2022-03-26 21:00
end: 2022-03-27 05:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6250.html
teaser: BenefizdiscoDJs:Bellawinadre$Franka Frizzante & Tante
  KäseFrauschGrünspanJess Passeri & Moku
isCrawled: true
---
Benefizdisco



DJs:

Bellawina

dre$

Franka Frizzante & Tante Käse

Frausch

Grünspan

Jess Passeri & Mokum

Molicc



- all styles in the mix - P18! -



2G++ 

(Geimpft/Genesen/Geboostert PLUS Test)

Jede Person muss ein offizielles Testergebnis vorzeigen (tagesaktuell). 



http://www.conne-island.de/termin/nr6250.html