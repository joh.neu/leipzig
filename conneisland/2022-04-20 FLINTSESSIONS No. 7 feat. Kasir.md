---
id: "6267"
title: FLINT*SESSIONS No. 7 feat. Kasir
start: 2022-04-20 18:00
end: 2022-04-20 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6267.html
teaser: FLINT*SESSIONS No. 7 feat. KasirDie Flint*sessions sind endlich
  zurück!Nach pandemiebedingter Pau
isCrawled: true
---
FLINT*SESSIONS No. 7 feat. Kasir



Die Flint*sessions sind endlich zurück!

Nach pandemiebedingter Pause öffnen wir am 20.4. um 18 Uhr die Türen des Conne Island. 

Diesmal mit dabei:

Kasir Apart

// Hip Hop aus dem Ruhrpott

https://soundcloud.com/kasirapart

FLINTA* Repräsentation in den Medien - Talk mit Laura Klar

Laura Klar wohnt in Leipzig und arbeitet als PR-Managerin für Musiker*innen bei der Agentur MONA LINA. Auch in ehrenamtlichen Tätigkeiten geht sie ihrer Liebe für die Musik nach und setzt dabei stets einen Fokus auf die Repräsentation von FLINTA*-Artists: Bei dem Blog 365Fe*male MCs schreibt sie als Autorin und moderiert ihre eigene Sendung FLINTA* gaze bei Sphere Radio. Nebenbei studiert Laura Multimedia & Autorschaft an der Uni Halle.

Jam

Danach ist die Bühne für euch offen, bringt gerne eigene Instrumente mit und lasst uns jammen!

--------------------------------------------

Hinweise: 

- FLINTA* only

(Female-Lesbian-Intersex-Nonbinary-Trans-Agender*)

- Einlass unter 2G+

Hinweis zu Barrierefreiheit: 

Der Saal des Conne Island ist rollstuhlzugänglich, gerne helfen wir bei weiteren Fragen und dem Zugang zur Bühne.



http://www.conne-island.de/termin/nr6267.html