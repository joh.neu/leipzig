---
id: "6296"
title: 29th Little Sista Skate Cup
start: 2022-08-27 10:00
end: 2022-08-27 14:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6296.html
teaser: 29th Little Sista Skate CupWenn du als Kind den Spruch gehört hast, dass
  das Schauen in den Bildsc
isCrawled: true
---
29th Little Sista Skate Cup



Wenn du als Kind den Spruch gehört hast, dass das Schauen in den Bildschirm viereckige Augen verursacht, dann war damit gemeint, dass du durch das viele Geglotze auf den Bildschirm eine Brille als Sehhilfe bekommen kannst! Haste nicht gewusst, ne?! Ich wette, viele, die damals ihren Kindern das erzählt haben, auch bloß nicht!



Und zack!, wieder etwas schlauer! Bitte sehr! Gern geschehen! Wo wir bei Bildschirmen sind: Um den diesjährigen, mittlerweile 29sten Little Sista Skate Cup mitverfolgen zu können, brauchst du KEINEN Bildschirm! Dieses Jahr musst du tatsächlich deine vollgefurzte, durch Covid schon durchgesessene Couch verlassen, DENN WIR SIND ZURÜCK IM BOWL!



Ganz richtig: Am Samstag, dem 27.08.2022, im Conne Island Skatepark! Ein Contest, wieder offline! Richtig skaten! Mit richtig Leuten! Und richtig Musik! Unter der Sonne! Ein Tag, an dem dir dein Handy nur Bildschirmzeit zeigt, weil du deine Kumpels und Kumpelinen anrufen musst, damit sie endlich auch im Island auftauchen! Also! 27.08.2022, Conne Island! Sehen uns da!



http://www.conne-island.de/termin/nr6296.html