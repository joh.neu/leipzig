---
id: "6019"
title: <strike>SKATING POLLY</strike> (abgesagt)
start: 2022-02-23 19:00
end: 2022-02-23 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6019.html
teaser: SKATING POLLY (abgesagt)*** Leider müssen wir das Konzert für Februar
  absagen. Wir suchen nach ein
isCrawled: true
---
SKATING POLLY (abgesagt)



*** Leider müssen wir das Konzert für Februar absagen. Wir suchen nach einem neuen Termin!***



Skating Polly aus Oklahoma haben Kelly Mayo und Peyton Bighorse schon lange bevor sie selbst auf Konzerte gehen durften gegründet. Laut Selbstbezeichnung machen sie "ugly pop art rock", zu ihren Vorbildern zählen Veruca Salt, X, The Breeders, L7 und Babes In Toyland.  

Auch wenn es noch in ferner Zukunft liegt und es gerade schwer vorstellbar erscheint, wollen wir die Show mit Skating Polly schon ankündigen, weil wir uns freuen wie bolle!



Zusammen mit fight like a grrrl booking und Sedate Booking.





Tickets ab Herbst 2021



"There's a difficult to describe, yet timeless quality to certain songs that transcends genre or era. It's something that you can't fake or contrive and it's what lies at the core of Skating Polly's music. The multi instrumentalist duo of Kelli Mayo and Peyton Bighorse started their band in Oklahoma in 2009. They recorded their debut album Taking Over The World in 2010 and achieved instant acclaim from underground music icons like X's Exene Cervenka (who produced 2013's Lost Wonderfuls) and Beat Happening's Calvin Johnson (who produced 2014's Fuzz Steliacoom.) After the release of 2016's The Big Fit, Veruca Salt's co-frontwomen Louise Post and Nina Gordon reached out and said they wanted to write with the band. What started as a writing session ultimately became 2017s New Trick EP. Now armed with a third memberKellis brother Kurtis Mayothe trio are about to release their latest full length, The Make It All Show.



Skating Polly really need to be seen in a club to fully grasp what makes them so special. It can get pretty chaotic when we're playing. People have said it feels like it could fall apart at any moment but in a good way, Bighorse says with a laugh. We try to make our music honest and engaging and I think that's what drew us to people like Nina and Louise or Exene; you can probably guess a lot of the acts that we love, but were able to keep making music that always sounds like us, Mayo explains. We really try to make the songs the focus instead of trying to flaunt technical musical abilities, adds Bighorse  and that honesty and optimism is why everyone from legendary musical figures as well as hardcore fans have gravitated toward Skating Polly's music. The Make It All Show is the most fully realized work by Skating Polly yet and hints at an even brighter future for one of rocks most promising acts."



http://www.conne-island.de/termin/nr6019.html