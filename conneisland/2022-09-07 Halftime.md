---
id: "6290"
title: Halftime
start: 2022-09-07 18:00
end: 2022-09-07 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6290.html
teaser: HalftimeDer Einlass ist begrenzt. Daher haben wir einen Telegram Channel
  eingerichtet, über den wi
isCrawled: true
---
Halftime



Der Einlass ist begrenzt. Daher haben wir einen Telegram Channel eingerichtet, über den wir euch informieren, ob der Freisitz voll ist. Wenn das der Fall ist, vermeidet bitte lange Schlangen und kommt später wieder. Wir sagen bescheid, wenn wieder Platz ist!

https://t.me/joinchat/ME7JEF1Gs4Y3Mjhi



Spendenempfehlung 2-5



Layout by: https://www.instagram.com/leonleon.mi/



x no dogs allowed x 

x good weather only x 



http://www.conne-island.de/termin/nr6290.html