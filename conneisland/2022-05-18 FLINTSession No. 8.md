---
id: "6297"
title: FLINT*Session No. 8
start: 2022-05-18 18:00
end: 2022-05-18 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6297.html
teaser: FLINT*Session No. 8Flint*tones Le präsentieren:FLINT*SESSIONS NO. 8 -
  feat. Gypsy WingsJamsessio
isCrawled: true
---
FLINT*Session No. 8



Flint*tones Le präsentieren:

FLINT*SESSIONS NO. 8 - feat. Gypsy Wings

Jamsession-Konzert-Workshop

FLINTA* only



WANN: 18. Mai 2022, Einlass ab 18 Uhr

WO: Conne Island, Koburger Str 3 in Leipzig

KONZERT: Gypsy Wings 

(https://www.gypsywingsduo.com/media)

WORKSHOP: "Vocal Truth" mit Agata Jagoda (https://agatajagoda.de/)

Es geht in den Frühling und die nächste Flint*session steht an. 

Mit Gypsy Wings erwartet euch musikalische Vielfalt, einfühlsame 

Melodien fürs Herz, Texte zum Nachsinnen und eine energiegeladene Stimmung, die mit Einflüssen aus Rock, Folk, Opera, Jazz und Funk bewegt.

Im Mini-Workshop mit Agata Jagoda hast Du die Chance

deinen authentischen Stimmklang im Hier und Jetzt zu erforschen.

In einer geschützten Atmosphäre tauchen wir als Gruppe soft aber ehrlich in eine Gruppenimprovisation ein und machen so eine einzigartige Klangerfahrung.

Last but not least jammen wir wieder gemeinsam - also packt eure Instrumente ein und kommt auf die Bühne!

Wir freuen uns auf euch!

eure Flint*tones

Das Ganze findet im Saal des Conne Island statt, 2G erforderlich (Genesen/Geimpft)

WTF is FLINTA*? (Female-Lesbian-Intersex-Nonbinary-Trans-Agender*)

OPEN4ALL - bei jeder 3. Session sind alle Musiker*innen eingeladen, alle anderen VAs sind FLINTA* only.

und das wichtigste zum Schluss:

Um die Veranstaltung für alle Interessent*innen so angenehm und sicher wie möglich zu machen bitten wir um Awareness. Das bedeutet unter anderem:

- respektvoller Umgang miteinander - bei Fragen oder Anliegen könnt ihr euch jederzeit an unser Team am Einlass wenden.

- Jamsession heißt bei uns auch: Jeder hat einen Platz auf der Bühne verdient. Besonders möchten wir jedoch FLINT*/FLINTA*-Personen ermutigen sich auszuprobieren.

- Wir stehen gerne mit euch im Dialog. Ziel ist es, Diskriminierungen aus Gründen der Religion oder Weltanschauung, aufgrund einer Behinderung, des Alters, der Herkunft, sexuellen oder geschlechtlichen Identität auf unseren Veranstaltungen zu verhindern bzw zu beseitigen. Das geht nur mit eurer Mitarbeit!

Barrierefreiheit:

Bei Fragen oder Anregungen zur Zugänglichkeit sind wir jederzeit über unsere Socials wie auch vor Ort erreichbar.

Gerne begleitet euch auch jemand von der nächsten Haltestelle zum Event.

weitere Infos findet ihr auf: https://www.conne-island.de/barrierefreiheit.html



http://www.conne-island.de/termin/nr6297.html