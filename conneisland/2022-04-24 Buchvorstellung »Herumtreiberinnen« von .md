---
id: "6265"
title: "Buchvorstellung: »Herumtreiberinnen«"
start: 2022-04-24 16:30
end: 2022-04-24 20:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6265.html
teaser: »Herumtreiberinnen« von Bettina Wilpert Manja ist 17 Jahre alt und lebt
  im Leipzig der 1980er Jahr
isCrawled: true
---
»Herumtreiberinnen« von Bettina Wilpert 



Manja ist 17 Jahre alt und lebt im Leipzig der 1980er Jahre. Ihre beste Freundin Maxie und sie schwänzen die Schule, brechen in Schrebergärten ein und treffen sich im Freibad oder auf dem Rummel mit Jungs, bis Manja im Zimmer des Vertragsarbeiters Manuel von der Volkspolizei erwischt wird und auf die Venerologische Station für Frauen mit Geschlechtskrankheiten kommt.

Eingewoben in den Roman sind auch Erlebnisse von Lilo, die in den 1940er Jahren an diesem Ort festgehalten wurde, da sie mit ihrem Vater für den kommunistischen Widerstand gearbeitet hat, und der Sozialarbeiterin Robin, die in den 2010er Jahren in diesem Haus  nun eine Unterkunft für Geflüchtete  tätig ist.



Der Roman »Herumtreiberinnen« erzählt die Geschichten von drei jungen Frauen aus verschiedenen Zeiten und stellt die Frage, welchen Einfluss diese Zeit und die jeweilige Staatsform auf ihre Leben hatten. Ein Haus in der Leipziger Lerchenstraße ist das verbindende Element der drei Erzählstränge.



»Herumtreiberinnen« ist nach »nichts, was uns passiert« der zweite Roman der Leipziger Autorin Bettina Wilpert. Zuletzt war sie außerdem Teil der Redaktion des Buches »Auf dem Klo habe ich noch nie einen Schwan gesehen - Erinnerungen aus 30 Jahren Conne Island«.



Moderiert wird die Lesung von Anna Kow



Gefördert von: MONOM Stiftung für Veränderung 





Die Lesung findet unter der 3G Regelung statt. Bitte tragt eine Maske, am Platz könnt ihr sie abnehmen.



http://www.conne-island.de/termin/nr6265.html