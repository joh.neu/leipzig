---
id: "6251"
title: "Podiumsdiskussion: Gender und die Linke"
start: 2022-04-29 18:30
end: 2022-04-29 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6251.html
teaser: "Podiumsdiskussion: Gender und die LinkeDie Idee einer Befreiung der
  Geschlechter datiert zurück vo"
isCrawled: true
---
Podiumsdiskussion: Gender und die Linke



Die Idee einer Befreiung der Geschlechter datiert zurück vor den Marxismus. Es waren utopische Sozialisten wie Charles Fourier und Mary Wollstonecraft, die formulierten, dass das Versprechen von Gleichheit, Freiheit und Brüderlichkeit der bürgerlichen Revolutionen nur von Bedeutung sei, wenn es alle Menschen beträfe. Diese Forderung nahm der historische Marxismus des 19. und frühen 20. Jahrhunderts in der Frage nach der Befreiung der Frau auf und versuchte, sie in der proletarischen Weltrevolution umzusetzen. Nachdem der Versuch einer solchen Revolution in den Jahren 1919 bis 1923 als gescheitert galt, schienen Fragen nach Geschlecht von geringer Bedeutung für die Überreste marxistischer und sozialistischer Politik. Es war die Neue Linke, die Fragen nach Sexualität und Geschlecht erneut aufwarf und als Kritik des Marxismus in Stellung brachten, da dieser die Themen vernachlässigt habe. Die Zweite Welle des Feminismus, die sich aus dem gescheiterten Projekt der Neuen Linken ergab, war desillusioniert: Eine Befreiung aller Geschlechter wurde gelesen als eine Betäubung gegen die realen Machtverhältnisse eines jahrtausendealten Patriarchats - eine Befreiung der Geschlechter könne nicht bis zu einer imaginierten Diktatur des Proletariats vertagt werden. Doch bereits bald darauf zeichneten sich auch innerhalb des Feminismus ideologische und politische Differenzen und Konflikte ab. Der in den 1950ern vom Psychologen John Money eingeführte Begriff Gender, der die psychische und soziale Dimension des Geschlechts betont, hielt Einzug in feministische Diskussionen, am prominentesten vielleicht in Judith Butlers Gender Trouble. Spätestens seit diesem Zeitpunkt scheint der Begriff Gender zunehmend politische Bedeutung zu erhalten, auch und vielleicht besonders innerhalb der Linken. Als Begriff scheint er traditionelle Grenzen zwischen den Geschlechtern einzureißen, aber auch neu zu errichten. Und auch für die Linke ist Gender ein Zankapfel, über dessen korrekte Beurteilung sich Gruppen und Zeitungen spalten.  



Ist Gender eine Leerstelle des Marxismus, die nur durch neue Formen der Theorie komplettiert werden kann? Welche politische Bedeutung hat der Begriff Gender für die Linke heute? Wie steht es um Gender-Befreiung mehr als 30 Jahre nachdem sie als politisches Projekt ausgerufen wurde? Was hat die Linke gelernt aus den 200 Jahren Kampf um die Befreiung der Geschlechter? Welche politische Rolle käme Gender in einem Kampf um die Emanzipationen der Geschlechter zu?"



Sprecher*Innen: 

- Roswitha Scholz

- Stefan Hain 

- Sara Rukaj



Eine Veranstaltung von Platypus Leipzig



http://www.conne-island.de/termin/nr6251.html