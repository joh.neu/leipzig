---
id: "6264"
title: "Japan: Kapitalismus ohne Klassenkampf?"
start: 2022-03-27 18:30
end: 2022-03-27 22:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6264.html
teaser: "Japan: Kapitalismus ohne Klassenkampf? Zur Kritik des
  KaiserkapitalismusJapan ist zur Projektionsf"
isCrawled: true
---
Japan: Kapitalismus ohne Klassenkampf? Zur Kritik des Kaiserkapitalismus



Japan ist zur Projektionsfläche verschiedener gesellschaftlichen Lager in Deutschland geworden. Aufgrund ihrer Militanz faszinieren Studentenbewegung von 1968 und die Rote Armee Fraktion Japans Teile der radikalen Linken  eine Militanz die zugleich dem Bild von der arbeitsamen und unternehmensloyalen japanischen Bevölkerung widerspricht. In den 1980er und 1990ern feierte die Managementliteratur vor dem Hintergrund der sinkenden Profitabilität des deutschen Kapitals den Toyotismus als Vorbild zur Reorganisation von Arbeit und Wertschöpfungsketten. Das japanische Regulationsmodell galt als Alternative zum angloamerikanischer Neoliberalismus. Heute diskutiert die AfD den japanischen Weg als Alternative zur vermeintlich zuwanderungsfreundlichen Politik der Bundesrepublik.

Soichiro Sumida befasst sich mit der widersprüchlichen sozialen Realität Japans. Ideologiekritisch richtet sich sein Blick auf das Japan der Nachkriegsära, die historischen Regulationsphasen des Kapitalismus in Japan und die japanische Arbeiter*innenbewegung. Wieso bildete sich in Japan trotz seines Wachstums nach 1945 keine starke Arbeiter*innenbewegung heraus? Was hielt die japanische Gesellschaft zusammen trotz bestandsbedrohender Krisen und sozialer Desintegrationsprozesse? Warum sind die sozialen Bewegungen in Japan vergleichsweise sehr gering? Welche politischen Formierungsprozesse gibt es in der japanischen Linken und wie entwickelte sich der japanische Marxismus?



Soichiro ist derzeit Post-Doc an der Universität Oldenburg. Er ist an der Übersetzung der MEGA (Marx-Engels-Gesamtausgabe) in Japanische beteiligt und ist aktiv in Tokios antirassistischen Zusammenhängen.



Veranstalter "Amici della Conricerca"

Wir werden bei der Veranstaltung um Spenden bitten, um die Kosten zu decken



Der Vortrag findet unter der 3G-Regelung im Café des Conne Island statt.



http://www.conne-island.de/termin/nr6264.html