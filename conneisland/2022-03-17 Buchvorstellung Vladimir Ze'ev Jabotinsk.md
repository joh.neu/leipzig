---
id: "6247"
title: "Doppelbuchvorstellung: »Die jüdische Kriegsfront« von Jabotinsky +
  »Schiffbruch beim Spagat Wirres aus Geist und Gesellschaft 1« von
  Braunstein/Hesse"
start: 2022-03-17 19:00
end: 2022-03-17 23:00
address: Conne Island, Koburger Str. 3, Leipzig
link: http://www.conne-island.de/termin/nr6247.html
teaser: "Doppelbuchvorstellung: »Die jüdische Kriegsfront« von Vladimir Ze'ev
  Jabotinsky (ça ira) +  »Schiffb"
isCrawled: true
---
Doppelbuchvorstellung: »Die jüdische Kriegsfront« von Vladimir Ze'ev Jabotinsky (ça ira) +  »Schiffbruch beim Spagat Wirres aus Geist und Gesellschaft 1« von Dirk Braunstein / Christoph Hesse (ça ira)



Die 2. Vorstellung des Abends (mit Braunstein) muss terminlich verschoben werden. Wie freuen uns auf Anselm!



Kommt gern getestet

Der Eintritt ist Frei



Buchvorstellung: Vladimir Ze'ev Jabotinsky - Die jüdische Kriegsfront



Jabotinsky versucht Anfang 1940 in diesem seinem letzten und posthum publizierten Buch  noch im selben Jahr unter dem Titel The Jewish War Front (2. Aufl. 1942 mit dem Titel The War and the Jew) erschienen  die Situation zu umreißen, wie sie sich für den Zionismus in und nach dem eben begonnenen Krieg darstellen werde. Er nimmt zwar bereits den Beginn der Vernichtung der Juden in Polen wahr, spricht von »systematischer Vernichtung«, es entzieht sich aber selbst ihm  und das nach all den Erfahrungen, die er seit seiner Jugend von Verfolgung und Pogromen gemacht hatte  die Möglichkeit zu denken, dass die gerade stattfindenden Deportationen tatsächlich zum Zweck der totalen Vernichtung erfolgen sollten. Als Konsequenz des Kriegs sieht er darum Millionen polnischer Juden in Gefahr, die nicht zu den von ihm befürchteten zahlreichen Hungertoten zählen und an den Orten der Deportation überleben würden, aber danach erneut und umso mehr der antisemitischen Todesdrohung ausgesetzt wären  so wie es die wenigen Überlebenden nach dem Zweiten Weltkrieg dann wirklich waren. Während also Jabotinsky noch den Plan einer großangelegten Evakuierung von mehreren Millionen Juden nach Palästina entwarf, konnte die Untergrundbewegung der Bricha schließlich nur noch die Fluchthilfe von einigen Hunderttausenden organisieren.



So ist dieses 1940, kurz nach dem Tod des Autors, erschienene Buch, das nicht zuletzt ausführliche Entwürfe für die Verfassung des zu gründenden zionistischen Staats enthält, die letzte Momentaufnahme des Zionismus vor der Shoah.



An diesem Abend stellt Anselm Meyer (Berlin) das Buch vor.



Buchvorstellung: Dirk Braunstein / Christoph Hesse - Schiffbruch beim Spagat

Wirres aus Geist und Gesellschaft 1



»Unerschöpflich der Vorrat des Dummdeutschen, der sich in wissenschaftlichen oder parawissenschaftlichen Publikationen findet«, warnte Eckhard Henscheid in einem bereits 1985 erschienenen und in späteren Auflagen stetig erweiterten Wörterbuch, das dem vorliegenden von ferne Modell steht. Geschöpft wird hier allein aus dem Vorrat der Wissenschaften, die herkömmlich solche des Geistes oder der Gesellschaft und inzwischen lieber Kulturwissenschaften heißen. Zu Recht: denn die vor vierzig Jahren verkündete Austreibung des Geistes aus den Geisteswissenschaften ist vollbracht. Was aber nicht bedeutet, daß die von ihm Verlassenen endlich Ruhe gäben.



Wo der gewöhnliche Mensch eine Frage stellt, geht der studierte mindestens in Fragestellung. Er allein kennt das Geheimnis, wie man eine Stellung formuliert. Hochkomplexe und noch viel kompliziertere, ja auch sturzbanale Gedanken kann er in wundersame Wortfolgen verwandeln und also Stränge mitsamt deren Verflechtungen in einem Gefüge beleuchten, Diskurse vielschichtig verzahnen und Dimensionen in voller Breite ausloten. Ein solcher Spagat, bei dem jeder Satz Schiffbruch erleidet, ist möglich erst in einer Sprache, in der nur deshalb nichts mehr undenkbar scheint, weil in ihr schon gar nichts mehr gedacht wird. Das Verhältnis von Gedanke und Ausdruck, einmal von dem Anspruch befreit, daß da ein irgend durchsichtiges, mit Vernunft zu begreifendes oder immerhin zu bestimmendes Verhältnis überhaupt besteht, ist nur mehr schlechte Konvention. Aussichtslos, den undurchdringlich harten Sprachschrott, der sich in den sogenannten weichen Fächern aufgehäuft hat, je wieder wegzuräumen. Wie viel Sisyphusarbeit inkl. Tantalusqualen einem da bevorsteht, mag dieses provisorische Wörterbuch erst erahnen lassen.



Dirk Braunstein (Frankfurt/Main) wird aus dem Buch lesen.





Die Lesung findet unter der 3G Regelung statt. Testet euch trotzdem gern alle vorher.





http://www.conne-island.de/termin/nr6247.html