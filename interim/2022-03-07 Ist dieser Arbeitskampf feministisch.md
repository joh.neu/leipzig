---
id: "645951829806024"
title: Ist dieser Arbeitskampf feministisch?
start: 2022-03-07 18:00
end: 2022-03-07 20:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/645951829806024/
image: 274794467_4892328644186223_4995874553113453076_n.jpg
isCrawled: true
---
Am Vorabend des feministischen Kampftags diskutieren wir über die Verhandlungen im Sozial- und Erziehungsdienst:  

Ist dieser Arbeitskampf feministisch?
Welche Aussichten auf Verbesserung bringen die aktuellen Verhandlungen? 
Wie kann Solidarität gezeigt werden? 

Corona hat mehr denn je gezeigt, dass Sorgearbeit im Sozial- und Erziehungsdienst für eine solidarische Gesellschaft unverzichtbar ist - sei es für Kinder, Familien, Menschen mit Behinderung oder Geflüchteten. 83% der Sozialarbeiter*innen sind Frauen1. Ihre Arbeit wird schlecht bezahlt, es gibt massiven Personalmangel und keine Planungssicherheit trotz Verhandlungen am 25. Februar - eine gute Woche später diskutieren wir darüber mit Gewerkschafter*innen, Beschäftigten und FemStreik-Aktivist*innen.
1Weil die Statistiken nur binäre Geschlechter erfassen, schreiben wir hier „Frauen“.

WANN? Montag, 07.03.22 18 - 20 Uhr
WO? INTERIM mit 2G+ oder Stream unter: https://youtu.be/wZQnBpdx4qg
