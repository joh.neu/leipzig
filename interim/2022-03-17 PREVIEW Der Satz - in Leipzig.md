---
id: "683256656040051"
title: PREVIEW Der Satz - in Leipzig
start: 2022-03-17 16:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/683256656040051/
image: 275262805_365519712245775_8970444954266024349_n.jpg
isCrawled: true
---
VERANSTALTUNG ZUR BUCHMESSE LEIPZIG 2022 - in Leipzig

PREVIEW "Der Satz"

Lesung mit Isobel Markus

Eine Reise in die frühere Heimat, Erinnerungen an eine wilde Kindheit in der Lüneburger Heide, Abenteuer mit den Brüdern Mick und Joon und der Freundin Tati, das immer unerklärliche Verhalten der Erwachsenen, versteckte Hütten im Wald, spleenige Dorfbewohner, Sommertage in der Kiesgrube und Erkundungstouren in der verfallenen Ziegelei, die erste Liebe, vor den Erwachsenen geheim gehaltene Erlebnisse – und ein tragisches Ereignis, das damals, vor dreißig Jahren, alles veränderte und das Stück für Stück in das Gedächtnis von Linda, der Ich-Erzählerin, zurückkehrt. 

Ort: Interim by linXXnet, Demmeringstraße 32, 04177 Leipzig

powered by neues deutschland: https://www.nd-aktuell.de/termine/89041.html