---
id: "269228802026077"
title: "Auftakt #verlagegegenrechts"
start: 2022-03-16 20:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/269228802026077/
image: 275666873_4920330301386057_80788480003904182_n.jpg
isCrawled: true
---
#verlagegegenrechts ist ein Aktionsbündnis gegen rassistisches, antifeministisches und homofeindliches Gedankengut. Mittlerweile haben sich über 80 Verlage und 200 Einzelpersonen und Initiativen dem Bündnis angeschlossen. #vgr organisiert u.a. Veranstaltungen auf den Buchmessen in Frankfurt und Leipzig und diskutiert das Jahr über auf Podien. Eine Arbeitsgruppe trifft sich regelmäßig in Berlin, um weitere Aktionen und Positionen zu diskutieren. 

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig