---
id: "350778016920468"
title: Isobel Markus Buchvorstellung und Gespräch
start: 2022-03-17 16:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/350778016920468/
image: 274521535_4878899818862439_9123925448204250372_n.jpg
isCrawled: true
---
Eine Reise in die frühere Heimat, Erinnerungen an eine wilde Kindheit in der Lüneburger Heide, versteckte Hütten im Wald, spleenige Dorfbewohner, das immer unerklärliche Verhalten der Erwachsenen,  Sommertage in der Kiesgrube – und ein tragisches Ereignis, das damals, vor dreißig Jahren, alles veränderte und das Stück für Stück in das Gedächtnis von Linda, der Ich-Erzählerin, zurückkehrt. Soll sie sich zumuten, ein bestimmtes inneres Kästchen mit Erinnerungen zu öffnen oder soll sie es geschlossen lassen? Isobel Markus’ Romandebüt handelt von Verdrängung, Sprachlosigkeit und Schuldgefühlen, von Entscheidungen, die, so klein sie auch zu sein scheinen, einen folgenreichen Einfluss auf Lebenswege haben können.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig