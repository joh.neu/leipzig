---
id: "685670089285420"
title: Klaus Weber Buchvorstellung und Gespräch Wagenknecht - Deutsches Volk &
  nationaler Sozialismus
start: 2022-03-17 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/685670089285420/
image: 274545104_4878896822196072_8664874958135706196_n.jpg
isCrawled: true
---
Wie erkennen wir, was einem neuen Faschismus Vorschub leistet? Dazu müssen die ökonomischen, juristischen, kulturellen und weitere materielle wie ideologische »Felder« im Zusammenspiel untersucht werden, aber es sind auch Personen, die an der Etablierung neuen faschistischen Denkens mitwirken. Die Reihe gestalten der faschisierung versucht aktuelle Tendenzen und konzeptive Ideolog/innen in Philoso- phie, Literatur und Politik auszumachen. Zu Beginn des Bundestagswahlkampfs 2021 präsentierte Sahra Wagenknecht in Buchform eine Generalabrechnung mit »Lifestylelinken«, »Linksliberalen«, sozialdemokratischen und linken Parteien – sie alle hätten den Aufstieg der AfD politisch und kulturell zu verantworten wegen ihrer moralgetränkten Bezugnahme auf teils skurrile Minderheiten und Konzentration auf Diversitäts- und Identitätsfragen. Dabei hätte man soziale Fragen und die »normalen Menschen« vernachlässigt und sie zur Wahl neofaschistischer Parteien genötigt. Wagenknecht plädiert für eine gemeinsame nationale Perspektive, welche »normale Arbeitnehmer« und »einfache Leute« wieder »Gemeinsinn« spüren lässt. Ist es ein Wunder, dass Björn Höcke ihr einen »klaren, unverstellten Blick auf die Verhältnisse« bescheinigt und sie einlädt, zur AfD zu wechseln? Das Buch "Wagenknecht – Deutsches Volk & nationaler Sozialismus" versammelt Texte zur Kritik an Sahra Wagenknechts ökonomischen, politischen sowie kulturellen Diagnosen und Perspektiven. Den im gewerkschaftlichen Kontext aktiven Autor*innen geht es nicht um die Strategiebildung der Partei,sondern um eine inhaltliche Auseinandersetzung. Klaus Weber ist ein habilitierter deutscher Psychologe, der als Professor an der Hochschule für angewandte Wissenschaften München lehrt. Seine Arbeitsschwerpunkte sind: Sozialpsychologie, Soziale Arbeit und Faschismus, Resozialisierung, Subjekt und Gesellschaft und Gesellschaftskritik. An der Hochschule für angewandte Wissenschaften ist Weber Vertrauensdozent der Rosa-Luxemburg-Stiftung sowie der Hans-Böckler-Stiftung. Klaus Weber ist Fraktionsvorsitzender der Linksfraktion im Berzirkstag Oberbayern.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig