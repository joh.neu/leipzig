---
id: "1025926071607434"
title: "Gönül Kıvılcım: Bandenkrieg, Polizeigewalt und die Kehrseite der
  boomtown Istanbul-Buchvorstellung"
start: 2022-03-18 16:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/1025926071607434/
image: 274523906_4878893275529760_5277276527988744419_n.jpg
isCrawled: true
---
Mit dem Grundschulzeugnis in der Hand kehrt Sinan in das leergeräumte Häuschen in der Armensiedlung zurück. Sein Vater hat sich mit seiner neuen Frau aus dem Staub gemacht und für den ungeliebten Sohn keinen Platz mehr. Sinan treibt sich auf den Plätzen herum, auf denen die Gestrandeten sich sammeln, die Kinder aus den Dörfern, die Abgestürzten, und schließt sich einer Kinderbande an. Er wird zu Sinan die Klinge, dem Jungen, der sich die Arme ritzt, das Betteln, Stehlen und Sprücheklopfen lernt, und dass man dem Hunger und der Kälte mit Pillen und Verdünner beikommen kann. Die Händler vertreiben sie, die Polizei jagt sie und setzt sie an den Stadträndern aus...Mit Witz und Wärme, ungeschönt und bisweilen brutal schildert Gönül Kıvılcım das Leben einer Straßenkindergang, die Gewalt auf Polizeistationen, die Kehrseite der boom town Istanbul.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig