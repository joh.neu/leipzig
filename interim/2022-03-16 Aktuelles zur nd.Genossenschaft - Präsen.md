---
id: "333304855408452"
title: Aktuelles zur nd.Genossenschaft - Präsentation/Gespräch
start: 2022-03-16 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/333304855408452/
image: 274652979_4878903938862027_558482139092692082_n.jpg
isCrawled: true
---
Im August 2021 wurde von Mitarbeitenden der sozialistischen Tageszeitung "neues deutschland" die nd.Genossenschaft gegründet. Sie ist offen auch für Leserinnen und Leser der Zeitung, von denen bereits weite über 500 Genossenschaftsanteile gezeichnet haben. Inzwischen hat die nd.Genossenschaft eG die Herausgabe und wirtschaftliche Verantwortung für die Zeitung übernommen. Der neue Verlagsleiter Rouzbeh Taheri und Mitglieder des neuen Vorstands und Aufsichtsrates geben Auskunft über die "Mühen der Ebenen"...


Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig