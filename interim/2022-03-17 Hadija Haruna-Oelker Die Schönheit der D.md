---
id: "464612045368612"
title: "Hadija Haruna-Oelker: Die Schönheit der Differenz      Miteinander
  anders denken  Sachbuch"
start: 2022-03-17 20:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/464612045368612/
image: 274523906_4878895078862913_5149708186872470311_n.jpg
isCrawled: true
---
Hadija Haruna-Oelker, Journalistin, Politikwissenschaftlerin und Moderatorin, beschäftigt sich seit langem mit Rassismus, Intersektionalität und Diskriminierung. Sie ist davon überzeugt, dass wir alle etwas von den Perspektiven anderer in uns tragen. Dass wir voneinander lernen können. Und einander zuhören sollten. In ihrem Buch erzählt sie ihre persönliche Geschichte und verbindet sie mit gesellschaftspolitischem Nachdenken. Sie erzählt von der Wahrnehmung von Differenzen, von Verbündetsein, Perspek-tivwechseln, Empowerment und von der Schönheit, die in unseren Unterschieden liegt. Ein hochaktuelles Buch, das drängende gesellschaftspolitische Fragen stellt und Visionen davon entwickelt, wie wir Gelerntes verlernen und Miteinander anders denken können: indem wir einander Räume schaffen, Sprache finden und uns mit Offenheit und Neugier begegnen.

Hadija Haruna-Oelker, geboren 1980, lebt und arbeitet als Autorin, Redakteurin und Moderatorin in Frankfurt am Main. Sie arbeitet für den Hessischen Rundfunk – u.a. für „Der Tag“ (hr2 Kultur). Zudem moderiert sie das regelmäßige Format „StreitBar“ in der Bildungs-stätte Anne Frank und schreibt eine monatliche Kolumne in der Frankfurter Rundschau. Sie ist Preisträgerin des KAUSA Medien-preises 2012 und des Kurt-Magnus-Hörfunkpreises 2015. Gemeinsam mit Kübra Gümüºay und Uda Strätling übersetzte sie „The Hill We Climb“ von Amanda Gorman. Darüber hinaus ist sie im Journalist*innen-Verband Neue Deutsche Medienmacher*innen und in der Initiative Schwarze Menschen in Deutschland aktiv.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig