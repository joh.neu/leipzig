---
id: "469665638035104"
title: "Kristina Lunz: Die Zukunft der Außenpolitik ist feministisch Buchvorstellung"
start: 2022-03-18 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/469665638035104/
image: 274698060_4878892008863220_7630133172423575078_n.jpg
isCrawled: true
---
Immer noch dominieren alte, weiße, westliche Männer die Politik sowie Theorie und Praxis der internationalen Beziehungen. Dadurch werden die Bedürfnisse von Frauen und Minderheiten permanent ignoriert. Die Welt ist voller Kriege, Krisen und Unrecht. Kristina Lunz tritt mit ihrem „Centre for Feminist Foreign Policy” an, das zu ändern. Die Politikwissenschaftlerin, Aktivistin und Entrepreneurin denkt Frieden, Menschenrechte und Gerechtigkeit mit Außenpolitik zusammen und will so einen Paradigmenwechsel einleiten: Machtgebaren und militärischen Muskelspielen setzt sie Mediation in Friedensverhandlungen, feministische Machtanalysen und Klimagerechtigkeit entgegen. Realpolitik wird gegen Utopien ausgetauscht, und Botschafterinnen gibt es genauso viele wie Botschafter. So kann das Gegeneinander der Nationen endlich abgelöst werden, und alle werden in größerer Sicherheit und mit weniger Konflikten leben können.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig