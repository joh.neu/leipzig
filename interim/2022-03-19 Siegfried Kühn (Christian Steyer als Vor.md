---
id: "633206367757280"
title: Siegfried Kühn (Christian Steyer als Vorleser) Du küsst wie Rachmaninow
  Eine Liebesgeschichte
start: 2022-03-19 18:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/633206367757280/
image: 274526768_4878885538863867_4002148491467460354_n.jpg
isCrawled: true
---
In Potsdam gibt es das von düsteren Gerüchten umwaberte »Militärstädtchen« der Sowjetarmee. Die hält enge Verbindung zum Geheimdienst des Landes. In Potsdam, so legt es das Abkommen fest, residieren auch die Militärmissionen der Westmächte und dürfen sich fast frei im Land bewegen, wobei sie streng vom selben Geheimdienst beobachtet werden. Genauso wie die Potsdamer selbst. – »Wenn man etwas zu bereden hatte, traf man sich an der Grillbar im Interhotel.« So halten es auch Wanessa und ihre Freundin. An diesem Abend lernen die jungen Frauen zwei Männer kennen, Männer in Uniform. »Wanessa tippte zuerst auf Feuerwehr, doch dann hörte man sie Französisch sprechen. ›Die sind von der Militärmission‹, sagte Vera. ›Sind das nicht Spione?‹, fragte Wanessa. ›Aber süß die beiden‹, sagte Vera.« Wanessa verliebt sich in den Franzosen Claude. Nach einem gescheiterten Fluchtversuch findet sich Wanessa in einer Zelle im »Militärstädtchen« wieder. Doch einer der Geheimdienstler hält schützend die Hand über sie – und steht sich bald mit Claude auf Leben und Tod gegenüber.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig