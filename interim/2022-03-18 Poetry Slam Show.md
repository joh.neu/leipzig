---
id: "4907332879381939"
title: Poetry Slam Show
start: 2022-03-18 20:00
locationName: Interim
address: Demmeringstraße 32, 04177 Leipzig
link: https://www.facebook.com/events/4907332879381939/
image: 274509466_4878890598863361_8388512210493856321_n.jpg
isCrawled: true
---
Drei hochkarätige Slam Poet:innen gestalten den Abend mit ihren Texten. Statt sie im Wettbewerb gegeneinander antreten zu lassen, überlassen wir ihnen ohne Druck und Competition die Bühne, um brandneue Spoken Word Texte zu performen. Freut euch auf haarscharf beobachtete Gesellschaftkritik,  einfühlsame Lyrik und unterhaltsame Prosa. Durch den Abend moderiert Autorin, Veranstalterin und höchst inoffizielle Kiezkaiserin Josephine von Blueten Staub es treten auf: Pauline Füg, Inke Sommerlang & Jonas Galm.

Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

Der Eintritt ist frei.

Kartenvorbestellungen unter 0341-49273148 oder per Mail tickets@linxxnet.de

Es gelten die 2G+ Regeln aufgrund der aktuellen Bestimmungen zum Infektionsschutz.

Die Veranstaltung wird gestreamt und ist hier abrufbar:
https://www.youtube.com/c/linXXnetLeipzig