---
name: Animal Liberation Leipzig
website:  https://animalliberationmarchleipzig.home.blog/
email: animalliberationmarchleipzig@gmail.com
scrape:
  source: facebook
  options:
    page_id: AnimalLiberationMarchLeipzig
---
Animal Liberation Leipzig ist ein Zusammenschluss verschiedener Aktivst*innen lokaler Tierrechts- & Tierbefreiungsgruppen. #ALLtogether