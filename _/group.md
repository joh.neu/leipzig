---
name: Pseudogroup
website: _
---
Sonstige, meist einmalige Veranstaltungen welche keine eigene Gruppe im Repository besitzen.
Diese Gruppe ist nicht sichbar.

