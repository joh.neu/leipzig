---
id: "790539438867813"
title: "Reproduktive Selbstbestimmung im Iran: Eine materialistische Kritik an
  Staat und Religion"
start: 2022-05-20 18:00
locationName: Leipzig An Den Tierkliniken
link: https://www.facebook.com/events/790539438867813/
image: 278733964_516575826768991_2942736238295015222_n.jpg
isCrawled: true
---
Vortrag und Diskussion mit Mina Ahadi 

Das iranische Regime träumt von einem
bevölkerungsreichen, großen schiitischen Staat. Erst letztes Jahr
verabschiedete Ebrahim Raisi, Präsident der Islamischen Republik Iran, das "Gesetz für die
Verjüngung der Gesellschaft und den Schutz der Familie", welches ein
selbstbestimmtes Sexualleben und die freie Entscheidung über
Familienplanung für Frauen quasi unmöglich gemacht hat. Frauen, die
abtreiben oder außerehelichen Sex haben werden juristisch verfolgt; im
schlimmsten Falle werden sie wegen verletzten „Ehrgefühlen" ermordet. Im
Vortrag spricht Mina Ahadi über diese gefährlichen Zustände und
beleuchtet sie aus feministischer Perspektive. 

Die Veranstaltung findet am 20.05. um 18:00 Uhr im IfZ statt. Sie ist Teil unserer Mobi-Reihe zum feministischen Gegenprotest am 11.06.2022 in Annaberg-Buchholz zum Schweigemarsch christlicher Fundamentalist:innen. 

Bitte beachtet die 2G Beschränkung und bringt  zusätzlich einen tagesaktuellen Test mit. 

Die Veranstaltung ist eine Kooperation mit weiterdenken - Heinrich-Böll-Stiftung Sachsen.