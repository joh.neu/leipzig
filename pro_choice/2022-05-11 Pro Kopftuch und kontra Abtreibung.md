---
id: "1372939473158283"
title: Pro Kopftuch und kontra Abtreibung
start: 2022-05-11 19:30
locationName: Wohn- und Kulturprojekt B12
address: Braustraße 20
link: https://www.facebook.com/events/1372939473158283/
image: 275943653_494765878949986_633331639237985391_n.jpg
isCrawled: true
---
Über die Frauenverachtung von christlichem und islamischem Fundamentalismus in zunehmend autoritären Zeiten
- Vortrag und Diskussion mit Koschka Linkerhand


Die Kritik der Religion ist im Feminismus ziemlich eingeschlafen. Dabei wird die aktuelle autoritäre Revolte gegen die Selbstbestimmungsrechte von Frauen, Mädchen und LGBTI nicht nur von Faschist*innen, sondern auch von fundamentalistischen Religionen befeuert. Im Vortrag soll das anhand des Kopftuchgebots im politischen Islam und des Abtreibungsverbots im Katholizismus vergleichend erläutert werden. Was ist die Aufgabe einer feministischen und antifaschistischen Religionskritik?

Die Veranstaltung wird gefördert durch Weiterdenken - Heinrich-Böll-Stiftung Sachsen. 

Die Veranstaltung findet am 11.05.2022 um 19:30 Uhr im Garten des Wohn- und Kulturprojekts B12 statt. Sie ist Teil unserer Mobi-Reihe zum feministischen Gegenprotest am 11.06.2022 in Annaberg-Buchholz zum Schweigemarsch christlicher Fundamentalist*innen. 

Weitere Informationen zum Coronaschutz und für schlechtes Wetter folgen.
