---
id: "475180624011174"
title: Offenes Treffen / Open Meeting / لقاء مفتوح
start: 2022-03-03 18:00
end: 2022-03-03 19:30
address: interaction Leipzig
link: https://www.facebook.com/events/475180624011174/
image: 274705421_3219022808422154_3986991008089527572_n.jpg
isCrawled: true
---
[ english version below ]

### OFFENES TREFFEN ###

Beim offenen Treffen kannst du erfahren, was wir bei interaction Leipzig genau machen, was demnächst so los ist und wie du dich einbringen kannst. Außerdem ist das offene Treffen auch eine Möglichkeit, neue Menschen kennenzulernen und ins Gespräch zu kommen.

Teilnahme nur: 
* mit Nachweis der Genesung oder zweifacher Impfung + tagesaktuellem Schnelltest
ODER
* mit Nachweis dreifacher Impfung (Booster)

--

gefördert vom Sozialamt der Stadt Leipzig.

+++

### OPEN MEETING ###

At the open meeting, you can find out exactly what we do at interaction Leipzig, what's coming up, and how you can get involved. In addition, the open meeting is also an opportunity to meet new people and get into conversation.

Participation only: 
* with proof of recovery or double vaccination + rapid test (not older than 24 hours)
OR
* with proof of triple vaccination (booster)

--

funded by the Social Welfare Office of the City of Leipzig

----
### اللقاء المفتوح ###

في اللقاء المفتوح يمكنكم التعرف علينا و على نوعية الأنشطة والفعاليات التي نقوم بها و كيف يمكنكم الإستفاده منها. بالإضافة إلى ذلك ، يعد الاجتماع المفتوح أيضًا فرصة للتعرف على أشخاص جدد والدخول في محادثات.

الحضور فقط:
 بإثبات التطعيم الأول و الثاني أو التعافي أو نتيجة الاختبار السريع السلبية من نفس اليوم.
أو
بإثبات تطعيم كامل (ثلاثة تطعيمات)



الفعالية ممولة من مكتب الرعاية الاجتماعية في مدينة لايبزيغ.
