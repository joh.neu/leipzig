---
id: "4897713933669527"
title: Offener interaction Chor // open interaction choir
start: 2022-05-25 19:00
end: 2022-05-25 20:30
locationName: ZIMMT e.V. Torgauer Str. 80, 04318 Leipzig (Eingang durch die
  erste gelbe Tür auf der rechten Seite I Entrance through the first yellow door
  on the right side I الدخول من خلال الباب الأصفر علي الجانب الأيسر)
link: https://www.facebook.com/events/5042386389202280/
image: 274539845_3219015775089524_1868763366801711850_n.jpg
isCrawled: true
---
### INTERACTION CHOR ###

[ english below / الترجمة إلى اللغة العربية في الاسفل ]

Wir laden euch herzlich ein zur offenen Probe des interaction Chors!

Unsere Proben sind frei und offen für alle, die mitsingen wollen. Wir möchten unsere Runde vergrößern und freuen uns auf eure Stimmen!  

Wir singen, worauf wir gemeinsam Lust haben. Mehrsprachig und mehrstimmig. Lieder aus verschiedenen Ländern und Genres.

Vorkenntnisse sind nicht nötig. Die wichtigste Voraussetzung ist Begeisterung für das gemeinsame Singen und die Bereitschaft regelmäßig den Chor zu besuchen.

Die Probe wird geleitet von Viktoriia und Ian.

Es gibt ausreichend Platz im Proberaum, um Abstände einzuhalten. 

----
Gefördert vom Kulturamt Leipzig und dem Freistaat Sachsen.
Sei Teil der musikalischen interaction Facebook-Gruppe: http://bit.ly/1RBryJF

+++

### INTERACTION CHOIR ###

We invite you to the open rehearsal of the interaction choir!

Our rehearsals are free and open for everyone willing to enjoy singing together. We are looking forward to enlarge our group and are excited for your voices to join! 

We sing whatever we feel like singing together. Multilingual and polyphonic. Songs from different countries and genres. 

Previous experience is not necessary. The only requirement is your desire to sing together and willingness to attend  regularly.

The rehearsals are hold by local musicians: Viktoriia und Ian.

There is enough space in the rehearsal room to keep distances. 

----
Funded by the Cultural Office of Leipzig and the Free State of Saxony.
Be part of the musical interaction Facebook group: http://bit.ly/1RBryJF

+++

### مجموعة كورال إنترآكشن ###

أخيراً و بعد طول إنتظار تستطيع مجموعة كورال إنترأكشن بالقاء مجدداً و بشكل دوري لعدة مرات.
نحن نغني فقط ما نشعر بالرغبة في القيام به معًا، أغاني من مختلف البلدان والأنواع وبلغات متعددة. المعرفة السابقة ليست ضرورية. كل المطلوب هو الحماس للغناء مع مجموعة والاستعداد لزيارة الكورال بانتظام.
تقود الجوقة فيكتوريا ميدفيدكو من أوكرانيا (عازفة البيانو والمغنية).
توجد مساحة كافية في غرفة التدريب للحفاظ على المسافات اللازمة بين الأشخاص. نحن نلتزم بشكل كامل بقوانين الوقاية من فيروس كوفيد ١٩.

---
كن جزءًا من مجموعة التفاعل الموسيقي على Facebook: http://bit.ly/1RBryJF