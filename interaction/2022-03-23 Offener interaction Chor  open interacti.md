---
id: "4799392303501691"
title: Offener interaction Chor // open interaction choir
start: 2022-03-23 19:00
end: 2022-03-23 20:30
address: ZIMMT e.V. Torgauer Str. 80, 04318 Leipzig (Eingang durch die erste
  gelbe Tür auf der rechten Seite I Entrance through the first yellow door on
  the right side I الدخول من خلال الباب الأصفر علي الجانب الأيسر)
link: https://www.facebook.com/events/4799392303501691/
image: 274539845_3219015775089524_1868763366801711850_n.jpg
isCrawled: true
---
### INTERACTION CHOR ###
[ english below / الترجمة إلى اللغة العربية في الاسفل ]
Wir laden euch herzlich ein zur offenen Probe des interaction Chors!
Wir singen, worauf wir gemeinsam Lust haben. Mehrsprachig und mehrstimmig. Lieder aus verschiedenen Ländern und Genres.
Vorkenntnisse sind nicht nötig. Die wichtigste Voraussetzung ist Begeisterung für das gemeinsame Singen und die Bereitschaft regelmäßig den Chor zu besuchen.
Die Probe wird geleitet von Viktoriia Medvedko (Musikerin und Kulturmanagerin aus der Ukraine).

Es gibt ausreichend Platz im Proberaum, um Abstände einzuhalten. Unser Hygienekonzept orientiert sich an den jeweils geltenden Corona-Maßnahmen.

Teilnahme nur: 
* mit Nachweis der Genesung oder zweifacher Impfung + tagesaktuellem Schnelltest
ODER
* mit Nachweis dreifacher Impfung (Booster)
----
Gefördert vom Kulturamt Leipzig und dem Freistaat Sachsen.
Sei Teil der musikalischen interaction Facebook-Gruppe: http://bit.ly/1RBryJF

+++
### INTERACTION CHOIR ###
We invite you to the open rehearsal of the interaction choir!
We sing whatever we feel like singing together. Multilingual and polyphonic. Songs from different countries and genres.
Previous experience is not necessary. The most important requirement is enthusiasm for singing together and willingness to attend the choir regularly.
The rehearsals are led by Viktoriia Medvedko (musician and cultural manager from Ukraine).
There is enough space in the rehearsal room to keep distances. Our hygiene concept is based on the current Corona measures.
Participation only: 
* with proof of recovery or double vaccination + rapid test (not older than 24 hours)
OR
* with proof of triple vaccination (booster)
----
Funded by the Cultural Office of Leipzig and the Free State of Saxony.
Be part of the musical interaction Facebook group: http://bit.ly/1RBryJF
+++
### مجموعة كورال إنترآكشن ###
أخيراً و بعد طول إنتظار تستطيع مجموعة كورال إنترأكشن بالقاء مجدداً و بشكل دوري لعدة مرات.
نحن نغني فقط ما نشعر بالرغبة في القيام به معًا، أغاني من مختلف البلدان والأنواع وبلغات متعددة. المعرفة السابقة ليست ضرورية. كل المطلوب هو الحماس للغناء مع مجموعة والاستعداد لزيارة الكورال بانتظام.
تقود الجوقة فيكتوريا ميدفيدكو من أوكرانيا (عازفة البيانو والمغنية).
توجد مساحة كافية في غرفة التدريب للحفاظ على المسافات اللازمة بين الأشخاص. نحن نلتزم بشكل كامل بقوانين الوقاية من فيروس كوفيد ١٩.
الحضور فقط للأشخاص الذين تم تطعيمهم و المتعافين من كوفيد ١٩.
الحضور فقط:
بإثبات التطعيم الأول و الثاني أو التعافي أو نتيجة الاختبار السريع السلبية من نفس اليوم.
أو
بإثبات تطعيم كامل (ثلاثة تطعيمات)

---
كن جزءًا من مجموعة التفاعل الموسيقي على Facebook: http://bit.ly/1RBryJF