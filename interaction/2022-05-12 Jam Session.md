---
id: "1342406086263637"
title: Jam Session
start: 2022-05-12 18:00
end: 2022-05-12 21:00
locationName: Arno-Nitzsche Straße 37, 04277 Leipzig
link: https://www.facebook.com/events/1342406086263637/
image: 274640121_3219021735088928_1973610000789908867_n.jpg
isCrawled: true
---
[ english below / الترجمة إلى اللغة العربية في الاسفل ]

Unsere Jam Session ist ein offener und diskriminierungsfreier Treff zum Musikmachen und -hören.

Alle sind herzlich eingeladen mitzumachen, egal ob mit oder ohne Vorkenntnisse. Wir wollen gemeinsam Instrumente spielen, Musikstile aus unterschiedlichen Ländern ausprobieren, vielleicht auch nur zuhören, uns austauschen und zusammen Spaß haben.

Wenn ihr Instrumente habt, bringt sie gerne mit! Wir haben aber auch einige Vor Ort.

Eine gemeinsame Veranstaltung von Wir Sind Paten Leipzig und interaction Leipzig e.V.

Bitte beachtet, dass der Genuss von alkoholischen Getränken bei dieser Veranstaltung nicht erwünscht ist.

--
gefördert vom Kulturamt der Stadt Leipzig.
+++

Our jam session is an open and non-discriminatory meeting for making and listening to music.

Everyone is welcome to participate, whether with or without previous knowledge. We want to play instruments together, try out music styles from different countries, maybe just listen, exchange ideas and have fun together.

If you have instruments, please bring them with you! We also have some on site.

A joint event of Wir Sind Paten Leipzig and interaction Leipzig e.V.

Please note that the consumption of alcoholic beverages is not welcome at this event.

--
funded by the Cultural Affairs Office of the City of Leipzig
----

+++

بالموسيقي تتواصل الناس بغض النظر عن اصلهم و لغتهم أو ثقافتهم. و تحت هذا الشعار نعقد جلستنا الموسيقية كإجتماع مفتوح للجميع لعزف و ارتجال الموسيقى وأيضا للإستماع إليها.

نتشرف بدعوة جميع المهتمين للإشتراك معنا في هذه الجلسة الموسيقية ولا يهم إذا كانت لديكم خبرة / تجربة سابقة.
نود العزف و الغناء سوياً و تجربة الأساليب الموسيقية من الثقافات المختلفة و الحديث و تبادل الأفكار عن هذه الأساليب.

إذا كانت لديك آلة موسيقية يرجى إحضارها معك و لدينا أيضا بعض الآلات الموسيقية يمكن للجميع إستخدامها.

هذا الحدث منظم بالعمل المشترك من قبل Wir Sind Paten Leipzig و  Interaction Leipzig e.V.

الرجاء أخذ العلم، أن تناول المشروبات الكحولية 
ممنوع في هذا الحدث.

الفعالية ممولة من مكتب الرعاية الاجتماعية في مدينة لايبزيغ.