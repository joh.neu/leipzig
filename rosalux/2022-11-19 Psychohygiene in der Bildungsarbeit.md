---
id: 6IVML
title: Psychohygiene in der Bildungsarbeit
start: 2022-11-19 10:00
end: 2022-11-20 18:00
locationName: Poliklinik Leipzig
address: "Taubestraße 2, 04347 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/6IVML/
teaser: Einen Umgang finden mit Eindrücken &amp; Resilienz
isCrawled: true
---
Eine Veranstaltung der Refugee Law Clinic Leipzig in Kooperation mit der RLS Sachsen

In diesem Workshop wollen wir uns mit dem Spannungsfeld von Tatendrang und Nachhaltigkeit im Ehrenamt und in besonders herausfordernden politischen Bereichen beschäftigen: der Bildungsarbeit. Wie ist es möglich in akuten politischen Situationen zu handeln und dennoch nicht die eigene Psychohygiene zu vergessen? Wir wollen alle bestmögliche Politik machen und dies demnach auch langfristig, doch in kritischen ehrenamtlichen Verhältnissen fällt dies oft schwer: Engagierte sind überlastet, überfordert, ausgebrannt. 

An den geplanten zwei Tagen werden wir uns mit der Debatte um das Feld des kritischen Ehrenamtes beschäftigen und am Beispiel der Arbeit mit Geflüchteten belastende Situationen skizzieren. Wir wollen Rollen und Dynamiken herauskristallisieren mit Hilfe derer wir anschließend nachhaltige Strategien zur Psychohygiene und Empowerment erarbeiten und üben können. Wir laden explizit Personen aus kritischen Arbeitsfeldern ein und können Sprachmittlung zur Verfügung stellen.



**Anmeldungen per Mail unter: **[**verwaltung@rlc-leipzig.de**](<mailto:verwaltung@rlc-leipzig.de>)

**Testet euch vorher und tragt Maske während der Veranstaltung.**



Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

