---
id: 1VQWO
title: Der Krieg der Erinnerungen – Das östliche Europa und der Zweite Weltkrieg
start: 2022-09-14 19:00
end: 2022-09-14 21:00
locationName: Neues Schauspiel Leipzig
address: "Lützner Straße 29, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1VQWO/
teaser: "Reihe: Vernichtungskrieg im Osten "
isCrawled: true
---
Eine Veranstaltung der VVN-BdA leipzig, der Naturfreundejugend Leipzig, dem wbg Verlag und der RLS Sachsen.

Mit *Katja Makhotina* (Historikerin, Bonn) und *Vera Dubina* (Historikerin, Moskau)

Am 1. September jährt sich der deutsche Überfall auf Polen und damit der Beginn des Zweiten Weltkrieges zum 83. Mal. Insbesondere in den postsowjetischen Staaten hat die Erinnerung an den Zweiten Weltkrieg bis heute eine zentrale und nicht minder umkämpfte Bedeutung. Welche Rolle spielt die Erinnerung im heutigen Russland? Welche Bedeutung hat die Kriegserinnerung, in der unter dem russischen Angriffskrieg leidenden Ukraine? Und wie ist die erinnerungspolitische Lage in den baltischen Staaten und Belarus? Einen Einblick in dieses umkämpfte Feld des Gedenkens wird Ekaterina Makhotina geben. Im anschließenden Podiumsgespräch werden wir die Erinnerungsarbeit von Memorial vorstellen und auf die praktischen Auswirkungen staatlicher Geschichtspolitik an diesem Beispiel eingehen.

Verweisen möchten wir auf das kürzlich erschienen Buch von Franziska Davies und Katja Makhotina „Offene Wunden Osteuropas“ (Darmstadt, wbg, 2022), über das wir auch sprechen werden.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

