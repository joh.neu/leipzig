---
id: UQQQ5
title: Lokal extrem Rechts
start: 2022-05-11 19:30
end: 2022-05-11 21:30
locationName: Neues Schauspie Leipzig
address: "Lützner Straße 29, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/UQQQ5/
teaser: Analysen alltäglicher Vergesellschaftungen
isCrawled: true
---
<div id="magicdomid39">Mit <em>Anke Schwarz</em>, <em>Paul Zschocke</em> und <em>Axel Salheiser</em> (Autor*innen), <em>Daniel Mullis</em> und <em>Judith Miggelbrink</em> (Herausgeber*innen) und <em>Solvejg Höppner</em> (Kulturbüro Sachsen)</div>

<div id="magicdomid43">Eine Veranstaltung der RLS Sachsen in Kooperation mit chronik.LE</div>

<div></div>

<div>Ursachen und Hintergründe für das Erstarken extrem rechter Politik werden in Wissenschaft und Medien intensiv diskutiert. Dabei fehlt es gerade in wissenschaftlichen Debatten zumeist an qualitativen Analysen und differenzierten räumlichen Betrachtungen jenseits von Stadt-Land- oder Ost-West-Gräben. Der Sammelband „Lokal extrem Rechts. Analysen alltäglicher Vergesellschaftungen“, herausgegeben von Daniel Mullis und Judith Miggelbrink, adressiert diese Lücke. In den verschiedenen Beiträgen wird das Lokale als Ort individueller und emotionaler Aneignungsprozesse diskutiert und lokale Vergesellschaftungen im Kontext politischer Transformation analysiert. Die Beiträge eröffnen insgesamt einen vergleichenden Blick auf politische Konflikte, extrem rechte Raumnahmen und gesellschaftliche Widerstände. Darüber, was das Lokale für die extreme Rechte bedeutet, die Rolle von Zivilgesellschaft sowie mögliche Handlungsspielräume gegen rechts, wollen wir gemeinsam diskutieren.</div>

<div id="magicdomid70"></div>

<div id="magicdomid49"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div>

