---
id: F73BC
title: Die Neue Seidenstraße und Feindbild China - Was wir alles nicht über die
  Volksrepublik wissen
start: 2022-03-20 11:00
end: 2022-03-20 13:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04229 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/F73BC/
teaser: "REIHE: ROSAS SALON"
isCrawled: true
---
<div spellcheck="true" id="magicdomid2">Mit <em>Uwe Behrens</em> (Autor, China-Experte) und <em>Frank Schumann</em> (Verleger)</div>

<div spellcheck="true" id="magicdomid8">Eine gemeinsame Veranstaltung der Eulenspiegel Verlagsgruppe mit der RLS Sachsen</div>

<div spellcheck="true" id="magicdomid9"></div>

<div spellcheck="true" id="magicdomid11">China ist heute die zweitstärkste Volkswirtschaft der Welt, nach den USA. In Ökonomie und Ökologie marschiert das Land voran, und auch im Kampf gegen die Coronapandemie zeigt es sich erfolgreicher als westliche Staaten. Uwe Behrens (*1944), Studium an der Hochschule für Verkehrswesen „Franz List“ in Dresden, Promotion, Vertreter der DDR bei Intercontainer in Basel, Logistiker bei Deutrans-Transcontainer. Ab 1990 arbeitete er in China für verschiedene Logistikunternehmen. 2017 nach Deutschland zurückgekehrt, kontrastierte er seine Beobachtungen und Erfahrungen mit den Darstellungen in den Medien und liefert mit seinen Büchern eine erfrischend kenntnisreiche und kompetente Beurteilung des neuen China und der Neuen Seidenstraße.&nbsp;</div>

<div spellcheck="true" id="magicdomid12"></div>

<div spellcheck="true" id="magicdomid13"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div>

