---
id: VJ7NR
title: Clara Zetkin – Kämpferin für mehr Gleichheit
start: 2022-07-05 18:00
end: 2022-07-05 20:00
locationName: Freisitz des Glashauses
address: "Karl-Tauchnitz-Str. 26, 04107 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/VJ7NR/
isCrawled: true
---
Clara Zetkin war Sozialistin, Klassenkämpferin, Pazifistin, Antifaschistin, Rednerin, Redakteurin, Politikerin und – auch wenn sie sich wohl nicht so genannt hätte – Feministin. Sie setze sich unermüdlich für mehr Gerechtigkeit, Gleichberechtigung und Solidarität ein und für die Beseitigung von Klassen- und Geschlechterunterschieden. Sie eckte an, wurde missverstanden und posthum auch ideologisch ausgenutzt.

Wer war Clara Zetkin? Was können wir heute von ihr lernen? Wo würden wir ihr heute widersprechen? Diesen Fragen wollen wir gemeinsam mit Zetkin-Expertinnen auf den Grund gehen.

**Podiumsgäste:**

- Dr. Gisela Notz, Sozialwissenschaftlerin und Historikerin
- Dr. Mirijam Sachse, Mitarbeiterin Archiv der deutschen Frauenbewegung
- Vorleserin: Gabriele Chitealá

<!-- -->

Moderation: Anika Taschke (Rosa-Luxemburg-Stiftung), gemeinsam mit Albert Scharenberg Host des Podcasts [«Rosalux History»](<https://www.rosalux.de/rosalux-history>)

Eine gemeinsame Veranstaltung des Projektes fem/pulse der Louise-Otto-Peters-Gesellschaft e.V. und der Rosa-Luxemburg-Stiftung.

