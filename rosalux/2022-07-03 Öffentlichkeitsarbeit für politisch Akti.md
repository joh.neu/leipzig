---
id: NO949
title: "ENTFÄLLT: Öffentlichkeitsarbeit für politisch Aktive (verschoben auf
  September)"
start: 2022-07-03 10:00
end: 2022-07-03 16:00
locationName: Rosa Luxemburg Stiftung Sachsen
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/NO949/
isCrawled: true
---
 Mit *Steven Hummel* (Rosa Luxemburg Stiftung Sachsen, chronik.LE)



Ihr habt coole und politisch wichtige Inhalte, die ihr gern einer größeren Öffentlichkeit präsentieren wollt, aber wisst nicht wie genau? Ihr habt verschiedene Social-Media-Profile aber keinen Einblick, ob ihr eigentlich viel zu viel, zu wenig oder etwas ganz Falsches dort postet? Ihr wisst nicht wie Journalist\*innen angesprochen und zur Berichterstattung bewegt werden können? Dann seid ihr in diesem Workshop genau richtig, denn klar ist: Öffentlichkeitsarbeit passiert nicht irgendwie nebenbei, sondern muss bereits in der Planung und Konzeption von Veranstaltungen und Projekten mit berücksichtigt werden. Genau daran wollen wir - mit euren konkreten Erfahrungen und Projekten - gemeinsam arbeiten und Ideen und überzeugende Pläne für eure Öffentlichkeitsarbeit entwickeln.

Der Workshop richtet sich hauptsächlich an außerparlamentarisch aktive Menschen mit haupt- und ehrenamtlichem Interesse an Öffentlichkeitsarbeit. Vorkenntnisse sind nicht nötig, aber hilfreich. Eigene Erfahrungen können gern eingebracht werden.



Steven Hummel arbeitet seit 2020 bei der Rosa Luxemburg Stiftung Sachsen und ist dort zur Zeit unter anderem für Öffentlichkeitsarbeit zuständig. Außerdem engagiert er sich ehrenamtlich bei der Dokumentationsplattform chronik.LE (www.chronikLE.org) und betreut dort ebenfalls die Öffentlichkeitsarbeit.



**Anmeldung bis 27. Juni per Mail: anmeldung@rosalux-sachsen.de**



*In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.*