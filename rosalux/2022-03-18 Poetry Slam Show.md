---
id: SGE78
title: Poetry Slam Show
start: 2022-03-18 20:00
end: 2022-03-18 22:00
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/SGE78/
isCrawled: true
---
Eine Veranstaltung des Projekt- und Abgeordnetenbüros Interim und der RLS Sachsen

Drei hochkarätige Slam Poet:innen gestalten den Abend mit ihren Texten. Statt sie im Wettbewerb gegeneinander antreten zu lassen, überlassen wir ihnen ohne Druck und Competition die Bühne, um brandneue Spoken Word Texte zu performen. Freut euch auf haarscharf beobachtete Gesellschaftskritik, einfühlsame Lyrik und unterhaltsame Prosa. Durch den Abend moderiert Autorin, Veranstalterin und höchst inoffizielle Kiezkaiserin Josephine von Blueten Staub.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

