---
id: KPBJ8
title: Der Kuaför aus der Keupstraße
start: 2022-08-23 19:00
end: 2022-08-23 21:00
locationName: naTo
address: "Karl-Liebknecht-Str. 46, 04275 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/KPBJ8/
isCrawled: true
---
Eine Veranstaltung der Gruppe Rassismus tötet! Leipzig und der Cinémathèque Leipzig in Kooperation mit der RLS Sachsen.

Am 9. Juni 2004 explodierte eine Nagelbombe vor dem Geschäft des Frisörs Özcan Yildirim in der Kölner Keupstraße. Schnell wird er in den Augen der ermittelnden Behörden zum potentiellen Täter. Ein rassistisches Motiv wird weitestgehend ausgeblendet. Erst sieben Jahre später werden die wahren Täter\*innen enttarnt: die Rechtsterrorist\*innen des selbsternannten Nationalsozialistischen Untergrunds (NSU). Die Ermittlungen gegen die Opfer werden eingestellt. Es haben sich jedoch tausende Seiten an Ermittlungsakten angehäuft, die das skandalöse Vorgehen der Behörden dokumentieren.

Auf eindrückliche Weise zeigt DER KUAFÖR AUS DER KEUPSTRASSE wie tiefgreifend der Bombenanschlag, aber auch die Verdächtigungen danach, das Leben im Kölner Stadtteil Mülheim erschüttert haben. So wie in Köln wurden auch in den anderen Städten, in denen der NSU gemordet hat, zumeist die Angehörigen und ihr Umfeld verdächtigt. Als der Film erschien, eröffnete dieser die Diskussion über die Frage von strukturellem Rassismus in Deutschland auf eine neue Art: aus der Perspektive der Betroffenen.

On June 9, 2004, a nail bomb exploded in front of the hairdresser Özcan Yildirim's shop on Cologne's Keupstrasse. In the eyes of the investigating authorities, he quickly becomes a potential perpetrator. A racist motif is largely ignored. The real perpetrators were only exposed after seven years: the right-wing terrorists of the self-proclaimed National Socialist Underground (NSU). The investigations against the victims are stopped. However, thousands of pages of investigation files have accumulated documenting the scandalous actions of the authorities. In an impressive way DER KUAFÖR AUS DER KEUPSTRASSE shows how profoundly the bomb attack, but also the suspicions that followed, shook life in the district Mülheim of Cologne. As in Cologne, in the other cities in which the NSU murdered, it was mostly the relatives and those around them who were suspected. When the film was published, it opened up the discussion about structural racism in Germany in a new way: from the perspective of those affected. (German & Turkish with German subtitles)

Die Veranstaltung ist Teil der Veranstaltungsreihe zum Pogrom in Rostock-Lichtenhagen vor 30 Jahren. Weitere Informationen auf der [Webseite von Rassismus tötet! Leipzig](<https://www.rassismus-toetet-leipzig.org/index.php/veranstaltungsreihe-zum-pogrom-in-rostock-lichtenhagen/>).

