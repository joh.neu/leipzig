---
id: YPL4F
title: Frieden und Freiheit für die Ukraine – Was tun?
start: 2022-09-06 19:00
end: 2022-09-06 21:00
locationName: Pögehaus
address: "Hedwigstrasse 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/YPL4F/
isCrawled: true
---
Mit* Jan van Aken* (RLS), *Jule Nagel* (Mdl) und *Boris Krumnow* (Osteuropaexperte)

Eine Veranstaltung des Abgeordnetenbüros linxxnet und der RLS Sachsen in Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.

Der russische Angriffskrieg auf die Ukraine ist ein verbrecherischer Akt. Unsere Solidarität gilt den Menschen in der Ukraine, die sich – auch mit der Waffe in der Hand – gegen die russische Armee wehren. Aber wie lässt sich eine solche Solidarität auch zivil denken, ohne gleich 100 Milliarden für die Bundeswehr auszugeben oder Waffen nach Kiew zu liefern? Welche mögliche Lösungen gibt es für diesen Konflikt mit einem derart skrupellosen Aggressor? Und wie kann eine künftige Friedensordnung für Europa jetzt noch aussehen? Mit Jan van Aken wollen wir der Frage nachgehen, was könnte, was sollte Deutschland tun, um den Menschen in der Ukraine und in ganz Europa eine friedliche Perspektive zu sichern?

Jan van Aken arbeitet zu internationalen Konflikten bei der Rosa-Luxemburg-Stiftung. Er ist promovierter Biologe, arbeitete als Gentechnikexperte für Greenpeace und von 2004 bis 2006 als Biowaffeninspekteur für die Vereinten Nationen. Zwischen 2009 und 2017 war er Abgeordneter der Linksfraktion im Deutschen Bundestag.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*







