---
id: QN4TI
title: "Die vergessenen Migrant:innen       "
start: 2022-09-28 19:00
end: 2022-09-28 21:00
locationName: Pöge-Haus
address: "Hedwigstraße 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/QN4TI/
isCrawled: true
---
Mit *Djif Djimeli* (Regisseur)

Eine Veranstaltung des sächsischen Flüchtlingsrat und Protest LEJ (angefragt) in Kooperation mit der RLS Sachsen.

Der Dokumentarfilm von Djif Djimeli beleuchtet in den Portraits von vier Migrant:innen, die in Mali unterwegs sind, die Auswirkungen des Klimawandels und den Einfluss des EU-Grenzregimes auf Mobilität in Westafrika: Eine politische Aktivistin, die im Steinbruch arbeitet, ein Fischer, den der Klimawandel zu einem neuen Geschäftsmodell zwingt, ein Hühnerzüchter ohne Hühner und ein Stoffhändler ohne Kund:innen – zwischen Binnenmigration und dem Traum von Europa suchen sie alle auf ihre Weise nach besseren Perspektiven.

Im Anschluss findet ein Podiumsgespräch mit dem Regisseur sowie weiteren Perspektiven auf Klimawandel als Ursache für Flucht und Migration statt.

<div class="infobox">Trailer: <a href="https://www.youtube.com/watch?v=yoyW2TpPFBM" target="_blank" rel="noreferrer">https://www.youtube.com/watch?v=yoyW2TpPFBM</a>Infos&nbsp;<a href="https://afrique-europe-interact.net/2089-0-Film.html" target="_blank" rel="noreferrer">https://afrique-europe-interact.net/2089-0-Film.html</a>Filmlänge: 60 MinutenDiskussion: 45 - 60 Minuten</div>

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

