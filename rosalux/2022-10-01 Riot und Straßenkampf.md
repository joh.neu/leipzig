---
id: 6NQ56
title: Eine feministische Perspektive auf aktuelle soziale Kämpfe
start: 2022-10-01 14:30
end: 2022-10-01 15:30
locationName: Garage Ost
address: "Hermann-Liebmann-Strasse 65-67, 04315  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/6NQ56/
teaser: Urbane soziale Bewegungen als erweiterte Klassenkämpfe
isCrawled: true
---
Mit *Sarah Uhlmann *

Eine Veranstaltung des Institut für Zukunft in Kooperation mit der RLS Sachsen im Rahmen des Balance Club Culture Festivals 2022

Immer wieder wurde in den letzten Jahren eine Spaltung zwischen Identitätspolitik auf der einen und Klassenpolitik auf der anderen Seite herbeigeredet. Entgegen dieser Wahrnehmung will der Vortrag der These nachgehen, dass alle soziale Bewegungen und Konflikte Ausdruck von Klassenkämpfen sind. Hierfür werden feministische Theorien politischer Ökonomie herangezogen, mithilfe derer man zu einem erweiterten Verständnis des Kapitalismus gelangt. Demnach basiert der Kapitalismus nicht allein auf der Ausbeutung von Lohnarbeiter\*innen, Privateigentum und einem Markt zum Warentausch. Vielmehr ist die Produktion von Gütern und Dienstleistungen (und damit die des Mehrwerts) maßgeblich auf die Produktion und Reproduktion der Menschen und zugleich auf eine vergeschlechtlichte und rassifizierte Arbeitsteilung angewiesen. Die kapitalistische Totalität lässt sich somit nur in der Verschränkung der Sphäre der Produktion und Reproduktion erfassen. Vor dem Hintergrund dieser Analyse soll im Vortrag erläutert werden, wie wir durch eine zunehmende Privatisierung und Kommodifizierung von immer weiteren gesellschaftlichen Bereichen in eine fundamentale Krise der sozialen Reproduktion geraten sind. Zugleich erlaubt diese Perspektive, die damit verbundenen Kämpfe als erweiterte Klassenkämpfe zu deuten. Dabei handelt es sich aber nicht nur um eine begriffliche Zuschreibung. In dem Vortrag soll vielmehr diskutiert werden, inwiefern feministische, ökologische Bewegungen, Black Lives Matter und Wohnraumkämpfe miteinander verwoben sind und welche Eigenschaften und Potenziale diese sogenannten Reproduktionskämpfen besitzen.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*



