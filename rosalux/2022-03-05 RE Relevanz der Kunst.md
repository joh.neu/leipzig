---
id: 95E3C
title: "RE: Relevanz der Kunst"
start: 2022-03-05 19:00
end: 2022-03-05 21:00
locationName: Festsaal der Hochschule für Grafik und Buchkunst
address: "Wächterstraße 11, 04107  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/95E3C/
isCrawled: true
---
<div spellcheck="true" id="magicdomid34">mit <em>Philipp Farra, Olga Hohmann, Niklas Draeger, Jacki Fahrenwald, Jane Heymann, Sidar Kurt, Marcella Matos</em></div>

<div spellcheck="true"></div>

<div spellcheck="true">Kunstinstitutionen, insbesondere Kunsthochschulen verhandeln — implizit oder explizit — Fragen nach künstlerischer und gesellschaftlicher Praxis, (de)markieren ihre Schnittstellen und Widersprüche. Wie übersetzt sich der Diskurs um die Relevanz der Kunst in die eigene Arbeit? Ist es sinnvoll, aktuell politische Themen in die eigene Praxis aufzunehmen und vice versa: welche Relevanz entfaltet künstlerische Praxis, wenn sie das nicht tut? Welche institutionelle Macht haben Kunsthochschulen als Ausbildungsinstitutionen mit ihren spezifisch historisch materialisierten Kontexten?</div>

<div spellcheck="true" id="magicdomid38">Fünf Freunde treffen sich zu einer Dinnerparty. Beim geselligen Zusammensein kommt es zu Auseinandersetzungen. Man tauscht sich aus über Visaprobleme, Cancel Culture, Überlegungen zur Produktion von Kunst und den Schnittstellen und Widersprüchen innerhalb künstlerischer und gesellschaftlicher Praxis.</div>

<div spellcheck="true" id="magicdomid39"></div>

<div spellcheck="true" id="magicdomid40">TEAM: <em>Sarah Becker, Irma Blumstock, Felix Pötzsch, Maxi Richter, Nadine Rangosch, Fabian Hampel, Anna Sophie Knobloch, Mina Bonakdar, Leander Kreissl, Katarina Dacic</em></div>

<div spellcheck="true"></div>

<div spellcheck="true"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div>

