---
id: WKNKC
title: “Weder links, noch rechts?” Das Konzept Querfront
start: 2022-10-06 19:00
end: 2022-10-06 21:00
locationName: "Pögehaus "
address: "Hedwigstr. 20 04315, 04365 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/WKNKC/
teaser: "Reihe: &quot;Neue&quot; politische Unübersichtlichkeiten"
isCrawled: true
---
Mit *Volkmar Wölk* (Autor)

genauer Ort wird noch bekannt gegeben

Ein Gespenst geht um in Deutschland. Das Gespenst der „Querfront“. Alles und nichts erscheint als Querfront. Wenn Linke gegen die soziale Kälte mit einem heißen Herbst protestieren wollen, gleichzeitig Rechte gegen die Demokratie mobilisieren: Querfront.

Die Bildung einer Querfront ist ein politisches Konzept, ein Konzept der extremen Rechten. Es ist stets verbunden mit dem Versuch der Verknüpfung der sozialen Frage mit dem Nationalismus. Die Spur dieses Konzeptes zieht sich vom Frankreich des Jahres 1911 mit dem „Cercle Proudhon“ über die antidemokratischen Denker der Weimarer Republik bis zur Neuen Rechten in der Gegenwart. Und es fand teilweise Widerhall in der Linken: beim Schlageter-Kurs der historischen KPD ebenso wie beim „Friedenswinter“ von Teilen der Friedensbewegung in der Gegenwart.

Volkmar Wölk (Grimma) bietet einen historischen Abriss und eine Ideologieanalyse dieses Ansatzes.



**Weitere Veranstaltungen der Reihe "Neue" politische Unübersichtlichkeiten: **

- Von Kraken, Kapitalisten und Kriegstreibern - Anschlussfähigkeit von antisemitischen Denkmustern in linken Strukturen. [Chemnitz, 15. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/5LJPK/von-kraken-kapitalisten-und-kriegstreibern?cHash=211b4b25659f547ff9a654a8edf1b8f2>)
- Antifeminismus - Was ist das (nicht)? [Chemnitz, 24. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/2RR28/antifeminismus---was-ist-das-nicht?cHash=0714ae9a120a831c61a6077204fd3474>)
- Das Comeback von Sozialdarwinismus und Biologismus. [Dresden, 25. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/VBQAO/das-comeback-von-sozialdarwinismus-und-biologismus?cHash=d5d5af3cfac32fc8f064db6c98712615>); [Chemnitz, 26. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/US7QK/das-comeback-von-sozialdarwinismus-und-biologismus?cHash=79108045ba7337769dda1beb2a2218ec>); [Leipzig, 27. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/IBQYD/das-comeback-von-sozialdarwinismus-und-biologismus?cHash=749af3512b5904f13504319f3f0d556d>)
- Radikalisierter Konservatismus. [Online, 01. November 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/ENSTI/radikalisierter-konservatismus?cHash=f9740a543c49f93b1c57dbfdd74be72f>)
- Was ist Antikapitalismus von rechts? [Online, 07. November 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/RTYKY/was-ist-antikapitalismus-von-rechts?cHash=e0f9d534153639189365a6b5452d93a3>)

    


<!-- -->

**Hinweis zum Infektionsschutz**

Für unsere Veranstaltungen gilt Folgendes: Zum Schutz aller bitten wir, sich vor der Veranstaltung zu testen und vor Ort eine Maske zu tragen. Bleiben Sie bitte zu Hause, wenn sie Erkältungssymptome haben oder sich unwohl fühlen.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

