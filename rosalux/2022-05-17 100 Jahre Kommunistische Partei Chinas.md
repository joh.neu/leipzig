---
id: HBUER
title: 100 Jahre Kommunistische Partei Chinas
start: 2022-05-17 19:00
end: 2022-05-17 21:00
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/HBUER/
isCrawled: true
---
<div id="magicdomid367" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z">Mit </span><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z i"><em>Wolfram Adolphi</em></span><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"> (Journalist, Politikwissenschaftler)</span></div>

<div id="magicdomid368" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"></span><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"></span></div>

<div id="magicdomid370" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z">Der rasante Aufschwung Chinas findet unter Führung der kommunistischen Partei des Landes statt, die gerade 100 Jahre alt geworden ist. Zu begreifen ist das nur mit Neugier, nie mit Vorurteil. Das Wohl und Wehe Chinas ist aufs Engste verknüpft mit dem Wohl der Menschheit überhaupt. Ein Blick in die 100-jährige Parteigeschichte macht diesen Zusammenhang erst richtig begreiflich.</span></div>

<div id="magicdomid371" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"></span></div>

<div id="magicdomid372" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z">Wolfram Adolphi ist Politikwissenschaftler und Journalist. Er veröffentlichte bereits mehrere Sachbücher und Romane zu China. Zuletzt ist von ihm die Roman-Trilogie "Hartenstein" (2015, 2018, 2020) erschienen welche sich mit innerfamiliären Auseinandersetzungen und deutscher Erinnerungskultur beschäftigt.</span></div>

<div id="magicdomid373" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"></span></div>

<div id="magicdomid374" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></span></div>

