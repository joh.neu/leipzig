---
id: ANMT9
title: Brasilien über alles!
start: 2022-06-27 19:00
end: 2022-06-27 21:00
locationName: Translib
address: "Goetzstraße 7, 04177  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/ANMT9/
teaser: Bolsonaro und die rechte Revolution
isCrawled: true
---
Mit* Niklas Franzen* (Journalist&Autor)



„Brasilien über alles, Gott über allen“, lautete im Jahr 2018 die Wahlkampfparole von Jair Bolsonaro. Sein fulminanter Aufstieg zum Präsidenten Brasiliens hat das Land stark verändert und einer „konservativen Revolution“ ausgesetzt: Soziale und gesellschaftliche Errungenschaften, die nach der Militärdiktatur mühsam errungen wurden, sind bereits systematisch zurückgedreht und der rechte Kulturkampf zwingt die meisten Oppositionellen zur Flucht ins Ausland. Mit Bolsonaros Segen konnten sich fundamentalistische Christ\*innen in den politischen Institutionen des Landes festsetzen, während Goldgräber\*innen und Holzfäller\*innen ganze Landstriche in Amazonien erobert haben, und immer mehr Waffen im Umlauf sind. Mit seiner unkonventionellen Art unterhöhlt Bolsonaro zudem viele Grundsätze des politischen Systems. Die Inszenierung als Anti-Politiker hat er perfektioniert und die sozialen Medien setzt er als Waffe ein. Brasiliens Präsident steht für eine neue Art des Rechtsradikalismus, die keine Panzer mehr auf den Straßen braucht, und in einer unheiligen Allianz von Neoliberalen, Militärs und Fundamentalist\*innen das Land nach ganz rechts zu drehen versucht.

Die Folgen seiner Amtszeit sind derweil verheerend, denn Bolsonaro hat das größte Land Lateinamerikas an den Rand des Kollaps geführt: traumatisiert durch die Pandemie, als Aussätziger im Ausland gehandelt, zernagt durch die Wirtschaftskrise. Dennoch kann sich der Rechtsradikale auf den harten Kern seiner Unterstützer\*innen verlassen. Mit dem Bolsonarismus gibt es eine schlagkräftige Bewegung, die treu hinter ihrem Idol steht. Die für Oktober 2022 angesetzte Präsidentschaftswahl wird demnach eine harte Bewährungsprobe für Brasilien sein.

In der Buchvorstellung wirft der Autor Niklas Franzen den Blick auf ein Land im Krisenmodus: Wo sind die Ursprünge der rechten Revolte in Brasilien zu verorten? Wie gestaltet sich das soziale Leben unter den Bedingungen einer rechtspopulistischen Durchdringung der Gesellschaft und wo verlaufen dennoch Bruchlinien in Bolsonaros Projekts? Was droht schließlich dem Land in der Zukunft, welche möglichen Verheerungen und politischen Perspektiven lassen sich für Brasilien heute ausmachen?

Niklas Franzen ist Journalist und ehemaliger Brasilien-Korrespondent. Soeben ist sein Buch „Brasilien über alles. Bolsonaro und die rechte Revolte“ bei Assoziation A erschienen ([https://www.assoziation-a.de/buch/Franzen\_Brasilien](<https://www.assoziation-a.de/buch/Franzen_Brasilien?fbclid=IwAR1IZq4EiEE_gnZBSB21VohTijkEO8c6drl3Pb1y8IRk00CWKvJVp3yfuIk>)), welches an dem Abend vorgestellt und diskutiert wird.

Bitte testet euch vorher und bleibt bei Corona-Symptomen zu Hause.

Eine Veranstaltung in Kooperation mit der Translib und der Rosa Luxemburg Stiftung Sachsen.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushalts*

