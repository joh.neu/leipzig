---
id: EZQ8C
title: Klar und deutlich mit Stimme und Stift
start: 2022-05-07 10:00
end: 2022-05-07 16:00
locationName: Rosa Luxemburg Stiftung Sachsen
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/EZQ8C/
teaser: Workshop zu Moderation und Visualisierung
isCrawled: true
---
<div id="magicdomid320" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z">Mit <em>Almut Heinrich</em> und <em>Adam Williams</em> (beide Moderator*innen und politische Bildner*innen)</span></div>

<div id="magicdomid321" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"></span><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"></span></div>

<div id="magicdomid323" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z">Ob Teamsitzung, Podiumsdiskussion oder Plenum: Wo gemeinsamer Austausch stattfindet, kann Gesprächsttruktur hilfreich sein. Wo Themen anschaulich vorbereitet und visuell festgehalten werden, bleiben Ergebnisse besser im Gedächtnis.</span></div>

<div id="magicdomid324" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z">In diesem Workshop stehen die Themen Moderation und Visualisierung im Vordergrund. Er richtet sich an Interessierte, die verschiedene Grundtechniken lernen und ausprobieren wollen. Zwei erprobte Moderator:innen zeigen Tipps und Tricks, wie die Vorbereitung und Durchführung von Moderation in Gruppenkontexten gelingt. Vorkenntnisse sind nicht nötig.</span></div>

<div id="magicdomid325" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"></span></div>

<div id="magicdomid326" class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"><strong>Anmeldung bis 02. Mai per mail: <a href="mailto:anmeldung@rosalux-sachsen.de">anmeldung@rosalux-sachsen.de</a></strong></span></div>

<div class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"><strong></strong></span></div>

<div class="ace-line"><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z"><div id="magicdomid431" class="ace-line"><em><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z i">In </span><span class="author-a-z78zhz78zqz70zaz74zz66zz82zb73z87zz90zlz72z">Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.</span></em></div></span></div>

