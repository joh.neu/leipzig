---
id: NQWAI
title: Offener Prozess - NSU-Aufarbeitung in Sachsen
start: 2022-05-05 15:00
end: 2022-05-05 17:00
locationName: Galerie für zeitgenössische Kunst
address: "Karl-Tauchnitz-Straße 09-11, 04107 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/NQWAI/
isCrawled: true
---
<div spellcheck="true" id="magicdomid16">Mit <em>Hanna Thuma</em> (Vermittler:in Offener Prozess)</div>

<div spellcheck="true"></div>

<div spellcheck="true" id="magicdomid17">Eine Veranstaltung der RLS Sachsen und des Projekts Offener Prozess – NSU-Aufarbeitung in Sachsen des ASA-FF e.V. und YUNIK Konferenz für kulturelle Bildung – veranstaltet von der Bundeszentrale für politische Bildung, der Kulturstiftung des Bundes und der Kulturstiftung der Länder in Kooperation mit der Stadt Leipzig, dem Sächsischen Staatsministerium der Justiz und für Demokratie, Europa und Gleichstellung und dem Sächsischen Staatsministerium für Wissenschaft, Kultur und Tourismus</div>

<div spellcheck="true" id="magicdomid19"></div>

<div spellcheck="true" id="magicdomid20">Die Ausstellung Offener Prozess widmet sich dem NSU-Komplex und nimmt dabei die Ost-Deutsche Realität, insbesondere in Sachsen, zum Ausgangspunkt. Sie erzählt eine Geschichte des NSU-Komplexes, die von den Migrationsgeschichten, den Kontinuitäten rechter und rassistischer Gewalt und vom Widerstand dagegen handelt. Mit dem Ansatz eines “lebendigen Erinnerns” rückt sie marginalisierte Perspektiven in den Mittelpunkt. Darüber hinaus nimmt sie strukturellen und institutionellen Rassismus ins Visier. Künstlerische Beiträge von Harun Farocki, Hito Steyerl, belit sağ, Želimir Žilnik, Ulf Aminde und Forensic Architecture u.a. widmen sich den Migrationsgeschichten und Lebensrealitäten von Gastarbeiter*innen, der rechtsterroristischen Gewalt sowie dem Alltagsrassismus in Deutschland. Aktivistische Initiativen erinnern an diejenigen, die Opfer dieser Gewalt geworden sind und an die Stimmen derer, die sich dagegen zur Wehr setzen. Zuhören wird hier als politische Praxis verstanden, Erinnern als Prozess. Diese Ausstellung fordert auf zum Handeln.</div>

<div spellcheck="true" id="magicdomid21"></div>

<div spellcheck="true" id="magicdomid22"><strong>Wegen begrenzter Plätze bitten wir um eine Anmeldung bis 30. April per E-Mail: anmeldung@rosalux-sachsen.de</strong></div>

<div spellcheck="true" id="magicdomid23"></div>

<div spellcheck="true" id="magicdomid24"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div>

