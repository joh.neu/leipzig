---
id: JB8ID
title: Sachsen im Klimawandel
start: 2022-05-18 18:00
end: 2022-05-18 20:00
locationName: Haus der Demokratie
address: "Bernhard-Göring-Straße 152, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/JB8ID/
teaser: Welche Herausforderungen stellen sich an Politik und Gesellschaft?
isCrawled: true
---
Mit *Wilfried Küchler* (Dipl. Meteorologe)

Eine Veranstaltung des Bildungsprojekts “Sachsen im Klimawandel” in Kooperation mit der RLS Sachsen.

Dürren, Hitze, Starkregen, Stürme wachsender Intensität und Häufigkeit charakterisieren Wetter und Witterung in Sachsen. Sogar Tornados bietet die Wetterküche! Dipl.-Meteorologe Wilfried Küchler nimmt die Gäste seines Vortrags mit auf eine kurzweilige Reise in die Welt der jüngsten klimatischen Veränderungen vor unserer Haustür. Er beschreibt die akuten Folgen ausgwählter Ereignisse und ordnet diese dem Phänomen der regionalen und globalen Erderwärmung zu . Insbesondere die Dürre der Jahre 2018, 2019 und 2020 verursachten lokal differenzierte Ernteertragsausfälle und wirkt in den sächsischen Forsten Jahrzehnte nach. Welche Forderungen sich aus diesen und weiteren Klimawandelfolgen für politische Verantwortungsträger und für die Gesellschaft ergeben, beantwortet und diskutiert im Anschluss Marco Böhme (MdL Die Linke).

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*



