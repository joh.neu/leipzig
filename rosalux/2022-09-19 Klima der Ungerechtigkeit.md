---
id: 1I8SR
title: Klima der Ungerechtigkeit
start: 2022-09-19 19:30
end: 2022-09-19 21:00
locationName: Neues Schauspiel Leipzig
address: "Lützner Straße 29, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1I8SR/
isCrawled: true
---
Mit ***Tetet Lauron*** (Beraterin für Internationale Klimapolitik, Rosa-Luxemburg-Stiftung New York), ***Max Becker*** (DIE.LINKE) und ***Clara Thompson*** (Bündnis Wald statt Asphalt), Moderation ***David Williams*** (Programmleitung Klimagerechtigkeit, Rosa-Luxemburg-Stiftung New York).

Laut dem Weltklimarat müssen unsere Emissionen drastisch reduziert werden damit die Erde für alle bewohnbar sein kann. Dabei hat die Klimakatastrophe bereits jetzt enorme Auswirkungen für marginalisierte Menschengruppen. Trotz grüner Regierungsbeteiligung kommt Deutschland seinem Versprechen zur internationalen Klimafinanzierung für marginalisierte Menschengruppen die stark von der Klimakatastrophe betroffen sind nicht nach. Warum es essenziell ist die globale Perspektive im Kampf für Klimagerechtigkeit mitzudenken, und wie sich Menschen auch lokal dafür einsetzen können, besprechen wir gemeinsam mit Tetet Lauron (Beraterin für Internationale Klimapolitik, Rosa-Luxemburg-Stiftung New York), Max Becker (Die Linke), und Clara Thompson (Bündnis Wald statt Asphalt), moderiert von David Williams (Programmleitung Klimagerechtigkeit, Rosa-Luxemburg-Stiftung New York).



#### Climate of injustice

According to the Intergovernmental Panel on Climate Change (IPCC) our emissions need to be drastically reduced to ensure the earth will be habitable for all. However, the climate catastrophe is already having major impacts on marginalized communities across the world. Despite Green government participation, Germany is not fulfilling its obligations in terms of climate finance for those communities most affected by climate catastrophe. We would like to discuss why considering the global perspective in the struggle for climate justice is so crucial, and how people can get involved in this struggle at the local level, with Tetet Lauron (Senior Advisor for International Climate Politics, Rosa-Luxemburg-Stiftung New York), Max Becker (Die Linke), and Clara Thompson (Bündnis Wald statt Asphalt), moderated by David Williams (Director International Climate Justice Program, Rosa-Luxemburg-Stiftung New York). 



In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.

