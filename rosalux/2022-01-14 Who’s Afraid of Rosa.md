---
id: 3BRPG
title: Who’s Afraid of Rosa
start: 2022-01-14 19:00
end: 2022-01-14 21:00
locationName: a&amp;o Kunsthalle
address: "Online, "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/3BRPG/
isCrawled: true
---
Mit *Prof. Dr. Jörg Heiser* (Prorektor der UDK Berlin), *Alexander Karschnia* (Dramaturg Volksbühne Berlin), *Renata Kaminska* (Teilnehmende Künstlerin) und *Julien Rathje* (a&o Kunsthalle)

Die künstlerischen Arbeiten, die in der Ausstellung “Who’s afraid of\_Rosa” gezeigt werden, setzen sich mit der historischen, politischen und kulturellen Bedeutung Rosa Luxemburgs auseinander. Das ästhetische Spektrum ist bewusst divers angelegt und gibt jeweils keine unmittelbare Lesart vor, die 1:1 gesellschaftspolitisch zu “übersetzen” wären. Jedoch sind sie in hohem Maße anschlussfähig für zeitgenössische Diskussionen zum Arbeitsbegriff, Körper und Politik oder Faschismus. Mit dem Symposium soll die diskursive Ebene des gesamten Ausstellungsprojektes eingeholt, sichtbar gemacht und für neue Impulse geöffnet werden. Der Schwerpunkt wird hier auf dem programmatischen Dreiklang “public space / private space / political space” liegen.

