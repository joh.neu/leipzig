---
id: EIK6K
title: Black Faces in White? Space
start: 2022-08-26 19:00
end: 2022-08-26 21:00
locationName: naTo
address: "Karl-Liebknecht-Str. 46, 04275 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/EIK6K/
isCrawled: true
---
Eine Veranstaltung der Cinémathèque Leipzig in Kooperation mit der Rosa Luxemburg Stiftung.

BLACK FACES IN WHITE? SPACE untersucht die Dynamiken und Komplexitäten des Schwarz-Seins in Deutschland. Die Dokumentation verschränkt persönliche Erfahrungen des Filmemachers Thabo Thindi, der dem Tod durch die deutsche Polizei nur knapp entkam, mit der Alltagsrealität von „anderen“ Schwarzen Einzelpersonen, die sich in einem sich als homogen verstehenden Land durchs Leben manövrieren. Der Film stellt sich den Kämpfen Schwarzer Konflikte. Die Frage nach Colourism, Sexualität, Identität, Pride, Nationalität und Kolonialismus nimmt dabei einen zentralen Teil der (erzählerischen) Handlung ein.

BLACK FACES IN WHITE? SPACE explores the dynamics and complexities of being Black in Germany. It intertwines personal experiences of the filmmaker Thabo Thindi who narrowly survived death at the hands of the German police and that of the everyday reality of the “other” Black individuals manoeuvring through life in a country that views itself as homogenous. The film confronts the struggles of Black conflicts, the question of colourism, sexuality, identity, pride, nationality and colonialism assume central part of the (narrative) plot. (English & German with English and German subtitles)

