---
id: 34DS6
title: GERTRUDE oder die Differenz
start: 2022-11-30 19:15
end: 2022-11-30 21:15
locationName: HTWK Leipzig
address: "Karl-Liebknecht-Straße 134, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/34DS6/
teaser: Film Screening und Pubklikumsgespräch
isCrawled: true
---
Video von Ute Richter mit Archivmaterial, Handzeichnungen, animierter Zeichnung, Sprecherin und Klang. ca. 60 min. In Kooperation mit Luise Ritter(animierte Zeichnung), Franka Sachse (Postproduktion), Ipke Starke (Ton),Hannes Drißner (Grafik), Verena Noll (Sprecherin).

Die Schule der Arbeit (1928-1933) war ein Modellprojekt der Leipziger Arbeiterbildung unter Leitung von Gertrud Hermes. Mit dem Neubau des Architekten Johannes Niemeyer in der Stieglitzstraße entstand 1928 ein moderner Ort, der für das gemeinsame Leben und Lernen junger Arbeiter geplant wurde. Im Leipziger Stadtarchiv sind die Akten von der Planung bis zur Schließung der Schule der Arbeit und Texte der Pädagogin Gertrud Hermes überliefert. Von der Einweihung im Oktober 1928 bis zum Überfall durch die SA im Frühjahr 1933 und der anschließenden Beschlagnahme durch dieNationalsozialisten wird der Verlauf der Geschichte schmerzhaft deutlich.

Nach Archivrecherchen der Künstlerin Ute Richter zur Schule der Arbeit entstand in Zusammenarbeit mit Luise Ritter und Franka Sachse das Video„Gertrud Hermes oder Die Differenz zwischen Gewalterfahrung und Demokratie“. Eine künstlerische Aneignung vergessener Geschichte, mit aktuellem Blick.

Gefördert durch das Kulturamt der Stadt Leipzig, den Bundesverband Bildender Künstlerinnen und Künstler aus Mitteln der Beauftragten der Bundesregierung für Kultur und Medien im Rahmen des Programms NEUSTART KULTUR, die Kulturstiftung des Freistaates Sachsen und die Akademie der Künste Berlin.

Mehr zum Projekt Schule der Arbeit unter: [www.schulederarbeit.de](<http://www.schulederarbeit.de>). Ein Projekt von Ute Richter und EUROPAS ZUKUNFT gesellschaft für zeitgenössische Kunst in Kooperation mit der RLS Sachsen.

Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.



