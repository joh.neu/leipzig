---
id: 1YG2R
title: "GERTRUD oder Die Differenz "
start: 2023-01-20 19:00
end: 2023-01-20 21:00
locationName: HALLE 14 – Zentrum für zeitgenössische Kunst
address: "Spinnereistraße 7, 04179 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1YG2R/
teaser: Film Screening und Gespräch
isCrawled: true
---
Film von Ute Richter mit Archivmaterial, Handzeichnungen, animierter Architekturzeichnung und Sprecherin. 70 min.

Luise Ritter (animierte Zeichnung), Franka Sachse (Postproduktion/Schnitt).

Screening und Gespräch

mit Radek Krolczyk, Autor und Galerist, Bremen

und Ute Richter, Künstlerin, Leipzig

Die Schule der Arbeit (1928–1933) war ein Modellprojekt der Leipziger Arbeiterbildung unter Leitung von Gertrud Hermes. Mit dem Neubau des Architekten Johannes Niemeyer in der Stieglitzstraße entstand 1928 ein moderner Ort, der für das gemeinsame Leben und Lernen junger Arbeiter geplant wurde.Im Leipziger Stadtarchiv sind die Akten von der Planung bis zur Schließung der Schule der Arbeit und Schreiben der Pädagogin Gertrud Hermes überliefert. Von der Einweihung im Oktober 1928 bis zum Überfall durch die SA im Frühjahr 1933 und der anschließenden Beschlagnahme durch die Nationalsozialisten wird der Verlauf der Geschichte schmerzhaft deutlich.

Nach Archivrecherchen der Künstlerin Ute Richter zur Schule der Arbeit entstand in Zusammenarbeit mit Luise Ritter und Franka Sachse der Film GERTRUD oder Die Differenz. Es ist eine künstlerische Aneignung vergessener Geschichte, mit aktuellem Blick.

Die Filmvorführung mit Gespräch ist der Abschluss des Forschungsprojektes der Künstlerin Ute Richter zum Gebäude der Schule der Arbeit in Leipzig. Poster im Leipziger Westen (30.12.2022 bis 23.01.2022), ein Faltblatt und die Website erinnern ebenfalls an die vergessene Geschichte. Website: [www.schulederarbeit.de](<http://www.schulederarbeit.de>).

Ein Projekt von Ute Richter in Kooperation mit EUROPAS ZUKUNFT Gesellschaft zeitgenössischer Kunst und der Rosa Luxemburg Stiftung Sachsen. Gefördert durch das Kulturamt der Stadt Leipzig. Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.



