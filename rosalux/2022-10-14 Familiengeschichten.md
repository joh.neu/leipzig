---
id: 1ZQ7T
title: Familiengeschichten
start: 2022-10-14 15:00
end: 2022-10-15 16:00
locationName: Wird noch bekannt gegeben
address: "Wird noch bekannt gegeben, 04365 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1ZQ7T/
teaser: Rechercheseminar
isCrawled: true
---
Mit *Johannes Spohr* (Historiker)

Eine Veranstaltung der VVN-BdA Leipzig, der Gedenkstätte für Zwangsarbeit Leipzig und der RLS Sachsen

Der Nationalsozialismus wirkt auf vielfältige Weisen bis in die heutige Gesellschaft hinein. Viele fragen sich heutzutage, welche Rollen ihre Familienangehörigen, Bekannten, Kolleg:innen oder Vereinsmitglieder während dieser Zeit innehatten. Wer sich auf den Weg der Recherche begibt, ist nicht immer im Besitz umfangreicher Dokumente. Manchmal liefern Erzählungen aus der Familie erste Anhaltspunkte. Einige beginnen die Suche ohne jegliches Vorwissen. Auch das ist möglich, denn Archive und Institutionen bieten heute zahlreiche Möglichkeiten, den eigenen Fragen nachzugehen.

Doch wie gelange ich an welche Informationen? Und wie sind die Ergebnisse zu verstehen? Diesen Fragen können die Teilnehmer:innen sich im Workshop mit professioneller Anleitung und Unterstützung widmen. Der Historiker Johannes Spohr leitet durch den Tag, liefert Grundlagen, Anregungen und Tipps und geht auf individuelle Fragen zur Recherche ein.

Weitere Informationen zum Recherchedienst 'present past' lassen sich auf der website [present-past.net](<https://www.present-past.net/#/>) finden.

**Seminarzeiten: Freitag: 15.00 - 19.00, Samstag: 10.00 - 16.00 Uhr**



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

