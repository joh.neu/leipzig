---
id: 1ULVZ
title: Die kurdische Frage in der Türkei
start: 2022-10-13 19:00
end: 2022-10-13 21:00
locationName: linXXnet
address: "Brandstraße 15, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1ULVZ/
teaser: Über die gewaltsame Durchsetzung von Nationalstaatlichkeit
isCrawled: true
---
Mit *Dr. Ismail Küpeli* (Politikwissenschaftler)

Eine Veranstaltung des Abgeordnetenbüros linxxnet und der RLS Sachsen.

Der Konflikt zwischen der Türkei und der kurdischen Bevölkerung hat eine lange und verwobene Geschichte. Um die gegenwärtigen Spannungen und die Konsequenzen der staatlichen Politik verstehen zu können, ist ein Blick in die Vergangenheit unumgänglich.

Dr. Ismail Küpeli nimmt sich dieses Komplexes an und analysiert vor dem Hintergrund der historischen Entwicklungen die autoritäre und gewaltsame 

Durchsetzung von Nationalstaatlichkeit in der Türkei. Darüber hinaus ist der Konflikt für die geschichtliche Entwicklung und der aktuellen Lage des 

gesamten Nahen und Mittleren Ostens bedeutend.

Dies nicht zuletzt dadurch, dass kurdische Bevölkerungsgruppen in vielen Staaten existieren und in vielen Staaten politisch relevante Akteur\*innen hervorgebracht haben, wie etwa neben der Türkei in Syrien, Iran und im Irak. Auf dieser Grundlage formuliert er darüber hinaus Empfehlungen für eine politische Bildung, die einen Beitrag für die Anerkennung von Pluralität und Diversität sowie für einen gesellschaftlichen Friedensprozess liefern kann.

**

Hinweis zum Infektionsschutz**

Für unsere Veranstaltungen gilt Folgendes: Zum Schutz aller bitten wir, sich vor der Veranstaltung zu testen und vor Ort eine Maske zu tragen. Bleiben Sie bitte zu Hause, wenn sie Erkältungssymptome haben oder sich unwohl fühlen.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

