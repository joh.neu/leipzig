---
id: JBRZI
title: Antislawismus
start: 2022-07-09 10:00
end: 2022-07-09 16:00
locationName: Ariowitsch-Haus
address: "Hinrichsenstraße 14, 04105 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/JBRZI/
teaser: "REIHE: Vernichtungskrieg im Osten"
isCrawled: true
---
<div id="magicdomid230">Mit <em>Sergej Prokopkin</em> (Jurist, Antidiskriminierungstrainer und Migrationsberater bei den Neuen deutschen Medienmacher*innen)</div>

<div id="magicdomid231">Eine Veranstaltung der VVN-BdA Leipzig und der RLS Sachsen</div>

<div id="magicdomid237"></div>

<div id="magicdomid234">In diesem Workshop geht es um eine Analyse der Funktionsweisen und strukturellen Verankerungen von Antislawismus als Diskriminierungsform. Dabei werden sowohl persönliche biografische Bezüge zum Thema beleuchtet, als auch die Perspektive der Akteur*innen aufgenommen, die ihre Arbeit gegen den Antislawismus richten. Durch die strukturierte Recherche zu institutionellen, kulturellen und individuellen Verankerungen dieser Diskriminierungsform in unterschiedlichen gesellschaftlichen Bereichen werden die Teilnehmer*innen für konkrete Ausprägungen von Antislawismus in Deutschland sensibilisiert. Zudem entsteht durch das Aufzeigen von Bezügen zu anderen Diskriminierungsformen ein differenziertes Bild der Systemischen Intersektionalität - Verschränkung des Antislawismus mit anderen Diskriminierungsformen. Weiterhin geht es darum, Handlungsoptionen und Strategien gegen diese Diskriminierungsform gemeinsam herauszuarbeiten und die Perspektive von Betroffenen sichtbar zu machen.</div>

<div id="magicdomid238"></div>

<div id="magicdomid236"><strong>Bitte bis zum 01. Juli anmelden unter: </strong><strong>anmeldung@rosalux-sachsen.de</strong></div>

<div><br data-mce-bogus="1"></div>

<div><div id="magicdomid246"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div></div>

