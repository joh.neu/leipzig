---
id: 1UDHK
title: “Хотят ли русские войны? Meinst du die Russen wollen Krieg?” (Jewgeni
  Jewtuschenko)
start: 2022-04-19 18:00
end: 2022-04-19 20:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/1UDHK/
teaser: "REIHE: Rosas Salon"
isCrawled: true
---
<div spellcheck="true" id="magicdomid20" aria-live="assertive" class="ace-line"><span class="author-a-z65zz80z3mfz82zw9z78zz87zz79zrz77zvfz79z">Eine Debatte mit dem Historiker und Osteuropa-Korresponden­ten Reinhard Lauterbach über den Ukraine-Konflikt.</span></div>

<div spellcheck="true" id="magicdomid22" aria-live="assertive" class="ace-line"></div>

<div spellcheck="true" id="magicdomid23" aria-live="assertive" class="ace-line"><span class="author-a-z65zz80z3mfz82zw9z78zz87zz79zrz77zvfz79z"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></span></div>

