---
id: 5SZ61
title: "AUSGEBUCHT: Feminism is a battlefield"
start: 2022-05-06 11:00
end: 2022-05-07 15:15
locationName: Galerie KUB
address: "Kantstraße 18, 04275 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/5SZ61/
isCrawled: true
---
<div spellcheck="true" id="magicdomid2">Mit <em>Bec Wonders</em> (Glasgow School of Art), <em>Judith Gro</em>sse (Archiv für Frauen-, Geschlechter und Sozialgeschichte), <em>Nora Diekmann</em> (Universität Bremen), <em>Laura Mohr</em> und <em>Nina Boerckel</em> (Kunstkollektiv FeZ), <em>Marlene</em> und <em>Dido</em> (Leuphana Universität Lüneburg), <em>Dayana Lau</em> (gender* bildet, MLU Halle-Wittenberg, Alice Salomon Archiv ASH Berlin), <em>Gisela Mackenroth</em> (Friedrich-Schiller-Universität Jena), <em>Marie-Christine Schoel</em> (Westfälische Wilhelms-Universität), <em>Pinar Tuzcu</em> (Universität Kassel), Kena Henrietta Stüwe (Humbolt Universität zu Berlin). Lena Bringman (HafenCity Universität Hamburg), Uta C. Schmitt (Netzwerk Frauen- und Geschlechterforschung NRW)</div>

<div spellcheck="true" id="magicdomid8">Eine Veranstaltung der MONAliesA, Lehrstuhl Geschlechtergeschichte der Friedrich-Schiller-Universität Jena in Kooperation mit der RLS Sachsen</div>

<div spellcheck="true" id="magicdomid9"></div>

<div spellcheck="true" id="magicdomid11">Queer- und Frauenbewegungen lagen und liegen mit gesellschaftlichen Herrschaftsverhältnissen im Streit, führ(t)en aber auch Konflikte unter- und zwischeneinander. Sie kämpf(t)en gegen Unterdrückung, Ausbeutung und Enteignung, ringen um Differenzen, Anerkennung und Umverteilung und formulier(t)en Utopien für eine andere Zukunft. Wie also lassen sich Queer- und Frauenbewegungen analysieren und befragen, wenn sie gleichzeitig als soziale Bewegungen stetig in Veränderung, in Bewegung begriffen sind?Am 6. und 7. Mai 2022 diskutieren Nachwuchswissenschaftler:innen, Aktivist:innen und Künstler:innen in der KUBgalerie Leipzig ihre Forschungsarbeiten rund um Konflikt- und Bewegungsräume in Queer- und Frauenbewegungen. Gemeinsam wollen wir uns einem Verständnis von sozialen Bewegungen über ihre Konflikte nähern und Quellen in den Mittelpunkt einer kritischen Auseinandersetzung rücken. Der Workshop wird Werkstattcharakter haben und ist interdisziplinär angelegt. Im Anschluss an die Erziehungswissenschaftlerin Susanne Maurer versteht der WorkshopFeminism is a battlefield Konflikte als Spuren und als Schlüssel zum Verständnis für länger währende Prozesse und wichtige Ereignisse und Themen der Bewegung, aber auch der gesamten Gesellschaft. Indem wir uns speziell den streitbaren Räumen und Konflikten, in denen Bewegungsziele ausgehandelt werden, zuwenden, wollen wir eine Perspektive eröffnen, die es ermöglicht Queer- und Frauenbewegungen in ihrer Heterogenität und Komplexität zu erforschen. Dabei werden sowohl historische als auch gegenwärtige Konflikt- und Bewegungsräume der Queer- und Frauenbewegungen im Fokus stehen.</div>

<div spellcheck="true" id="magicdomid12">Der Workshop wurde organisiert von: Constanze Stutz (TU Dresden), Barbara Schnalzger (Universität Leipzig &amp; MONAliesA), Sarah probst (Universität Fribourg), Pia Marzell (Friedrich-Schiller-Universität Jena), Christian Kleindienst (Universität Leipzig), Kassandra Hammel (Albert-Ludwigs-Universität Freiburg)</div>

<div spellcheck="true"></div>

<div spellcheck="true" id="magicdomid13">Förderung durch den Arbeitskreis Historische Frauen- und Geschlechterforschung, Heinrich Böll Stiftung, Frauenzentrum Zürich, Leipzig Stiftung und MONOM - Stiftung für Verantwortung.</div>

<div spellcheck="true"></div>

<div spellcheck="true" id="magicdomid14"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div>

