---
id: TNQI8
title: "Ein Kraftfeld innerhalb der Masse schaffen "
start: 2022-10-29 15:00
end: 2022-10-29 17:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/TNQI8/
teaser: Eine Stadtwanderung zu Arbeiterkultur, Bildung und Moderne im Leipziger Westen
isCrawled: true
---
Mit *Britt Schlehahn* (Kulturwissenschaftlerin)

Die Stadtwanderung widmet sich einem besonderen Kapitel in der Leipziger Stadtgeschichte. Die proletarische Kultur und ihre vielfältigen Einrichtungen in der Stadt verschwanden nicht erst nach 1989 aus dem öffentlichen Gedächtnis. Das ist sehr verwunderlich, weil sich in der Stadt einige sehr innovative Modelle entwickelten. Dazu gehört in den 1920er-Jahren beispielsweise die Leipziger Richtung der Erwachsenenbildung. Die Volkshochschulbewegung wollte „ein Kraftfeld innerhalb der Masse schaffen“, dabei aber die Neutralität im Parteienspektrum wahren. Volkshochschulheime stellten dafür ein besonderes Bildungsmodell für jugendliche Arbeiterinnen und Arbeiter dar. Die am 1. Oktober 1928 eröffnete “Schule der Arbeit” in der Stieglitzstraße steht für eine moderne Architektur zugeschnitten auf die Bedürfnisse der Wohn- und Bildungsgemeinschaft im Volks- hochschulheim.

Die Stadtwanderung, konzipiert von der Kulturwissenschaftlerin Britt Schlehahn, führt zu Orten der Arbeiterkultur, die für Bildung und Emanzipation stehen, und ist die erste Veranstaltung im Rahmen des Rechercheprojektes “Schule der Arbeit” der bildenden Künstlerin Ute Richter.

Dauer des Rundgangs ca. 2 Stunden



Das Material zur Schule der Arbeit ist jetzt auch online zugänglich unter: [https://www.schulederarbeit.de/](<https://www.schulederarbeit.de/>)



Eine Veranstaltung in Kooperation mit der Rosa-Luxemburg-Stiftung Sachsen in Zusammenarbeit mit EUROPAS ZUKUNFT Gesellschaft zeitgenössischer Kunst, gefördert durch das Kulturamt der Stadt Leipzig.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

*

