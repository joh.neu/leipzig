---
id: ACJ2A
title: Die Tagebücher des russisch/sowjetischen Schriftstellers Michael Prischwin
start: 2022-08-25 18:00
end: 2022-08-25 20:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/ACJ2A/
teaser: "REIHE: Rosas Salon"
isCrawled: true
---
Mit *Dr. habil Adelheid Latchinian* (Literaturwissenschaftlerin), Moderation *Prof. Dr. Manfred Neuhaus*

Lange vergraben, nun ans Licht gebracht. Was sagen uns die Tagebücher des Schriftstellers Michael Prischwin heute über Revolution und Sozialismus in seinem Lande?

Eine Buchvorstellung mit anschließender Diskussion von und mit der Leipziger Literaturwissenschaftlerin Dr. Adelheid Latchinian.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*



