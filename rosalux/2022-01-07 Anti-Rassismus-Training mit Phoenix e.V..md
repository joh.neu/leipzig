---
id: XCA3H
title: Anti-Rassismus-Training mit Phoenix e.V.
start: 2022-01-07 00:00
end: 2022-01-09 00:00
locationName: Stadtteilladen Lixer e.V.
address: "Pörstener Straße 9, 04229  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/XCA3H/
isCrawled: true
---
Eine Veranstaltung des Lixer e.V. in Kooperation mit der RLS Sachsen

Anmeldung: [lixer@riseup.net](<mailto:lixer@riseup.net>)

Die Zielsetzung des 3-tägigen Workshops ist es ein Bewusstsein und ein Verständnis für gesellschaftliche Bedingungen und die systematische Einschreibung des rassistischen Systems auf institutioneller, aber auch persönlicher Ebene zu erlangen.

Das Training hilft, die Eingebundenheit der eigenen Persönlichkeit in rassistische Denk- und Gefühlsmuster zu erkennen und einen Bogen zu schlagen zu der rassistischen Prägung in der Sozialisation. Im Alltag werden diese Prägungen durch Medien, durch Zusammensein in Familie, Beruf und Freund\*innenkreis immer wieder bestätigt. Die politische Alltagskultur verstärkt zudem diese Klischees. Das Training will Erkenntnis stärken, den Kontakt zum eigenen Ich verstärken und stellt letztendlich die Frage: Wie kann ich wirkungsvoll etwas gegen Rassismus unternehmen?

Gemeinsam werden erste Schritte und Möglichkeiten gesucht. Dabei arbeiten die Trainer\*innen mit verschiedenen Medien und Methoden, wie Einzel- und Gruppengespräch, Rollenspiel, Videos und anderen. Die Teilnehmer\*innen sollen dazu angehalten werden, sich nach dem Konzept der Critical Whiteness, des kritischen Weißseins, mit ihrer Rolle im System des Rassismus auseinanderzusetzen. Hierbei geht es darum, die Teilnehmenden aufzubauen. Gesellschaftlich können wir auf Dauer nur etwas verändern, wenn viele Menschen anfangen, das kleine und das große Geflecht des Rassismus zu erkennen, und bereit sind, sich zu fragen: Wer bin ich als Weiße\*r?

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

