---
id: WUONL
title: Presse- und Urheberrecht für politisch Aktive
start: 2022-06-17 10:00
end: 2022-06-17 16:00
locationName: Antidiskriminierungsbüro Sachsen
address: "Seeburgstraße 20, 04103  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/WUONL/
isCrawled: true
---
<div id="magicdomid540">Mit <em>Dr. Björn Elberling</em> (Rechtsanwalt)</div>

<div id="magicdomid541"></div>

<div id="magicdomid543">Eine Begegnung mit dem Presse- und Urheberrecht kann heutzutage kaum eine politisch aktive Person mehr vermeiden: Darf ich von der Nazidemo Fotos machen oder hat die Polizei recht, dass das verbotene „Porträtfotos“ sind? Was darf ich öffentlich über die lokalen „Querdenker“, über die eklige Vermieterfirma, über den rechten Professor schreiben, ohne abgemahnt und im schlimmsten Fall verklagt zu werden? Aber auch umgekehrt: muss ich mir gefallen lassen, dass die AfD mich in einer Broschüre über „Linksextremisten“ angreift und wüste Behauptungen über mich aufstellt?</div>

<div id="magicdomid544">Die Veranstaltung gibt einen Überblick über die anwendbaren rechtlichen Regeln und Tipps für die Umsetzung – am liebsten anhand von Beispielsfällen, die die Teilnehmer*innen mitbringen.</div>

<div></div>

<div id="magicdomid545">Der Referent: Björn Elberling ist Rechtsanwalt in Kiel (Kanzlei Hoffmann und Dr. Elberling) und Leipzig (Kanzlei Eisenbahnstraße). Er ist schwerpunktmäßig im Strafrecht sowie im Presse- und Urheberrecht tätig und vertritt in beiden Rechtsgebieten häufig politisch aktive Menschen.</div>

<div></div>

<div id="magicdomid546"><strong>Anmeldung bis 09. Juni: </strong><a href="mailto:anmeldung@rosalux-sachsen.de"><strong>anmeldung@rosalux-sachsen.de</strong></a></div>

<div></div>

<div id="magicdomid547"><em>In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.</em></div>

