---
id: SNQT5
title: "Lilian Thuram: Das weiße Denken"
start: 2022-05-12 19:00
end: 2022-05-12 21:00
locationName: UT Connewitz
address: "Wolfgang-Heinze-Str. 12a, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/SNQT5/
isCrawled: true
---
<div id="magicdomid53">Mit <em>Lilian Thuram</em> (Autor und Gründer der Stiftung „Éducation contre le racisme, pour l’égalité“, ehemaliger Fußballprofi)</div>

<div id="magicdomid57">Eine Veranstaltung in Kooperation mit dem Fanprojekt Leipzig, Edition Nautilus, antifascist-europe, der Rosa-Luxemburg-Stiftung und der Fondation Lilian Thuram - Education contre le racisme</div>

<div id="magicdomid58"></div>

<div><strong>„Man wird nicht weiß geboren, man wird dazu gemacht.“</strong></div>

<div id="magicdomid61">Der frühere französische Fußballstar Lilian Thuram engagiert sich seit langem in der antirassistischen Bildungsarbeit. Anschaulich beschreibt er, wie die europäischen Gesellschaften die Kategorien Schwarz und weiß erfunden haben, um Kolonialismus, Versklavung und Ausbeutung zu rechtfertigen. Bis heute zementiert das weiße Denken Herrschaftsverhältnisse und Ungleichheit in der ganzen Welt. In vielen Beispielen, auch aus seiner persönlichen Erfahrung, zeigt Thuram, wie diese Deutungsmuster funktionieren und wie sie allgemeingültig werden konnten.</div>

<div id="magicdomid62">Thuram bezieht sich immer wieder auf postkoloniale Diskurse, auf Frantz Fanon und Aimé Césaire, James Baldwin und Maya Angelou, Toni Morrison und Achille Mbembe. Sein Buch ist ein zutiefst humanistischer Appell, eingeschliffene Denkstrukturen zu hinterfragen, um so das Fundament für neue Solidaritäten zu legen. Nur dann können wir einander endlich wieder als Menschen begegnen – und die Krisen der Gegenwart gemeinsam bewältigen.</div>

<div id="magicdomid72"></div>

<div id="magicdomid64">Die Veranstaltung findet auf Französisch und Deutsch mit Simultandolmetschung statt. Um die Übersetzung zu hören sind <strong>Kopfhörer mit Klinkekabel</strong> nötig. </div>

<div>Wir bitten darum diese wenn möglich selbst mitzubringen. </div>

<div></div>

<div>Wir bitten darum auf der Veranstaltung einen <strong>Mund-Nasen-Schutz</strong> zu tragen.</div>

<div><em></em></div>

<div><em>in Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.</em></div>

<div><p><strong>Alle&nbsp;Termine der Lesereise:</strong></p><ul><li><a href="https://www.rosalux.de/veranstaltung/es_detail/FSNUN" target="_blank">11.05.22</a> in Berlin, Volksbühne</li><li><a href="https://www.rosalux.de/veranstaltung/es_detail/SNQT5" target="_blank">12.05.22</a> in Leipzig, UT Connewitz</li><li><a href="https://www.rosalux.de/veranstaltung/es_detail/HVQW7" target="_blank">30.05.22</a> in München, Münchner Aids-Hilfe</li><li><a href="https://www.rosalux.de/veranstaltung/es_detail/I36VG" target="_blank">31.05.22</a> in Dortmund, Fan-Projekt Dortmund e.V.</li><li><a href="https://www.rosalux.de/veranstaltung/es_detail/R7EBA" target="_blank">01.06.22</a> in Hamburg, FC St. Pauli-Museum </li></ul></div>

