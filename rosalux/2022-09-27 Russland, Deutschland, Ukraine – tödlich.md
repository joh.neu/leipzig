---
id: CBV76
title: Russland, Deutschland, Ukraine – tödliches Spannungsfeld ohne Alternative?
start: 2022-09-27 18:00
end: 2022-09-27 20:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/CBV76/
teaser: "Zwischen angespannter Nachbarschaft und Weltordnungskrieg REIHE: Rosas Salon"
isCrawled: true
---
Mit *Dr. sc. Stefan Bollinger* (Historiker)

Ist es gegenwärtig überhaupt statthaft, erklären zu wollen, warum einerseits jeder Krieg ein Verbrechen ist, auch der in der Ukraine, und andererseits jeder Krieg Ursachen hat, die tief in der Vergangenheit wurzeln. Das macht die Sache nicht besser und das Elend nicht kleiner. Aber dieses Wissen hilft, um Vernunft und Logik walten zu lassen. Es nützt auch, um sich der Kriegspropaganda, die beide Seiten verbreiten, kritisch zu widersetzen. Der Politikwissenschaftler Stefan Bollinger analysiert, warum das Verhältnis zwischen uns Deutschen und den Russen so ist, wie es ist. Und warum es besser wäre, einfach mal die Luft anzuhalten und ein wenig nachzudenken. Mit seiner aktuellen Publikation ("Die Russen kommen! Wie umgehen mit dem Ukrainekrieg?") greift er in die laufende Diskussion ein und liefert Argumente gegen eine vereinfachende, emotionsgeladene Wahrnehmung..

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.



*

