---
id: FXTVH
title: Haymat Ost
start: 2022-10-14 17:00
end: 2022-10-15 23:30
locationName: Theater der Jungen Welt
address: "Lindenauer Markt 21, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/FXTVH/
teaser: Konferenz für Migration und migrantische Kämpfe des Ostens
isCrawled: true
---
Migration war und ist fester Bestandteil der ostdeutschen Gesellschaft. Bereits in die DDR migrierten Vertragsarbeiter\*innen, Studierende oder politisch Verfolgte. Ab den 1990er Jahren setzte sich die Einwanderung in die ostdeutschen Bundesländer fort. Sie prägt die hiesige Gesellschaft entschieden mit, und dennoch stellen diese Perspektiven, Lebensrealitäten und migrantische Kämpfe nach wie vor Leerstellen in Politik und im (historischen) Selbstverständnis der ostdeutschen und gesamtdeutschen Gesellschaft dar. Dies soll sich ändern!

Wir laden migrantische, postmigrantische und antirassistische Akteur\*innen und Initiativen des Ostens ein zum Zusammenkommen, Vernetzen und austauschen, um gemeinsame politische Forderungen zu entwickeln, neue Allianzen zu schmieden und Kämpfe zu verbinden.

In den letzten Jahren sind bereits viele Webprojekte, Podcast, journalistische und künstlerische Projekte zum Thema entstanden, viele migrantische Organisationen gestalten den politischen Alltag entschieden mit. Gleichzeitig führten uns die Landtagswahlen in den neuen Bundesländern oder die rassistische Gewalt in Chemnitz 2018 vor Augen dass migrationspolitische Forderungen und demokratische Errungenschaften immer wieder angegriffen und erkämpft werden müssen. Die #unteilbar und Welcome united Parade in Dresden als auch das NSU Tribunal in Chemnitz und Zwickau 2019 stellten für viele Menschen zentrale politische Ereignisse dar. Hier kamen migrantische, postmigrantische und antirassistische Bewegungen zusammen, um für eine solidarische Gesellschaft zu kämpfen. Zudem verdeutlichten diese Ereignisse noch einmal mehr, dass die (post)migrantische Gesellschaft längst Realität des Ostens ist. Daran wollen wir auf der Konferenz anschließen.

Eine zweitägige Konferenz zur Vernetzung, mit Workshops, Lesungen, Filmvorstellungen und Diskussionen über Migration, migrantische Kämpfe und Antirassismus in der DDR bis heute.

#### Programm

##### Freitag, 14.10.

Ab 17 Uhr: Einlass

18 Uhr: Begrüßung

Mit Lydia Lierke (Bildungskoordinatorin für Migration, Rosa-Luxemburg-Stiftung) und Kooperationspartner\*innen (Verband binationaler Famlien und Partnerschaften, JugendSTIL\* und ZEOK e.V/mikopa)

18:50 Uhr: **«Der Osten bleibt Migrantisch!»

**Szenische Lesung mit Vu Thi Hoang Ha, Christan Hernán Gárate Garay (Autor\*innen von «...die DDR schien mir eine Verheißung»), Francisca Raposo (Autorin und Schülerin der «Schule der Freundschaft») und Yasemin Said, Landouma

Moderation: Gonca Sağlam (narratif-Magazin)

20:30 Uhr: Gemeinsames Essen: Küfa im Casa (Josephstr.).

##### Samstag, 15.10.

10 Uhr: Keynote

**«Haymat – Wo die Gesellschaft der Vielen zu Hause ist»

**Referent\*innen: Massimo Perinelli (Referent für Migration, Rosa-Luxemburg-Stiftung), Aurora Rodonó (freie Kulturarbeiterin)

Die Geschichte der Einwanderung ist eine Geschichte der Kämpfe gegen Ausbeutung und Diskriminierung, um soziale, politische und juristische Rechte sowie für Würde und ein gutes Leben unter den Bedingungen von strukturellem Rassismus. Die Keynote lädt ein zu einer Verteidigung unserer Geschichte der Migration als transformatorische Kraft für eine solidarische Gesellschaft von morgen.​​​​​​​

11 - 13 Uhr: Workshopphase I

**«If it's not interpreted, it's not my revolution!»

**Referent\*innen: Mitglieder des [Dolmetschkollektivs interpRise](<https://interprise.nirgendwo.info/wer-sind-wir/>), aktivistische und unterbezahlte Dolmetscher\*innen

Emanzipatorische Kämpfe um, gegen und an Grenzen sind nicht einsprachig. Wir geben einen kurzen Einblick in politische Bewegungen rund um Mehrsprachigkeit und Dolmetschen, laden ein zu einem Austausch über die eigenen Sprachbiographien und denken gemeinsam nach über Möglichkeiten, Sprachbarrieren in aktivistischen Räumen abzubauen. Es geht um Sprache(n), Macht, Dolmetschen, Aktivismus, Sicht- und Hörbarkeit. Der Workshop findet in deutscher Lautsprache statt.

**«Unlearning the given – Den NSU-Komplex ausstellen»**

Referent\*innen: Hanna Thuma («Offener Prozess»), Hannah Zimmermann (Projektleitung «Offener Prozess», ASA FF e.V.)

Einblick in das einnerungspolitische Projekt «Offener Prozess – NSU Aufarbeitung in Sachsen». Der interaktive Workshop gibt Einblicke in die erinnerungskulturelle Arbeit des Projektes «Offener Prozess». Die Ausstellung «Offener Prozess» widmet sich dem NSU-Komplex. Sie nimmt dabei die Ost-Deutsche Realität, insbesondere in Sachsen, zum Ausgangspunkt, um eine Geschichte des NSU-Komplexes zu erzählen, die von den Migrationsgeschichten und den Kontinuitäten rechter und rassistischer Gewalt und des Widerstandes dagegen ausgeht.

**«Migration in die DDR und zurück»** Erzählcafé

Im gemeinsamen Gespräch mit anderen ihrer Generation erzählen Francisca Raposo (Chimoio/Mosambik) und Angelika Nguyen (Berlin) über das Leben in und nach der DDR, Migration aus erster und zweiter Generation.

Dokumentarfilm [«Bruderland ist abgebrannt»](<https://www.bpb.de/mediathek/video/317607/bruderland-ist-abgebrannt>) und Essay [«Mutter, wie weit ist Vietnam?»](<https://heimatkunde.boell.de/de/2014/01/29/mutter-wie-weit-ist-vietnam>)

Moderation: Việt Đúc Phạm

**«Exile Faces» **Talk and screening

Referent: Thabo Thindi (Fotografie und Film) *In englischer Sprache*

Apartheid forced a number of South Africans into exile, and some sought refuge in the former divided Germany especially in the East. Many returned home after the collapse of apartheid in the 1990s but a number of them still live in Germany today. In this interviews series anti-apartheid activists like Eric Singh who has recently passed, «Bra Chipa» Stanford Moagi, and others talk about their own views regarding the GDR and South Africa then and today and explain why their return to the country remains a distant dream.

13 - 14 Uhr: Mittagspause / Essen

14 - 16 Uhr: Workshopphase II

**«Solidarität oder/und Propaganda: Migration aus Mosambik in die DDR in filmischen Dokumenten» **Film und Diskussion

Referentin: Julia Oelkers (Journalistin und Dokumentarfilmerin)

1979 unterzeichneten die DDR und die Volksrepublik Mosambik Verträge, in deren Folge tausende junge Frauen und Männer in die DDR kamen und hier arbeiteten, lernten oder studierten. Dieser Austausch wurde als Ausdruck «internationaler Solidarität und brüderlicher Hilfe» in beiden Staaten auch propagandistisch begleitet. Im Workshop schauen wir einige Archiv-Filmausschnitte aus Mosambik und der DDR aus den Jahren 1979-1984 und 1990.

**«Haymat – Los!(?)»**

Referent\*innen: Anna Sabel, Nino I. Bautz (Verband binationaler Familien und Partnerschaften in Leipzig)

Welche Gemeinsamkeiten und Unterschiede im Hinblick auf (antimuslimischen) Rassismus können wir ausmachen, wenn wir uns den zeitgeschichtlichen Verlauf der beiden deutschen Länder zwischen 1970 bis zur Gegenwart anschauen? Wie wirkt(e) «Veranderung» in Ost und West? Und wie nach der Vereinigung beider Länder? In diesem Workshop wollen wir uns auf Grundlage einer von uns entwickelten «Zeiten-Übung» auf eine gemeinsame diskursive Spurensuche machen. 

​​​​​​​

**«Linkskanax Ost - Erstes Vernetzungstreffen»

**Referentin: Lena Saniye Güngör (Abgeordnete der Linksfraktion im Thüringer Landtag)

Ziel des Workshops ist es uns erstmalig als migrantische/von Rassismus betroffene Personen in Ostdeutschland innerhalb der Partei DIE LINKE zusammenzufinden. Wir wollen den gemeinsamen Raum nutzen, um weitere Schritte der inhaltlichen und strategischen Zusammenarbeit zu besprechen.

**«Vernetzungsworkshop /creating postmigrantische Kulturräume»**

Referenten: Trong do Duc (Initiative Postmigrantisches Radio), Johannes Bär (ZEOK e.V/Roots and Sprouts)

Im Vorfeld der Haymat-Konferenz veranstalteten wir ein gemeinsames Dinner zur Vernetzung politischer und kultureller (post)migrantischer Initiativen in Leipzig. Viele städtische Akteur\*innen äußerten den Wunsch nach einem dauerhaften Austausch- und Vernetzungsraum für (post)migrantische Kulturräume. Dieser Workshop soll daran anknüpfen und einen moderierten Gesprächsraum öffnen, in dem diskutiert wird, welche Bedarfe die teilnehmenden Personen haben und wie (post)migrantische Kulturräume gehalten, besetzt und kollektiv gestaltet werden können.

16 - 17 Uhr: Kaffeepause mit «la reine du Makrouts», marrokanische Spezialitäten

17 - 18:30 Uhr: **(post)migrantische Kulturräume in Ost- und Mitteldeutschland **Panel / Diskussion

Moderation: Trong Do Duc, Johannes Bär

Mit:

- (post)migrantisch-lokale Perspektive: Kunst Junger Muslim\*innen
- (post)migrantisch-diasporische Perspektive: Kurator\*innen der Ausstellung Where is my karaoke?

u.a.

<!-- -->

Die urbanen Räume der neuen Bundesländer werden zunehmend migrantisch geprägt. Dies zeigt sich nicht nur in der allmählichen Anerkennung der Lebensrealitäten von Menschen, die hier seit fast 50 Jahren in transnationalen Weltbezügen leben, sondern auch an den jüngeren Generationen an (post)migrantischen Akteur\*innen, die sich mehr und mehr soziale und kulturelle Teilhabe erkämpfen. Das Panel gibt Impulse, wie die Sichtbarkeit dieser Entwicklung noch weiter verstärkt werden und wie Ost- und Mitteldeutschland aus (post)migrantischer Perspektive mitgestalten und verändert werden kann.

19 - 20:30 Uhr: **«Rassismuskritische Bündnispolitiken des Ostens» **Abschlusspanel

In Kooperation mit dem Verband binationaler Familien und Partnerschaften e.V. und ZEOK e.V/mikopa.

Wir sprechen mit verschiedenen Akteur\*innen über rassismuskritische Bündnispolitiken des Ostens, über ihre Erfahrungswerte aus den letzten Jahren und Visionen für die Zukunft. Denn angesichts derzeitiger Krisen und der anstehenden Landtagswahlen in Sachsen müssen wir uns fragen, welchen Bedingungen sich eine rassismuskritische Bündnispolitik des Ostens stellen muss und wer Teil davon sein sollte.

Moderation: Öczan Karadeniz (Verband binationaler Famlien und Partnerschaften e.V)

Referent\*innen:

- Mohamed Okasha (Dachverband sächsischer Migrant\*innenorganisation)
- Lena Saniye Güngör (Abgeordnete der Linksfraktion im Thüringer Landtag)
- Nuria Silvestre Fernandez (Migrant\*innenbeirat Leipzig, Grüne Fraktion Leipzig) und andere

<!-- -->

Abendessen: Küfa in der Bäckerei (Josephstr.)

19 - 21 Uhr: Gemeinsamer Abschluss

Ab 21 Uhr: Pixi Bar (Queere Kollektiv-Kneipe in der Georg Schwarz Str.) mit Xubii (Music of Color)



---

#### In Kooperation mit:

- [Theater der Jungen Welt](<https://www.theaterderjungenweltleipzig.de/>)
- [Verband binationaler Familien und Partnerschaften](<https://binational-leipzig.de/index.php/ueber-uns.html>)
- [Jugendstil ](<https://jugendstil-projekt.de/>)
- [Roots and Sprouts](<https://roots-and-sprouts.de/>)
- [mikopa](<https://mikopa.de/>) und [Zeok eV](<https://www.zeok.de/>)
- [DaMOst](<https://www.damost.de/>)

<!-- -->

Im Foyer des Theaters können sich während der ganzen Konferenz Initiativen, Vereine und Projekte vorstellen, austauschen und vernetzen. Der Raum wird kuratiert vom Projekt Jugendstil, eine Kooperationspartner\*in der Rosa-Luxemburg-Stiftung.

---

#### Begleitende Ausstellungen zur Konferenz:

**«Offener Prozess» **Satelliten-Ausstellung

Die Satelliten verstehen sich als städteübergreifende «Lautsprecher» für die Inhalte der Ausstellung. Sie zeigen verschiedene Richtungen des NSU-Komplexes auf und verweisen auf die tiefgreifenden zeitlichen und örtlichen Dimensionen. Ihr Anspruch ist dabei nicht eine umfassende Einführung in den NSUKomplex zu geben, sondern auf Kontinuitäten von und Widerstand gegen rassistische und rechte Gewalt zu verweisen.

**«P wie Protest – Widerstandsmomente in Ostdeutschland» **

Mitte Oktober erscheint bei Edition Assemblage das Buch «P wie Protest. Ein Widerstandswörterbuch in Bildern.» vom Verband binationaler Familien und Partnerschaften (Hrsg): 

32 gute alte Protestformen trägt dieses Büchlein zusammen, wie Boykott, Schweigemarsch und Lichterkette. Illustriert werden die Begriffe mit Momenten des Widerstands, der sich beispielsweise gegen die deutsche Kolonialherrschaft wandte, für eine Aufklärung des Mordes an Oury Jalloh einsetzte, die Morde des NSU noch vor seiner sogenannten Selbstenttarnung als rassistische Verbrechen anprangerte oder den Schutz von BIPoC im nationalen Jubel der Vereinigung organisierte.

Auf der Konferenz wird es einen Einblick in Buch und Ausstellung geben.

---

#### Wichtige Infos zur Veranstaltung:

Anmeldung:

Zur besseren Planung der Konferenz, ist um Anmeldung gebeten.

<div class="infobox">Die Pflicht zum Tragen einer <strong>Mund-Nasen-Schutzmaske</strong> beim Betreten des Gebäudes ist derzeit aufgehoben. Eine 3G-Nachweispflicht entfällt. Die Rosa-Luxemburg-Stiftung empfiehlt dennoch ihren Gästen das Tragen einer Maske, um sich und anderen einen sicheren Besuch der öffentlichen Veranstaltungen zu ermöglichen.</div>

Kinderbetreuung: 

Am Samstag, dem 15.10., wird eine Kinderbetreuung angeboten. Falls ihr darauf Anspruch nehmen wollt, meldet das bitte in der Anmeldung an (Uhrzeit und Anzahl der zu betreuenden Kinder).

Sprachen: 

Die meist gesprochene Sprache auf der Konferenz ist deutsch. Manche Workshops finden auch auf Englisch statt (Sprachen werden im Programm ersichtlich gemacht). Auf Nachfrage wird nach Möglichkeit Verdolmetschung angeboten.

Versorgung:

Während der Konferenz ist eine Versorgung mit üblichen Tagungsgetränken und Snacks sowie einem Mittagsbuffet und einer Küfa zur Abendversorgung gewährleistet. Das Essen ist vegan und vegetarisch.

Barrieren:

Die Zugänge zu den Räumlichkeiten des Theaters und den Workshop-Räumen in der Demmeringstr. 32 [sind barrierearm](<https://www.theaterderjungenweltleipzig.de/ihr-besuch/anreise>).

Mehr Informationen zur Barrierefreiheit von Veranstaltungen: [www.rosalux.de/barrierefreiheit](<http://www.rosalux.de/barrierefreiheit>)

Hygienekonzept: 

Wir können heute noch nicht absehen, unter welchen Coronabedingten Auflagen die Haymat Ost Konferenz stattfinden wird. Wir melden uns diesbezüglich rechtzeitig bei allen Angemeldeten. Damit sich alle wohl fühlen können, wird zur Nutzung von Mund-und Nasenschutz gebeten.

[Zur Anmeldung ](<https://info.rosalux.de/#Buchung/fxtvh>)

