---
id: IBQYD
title: "Das Comeback von Sozialdarwinismus und Biologismus "
start: 2022-10-27 19:00
end: 2022-10-27 21:00
locationName: Luru-Kino
address: "Spinnereistraße 7, 04179  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/IBQYD/
teaser: "Reihe: &quot;Neue&quot; politische Unübersichtlichkeiten"
isCrawled: true
---
Mit *Peter Bierl* (freier Journalist)

Die Maßnahmen gegen das Corona-Virus würden nur Menschen retten, die aufgrund ihres Alters sowieso bald sterben müssen, erklärte der Tübinger Oberbürgermeister Boris Palmer. Der Publizist Jakob Augstein meinte, nur eine Minderheit sei ernsthaft gefährdet: „Die Politik hat beschlossen, zugunsten dieser Minderheit der Mehrheit sehr schwere Lasten aufzubürden.“ So schlimm sei das Virus nicht, gefährdet seien vor allem die Alten, schrieb Anselm Lenz, Organisator der ersten Proteste gegen Corona-Maßnahmen.

In einer Gesellschaft, in der alle in Konkurrenz zueinanderstehen und ständig Gewinner:innen und Verlierer:innen sortiert werden, ist Sozialdarwinismus die halbbewusste Alltagsreligion. Seit Jahrzehnten erklärt der australische Bioethiker Peter Singer Behinderte, Demente und Neugeborene für Menschen zweiter Klasse, verharmlost Euthanasie als Erlösung und empfiehlt sie, um Geld zu sparen. Thilo Sarrazin behauptet Migrant:innen und Hartz-IV-Empfänger:innen hätten minderwertiges Erbgut. Der Staat solle bio-deutschen Frauen aus der akademischen Mittelschicht mehr Geld geben, damit sie neben Studium und Karriere Kinder kriegen. Die Giordano-Bruno-Stiftung beschreibt in ihrem „Manifest für einen evolutionären Humanismus“ (2005) den Menschen als Bioroboter, preist Kapitalismus als natürliche Wirtschaftsweise und bezeichnet es als männliches Vorrecht Sex mit vielen Frauen haben zu können, abgeleitet aus der Größe der Hoden.

Solche Stimmen kommen nicht von genuin rechten Personen und Gruppen: Palmer gehört den Grünen an. Sarrazin trieb als SPD-Finanzsenator in Berlin den Sozialabbau voran, Singer ist prominenter Vordenker des Veganismus und plädiert für eine darwinistische Linke, die Ungleichheit als natürlich akzeptieren soll. Die Giordano-Bruno-Stiftung, die Singer mit einem Ethik-Preis auszeichnete, organisiert antiklerikale Kampagnen und versteht sich als linksliberal.

Sie alle sind Wiedergänger einer darwinistischen ‚Linken‘, deren Geschichte und Gegenwart der Referent Peter Bierl in seinem neuen Buch „Unmenschlichkeit als Programm“ seziert. In dieser Veranstaltung skizziert und diskutiert er seine Befunde.

Peter Bierl arbeitet als freier Journalist. Der Vortrag basiert auf seinem neuesten Buch***[Unmenschlichkeit als Programm ](<https://www.verbrecherverlag.de/book/detail/1071>)****.* Zuletzt veröffentlichte er „Die Legende von den Strippenziehern. Verschwörungsdenken im Zeitalter des Wassermanns“ (2021), „Die Revolution ist großartig. Was Rosa Luxemburg uns heute noch zu sagen hat“ (2020), „Keine Heimat nirgendwo. Eine linke Kritik der Heimatliebe“ (2020) sowie das „Einmaleins der Kapitalismuskritik“ (2018).



**Weitere Veranstaltungen der Reihe "Neue" politische Unübersichtlichkeiten: **

- "Weder links, noch rechts" Das Konzept Querfront. [Leipzig, 06. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/WKNKC/weder-links-noch-rechts”-das-konzept-querfront?cHash=107764db09075d3c49ac14798b09bc8d>)
- Von Kraken, Kapitalisten und Kriegstreibern - Anschlussfähigkeit von antisemitischen Denkmustern in linken Strukturen. [Chemnitz, 15. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/5LJPK/von-kraken-kapitalisten-und-kriegstreibern?cHash=211b4b25659f547ff9a654a8edf1b8f2>)
- Antifeminismus - Was ist das (nicht)? [Chemnitz, 24. Oktober 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/2RR28/antifeminismus---was-ist-das-nicht?cHash=0714ae9a120a831c61a6077204fd3474>)
- Radikalisierter Konservatismus. [Online, 01. November 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/ENSTI/radikalisierter-konservatismus?cHash=f9740a543c49f93b1c57dbfdd74be72f>)
- Was ist Antikapitalismus von rechts? [Online, 07. November 2022](<https://sachsen.rosalux.de/veranstaltung/es_detail/RTYKY/was-ist-antikapitalismus-von-rechts?cHash=e0f9d534153639189365a6b5452d93a3>)

<!-- -->



**Hinweis zum Infektionsschutz**

Für unsere Veranstaltungen gilt Folgendes: Zum Schutz aller bitten wir, sich vor der Veranstaltung zu testen und vor Ort eine Maske zu tragen. Bleiben Sie bitte zu Hause, wenn sie Erkältungssymptome haben oder sich unwohl fühlen.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

