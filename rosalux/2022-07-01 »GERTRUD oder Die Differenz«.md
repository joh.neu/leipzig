---
id: EMRJF
title: »GERTRUD oder Die Differenz«
start: 2022-07-01 21:00
end: 2022-07-01 23:00
locationName: Garten des Kunstraums IDEAL
address: "Schulze-Delitzsch-Str. 27, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/EMRJF/
teaser: Video von Ute Richter mit Billboard am Neustädter Markt
isCrawled: true
---
<font size="4" face="HelveticaNeue-Bold"><font size="4" face="HelveticaNeue-Bold"><p align="left"><strong>Die Schule der Arbeit (1928–33) in Leipzig, ein</strong><strong>e vergessene Geschichte:</strong></p><p align="left"><font size="4" face="HelveticaNeue"><font size="4" face="HelveticaNeue">&nbsp;Video von <em>Ute Richter</em> mit Archivmaterial, Handzeichnungen, animierter Zeichnung, Sprecherin und Klang. ca. 60 min. In Kooperation mit<em> Luise Ritter</em>(animierte Zeichnung), <em>Franka Sachse</em> (Postproduktion), <em>Ipke Starke</em> (Ton),<em>Hannes Drißner</em> (Grafik), <em>Verena Noll</em> (Sprecherin).</font></font></p></font></font>

<font size="4" face="HelveticaNeue"><font size="4" face="HelveticaNeue"><p align="left">Die Schule der Arbeit (1928-1933) war ein Modellprojekt der Leipziger Arbeiterbildung unter Leitung von Gertrud Hermes. Mit dem Neubau des Architekten Johannes Niemeyer in der Stieglitzstraße entstand 1928 ein moderner Ort, der für das gemeinsame Leben und Lernen junger Arbeiter geplant wurde. Im Leipziger Stadtarchiv sind die Akten von der Planung bis zur Schließung der Schule der Arbeit und Texte der Pädagogin Gertrud Hermes überliefert. Von der Einweihung im Oktober 1928 bis zum Überfall durch die SA im Frühjahr 1933 und der anschließenden Beschlagnahme durch dieNationalsozialisten wird der Verlauf der Geschichte schmerzhaft deutlich.</p></font></font>

<font size="4" face="HelveticaNeue"><font size="4" face="HelveticaNeue">Nach Archivrecherchen der Künstlerin Ute Richter zur Schule der Arbeit entstand in Zusammenarbeit mit Luise Ritter und Franka Sachse das Video„</font></font>

*<font size="4" face="HelveticaNeue-Italic"><font size="4" face="HelveticaNeue-Italic">Gertrud Hermes oder Die Di</font></font>

<font size="4" face="HelveticaNeue-Italic"><font size="4" face="HelveticaNeue-Italic">ff</font></font>

<font size="4" face="HelveticaNeue-Italic"><font size="4" face="HelveticaNeue-Italic">erenz zwischen Gewalterfahrung und Demokratie</font></font>

*<font size="4" face="HelveticaNeue"><font size="4" face="HelveticaNeue">“.&nbsp;Eine künstlerische Aneignung vergessener Geschichte, mit aktuellem Blick.</font></font>

*<font size="4" face="HelveticaNeue-Italic"><font size="4" face="HelveticaNeue-Italic"><p align="left">Gefördert durch das Kulturamt der Stadt Leipzig, die RLS Sachsen, den Bundesverband Bildender Künstlerinnen und Künstler aus Mitteln der Beauftragten der Bundesregierung für Kultur und Medien im Rahmen des Programms NEUSTART KULTUR, die Kulturstiftung des Freistaates Sachsen und die Akademie der Künste Berlin.</p></font></font>

*