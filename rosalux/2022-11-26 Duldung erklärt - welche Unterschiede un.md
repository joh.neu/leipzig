---
id: DUPYT
title: Duldung erklärt - welche Unterschiede und Handlungsmöglichkeiten gibt es?
start: 2022-11-26 14:00
end: 2022-11-26 16:30
locationName: Poliklinik Leipzig
address: "Taubestraße 2, 04347 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/DUPYT/
isCrawled: true
---
Eine Veranstaltung der Refugee Law Clinic in Kooperation mit der RLS Sachsen

In unserer Tätigkeit erreichen uns immer wieder Fragen wie: Was genau bedeutet denn jetzt „Duldung“? Ist ein Duldungspapier eine Garantie dafür, nicht abgeschoben zu werden? Sind alle Inhaber\*innen einer Duldung akut abschiebebedroht? Mein\*e Freund\*in hat eine Duldung, wir brauchen also ein Kirchenasyl, oder? Und was mach ich, wenn ich kein Duldungsdokument bekomme? 

Die Fragen zeigen: Über die aufenthaltsrechtlichen Status einer Duldung kursiert sowohl bei Betroffenen als auch Unterstützer\*innen viel Unwissen und Unsicherheiten: Die Duldung ist kein Aufenthaltstitel. Sofern ein Asylantrag formell unzulässig ist (durch z.B. Dublin-III-VO, Drittstaatenregelungen, sogenannte sichere Herkunftsländer) oder am Ende des Asylverfahrens eine materielle Ablehnung steht, steht betroffenen Personen zumeist die Erteilung einer Duldung bevor.

Aber Duldung ist nicht gleich Duldung: Es gibt unterschiedliche Formen von Duldungen. In §§ 60a, b, c, d AufenthG ist die sog. „vorübergehende Aussetzung der Abschiebung“, die sogenannte Duldung, geregelt. Diese Bezeichnung ist jedoch etwas irreführend: Denn in nicht jedem Fall ist die Abschiebung tatsächlich ausgesetzt, sie ist jedoch nicht unmittelbar möglich. Bei bestimmten Duldungstypen besteht außerdem keine akute Abschiebebedrohung, es gilt also unangemessene Panik zu vermeiden. Bei geduldeten Menschen obliegt es den zuständigen Ausländerbehörden, zu prüfen, ob und welche Duldungsgründe vorliegen und ob ein sogenanntes inlandsbezogenes Abschiebungshindernis vorliegt – sprich, die Abschiebung trotz der Ausreisepflicht nicht vollzogen werden darf oder kann. Diese Gründe müssen den zuständigen Ausländerbehörden vorgetragen bzw. dargestellt und nachgewiesen werden. 

Wie dies in der Praxis ablaufen kann, wollen wir im Rahmen der Veranstaltung gemeinsam erarbeiten. Teilnehmer\*innen sollen nach dem Vortrag die wichtigsten Fakten über den Aufenthaltsstatus der Duldung kennen. Darüber hinaus wird ein Fokus auf die asyl- und aufenthaltsrechtlichen Perspektiven von Afghan\*innen in Duldungen gelegt und Inhalte und Ergebnisse aus unsere Beratungsarbeit in den letzten Monaten vorgestellt.



**Anmeldungen per Mail unter: **[**verwaltung@rlc-leipzig.de**](<mailto:verwaltung@rlc-leipzig.de>)

**Testet euch vorher und tragt Maske während der Veranstaltung.**



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

