---
id: 8MHS8
title: Ökosozialismus
start: 2023-02-17 19:00
end: 2023-02-17 21:00
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/8MHS8/
isCrawled: true
---
Mit *Alexander Neupert-Doppler* (Politikwissenschaftler & Philosoph, Hochschule Düsseldorf)

Eine Veranstaltung der RLS Sachsen in Kooperation mit Prisma. interventionistische Linke Leipzig



Ein kleines Gespenst geht um in den Debatten – das Gespenst des Ökosozialismus. Rechtspopulist:innen werfen grünen Parteien Ökosozialismus vor, diese distanzieren sich. Sogar der Papst weiß: »Diese Wirtschaft tötet«, aber was ist die Alternative zum fossilen Kapitalismus? Seit den 1970er-Jahren diskutieren Philosoph:innen und Ökolog:innen, Anarchist:innen und Marxist:innen, Aktivist:innen und Sozialdemokrat:innen, Feminist:innen und Grüne über die Utopie des Ökosozialismus. Was sind ihre Kritiken am kapitalistischen Wachstum? Wie sollten sich Ökosozialist:innen organisieren? Taugt der Ökosozialismus als Motivation für eine nachhaltige Gesellschaft? Geht es um Revolution oder Transformation? Welche Bedürfnisse drücken sich in der Rede von einem system change aus? Diese Fragen zur Utopie des Ökosozialismus werden in diesem Buch anhand von 50 Texten aus 50 Jahren ökosozialistischer Debatte – von 1970 bis 2020 – diskutiert. Es bietet Überblickwissen zur Politischen Theorie des Ökosozialismus wie auch einen Einstieg ins Thema für interessierte Aktivist:innen.

Dr. Alexander Neupert-Doppler ist Philosoph und Politikwissenschaftler. Er veröffentlichte Bücher zum 'Staatsfetischismus' (2013), zu 'Utopie' (2015), historischen 'Gelegenheiten/Kairós' (2019) und ‚Organisation‘ (2021). Neupert-Doppler arbeitet zurzeit als Vertretungsprofessor für Sozialphilosophie an der Hochschule Düsseldorf.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

