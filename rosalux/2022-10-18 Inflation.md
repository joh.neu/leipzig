---
id: YBPGI
title: Inflation
start: 2022-10-18 19:00
end: 2022-10-18 21:00
locationName: UT Connewitz
address: "Wolfgang-Heinze-Str. 12a, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/YBPGI/
teaser: mit Ole Nymoen und Wolfgang M. Schmitt
isCrawled: true
---
Mit *Ole Nymoen* und *Wolfgang M. Schmitt*

*Eine Veranstaltung der RLS Sachsen in Kooperation mit den Kritischen Einführungswochen an der Uni Leipzig.*

Die Inflation war lange Zeit nur ein imaginiertes Problem: Mehr als ein Jahrzehnt lang warnten Crash-Propheten vor ihr, doch sie blieb immer aus. Eher war eine deflationäre Tendenz zu beobachten. Nun aber ist die Inflation da. Hatten die Mahner etwa immer schon Recht? Keineswegs, denn es ist nicht die lockere Geldpolitik der Zentralbanken, die für rasant steigende Preise sorgt. Grund für die Inflation sind externe Schocks wie Krieg, Corona und Lieferengpässe. Mit Blick auf die künftigen Krisen und auf die Demographie könnten wir möglicherweise sogar eine lang andauernde Stagflation bekommen. Was also tun? Wie kann die Inflation gestoppt werden? Wer sollte entlastet und wer zur Kasse gebeten werden? Wie sieht eine progressive Wirtschafts- und Geldpolitik für dieses Jahrzehnt aus?

Ole Nymoen und Wolfgang M. Schmitt sprechen im [Podcast “Wohlstand für Alle”](<https://wohlstandfueralle.podigee.io/>) wöchentlich über Geld. Jeden Mittwoch wird ein anderes Wirtschaftsthema behandelt und einen anderer Blick auf ökonomische Zusammenhänge geworfen.

Zusammen haben sie 2021 bei Suhrkamp das [Buch „Influencer. Die Ideologie der Werbekörper“](<https://www.suhrkamp.de/buch/influencer-t-9783518076408>) veröffentlicht.



**Hinweis zum Infektionsschutz**

Für unsere Veranstaltungen gilt Folgendes: Zum Schutz aller bitten wir, sich vor der Veranstaltung zu testen und vor Ort eine Maske zu tragen. Bleiben Sie bitte zu Hause, wenn sie Erkältungssymptome haben oder sich unwohl fühlen.



*In Kooperation mit der Rosa-Luxemburg-Stiftung: Gesellschaftsanalyse und politische Bildung e.V.

*

