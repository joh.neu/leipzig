---
id: AHWHM
title: Rom*nja als vergessene Opfer im Vernichtungskrieg
start: 2022-07-06 19:00
end: 2022-07-06 21:00
locationName: Galerie für zeitgenössische Kunst
address: "Karl-Tauchnitz-Straße 09-11, 04107 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/AHWHM/
teaser: "REIHE: Vernichtungskrieg im Osten"
isCrawled: true
---
<div id="magicdomid214">Mit <em>Martin Holler</em> (Historiker)&nbsp;und <em>Gjulner Sejdi</em> (Romano Sumnal e.V.)</div>

<div id="magicdomid219">Eine Veranstaltung der VVN-BdA Leipzig e.V. und der RLS Sachsen in Kooperation mit Romano Sumnal e.V.&nbsp;und RomaRespect bei Weiterdenken - HBS Sachsen und der Gedenkstätte für Zwangsarbeit Leipzig</div>

<div id="magicdomid242"></div>

<div id="magicdomid222">Der deutsche Überfall auf die Sowjetunion im Juni 1941 war von Anfang an als Vernichtungskrieg angelegt, der sich in den ersten Kriegsmonaten noch weiter radikalisierte und schließlich auch die Minderheit der Roma in ihrer Gesamtheit betraf. Der Vortrag zeichnet den Verlauf der Verfolgungsmaßnahmen gegen Roma nach, die sich im Jahre 1942 zu einem Völkermord ausweiteten. Dabei wird auch der Frage nachgegangen, welchen Einfluss die verschiedenen Besatzungszonen auf die Intensität und Systematik des Massenmordes hatten.Neben dem Blick auf diese oft vergessene Opfergruppe soll in der Veranstaltung auf die noch weniger beachteten Aspekte des Widerstandes und des Überlebens der Rom*nija geschaut werden. Im Anschluss sprechen wir mit Gjulner Sejdi (Romano Sumnal e.V.) auch über die aktuelle Situation von überlebenden Rom*nja und ihren&nbsp;Familien&nbsp;in der heutigen Ukraine.</div>

<div></div>

<div id="magicdomid241"></div>

<div id="magicdomid240"><div id="magicdomid246"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div></div>

