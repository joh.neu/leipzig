---
id: NXTEO
title: "Das Transitghetto Bełżyce in der “Aktion Reinhardt” "
start: 2022-11-04 19:00
end: 2022-11-04 21:00
locationName: "Stadtteilladen Lixer | Online "
address: "Pörstenerstrasse 09, 04229 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/NXTEO/
teaser: "REIHE: Vernichtungskrieg im Osten. "
isCrawled: true
---
Mit Steffen Hänschen (Historiker, Bildungswerk Stanisław Hantz e.V.)

Eine Veranstaltung der VVN-BdA Leipzig und der RLS Sachsen

Die große Mehrheit der Juden und Jüdinnen, die Ende 1941 noch auf dem Gebiet des Deutschen Reichs lebten, wurde 1942 “nach Osten” deportiert und ermordet. Auch aus dem Protektorat Böhmen und Mähren, der Slowakei und Luxemburg verschleppten die Nationalsozialisten weite Teile der jüdischen Bevölkerung in die Ghettos und Lager im besetzten Polen. Ein Hauptziel war der Distrikt Lublin, wo die Deportierten nicht sofort getötet, sondern zunächst auf Transitghettos verteilt wurden. Eines davon war das Ghetto Bełżyce.

Am 10. Mai 1942 verließ ein Deportationstransport mit 1002 Jüdinnen und Juden die Stadt Leipzig. Am 12. Mai erreichte der Transport die Stadt Bełżyce. 1942 begannen die “Aussiedlungen” in die Mordlager der “Aktion Reinhardt”: nach Bełżec, Sobibór und Treblinka. Die große Verschleppung von 2500 Jüdinnen und Juden aus Bełżyce fand am 13. Oktober wahrscheinlich in das Mordlager Sobibór statt. Andere kamen in Arbeitslager der Region. Die letzte kleine Gruppe jüdischer Bewohner\*innen wurde schließlich im April 1943 auf dem jüdischen Friedhof erschossen.

Im Zentrum des Vortrags stehen die Deportationen nach Bełżyce und das Geschehen am Ort.

Der Referent

Dr. Steffen Hänschen engagiert sich im Rahmen seiner Tätigkeit im Bildungswerk Stanisław Hantz e.V. in der Lubliner Region

Hinweis zum Infektionsschutz

Für unsere Veranstaltungen gilt Folgendes: Zum Schutz aller bitten wir, sich vor der Veranstaltung zu testen und vor Ort eine Maske zu tragen. Bleiben Sie bitte zu Hause, wenn sie Erkältungssymptome haben oder sich unwohl fühlen.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes*

<div class="infobox">Livestream am 04.11.2022 ab 19:00 Uhr auf unserem <a href="https://youtu.be/pSamLrO1cUw" target="_blank" class="button" rel="noreferrer">YouTube Kanal</a></div>

