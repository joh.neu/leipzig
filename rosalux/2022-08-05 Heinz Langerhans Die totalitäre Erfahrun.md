---
id: YLD2M
title: "Heinz Langerhans: Die totalitäre Erfahrung. Werkbiographie und Chronik"
start: 2022-08-05 19:00
end: 2022-08-05 21:00
locationName: Pöge-Haus
address: "Hedwigstr. 20 , 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/YLD2M/
isCrawled: true
---
Mit *Felix Klopotek* (Journalist und Autor)

Wer war Heinz Langerhans (1904-1976), und was ist sein politisches Vermächtnis? Der Autor Felix Klopotek hat sich auf die Suche begeben nach Leben und Werk eines zwar nicht vergessenen, aber zu wenig beleuchteten Intellektuellen, dessen Analysen zu einem marxistischen Verständnis von Kapitalismus, Faschismus und Stalinismus beitragen können.

Heinz Langerhans hat im Umkreis der Korsch-Gruppe und des Frankfurter Instituts für Sozialforschung noch in der Weimarer Republik eine Kritik der etablierten Arbeiterbewegung vorgelegt, die den Durchbruch des Faschismus zu erklären hilft. Doch erst durch die Erfahrungen seiner Haft- und KZ-Jahre – Langerhans war 1933 im Widerstand – war es ihm möglich, diese Kritik zu einem umfassenden Bild des Schreckens des 20. Jahrhunderts auszuarbeiten.

Im amerikanischen Exil, wohin er sich nach dramatischer Flucht durch Belgien und Frankreich retten konnte, legte er eine Totalitarismustheorie vor, die streng materialistisch gearbeitet ist – nämlich stur ausgehend von der Analyse der Produktivkräfte. Die Sprache, die er dafür gefunden hat, ist knapp und hart, lyrisch klar. Tatsächlich verstand sich Langerhans, der mit Bertolt Brecht gut bekannt war, als Dichter. Es gehört zur Tragik dieser Zeit, dass Langerhans – trotz seiner Kontakte zu Adorno, Horkheimer, Karl Korsch oder eben Brecht – diese Schriften nie veröffentlichen konnte (oder wollte?). Sie verschwanden unter Bergen von Papier, während Langerhans in der Nachkriegszeit in den USA und Westdeutschland um seine akademische Etablierung und damit auch berufliche Existenz kämpfen musste. Zu dieser bitteren Geschichte gehört, dass sein Nachlass für Jahrzehnte verschollen war.

Die Biographie stellt erstmals das gesamte Werk Langerhans' vor, wertet dabei alle bekannten und zahlreiche neu entdeckte Quellen aus und arbeitet die Relevanz dieser Schriften für unsere Zeit heraus.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushalts.*

