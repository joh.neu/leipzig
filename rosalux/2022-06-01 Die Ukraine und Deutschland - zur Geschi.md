---
id: Z6QDT
title: Die Ukraine und Deutschland - zur Geschichte eines asymmetrischen Verhältnisses
start: 2022-06-01 19:00
end: 2022-06-01 21:00
locationName: Galerie KUB
address: "Kantstraße 18, 04275 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/Z6QDT/
teaser: "REIHE: Vernichtungskrieg im Osten"
isCrawled: true
---
<div id="magicdomid145">Mit <em>Johannes Spohr</em> (Historiker) </div>

<div></div>

<div>Eine Veranstaltung der VVN-BdA Leipzig, der Naturfreundejugend Leipzig und der RLS Sachsen</div>

<div></div>

<div id="magicdomid170"><span lang=""><p dir="ltr" align="left">Infolge des entgrenzten Krieges der Russischen Föderation gegen die Ukraine seit dem 24. Februar 2022 entwickeln sich auch deutsch-ukrainische Beziehungen unter neuen Vorzeichen. Dies drückt sich auf politisch-repräsentativer Ebene, aber auch in der Berichterstattung oder in den Hilfsaktionen für Geflüchtete aus. Diese Beziehungen haben eine lange Vorgeschichte, die sich bis zum Ersten Weltkrieg zurückverfolgen lassen. Im Vortrag sollen zentrale Etappen dieses Verhältnisses, seine Wahrnehmungsschemata und Verkehrsformen eingeführt werden, um zu einem besseren Verständnis der heutigen Vorgänge beizutragen. Ein besonderes Augenmerk liegt dabei auf dem bis heute wenig bekannten deutschen Vernichtungskrieg gegen die Sowjetunion, deren Teil die besonders betroffene Ukraine war, und der dortigen Besatzungsherrschaft.</p></span></div>

<div id="magicdomid173"><div><div id="magicdomid246"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div></div></div>

