---
id: M6U4F
title: "Hörspiel: Das Sonnenblumenhaus"
start: 2022-08-19 19:00
end: 2022-08-19 21:00
locationName: Kronenpark
address: "Bornaische Straße, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/M6U4F/
isCrawled: true
---
Eine Veranstaltung der Gruppe Rassismus tötet! Leipzig in Kooperation mit der RLS Sachsen.

Das von Dan Thy Nguyen und Iraklis Panagiotopoulos produzierte Stück dokumentiert das rassistische Pogrom von Rostock 1992 und „verarbeitet die Sicht der belagerten Menschen“. Diese wurden dafür ausfindig gemacht und interviewt. Aus ihren Aussagen wurde das Hörspiel entwickelt. Sie berichten über ihr Leben in Ostdeutschland, rassistische Anfeindungen und Angriffe, aber auch ihr selbstorganisiertes Handeln und Verteidigen im Sonnenblumenhaus gegen die deutschen Angreifer\*innen. Das Stück kann auf der [Webseite der Rosa Luxemburg Stiftung ](<https://www.rosalux.de/news/id/43068/das-sonnenblumenhaus>)nachgehört werden.

Die Veranstaltung ist Teil der Veranstaltungsreihe zum Pogrom in Rostock-Lichtenhagen vor 30 Jahren. Weitere Informationen auf der [Webseite von Rassismus tötet! Leipzig](<https://www.rassismus-toetet-leipzig.org/index.php/veranstaltungsreihe-zum-pogrom-in-rostock-lichtenhagen/>).

