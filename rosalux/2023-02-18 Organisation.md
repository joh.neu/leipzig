---
id: NZJNM
title: Organisation
start: 2023-02-18 11:00
end: 2023-02-18 18:00
locationName: Pöge-Haus
address: "Hedwigstraße 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/NZJNM/
isCrawled: true
---
Mit *Alexander Neupert-Doppler* (Politikwissenschaftler & Philosoph, Hochschule Düsseldorf)

Für Rosa Luxemburg ruht die Organisation gesellschaftlichen Wandels auf drei Säulen: Partei, Genossenschaften und Gewerkschaften. Zugleich hat sie an den bürokratischen Entgleisungen dieser Organisationsformen viel Kritik geübt, die wir im Workshop einführend nachvollziehen werden. Für die Neue Linke ab den 1960er Jahren wurden soziale Bewegungen, aber auch Betriebs- und Basisgruppen, wichtiger als die klassischen Großorganisationen. In einem zweiten Schritt geht es daher um die Frage: Was unterscheidet diese oppositionellen von traditionellen Organisationsformen? Wir wenden uns dann der Frage zu, welche Organisationsformen uns heute geeignet erscheinen. Vorkenntnisse werden nicht benötigt.

**Die Plätze sind begrenzt. Anmeldung bis 12.02. unter **[**anmeldung@rosalux-sachsen.de**](<mailto:anmeldung@rosalux-sachsen.de>)

Dr. Alexander Neupert-Doppler ist Philosoph und Politikwissenschaftler. Er veröffentlichte Bücher zum 'Staatsfetischismus' (2013), zu 'Utopie' (2015), historischen 'Gelegenheiten/Kairós' (2019) und ‚Organisation‘ (2021). Neupert-Doppler arbeitet zurzeit als Vertretungsprofessor für Sozialphilosophie an der Hochschule Düsseldorf.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

