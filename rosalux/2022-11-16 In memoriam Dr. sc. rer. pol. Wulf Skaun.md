---
id: H13KZ
title: In memoriam Dr. sc. rer. pol. Wulf Skaun
start: 2022-11-16 16:00
end: 2022-11-16 18:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/H13KZ/
teaser: Gedenkveranstaltung
isCrawled: true
---
Gedenkveranstaltung für Dr. sc. rer. pol. Wulf Skaun (5. Mai 1945 - 29. August 2022). Eine Hommage an einen Wahrheitssucher, begnadeten Journalisten, mutigen Forscher und leidenschaftlichen Hochschullehrer

**Hinweis zum Infektionsschutz**

Für unsere Veranstaltungen gilt Folgendes: Zum Schutz aller bitten wir, sich vor der Veranstaltung zu testen und vor Ort eine Maske zu tragen. Bleiben Sie bitte zu Hause, wenn sie Erkältungssymptome haben oder sich unwohl fühlen. 



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

