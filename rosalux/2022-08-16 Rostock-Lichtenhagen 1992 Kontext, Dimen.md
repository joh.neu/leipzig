---
id: OCZIS
title: "Rostock-Lichtenhagen 1992: Kontext, Dimensionen und Folgen rassitischer
  Gewalt"
start: 2022-08-16 19:00
end: 2022-08-16 21:00
locationName: Conne Island
address: "Koburger Straße 3, 04277 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/OCZIS/
isCrawled: true
---
Mit *Rassismus tötet! Leipzig

*Eine Veranstaltung der Gruppe Rassismus tötet Leipzig in Kooperation mit der RLS Sachsen.

Die Ereignisse von Rostock-Lichtenhagen im August 1992 gelten als das größte Pogrom der deutschen Nachkriegsgeschichte: Tagelang wurden die Bewohner\*innen einer Unterkunft für Geflüchtete und eines Wohnheims für vietnamesische Vertragsarbeiter\*innen mit Steinen und Brandsätzen angegriffen, während Tausende ihrer Nachbar\*innen Beifall klatschten. Nachdem die Polizei sich auf dem Höhepunkt der Gewalt zurückgezogen hatte, entgingen mehr als 100 Menschen in dem brennenden Haus nur knapp dem Tod in den Flammen. Der Eskalation vorausgegangen war eine rassistische Kampagne in den Medien und Politik. Ihr folgte nicht nur eine Welle rechter Gewalt, sondern auch die weitgehende Einschränkung des Grundrechts auf Asyl.

Die Veranstaltung ist Teil der Veranstaltungsreihe zum Pogrom in Rostock-Lichtenhagen vor 30 Jahren. Weitere Informationen auf der [Webseite von Rassismus tötet! Leipzig](<https://www.rassismus-toetet-leipzig.org/index.php/veranstaltungsreihe-zum-pogrom-in-rostock-lichtenhagen/>).

