---
id: 45GMC
title: Wer verliert, das ist noch gar nicht ausgemacht
start: 2022-09-15 18:00
end: 2022-09-15 20:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/45GMC/
teaser: "REIHE: Rosas Salon"
isCrawled: true
---
Mit *Dr. sc. Gerald Diesener* (Verleger), Moderation *Dr. habil. Monika Gibas*

Der Leipziger Verleger Dr. sc. Gerald Diesener stellt die Erinnerungen seines akademieschen Lehrers, Prof. Dr. Werner Berthold vor, der zu den Gründungsmitgliedern der Rosa-Luxemburg-Stiftung Sachsen gehörte.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

