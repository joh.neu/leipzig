---
id: O3K62
title: Staat(stheorie) für alle!
start: 2022-04-25 19:00
end: 2022-04-25 21:00
locationName: Interim
address: "Demmeringstraße 32, 04177 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/O3K62/
teaser: Die Einführung
isCrawled: true
---
<div spellcheck="true" id="magicdomid21">Mit <em>Uwe Hirschfeld</em> (RLS Sachsen)</div>

<div spellcheck="true"></div>

<div spellcheck="true">Ende Juni (24.-26.06.2022)&nbsp;veranstalten wir ein Wochenend-Workshop unter dem Titel “Staat(stheorie) für alle!”. Vorab wollen wir uns mit Euch über Eure Vorstellungen, Wünsche und Erfahrungen austauschen. Dazu laden wir Euch ganz herzlich ein.</div>

<div spellcheck="true"></div>

<div spellcheck="true">"Der Staat, das sind wir alle!“, sagen diejenigen, die am meisten von ihm profitieren. Unsere Veranstaltungsreihe wird sich (u.a.) damit beschäftigen, wie der moderne Staat, der zumindest formal die wirtschaftliche Macht von der politischen Macht getrennt hat, so funktioniert, dass er die ökonomischen Herrschaftsverhältnisse immer wieder reproduziert. Wir werden uns dazu mit den Ansätzen bei Marx/Engels befassen, uns aber auch mit Gramsci, Poulantzas und Foucault auseinandersetzen. Die Frage nach der praktischen Produktivität doch recht abstrakter Theorie soll immer wieder aufgegriffen und diskutiert werden. Auch wenn es kein trockenes Theorieseminar sein soll, wird doch die Bereitschaft vorausgesetzt, auch längere Texte zu lesen.</div>

