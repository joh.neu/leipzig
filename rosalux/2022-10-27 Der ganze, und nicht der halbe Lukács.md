---
id: WIV7J
title: Der ganze, und nicht der halbe Lukács
start: 2022-10-27 18:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/WIV7J/
teaser: "REIHE: Rosas Salon"
isCrawled: true
---
Mit *Dr. habil. Antonia Opitz* (Literaturwissenschaftlerin), Moderation *Dr. Konstanze Caysa* und *Prof. Dr. Manfred Neuhaus*

Dr. habil. Antonia Opitz berichtet über Stand und Probleme der von ihr mit herausgegebenen deutschsprachigen Ausgabe der Werke des ungarischen Philosophen Georg Lukács.

*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*



