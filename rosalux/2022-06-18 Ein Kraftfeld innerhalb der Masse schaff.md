---
id: PSL7V
title: Ein Kraftfeld innerhalb der Masse schaffen
start: 2022-06-18 15:00
end: 2022-06-18 17:00
locationName: Felsenkeller
address: "Karl Heine-Straße 32, 04299 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/PSL7V/
teaser: Eine Stadtwanderung  zu Arbeiterkultur, Bildung und Moderne im Leipziger Westen
isCrawled: true
---
Mit *Britt Schlehahn* (Kulturwissenschaftlerin)

Die Stadtwanderung widmet sich einem besonderen Kapitel in der Leipziger Stadtgeschichte. Die proletarische Kultur und ihre vielfältigen Einrichtungen in der Stadt verschwanden nicht erst nach 1989 aus dem öffentlichen Gedächtnis. Das ist sehr verwunderlich, weil sich in der Stadt einige sehr innovative Modelle entwickelten. Dazu gehört in den 1920er-Jahren beispielsweise die Leipziger Richtung der Erwachsenenbildung. Die Volkshochschulbewegung wollte „ein Kraftfeld innerhalb der Masse schaffen“, dabei aber dieNeutralität im Parteienspektrum wahren. Volkshochschulheime stellten dafür ein besonderesBildungsmodell für jugendliche Arbeiterinnen und Arbeiter dar. Die am 1. Oktober 1928 eröffnete »Schule der Arbeit« in der Stieglitzstraße steht für eine moderne Architektur zugeschnitten auf die Bedürfnisse der Wohn- und Bildungsgemeinschaft im Volks- hochschulheim.

Die Stadtwanderung, konzipiert von der Kulturwissenschaftlerin Britt Schlehahn , führt zuOrten der Arbeiterkultur, die für Bildung und Emanzipation stehen, und ist die erste Veranstaltung im Rahmen des Rechercheprojektes »Schule der Arbeit« der bildenden Künstlerin Ute Richter.



Mit Unterstützung der RLS Sachsen und dem Kulturamt der Stadt Leipizg.



Dauer: ca. 2 Stunden



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.

*

