---
id: IPMP4
title: Religions-, Islam-, Islamismuskritik?
start: 2022-04-29 17:30
end: 2022-04-29 20:00
locationName: SchönerHausen
address: "Eisenbahnstraße 176-182, 04315  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/IPMP4/
isCrawled: true
---
<div spellcheck="true" id="magicdomid5">mit <em>Utopie &amp; Praxis</em></div>

<div spellcheck="true" id="magicdomid6">Eine Veranstaltung von Jugend gegen rechts in Kooperation mit der RLS Sachsen</div>

<div spellcheck="true" id="magicdomid7"></div>

<div spellcheck="true">In der Veranstaltung wollen wir uns kritisch mit Religion im allgemeinen, dem Islam und Islamismus auseinandersetzen. Dabei wollen wir dem Thema aus einer linken und emanzipatorischen Sicht begegnen und herausfinden, unter welchen Bedingungen diese Kritik dazu beiträgt den rassistischen Diskurs zu durchbrechen und wie wir der vermeintlichen „Islamkritik“ entgegenwirken können, die von AfD und Co ausgeht.</div>

<div spellcheck="true" id="magicdomid10"></div>

<div spellcheck="true" id="magicdomid11">Corona-Regelung: 2G</div>

<div spellcheck="true" id="magicdomid12"></div>

<div spellcheck="true" id="magicdomid13"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div>

