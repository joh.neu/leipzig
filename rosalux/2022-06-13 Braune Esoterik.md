---
id: 9N7RB
title: Braune Esoterik
start: 2022-06-13 19:00
end: 2022-06-13 21:00
locationName: Pöge-Haus
address: "Hedwigstraße 20, 04315 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/9N7RB/
isCrawled: true
---
<div id="magicdomid506">Mit <em>Patrick Irmer</em> (Fachstelle Radikalisierungsprävention im Naturschutz)</div>

<div id="magicdomid507">Eine Veranstaltung der RLS Sachsen in Kooperation mit der Naturfreundejugend Leipzig</div>

<div id="magicdomid508"></div>

<div id="magicdomid510">Heile Welt und ein Leben in Liebe und Einklang mit der Natur: In der öffentlichen Wahrnehmung wird von esoterischen Gruppierungen oft ein positives Bild gezeichnet. Doch zahlreiche esoterische Zusammenschlüsse verorten sich selber in einem von Hass getriebenen Umfeld. Die Annahme der eigenen Überlegenheit durch geheimes Wissen der Ahnen, aber auch Ausgrenzung von Andersdenkenden oder Vernichtungsfantasien finden sich nicht nur in Einzelfällen.</div>

<div id="magicdomid511">Der Vortrag behandelt die Verbindung esoterischer und auch anthroposophischer Bewegungen zu menschenverachtenden Grundeinstellungen.</div>

<div></div>

<div id="magicdomid512"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div>

