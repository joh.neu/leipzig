---
id: NBNYT
title: Zwischen Davidstern und roter Fahne
start: 2023-02-01 18:00
end: 2023-02-01 20:00
locationName: Bibliothek &quot;Georg Maurer&quot;
address: "Zschochersche Straße 14, 04229 "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/NBNYT/
teaser: Buchvorstellung
isCrawled: true
---
Mit *Michael Uhl* (Autor und Historiker)

Eine Veranstaltung der RLS Sachsen in Kooperation mit der VVN-BdA Leipzig e.V. und dem Erich-Zeigner-Haus e.V.

Der Historiker Michael Uhl (Tübingen) stellt in Wort und Bild seine 2022 im Schmetterling Verlag erschienene Biografie der jüdischen Krankenschwester Betty Rosenfeld vor, die als Freiwillige der Internationalen Brigaden am Spanischen Bürgerkrieg teilnahm und als Flüchtling in Frankreich den aus Leipzig stammenden Spanienkämpfer Sally Wittelson kennenlernte (für den 2021 in der Endersstraße ein Stolperstein verlegt worden ist). Im Sommer 1942 wurde das Paar gemeinsam nach Auschwitz deportiert.



*Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.*

