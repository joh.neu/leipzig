---
id: 96WSM
title: "ENTFÄLLT: Auf den Spuren der Familie Liebknecht in der Leipziger Südvorstadt"
start: 2022-05-23 16:00
end: 2022-05-23 18:00
locationName: Karl-Liebknecht-Haus
address: "Braustrasse 15, 04109  "
link: https://sachsen.rosalux.de/veranstaltung/es_detail/96WSM/
isCrawled: true
---
<div id="magicdomid159">DIE VERANSTALTUNG MUSSTE LEIDER ABGESAGT WERDEN.</div>

<div></div>

<div></div>

<div>Mit <em>Tim Rood</em> (Stadtgeschichtliches Museum Leipzig)</div>

<div id="magicdomid160">Eine Veranstaltung des Stadtgeschichtlichen Museums und der RLS Sachsen</div>

<div id="magicdomid161"></div>

<div>Wir begeben uns auf die Spuren der Leipziger Arbeiterbewegung und besuchen wichtige Orte auf dem Lebensweg Karl Liebknechts und seiner Familie. Von ihrer Wohnung in der Braustraße, die zum wichtigen Aktionszentrum der deutschen Sozialdemokratie wurde, über das von Arbeitern organisierte Volkshaus bis hin zum Hochverratsprozess im heutigen Bundesverwaltungsgericht beleuchten wir die Bedeutung der Leipziger Südvorstadt für die Arbeiterbewegung.</div>

<div id="magicdomid168"></div>

<div id="magicdomid165">Wir bitten um Anmeldung bis 16. Mai per E-Mail: <a href="mailto:anmeldung@rosalux-sachsen.de">anmeldung@rosalux-sachsen.de</a></div>

<div></div>

<div><div><div id="magicdomid246"><em>Diese Maßnahme wird mitfinanziert durch Steuermittel auf der Grundlage des vom Sächsischen Landtag beschlossenen Haushaltes.</em></div></div></div>

