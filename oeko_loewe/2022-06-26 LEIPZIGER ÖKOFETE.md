---
id: "1188297401574214"
title: LEIPZIGER ÖKOFETE
start: 2022-06-26 12:00
end: 2022-06-26 19:00
locationName: Clara-Zetkin-Park
link: https://www.facebook.com/events/1188297401574214/
image: 275582427_10159951930292855_1008402118750161844_n.jpg
isCrawled: true
---
Am 26. Juni 2022 ist es wieder so weit: Wir feiern die phänomenale Leipziger Ökofete, die größte Umweltmesse Mitteldeutschlands! Der Eintritt ist frei.

🙋 Du bist Aussteller:in und willst Dir für Deinen Verein, Deine Initiative oder Behörde, Dein Unternehmen oder als Einzelpersonen einen Standplatz auf der Ökofete 2022 sichern? 

Dann bewirb Dich jetzt! 

➡️ https://www.oekoloewe.de/leipziger-oekoefete-teilnahme.html

Als Umweltmesse und Fest für die ganze Familie laden Stände und Mitmachangebote jedes Jahr auf der Leipziger Ökofete zum Mitmachen, Zuhören, Schlemmen und Feiern ein. Besucher:innen können nicht nur Bio- und Fairtrade-Produkte erstehen. Hier lernen Groß und Klein auch Wissenswertes über Umwelt, Nachhaltigkeit und Naturschutz, über pfiffige Lösungen und etwas über diejenigen, die sich in Leipzig für die Umwelt stark machen. Das sind Vereine, Initiativen und Ämter, aber auch Einzelpersonen und Unternehmen. Zum Schlemmen gibt es natürlich Speisen und Getränke aus Bio-Produktion. 

Alle Infos
➡️ oekoloewe.de/oekofete.html