---
name: Ökolöwe - Umweltbund Leipzig e.V. 
website: https://www.oekoloewe.de/
email: kontakt@oekoloewe.de
scrape:
  source: facebook
  options:
    page_id: 359153262854
---
Der Ökolöwe - Umweltbund Leipzig e.V. ist ein gemeinnütziger Verein, der im Interesse des Natur- und Umweltschutzes arbeitet. Wir setzen uns für eine umweltgerechte und ökologisch nachhaltige Entwicklung der Region Leipzig ein.