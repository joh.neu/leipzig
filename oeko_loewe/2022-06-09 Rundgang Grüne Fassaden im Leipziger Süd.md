---
id: "5079874175392960"
title: "Rundgang: Grüne Fassaden im Leipziger Süden"
start: 2022-06-09 17:00
end: 2022-06-09 19:00
locationName: Haus der Demokratie
address: Bernhard-Göring-Straße 152
link: https://www.facebook.com/events/5079874175392960/
image: 281359531_10160076383247855_2116053134569136432_n.jpg
isCrawled: true
---
💬 “Kletterpflanzen machen die Fassade kaputt!”
💬 “Fassadengrün ist zu teuer!”
💬 “Dann kommen die ganzen Spinnen ins Haus!”

Diese Vorurteile hört Ökolöwin Christiane beinahe täglich. 🌱 Sie leitet das Projekt “Kletterfix”, ist von Fassadenbegrünung überzeugt und möchte die Leipziger:innen für Kletterpflanzen begeistern.

Deshalb lädt sie am 9. Juni zum Rundgang durch Connewitz ein. Treffpunkt: 17 Uhr vor dem Haus der Demokratie (Bernhard-Göring-Straße 152).

💚 Das Event ist kostenlos und für alle Teilnehmer:innen geeignet, die sich für Grünfassaden interessieren, aber noch viele Fragen offen haben. Eine Anmeldung ist nicht notwendig.

Christiane zeigt nicht nur gelungene Beispiele, sondern auch gängige Planungsfehler. Dabei räumt sie mit Vorurteilen auf und zeigt, warum sich die Investition besonders für Hauseigentümer:innen lohnt. 🌿

Auf dem Spaziergang mit dabei ist Sven Taraba von der Leipziger Firma Fassadengrün e.K. Er wird insbesondere zu technischen Fragen rund um das Thema Ranksysteme für Kletterpflanzen und Fassadenaufbauten (z.B. Wärmeverbundsysteme) fachliche Inputs geben.

Weitere Infos zum Ökolöwen-Projekt: 
👉  oekoloewe.de/kletterfix.html