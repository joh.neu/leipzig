---
name:  Stadtgarten H17
website: https://www.facebook.com/Stadtgarten-H17-1551403855088744/
email: stadtgarten-h17@freiraumsyndikat.de
address: Hähnelstraße 17, Leipzig 
scrape:
  source: facebook
  options:
    page_id: 1551403855088744
---