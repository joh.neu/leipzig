---
name: Energiegenossenschaft Leipzig
website: https://www.energiegenossenschaft-leipzig.de/
email: info@eg-leipzig.de
scrape:
  source: facebook
  options:
    page_id: energiegenossenschaft.leipzig
---
In Zukunft werden immer mehr Menschen ihren Strom aus erneuerbaren Energieträgern beziehen. Nicht nur weil fossile Rohstoffe in absehbarer Zeit zur Neige gehen, sondern auch weil die Nutzung sauberer Energien mit unseren Wertvorstellungen besser zu vereinbaren ist.

Die Energiegenossenschaft Leipzig legt die Energiewende in die Hände der Menschen vor Ort und setzt sich aktiv für den Ausbau der erneuerbaren Energien ein – bürgernah und konsequent.