---
name: Kulturjurte Leipzig
website: https://kulturjurte-leipzig.de
email: kulturjurteleipzig@posteo.de
scrape:
  source: facebook
  options:
    page_id: kulturjurteleipzig
---
In der Kulturjurte Leipzig treffen sich Menschen aller Hintergründe, um gemeinsam an einer schöneren Zukunft zu basteln und das Leben zu feiern!