---
id: "292749736367492"
title: Alles, Alles muss befreit werden - Anarchismus und Tierbefreiung
start: 2022-04-27 19:00
locationName: Plaque
address: Industriestraße 101
link: https://www.facebook.com/events/292749736367492/
image: 278348869_1835885313282419_7278061963664724913_n.jpg
isCrawled: true
---
„Befreiung hört nicht beim Menschen auf“ – Einer der Slogans der Tierbefreiungsbewegung. Befreiung – auf allen Ebenen – das wollen auch Anarchist*innen. Was liegt also näher, als sich die Überschneidungen beider Bewegungen anzusehen?
Der Vortrag widmet sich unterschiedlichen Aspekten dieser Gemeinsamkeiten. Zwei historische, biografische Beiträge werden von neueren Diskussionssträngen gefolgt. Um in einem weiteren Schritt einen anarcha-veganen Organisationsansatz für die bio-vegane Landwirtschaft zu erkunden. Abschließend werden in einem Ausblick (über)regionale Projekt- bzw. Organisationsvorschläge gemacht.