---
id: "757745182055961"
title: Vortragstour – Ihr seid nicht allein! Tear Down Tönnies - Repression in
  der Tierbefreiungsbewegung
start: 2022-04-28 18:30
locationName: Plaque
address: Industriestraße 101
link: https://www.facebook.com/events/757745182055961/
image: 278569996_1835886543282296_6981227024399825406_n.jpg
isCrawled: true
---
Der Verein die tierbefreier*innen e.V. und weitere Gruppen organisieren eine bundesweite Vortragstour 2022. Im Mittelpunkt der Tour stehen die Repressionserfahrungen der Aktionsgruppe Tear Down Tönnies und wie wir uns als Bewegung mit den Aktivist:innen solidarisch zeigen können.

Im Oktober 2019 blockierten Aktivist:innen der Gruppe Tear Down Tönnies (TDT)* die Schlachtfabrik des größten Fleischkonzerns Europas, Tönnies in Kellinghusen (Schleswig Holstein). Knapp elf Stunden lang wurde so der reguläre Schlachtbetrieb verhindert. Ziel der Aktion war es, auf die prekären Arbeitsbedingungen, das endlose Tierleid und die starke Klima- und Umweltbelastung durch die Tierindustrie aufmerksam zu machen.

Seit August 2020 sehen sich die Aktivist:innen nun mit zivilrechtlichen Klagen, Schadensersatzforderungen in 5stelliger Höhe, einer Unterlassungserklärung und einer Vielzahl von Gerichtsverhandlungen konfrontiert. Damit will der Konzern Tönnies erreichen, die Proteste von Aktivist:innen der Tierbefreiungs- und Klimagerechtigkeitsbewegung langfristig zu unterbinden und im besten Fall einen Präzedenzfall für zukünftige Anklagen zu schaffen.

In dem Vortrag werden Aktivistis unter anderem darlegen, wie die Besetzung ablief, wie die Prozesse bisher verliefen, welchen support sie erfahren haben und was die Repression für die Tierbefreiungs-& Klimagerechtigkeitsbewegung bedeutet. Außerdem soll es Raum für Fragen, Austausch und Diskussion geben. Im Anschluss an den Vortrag öffnet die Bar.

* Tear Down Tönnies ist ein Zusammenschluss von Einzelpersonen und Aktivist:innen, die sich gegen die Tierindustrie einsetzen. Konkret will das Bündnis ein Ende der Ausbeutung und des Tötens von Tieren, der Umweltzerstörung sowie der Unterdrückung von Menschen erreichen und für eine befreite Gesellschaft jenseits des kapitalistischen Ausbeutungssystems eintreten.