---
name: die tierbefreier_innen Leipzig
website: http://tbleipzig.blogsport.eu/

scrape:
    source: facebook
    options:
        page_id: 102946016576366
---
Die Tierbefreier\*innen Leipzig sind eine Ortsgruppe des tierbefreier e.V., der seit über 30 Jahren für die Überwindung jeglicher Tierausbeutung kämpft.