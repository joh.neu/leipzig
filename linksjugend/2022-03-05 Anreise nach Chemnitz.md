---
id: "639825360455032"
title: Anreise nach Chemnitz
start: 2022-03-05 12:00
link: https://www.facebook.com/events/639825360455032/
image: 274922331_7077165122354976_8962087905257366285_n.jpg
isCrawled: true
---
+++ Naziaufmarsch in Chemnitz stoppen +++

Am 05.03. jährt sich auch in Chemnitz der Jahrestag der Bombardierung. Zu diesem Anlass rufen Faschos aus ganz Sachsen und den umliegenden Bundesländern dazu auf nach Chemnitz zu fahren.

Ihr Ziel ist es, die wöchentlichen Schwurbel- und Faschoaufmärsche als Sprungbrett zu nutzen und in Form eines Sternmarsches aus drei Richtungen in die Innenstadt zu marschieren.
Wir werden die Genoss*innen in Chemnitz nicht damit alleine lassen!
Wir rufen alle Antifaschist*innen aus Sachsen und Umgebung auf, in Chemnitz am 05.03. auf die Straße zu gehen und zu zeigen, dass wir Geschichtsrevisionismus und Rechtsextreme Ideologien niemals dulden werden!

Anreise:
Leipzig Hbf | 12:00 Uhr | Gleis 23
Rückfahrt wird dann besprochen.

Derzeit werden mehrere Solitickets organisiert, für Menschen die sich kein Ticket leisten können.

Fahrt mit uns nach Chemnitz und unterstützt die Antifaschist*innen vor Ort!

Nazis blockieren wo immer sie appearen - keinen Zentimeter dem Faschismus!