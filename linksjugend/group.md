---
name: Linksjugend
website: https://www.linksjugend-leipzig.de
email: kontakt@linksjugend-leipzig.de
scrape:
  source: facebook
  options:
    page_id: linksjugendleipzig
---
Wir haben uns als linker Jugendverband zusammengeschlossen, weil wir diese Gesellschaft grundlegend verändern wollen. Mit euch wollen wir den Weg zu einer anderen, einer freien, gerechten und solidarischen Gesellschaft beschreiten.