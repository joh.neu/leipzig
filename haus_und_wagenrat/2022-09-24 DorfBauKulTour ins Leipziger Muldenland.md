---
id: 78553-1664010900-1664031600
title: DorfBauKulTour ins Leipziger Muldenland
start: 2022-09-24 09:15
end: 2022-09-24 15:00
link: https://www.hwr-leipzig.org/event/dorfbaukultour-ins-leipziger-muldenland/
teaser: Gemeinschaftlich Wohnen und Arbeiten auf dem Land.Eine Tour mit Fahrrad
  oder Auto zuleerstehenden G
isCrawled: true
---
Gemeinschaftlich Wohnen und Arbeiten auf dem Land.

Eine Tour mit Fahrrad oder Auto zuleerstehenden Gebäuden und Umnutzungsprojekten im LEADER-Gebiet Leipziger Muldenland. 

Die Tour will Lust aufs gemeinschaftliche Wohnen und Arbeiten im Leipziger Muldenland machen und informiert zu Fördermitteln und Beratungsangeboten für Initiativen. Sie macht Station an vier leerstehenden kommunalen Gebäuden, die sich für gemeinschaftliche Wohn- und Arbeitsprojekte eignen würden. 

Wenn ihr mitmachen wollt, meldet euch unbedingt bis 20.9. an. Die Teilnahme ist kostenlos. 

email hidden; JavaScript is requiredbzw. Tel: 0341 58143369 [AB nutzen] 

Veranstaltung als pdf > Veranstaltungsseite 

Ablauf

9:15 UhrTreffpunkt für die Radfahrer: Bhf Wurzen

 

09:45–10:30 UhrAlte Schule LüptitzGoethestraße 5, 04808 Lüptitz 

11:00 –12:45 UhrEhemalige Kita in FalkenhainKarl-Marx-Straße 3704808 Falkenhain 







Vorstellung von Fördermöglichkeiten und Sanierungsbeispielen durch das LEADER-Regionalmanagement Leipziger Muldenland

Vorstellung des Beratungsangebots der »Dezentrale – Netz für gemeinschaftliches Wohnen in Sachsen«

Imbiss im Kräuterhof Falkenhain







13:30 UhrCoworking RöcknitzAn der Wasserburg 3,04808 Thallwitz OT Röcknitz 

14:30 UhrKornhaus ThallwitzSiedewitzstr. 25, 04808 Thallwitz 

Fahrradfahrt Richtung Eilenburg, ca. 4 km

Abfahrt S4 Richtung Leipzig um 15:06; 15:36 oder 16:06. 

Fahrradroute – Veranstaltung als pdf 

Die Tour kann mit Fahrrad und Auto gefahren werden. Es ist möglich, nur bei einem Teil der Stationen dabei zu sein.  

Fahrrad: Treffpunkt für die gemeinsame Radtour ist Sa, 24.9. 9:15 Uhr am Bahnhof Wurzen. Bitte beachten, dass Züge voll sein könnten. Anreise von Leipzig Hbf mit S-Bahn (8:40-9:10 Uhr) oder mit RE (9:00-9:16). Die Gesamtfahrradstrecke istdann ca. 35 km. Die Tour endet am Bahnhof Eilenburg. 

Pkw: Fahrt mit eigenem Pkw ist möglich, ggf. gibt es dann kleine Pausenzeiten, um auf die Radelnden zu warten. Wir bemühen uns, Mitfahrplätze zu vermitteln, keine Garantie. 

Bitte bei der Anmeldung angeben, ob noch Pkw-Plätze frei sind und der Kontakt an Mitfahrgelegenheit Suchende weitergegeben werden kann. 

email hidden; JavaScript is requiredbzw. Tel: 0341 58143369 [AB nutzen](bitte angeben, wieviele Leute dabei sind und mitwelchem Verkehrsmittel, Teilnahme am Imbiss). 

Während der Veranstaltung werden Bild- und Tonaufnahmen gefertigt.¹ 

Jedwede Haftung seitens der Veranstalter ist ausgeschlossen. 

 

Die Radtour ist eine Gemeinschaftsveranstaltung von:Dezentrale – Netz für gemeinschaftliches Wohnen in Sachsen,LAG Leipziger Muldenland e.V.,Sächsisches Landesamt für Umwelt, Landwirtschaft und Geologieund wird finanziert aus Mitteln der Europäischen Unionsowie des Sächsischen Ministeriums für Regionalentwicklung 

  



¹ Während der Veranstaltung werden Bild- und Tonaufnahmen gefertigt. Die Bildaufnahmen werden einzelne oder Gruppen von Teilnehmern zeigen, die nicht im Mittelpunkt des Bildes stehen. Medienvertreter, das LfULG sowie deren Kooperationspartner der Veranstaltung können die Aufnahmen zur Information der Öffentlichkeit publizieren. Dies betrifft insbesondere die Veröffentlichung auf www.sachsen.de, auf Social-Media-Kanälen der Sächsischen Staatsregierung (Facebook, Twitter, Instagram) und in Printmedien (Informationsbroschüren, Pressemitteilungen, Präsentationen). Jede teilnehmende Person hat das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die Anfertigung von Bild- und Tonaufnahmen, die ihre Person betreffen, Widerspruch einzulegen. Der Widerspruch ist der vor Ort Bild- oder Tonauf-nahmen fertigenden Person mitzuteilen.



 