---
id: 78502-1650907800-1650913200
title: Neustart! Mit Nachbarschaften der Klimakrise begegnen
start: 2022-04-25 17:30
end: 2022-04-25 19:00
address: Galerie KUB, Kantstraße 18, Leipzig, 04275
link: https://www.hwr-leipzig.org/event/neustart-mit-nachbarschaften-der-klimakrise-begegnen/
teaser: Vortrag im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen
  mit regenerativen Baustoffen“.
isCrawled: true
---
Vortrag im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen“. 

Anmeldung erforderlich 

Illustration: Forum Neustart Schweiz

Der Verein „Neustart Schweiz“ bemüht sich darum, in Nachbarschaften von etwa 500 Menschen, Formen anderen Wirtschaftens zu entwickeln – mit gemeinsam organisierter Versorgung und geteilten Ressourcen. Neue Genossenschaften in Zürich, Basel, Bern setzen diese Ideen  um. 

Dabei sind die neuen Ansätze nicht allein: Bereits 2007 haben sich mehr als 47 junge und alte Wohnbaugenossenschaften zusammengetan, um gemeinsam die Baugenossenschaft „mehr als wohnen“ zu gründen und ein neues Viertel in Zürich für mehr als 1.200 Menschen zu bauen: mit Wohn- und Arbeitsräumen – Genossenschaftlich, bezahlbar und ökologisch. 

Die Referentin Stefania Koller ist Architektin und Vorstandsmitglied des Vereins Neustart Schweiz. 

  

Die Ausstellung „Faktor Wohnen“ ist vom 24. März – 1.Mai 2022 mittwochs bis sonntags von 14 bis 18 Uhr geöffnet.