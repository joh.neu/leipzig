---
id: 78506-1651325400-1651338000
title: Pödelwitz hat Zukunft! Modelldorf der ökologischen und sozialen Nachhaltigkeit
start: 2022-04-30 13:30
end: 2022-04-30 17:00
address: Pödelwitz, Bushaltestelle, Pödelwitz, Groitzsch, 04539
link: https://www.hwr-leipzig.org/event/poedelwitz-hat-zukunft-modelldorf-der-oekologischen-und-sozialen-nachhaltigkeit/
teaser: Exkursion im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen
  mit regenerativen Baustoffen
isCrawled: true
---
Exkursion im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen“. 

Illustration: Pödelwitz hat Zukunft, Nadine Kradorf

Treffpunkt Pödelwitz Bushaltestelle (gemeinsame Anreise mit Bahn und Rad von Leipzig möglich Abfahrt 11:10 Uhr ab Leipzig Hbf nach Neukieritzsch, von da 6,5 km mit Rad) 

Das Bündnis „Pödelwitz hat Zukunft e.V.“, setzt sich sich für eine selbstbestimmte, sozial- & und klimagerechte Revitalisierung des Dorfes nach dem Ende der Braunkohle ein. Ein wichtiges Ziel dabei ist die 100% ige Versorgung des Dorfes aus erneuerbaren Energien. Die Exkursion bietet die Möglichkeit, Pödelwitz kennenzulernen und zum „Tag der erneuerbaren Energien“ mit der AG Energie des Bündnisses ins Gespräch kommen. 

AG Energie Bündnis „Pödelwitz hat Zukunft“ , Haus- und WagenRat e. V.