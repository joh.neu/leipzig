---
id: 78489-1648060200-1651428000
title: Ausstellung "Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen"
start: 2022-03-23 18:30
end: 2022-05-01 18:00
address: Galerie KUB, Kantstraße 18, Leipzig, 04275
link: https://www.hwr-leipzig.org/event/ausstellung-faktor-wohnen-oekologisch-umbauen-mit-regenerativen-baustoffen/
teaser: Geöffnet mittwochs bis sonntags von 14 bis 18 Uhr (23.3. – 1.5.2022)
  Einblick AusstellungEröffnu
isCrawled: true
---
Geöffnet mittwochs bis sonntags von 14 bis 18 Uhr (23.3. – 1.5.2022)

 



Einblick Ausstellung

Eröffnung mit Podiumsdiskussion am 23. März, 18:30 Uhr.

Weitere Veranstaltungen im Begleitprogramm. 



Die Art, wie wir aktuell bauen, ist klimaschädlich. Baustoffe tragen einen erheblichen Teil dazu bei. Der Verbrauch endlicher Rohstoffe und die oft energieaufwändige Herstellung schlagen sich dabei ebenso nieder wie der Rückbau und die Entsorgung. Hinzu kommt, dass besonders konventionelle Dämmstoffe unter realen Bedingungen häufig nicht an die in den Normen abgebildete Effizienz heranreichen. Das bedeutet Energieverluste während der Nutzungsdauer des Gebäudes. 













Die Wanderausstellung FAKTOR WOHNEN – Ökologisch um:bauen mit regenerativen Baustoffen zeigt bauphysikalische Vorteile von regenerativen Baustoffen und ordnet das Thema in den ökologischen und gesellschaftlichen Gesamtzusammenhang des Bauens und Wohnens ein. Sie präsentiert regenerative Baustoffe zum Anfassen.

 







Neben der Ausstellung gibt es ein umfangreiches Veranstaltungsprogramm. 

Diskutiert wird neben praktischen Baufragen das Bauen im Klimawandel, die ressourceneffiziente Stadt, die Rolle von Nachbarschaften. 

Veranstalter sind: 

Stiftung Trias, Stadt Leipzig, Bauraum MV, Netzwerk Leipziger Freiheit und Haus- und WagenRat e.V. 

 