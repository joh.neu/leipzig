---
id: 78495-1650562200-1650567600
title: Bauen mit Strohballen
start: 2022-04-21 17:30
end: 2022-04-21 19:00
address: Galerie KUB, Kantstraße 18, Leipzig, 04275
link: https://www.hwr-leipzig.org/event/bauen-mit-strohballen/
teaser: "Foto: Bauzirkel LeipzigVortrag im Rahmen der Ausstellung „Faktor
  Wohnen. Ökologisch um:bauen mit re"
isCrawled: true
---
Foto: Bauzirkel Leipzig

Vortrag im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen“. Anmeldung hier. 

Werner Ehrich ist Architekt und Zimmermann, sein Architekturbüro gründete er in den 90er Jahren mit Spezialrichtung „Gesunde Altbausanierung“. Er beschäfftigt sich seit über 15 Jahren mit Lehmbau, gibt Seminare und hatte mehrere Jahre eine Zimmerei, die sich auf Fachwerksanierung spezialisiert hatte. Der Anspruch des gesunden, umweltorientierten und nachhaltigen Bauens konnte er unteranderem in Leipzig Lindenau bei einem Einfamilienhaus umsetzen. Mit Strohballenbauweise und dem Verzicht auf Dampfbremsen und Folien fast im gesamten Gebäude. Die diffuionsoffene Außenhaut musste gesund und luftdicht sein.

Zum Planen und Bauen gehören seiner Ansicht nach alle Ebenen. Angefangen von der Philosophie bis zur Handsäge. 

Referent: Werner Ehrich, Perspektive – Büro für gesunde Architektur 

www.gesunde-architektur.de 

Die Ausstellung „Faktor Wohnen“ ist vom 24. März – 1.Mai 2022 mittwochs bis sonntags von 14 bis 18 Uhr geöffnet.