---
id: 78476-1644249600-1644255000
title: Offene digitale Projektberatung
start: 2022-02-07 16:00
end: 2022-02-07 17:30
address: Videokonferenz
link: https://www.hwr-leipzig.org/event/offene-digitale-projektberatung/
teaser: Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein
  Hausprojekt zu gründen, wen
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 

Die Veranstaltung bietet in einer kostenfreien Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die offene Beratung beginnt am Montag, den 7. Februar um 16 Uhr und findet digital per Videokonferenz statt.  

Um den Link zu erfahren, per E-Mail unter Kontakt anmelden.

 

Du kannst an diesem Termin nicht? 

Die offene Beratung findet in der Regel jeden ersten Montag im Monat statt. Da es in Ausnahmefällen zu terminlichen Verschiebungen kommen kann, informiere dich bitte immer vorher noch mal über unsere Website. Wenn du den Termin dort findest, dann findet er statt.