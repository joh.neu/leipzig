---
id: 78519-1649088000-1649095200
title: Offene Wohnprojektberatung
start: 2022-04-04 16:00
end: 2022-04-04 18:00
address: Galerie KUB, Kantstraße 18, Leipzig, 04275
link: https://www.hwr-leipzig.org/event/offene-wohnprojektberatung-2/
teaser: Aktuelle Konzeptverfahren in Leipzig für kooperatives und bezahlbares
  Bauen und Wohnen Anmeldung er
isCrawled: true
---
Aktuelle Konzeptverfahren in Leipzig für kooperatives und bezahlbares Bauen und Wohnen 

Anmeldung erforderlich. 

Die offene Wohnprojektberatung richtet sich an kooperative Baugemeinschaften und Bauwillige, die Beratungsbedarf zu eigenen kooperativen Bau- und Wohnprojekten und/oder zu den aktuellen Konzeptverfahren in Leipzig haben.

Die allgemeine Beratung wird durch einen Fachberater Recht (in Bezug auf mögliche Organisations- und Rechtsformen) und einen Fachberater für Energieeffizientes Bauen unterstützt. 

Fachberater*innen: 

Florian Schartel, Haus- und WagenRat e. V.

Dr. Tanja Korzer, Netzwerk Leipziger Freiheit

Fachberatung Recht

Fachberatung für Energieeffizientes Bauen 

Die Beratung findet im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen“ statt.

Die Ausstellung „Faktor Wohnen“ ist vom 24. März – 1.Mai 2022 mittwochs bis sonntags von 14 bis 18 Uhr geöffnet.