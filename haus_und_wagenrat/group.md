---
name:  Haus- und WagenRat e.V.
website: http://hwr-leipzig.org/
email: post@hwr-leipzig.org
address: Georg-Schwarz-Str. 19, 04177 Leipzig
scrape:
  source: ical
  options:
    url: http://hwr-leipzig.org/veranstaltungen/?ical=1
---
Der Haus- und WagenRat e.V. ist ein Verein für selbstorganisierte Räume in und um Leipzig. Er ist aus der Zusammenarbeit selbstorganisierter Hausprojekte und Wagenplätze hervorgegangen, die sich gemeinsam für eine solidarische Stadt einsetzen.