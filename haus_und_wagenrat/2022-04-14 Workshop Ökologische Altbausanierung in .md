---
id: 78498-1649952000-1649962800
title: "Workshop: Ökologische Altbausanierung in der Praxis. Was klappt und
  woran scheitert's?"
start: 2022-04-14 16:00
end: 2022-04-14 19:00
address: Galerie KUB, Kantstraße 18, Leipzig, 04275
link: https://www.hwr-leipzig.org/event/workshop-oekologische-altbausanierung-in-der-praxis-was-klappt-und-woran-scheiterts/
teaser: Ökologisch und trotzdem bezahlbar bauen – was klappt und woran
  scheitert’s im Hausprojekt-Baualltag
isCrawled: true
---
Ökologisch und trotzdem bezahlbar bauen

 – was klappt und woran scheitert’s im Hausprojekt-Baualltag? 

Workshop im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen“. Anmeldung hier. 

Foto: Bauzirkel Leipzig

Der Workshop bringt Hausprojekte und Baufachleute (aus Architektur, Planung, Handwerk, Baustoffhandel) zusammen. Im ersten Teil werden konkrete Bauvorhaben „aus dem Alltag“ vorgestellt, die zwar ökologische Ziele haben, aber auch günstige Mieten erreichen müssen, Eigenleistungen einbringen und anderes. Auf dieser Basis wird im zweiten Teil in Kleingruppen untersucht: „Was klappt und woran scheitert’s im Baualltag? Was muss sich ändern, damit ökologischeres Bauen Standard wird?“ Diese Erfahrungen werden am Schluss zusammengetragen und in großer Runde diskutiert: wie ändern wir die Bedingungen? 

Die Ergebnisse werden am Schluss konkretisiert, zusammengetragen und so aufbereitet werden, dass die Erkenntnisse mit in das nächste Vorhaben genommen werden können. 

Anmeldung erforderlich! Anmeldung ab 23. März hier.