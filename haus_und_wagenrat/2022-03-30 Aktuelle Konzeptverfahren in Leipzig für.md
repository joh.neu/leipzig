---
id: 78516-1648663200-1648668600
title: Aktuelle Konzeptverfahren in Leipzig für kooperatives und bezahlbares
  Bauen und Wohnen
start: 2022-03-30 18:00
end: 2022-03-30 19:30
address: Galerie KUB, Kantstraße 18, Leipzig, 04275
link: https://www.hwr-leipzig.org/event/aktuelle-konzeptverfahren-in-leipzig-fuer-kooperatives-und-bezahlbares-bauen-und-wohnen/
teaser: Anmeldung erforderlich  Aktuelle Konzeptverfahren und Beispiele für
  BaugemeinschaftenAktuelle K
isCrawled: true
---
Anmeldung erforderlich 



 



Aktuelle Konzeptverfahren und Beispiele für Baugemeinschaften

Aktuelle Konzeptverfahren für kooperatives und bezahlbares Bauen und Wohnen bieten zudem die Möglichkeit zur Verwirklichung der eigenen Wohnvorstellungen. In einer Informationsveranstaltung und einer Wohnprojektberatung können sich Baugemeinschaften und Bauwillige über die bereitgestellten Grundstücke und die Rahmenbedingungen der Leipziger Konzeptverfahren erkundigen. Leipziger Architekt:innen zeigen an unterschiedlichen Projekten die Vielfalt und Möglichkeiten des Bauens und Wohnens in Gemeinschaft. 

Referent*innen: 

Dr. Oliver Koczy, Stadt Leipzig 

Dr. Tanja Korzer, NLF Leipzig 

Gordon Tannhäuser, Dix Tannhäuser GbR 

Dirk Stenzel, ASUNA 

Tim Augustin, Augustin + Imkamp freie Architekten 

Anne Femmer, SUMMACUMFEMMER  

Philip Stapel, octagon Architekturkollektiv (angefragt) 

  



Infoveranstaltung im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen“. 

Die Ausstellung „Faktor Wohnen“ ist vom 24. März – 1.Mai 2022 mittwochs bis sonntags von 14 bis 18 Uhr geöffnet.