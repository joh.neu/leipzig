---
id: 78568-1664013600-1664031600
title: Exkursionen zum Leipziger Wohnprojektetag
start: 2022-09-24 10:00
end: 2022-09-24 15:00
link: https://www.hwr-leipzig.org/event/exkursionen-zum-leipziger-wohnprojektetag/
teaser: Besichtigungen und Rundgänge:– Besichtigung OurHaus– Baugemeinschaft W
  132– Wohnprojekt Sellerhau
isCrawled: true
---
Besichtigungen und Rundgänge:

– Besichtigung OurHaus

– Baugemeinschaft W 132

– Wohnprojekt Sellerhaus

– DorfBauKulTour Leipziger Muldenland 

Mehr: 

10. Leipziger Wohnprojekttage