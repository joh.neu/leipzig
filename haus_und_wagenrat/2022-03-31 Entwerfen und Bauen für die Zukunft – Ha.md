---
id: 78511-1648747800-1648758600
title: Entwerfen und Bauen für die Zukunft – Haus der Statistik und Leipziger
  Wohnprojekte
start: 2022-03-31 17:30
end: 2022-03-31 20:30
address: Galerie KUB, Kantstraße 18, Leipzig, 04275
link: https://www.hwr-leipzig.org/event/entwerfen-und-bauen-fuer-die-zukunft-haus-der-statistik-und-leipziger-wohnprojekte/
teaser: "Ausstellungsführung: 17:30 – 18:30 UhrStudierende der HTWK Leipzig und
  Leipziger Baugemeinschaftspr"
isCrawled: true
---
Ausstellungsführung: 17:30 – 18:30 Uhr

Studierende der HTWK Leipzig und Leipziger Baugemeinschaftsprojekte im Gespräch: 19:00 – 20:30 Uhr

im Rahmen der Ausstellung „Faktor Wohnen. Ökologisch um:bauen mit regenerativen Baustoffen“ 

Anmeldung erforderlich. 

 

Entwerfen und Bauen für die Zukunft – Haus der Statistik und Leipziger Wohnprojekte

Wie wollen wir wohnen? Welche Wohnmodelle werden sich in Zukunft durchsetzen? Schon seit geraumer Zeit vervielfältigen sich die Lebensmodelle und damit Wohnpraktiken und Raumnutzungen. Auch der Arbeits- und Bildungsalltag befindet sich im Wandel.  Am Beispiel des „Haus der Statistik“, Berlin entwickeln Architekturstudierende der HTWK Leipzig zukunftsfähige Wohnformen die zeigen, wie Nachverdichtung erfolgen und in den Bestand eingebettet werden kann, damit auch das bestehende Umfeld aufgewertet wird. Vertreter der Genossenschaft ZUsammenKUNFT eG geben Auskunft zum aktuellen Entwicklungsstand beim Modellprojekt „Haus der Statistik“.

Quasi als Spiegel zu den Entwurfsansätzen der Studierenden zeigen konkrete Projekte von Leipziger Baugemeinschaften, wie sie zukunftsfähige, auf die eigenen Bedürfnisse abgestimmte, Gebäude- und Nutzungskonzepte entwickelt haben. 

Referent*innen

Prof. Dorothea Becker/Prof. Frank Schüler und Studierende, HTWK Leipzig

Vertreter:innen Leipziger Bau- und Wohnprojekte

Konrad Braun, ZUsammenKUNFT eG, Berlin

Dr. Tanja Korzer, Netzwerk Leipziger Freiheit 

Die Studienarbeiten zum „Haus der Statistik“ sind im Rahmen der Ausstellung „Faktor Wohnen“ bis zum 07.04.2022 zu besichtigen.

Die Ausstellung „Faktor Wohnen“ ist vom 24. März – 1.Mai 2022 mittwochs bis sonntags von 14 bis 18 Uhr geöffnet.