---
id: "1371597090002868"
title: Küche für Alle
start: 2022-02-26 18:00
end: 2022-02-26 23:45
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/1371597090002868/
image: 274251083_4967055300016693_5805575343658877207_n.jpg
isCrawled: true
---
Jeder in Leipzig ist willkommen in unserem Haus! Genießt mal bitte unseren Platz und Speisen.
Ab 18:30 steht das Essen. Komm einfach mal vorbei.

– – –

Everyone in Leipzig is welcome to our place! Please enjoy our space and foods(Vegan, With donation).  Food serving from 6:30 p.m. If you are interested in, just come and join us. 

– – –

どなたでも参加できる「ごはんの会」です。18時30分から食事が始まります。投げ銭制です。気軽に遊びに来てください！

– – –

부담없이 놀러 오세요!
– – –

 请随时访问！

– – –

أهلاً بكم في البيت الياباني نرحب بالجميع هنا في مكانناو نرجو أن تستمتعو بالمكان والطعام , ويبدأ تقديم الطعام الساعة 7 مساءً. نرجو أن تشاركونا!

– – –

– – –

შაბათს გეპატიჟებათ ყველას Das Japanische Haus ვინც ლაიფციგში ცხოვრობს. დატკბით ჩვენი ადგილით და კერძებით. 19:00 საათიდან ერთად დავაგემოვნებთ მზა კერძს. 