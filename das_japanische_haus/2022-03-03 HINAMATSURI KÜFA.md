---
id: "730309271287683"
title: HINAMATSURI KÜFA
start: 2022-03-03 18:30
locationName: Das Japanische Haus e. V.
address: Eisenbahnstr.113b, 04315 Leipzig
link: https://www.facebook.com/events/730309271287683/
image: 275187962_5007593769296179_3415104601774302810_n.jpg
isCrawled: true
---
Hallo hallo!

Am 3. März das japanische „Hinamatsuri“-Fest feiern.

Am „Hinamatsuri“-Fest, auch Puppenfest oder Mädchenfest genannt, betet man für die gesunde Entwicklung und für das Glück der Kinder.

Zur Feier des Tages möchten wir Sushi zubereiten! 

Wir freuen uns auf euch zu sehen!


明日はひな祭りですー！日本の家ではひな祭りの巻き寿司をします！
みんな来てね〜
