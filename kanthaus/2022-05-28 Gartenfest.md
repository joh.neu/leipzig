---
id: "680981566300429"
title: Gartenfest
start: 2022-05-28 12:00
locationName: Kanthaus
address: Kantstrasse 20
link: https://www.facebook.com/events/680981566300429/
image: 281550145_7593452254058772_5661346022699022641_n.jpg
isCrawled: true
---
Kanthaus Gartenfest! Essen, Trinken und Musik. Jeder ist willkommen.