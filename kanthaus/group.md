---
name: Kanthaus
website: https://kanthaus.online/
email: hello@kanthaus.online
address: Kantstraße 20, 04808 Wurzen
scrape:
  source: facebook
  options:
    page_id: 1879293212141400
  filter:
    title: "^((?!Acroyoga).)*$"
--- 
Haus für nachhaltige Projekte